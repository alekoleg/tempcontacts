//
//  FileAdapterTests.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import XCTest

@testable import TemporaryContacts

class FileAdapterTests: XCTestCase {
    
    let adapter = FileAdapter()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCache() {
        let originalText = "Test"
        let data = originalText.data(using: .utf8)!
        let directory = FileAdapter.Directory.caches
        let pathComponents = ["files", "strings"]
        let fileName = "test.string"
        
        do {
            try adapter.setData(
                data,
                directory: directory,
                pathComponents: pathComponents,
                fileName: fileName
            )
        } catch {
            XCTAssert(false, "Ошибка сохранения данных: \(error)")
        }
        
        let cachedData = adapter.data(at: directory, pathComponents: pathComponents, fileName: fileName)
        XCTAssertNotNil(cachedData, "Должны существовать данные")
        
        let cachedText = String(data: cachedData!, encoding: .utf8)
        XCTAssert(originalText == cachedText, "Данные должны быть эквивалентны")
        
        let onlyOneFileNames = adapter.content(of: directory, pathComponents: pathComponents)
        XCTAssert(onlyOneFileNames.contains(obj: fileName), "Должен отдавать созданный файл")
        
        let otherData = "Other".data(using: .utf8)!
        let childFolderName = "child"
        let childPathComponents = pathComponents + [childFolderName]
        let otherFileName = "other.string"
        try! adapter.setData(
            otherData,
            directory: directory,
            pathComponents: childPathComponents,
            fileName: otherFileName
        )
        
        let onlyFiles = adapter.content(
            of: directory,
            pathComponents: pathComponents,
            includeDirectories: false
        )
        XCTAssert(onlyFiles == [fileName], "Должен отдавать только дочерние файлы")
        
        let contentWithDirectories = adapter.content(
            of: directory,
            pathComponents: pathComponents,
            includeDirectories: true
        ).sorted()
        let expectedResult = [fileName, childFolderName].sorted()
        XCTAssert(contentWithDirectories == expectedResult, "Должен отдать название файла и название дочерней папки")
        
        try! adapter.deleteContent(
            of: directory,
            pathComponents: pathComponents
        )
        let contentAfterDeleting = adapter.content(
            of: directory,
            pathComponents: pathComponents,
            includeDirectories: true
        )
        XCTAssert(contentAfterDeleting.isEmpty, "После удаления ничего не должно остаться")
    }
    
}
