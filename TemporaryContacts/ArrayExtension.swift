//
//  ArrayExtension.swift
//  TemporaryContacts
//
//  Created by User on 13/01/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
extension Array {
    
    func contains<T>(obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
    
    func sortContacts(displayOrder: SettingsDisplayOrder) -> [Any]{
        var sortDescription: [NSSortDescriptor]
        if displayOrder == .NameFirst{
            sortDescription = [
                NSSortDescriptor(key: "firstName", ascending: true),
                NSSortDescriptor(key: "lastName", ascending: true),
                NSSortDescriptor(key: "specialization", ascending: true)
            ]
        }
        else{
            sortDescription = [
                NSSortDescriptor(key: "lastName", ascending: true),
                NSSortDescriptor(key: "firstName", ascending: true),
                NSSortDescriptor(key: "specialization", ascending: true)
            ]
        }
        
        return (self as NSArray).sortedArray(using: sortDescription)
    }
    
}
