//
//  WelcomeTourViewController.swift
//  URemont
//
//  Created by User on 18/11/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit
import BWWalkthrough

/// Экран отображения туториала
final class WelcomeTourViewController: BWWalkthroughViewController {
    
    /// Действие по окончанию
    typealias DoneAction = (WelcomeTourViewController) -> Void
    
    @IBOutlet fileprivate var pageController: FXPageControl!
    
    /// Действие по окончанию
    var doneAction: DoneAction?
    
    /// Кнопка прпоуска
    @IBOutlet fileprivate var skipButton: UIButton!

    /// Страницы туториала
    fileprivate let pages: [WelcomeTourPageData] = [
        WelcomeTourPageData(image: "WT1", caption: "WTpage1".localized),
        WelcomeTourPageData(image: "WT2", caption: "WTpage2".localized),
        WelcomeTourPageData(image: "WT3", caption: "WTpage3".localized),
        WelcomeTourPageData(image: "WT4", caption: "WTpage4".localized),
        WelcomeTourPageData(image: "WT5", caption: "WTpage5".localized),
        WelcomeTourPageData(image: "WT6", caption: "WTpage5".localized),
        WelcomeTourPageData(image: "WT7", caption: "WTpage5".localized),
        WelcomeTourPageData(image: "WT8", caption: "WTpage5".localized, startButtonVisible: true),
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        automaticallyAdjustsScrollViewInsets = false
        self.delegate = self
        
        skipButton.layer.cornerRadius = skipButton.bounds.height / 2
        
        pageController.numberOfPages = pages.count
        pageController.isUserInteractionEnabled = false
        
        pages.forEach { page in
            let vc = WelcomeTourPage(pageData: page)
            vc.delegate = self
            add(viewController: vc)
        }
    }

    @IBAction func skip() {
        self.close(skipButton)
    }
}

extension WelcomeTourViewController: BWWalkthroughViewControllerDelegate {

    func walkthroughCloseButtonPressed() {
        if let action = doneAction {
            action(self)
        } else {
            self.dismiss(animated: false, completion: nil)
        }
    }

    func walkthroughPageDidChange(_ pageNumber: Int) {
        pageController.currentPage = pageNumber
        closeButton?.isHidden = pageNumber != pages.count - 1
        skipButton.isHidden = pageNumber == pages.count - 1
    }

}

extension WelcomeTourViewController: WelcomeTourPageDelegate {
    func start() {
        self.close(UIButton())
    }
}
