//
//  Notifications.swift
//  TemporaryContacts
//
//  Created by Konshin on 30.03.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    /// Синхроназация контактов с книгой вот-вот начнется
    static let contantsWillUpdateFromPhoneBook = NSNotification.Name(rawValue: "contantsWillUpdateFromPhoneBook")
    /// Синхроназация контактов с книгой закончена
    static let contantsDidUpdateFromPhoneBook = NSNotification.Name(rawValue: "contantsDidUpdateFromPhoneBook")
    /// Способ сортировки изменился
    static let contactsSortingTypeDidUpdate = NSNotification.Name(rawValue: "contactsSortingTypeDidUpdate")
    /// Системная книга обновилась в бекграунде
    static let systemContactsDidUpdateInAppBackground = NSNotification.Name(rawValue: "systemContactsDidUpdateInAppBackground")
    /// Тип отображения групп изменен
    static let groupDisplayModeDidUpdate = NSNotification.Name(rawValue: "groupDisplayModeDidUpdate")
    /// Закончили выбор группы для временного контакта
    static let didSelectTempGroup = NSNotification.Name(rawValue: "didSelectTempGroup")
    /// Создали визитку
    static let didLoadBusinessCard = NSNotification.Name(rawValue: "didLoadBusinessCard")
    /// Создали группу
    static let didFinishCreateGroup = NSNotification.Name(rawValue: "didFinishCreateGroup")
}
