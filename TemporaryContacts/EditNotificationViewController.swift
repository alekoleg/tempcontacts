//
//  EditNotificationViewController.swift
//  TemporaryContacts
//
//  Created by User on 11/01/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import UIKit
//import M13Checkbox
import CoreData

protocol EditNotificationViewControllerDelegate: class {
    func editNotificationControllerDidComplete(
        _ controller: EditNotificationViewController,
        with notificaion: AppNotification?
    )
}

class EditNotificationViewController: UITableViewController{
    
    enum Owner {
        case contact(Contacts)
        case group(Groups)
    }

    /// Редактируемое уведомление
    var notification: AppNotification?
    /// Оригинальная модель уведомления
    var originalNotification: AppNotification?
    var selectedDate: Date?
    var selectedText: String?
    var isContactNotification: Bool = true
    var fromContactBook = false
    var fromContactInfo = false
    var context: NSManagedObjectContext = CoreDataManager.instance.managedObjectContext
    
    /// Для кого создается уведомление
    var owner: Owner?
    
    weak var delegate: EditNotificationViewControllerDelegate?
    
    //Секция "Текст напоминания", индикаторы выделения
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var bodyTextView: UITextView!

    //Секция "Текст напоминания", индикаторы выделения
    @IBOutlet weak var tomorrowCheckLabel: UILabel!
    @IBOutlet weak var days2CheckLabel: UILabel!
    @IBOutlet weak var days3CheckLabel: UILabel!
    @IBOutlet weak var days4CheckLabel: UILabel!
    @IBOutlet weak var days5CheckLabel: UILabel!
    @IBOutlet weak var days6CheckLabel: UILabel!
    @IBOutlet weak var weekCheckLabel: UILabel!
    @IBOutlet weak var monthCheckLabel: UILabel!
    @IBOutlet weak var customNotificationDateTime: UIDatePicker!
    
    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var ownerNameField: UILabel!

	override var preferredStatusBarStyle: UIStatusBarStyle {
		get {
			return .lightContent
		}
	}

    override func viewDidLoad() {
        tableView.keyboardDismissMode = .onDrag
        
		self.updateLabelSelection(aftertime: 0)
        if notification != nil{
            selectedDate = notification!.date as Date?
            selectedText = notification!.text
        }

		self.updateTextBottonsVisability(callButtonSelected: selectedText == "Call".localized, deleteSelected: selectedText == "Delete".localized)
		self.bodyTextView.layer.cornerRadius = 16
		self.bodyTextView.layer.borderWidth = 1
		self.bodyTextView.layer.borderColor = ColorHelper.AppTintColor().cgColor
        self.bodyTextView.font = UIFont.appFont(size: 17, typeface: .light)
        self.bodyTextView.textColor = ColorHelper.AppTintColor()
        bodyTextView.textContainerInset = UIEdgeInsets(top: 9, left: 17, bottom: 9, right: 17)
		self.bodyTextView.placeholder = "edit-notification-vc.custom-text-field.placeholder".localized

        if selectedText != nil{
            bodyTextView.text = selectedText
        }
        
        if selectedDate != nil{
            customNotificationDateTime.date = selectedDate!
        }
        
        bodyTextView.delegate = self
        customNotificationDateTime.addTarget(self, action: #selector(notificationDateDidChanged), for:
                                                UIControl.Event.valueChanged)
        
        switch owner {
        case .contact(let contact)?:
            ownerNameField.text = contact.displayName
        case .group(let group)?:
            ownerNameField.text = group.name
        default:
            ownerNameField.text = nil
        }
        headerView.layoutSubviews()
    }
    
    @IBAction func cancelClick(_ sender: Any) {
        let _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitClick(_ sender: Any) {

        let notification: AppNotification?
        if let date = selectedDate {
            notification = self.notification ?? AppNotification(context: context)
            notification?.date = date
            notification?.text = selectedText ?? ""
        } else {
            notification = nil
        }
        if self.notification == nil {
            // Значит создали новую
            Metrica.instance.send(.notificationIsCreated)
        }
        delegate?.editNotificationControllerDidComplete(self, with: notification)
    }

    //Обработка нажатий на ячейки
    
    @IBAction func selectCallClick(_ sender: Any) {
        bodyTextView.text = "Call".localized
        selectedText = "Call".localized
        self.updateTextBottonsVisability(callButtonSelected: true, deleteSelected: false)
    }
    
    @IBAction func selectDeleteClick(_ sender: Any) {
        bodyTextView.text = "Delete".localized
        selectedText = "Delete".localized
        self.updateTextBottonsVisability(callButtonSelected: false, deleteSelected: true)
    }
    
    func calculateDate(aftertime: Int) -> Date? {
        guard let calendar = NSCalendar(calendarIdentifier: .gregorian) else { return nil }
        var dayComponent = DateComponents()
        let originalDate: Date?
        if aftertime < 8{
            dayComponent.day = aftertime
            originalDate = calendar.date(byAdding: dayComponent, to: Date())
        }
        else{
            dayComponent.month = 1
            originalDate = calendar.date(byAdding: dayComponent, to: Date())
        }
        
        guard let date = originalDate else {
            return nil
        }
        
        var components = calendar.components([.year, .month, .day ], from: date)
        components.hour = 10
        components.minute = 0
        return calendar.date(from: components)
    }
    
    func selectDate(aftertime: Int){
		self.updateLabelSelection(aftertime: aftertime)
		selectedDate = calculateDate(aftertime: aftertime)
    }

	func updateLabelSelection(aftertime: Int) {
		let calculateFont:((_ selected:Bool) -> UIFont) = { selected in
			return selected ? UIFont.museoSansCyrl(weight: .w700, size: 16) : UIFont.museoSansCyrl(weight: .w100, size: 16)
		}

		tomorrowCheckLabel.font = calculateFont(aftertime == 1)
		days2CheckLabel.font = calculateFont(aftertime == 2)
		days3CheckLabel.font = calculateFont(aftertime == 3)
		days4CheckLabel.font = calculateFont(aftertime == 4)
		days5CheckLabel.font = calculateFont(aftertime == 5)
		days6CheckLabel.font = calculateFont(aftertime == 6)
		weekCheckLabel.font = calculateFont(aftertime == 7)
		monthCheckLabel.font = calculateFont(aftertime == 8)
	}

    @IBAction func selectTomorrowClick(_ sender: Any) {
        selectDate(aftertime: 1)
    }
    
    @IBAction func select2DaysClick(_ sender: Any) {
        selectDate(aftertime: 2)
    }
    
    @IBAction func select3DaysClick(_ sender: Any) {
        selectDate(aftertime: 3)
    }
    
    @IBAction func select4DaysClick(_ sender: Any) {
        selectDate(aftertime: 4)
    }
    
    @IBAction func select5DaysClick(_ sender: Any) {
        selectDate(aftertime: 5)
    }
    
    @IBAction func select6DaysClick(_ sender: Any) {
        selectDate(aftertime: 6)
    }
    
    @IBAction func selectWeekClick(_ sender: Any) {
        selectDate(aftertime: 7)
    }
    
    @IBAction func selectMonthClick(_ sender: Any) {
        selectDate(aftertime: 8)
    }
    
    //Ввод кастомных значений
    
    @objc func notificationDateDidChanged(){
		self.updateLabelSelection(aftertime: 0)
        selectedDate = customNotificationDateTime.date
    }
    
    @IBAction func deleteClick(_ sender: Any) {
        let alert = UIAlertController(title: "DeleteNotificationTitle".localized, message: "DeleteNotificationMessage".localized, preferredStyle: UIAlertController.Style.alert)
        let editAction = UIAlertAction(title: "Delete".localized, style: .destructive) { [unowned self] (action) in
            self.bodyTextView.text = ""
            self.updateTextBottonsVisability(callButtonSelected: false, deleteSelected: false)
            self.selectedText = nil
            self.selectedDate = nil
            self.updateLabelSelection(aftertime: 0)
            if let n = self.notification, let context = n.managedObjectContext {
                context.delete(n)
                CoreDataManager.instance.saveContext(context)
                self.notification = nil
            }
            self.delegate?.editNotificationControllerDidComplete(self, with: nil)
        }
        alert.addAction(UIAlertAction(title: "Close".localized, style: UIAlertAction.Style.cancel, handler: nil))
        alert.addAction(editAction)
        alert.view.tintColor = ColorHelper.AppDarkTextColor()
        self.present(alert, animated: true, completion: nil)
    }


	// MARK: - Private -

	fileprivate func updateTextBottonsVisability(callButtonSelected:Bool, deleteSelected:Bool) {

		self.callButton.layer.cornerRadius = self.callButton.bounds.size.height / 2.0
		self.callButton.layer.borderWidth = 1
		self.callButton.layer.borderColor = callButtonSelected ? ColorHelper.AppGreenColor().cgColor : ColorHelper.AppTintColor().cgColor
		self.callButton.isSelected = callButtonSelected
		self.callButton.backgroundColor = callButtonSelected ? ColorHelper.AppGreenColor() : UIColor.clear
		self.callButton.setTitleColor(ColorHelper.AppTintColor(), for: .normal)
		self.callButton.setTitleColor(ColorHelper.AppTintColor(), for: .selected)

		self.deleteButton.layer.cornerRadius = self.deleteButton.bounds.size.height / 2.0
		self.deleteButton.layer.borderWidth = 1
		self.deleteButton.layer.borderColor = deleteSelected ? ColorHelper.AppRedColor().cgColor : ColorHelper.AppTintColor().cgColor
		self.deleteButton.isSelected = deleteSelected
		self.deleteButton.backgroundColor = deleteSelected ? ColorHelper.AppRedColor() : UIColor.clear
		self.deleteButton.setTitleColor(ColorHelper.AppTintColor(), for: .normal)
		self.deleteButton.setTitleColor(UIColor.white, for: .selected)
	}
}

extension EditNotificationViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        updateTextBottonsVisability(callButtonSelected: false, deleteSelected: false)
        selectedText = textView.text
    }
}

