//
//  SettingsEnum.swift
//  TemporaryContacts
//
//  Created by User on 27/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation

enum SettingsSelect: String{
    case StartPage = "StartPage",
    DisplayOrder = "DisplayOrder",
    GroupDisplay = "GroupDisplay"
}

enum SettingsStartPage: String{
    case all = "AllTab"
    case temp = "TempTab"
    case groups = "GroupsTab"
    case favorites = "favorites"
    case keys = "keys"
    case businessCards = "businessCards"
    case notifications = "notifications"
    
    static let allValues: [String] = [
        all.rawValue,
        temp.rawValue,
        groups.rawValue,
        favorites.rawValue,
        keys.rawValue,
        businessCards.rawValue,
        notifications.rawValue
    ]
}

enum SettingsDisplayOrder: String{
    case NameFirst = "NameFirst",
    FamilyFirst = "FamilyFirst"
    
    static let allValues:[String] = [NameFirst.rawValue, FamilyFirst.rawValue]
}

enum SettingsGroupDisplay: String{
    case Bars = "Bars",
    List = "List"
    
    static let allValues:[String] = [Bars.rawValue, List.rawValue]
}

/// Стиль отображения в визитнице
///
/// - list: Список без возможности листания визиток
/// - cards: Полноценная большая карточка с листанием
enum BusinessCardsDisplayMode: String {
    case list = "List"
    case cards = "cards"
}
