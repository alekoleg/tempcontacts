//
//  SettingsViewController.swift
//  TemporaryContacts
//
//  Created by User on 22/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit
import StoreKit
import RxSwift

private struct Constants {
    static let developerEmail = "temp.phones17@gmail.com"
}

protocol SettingsViewControllerDelegate: class {
    func settingsControllerDidUpdateSettings(_ settingsController: SettingsViewController, settings: Settings)
}

class SettingsViewController: UITableViewController {
    /// Выбираемый параметр
    fileprivate var selection: SettingsSelect = .StartPage
    
    @IBOutlet private weak var allTabSwitcher: UISwitch!
    /// Свитчер удаления старыху ведомлений
    @IBOutlet private weak var removeOldNotificationsSwitcher: UISwitch!
    
    weak var delegate: SettingsViewControllerDelegate?
    /// Роутинг настроек
    weak var router: ContactsRouter?
    
    var communicationHelper: CommunicationHelper?
    
    var purchasesManager: PurchasesManager?
    
    private let disposeBag = DisposeBag()

	override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
	}
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        hidesBottomBarWhenPushed = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		self.tp_makeGreenBackButton()
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 32, bottom: 0, right: 0)
        allTabSwitcher.isOn = Settings.instance.showAllTabWhenContactsDidUpdate
        removeOldNotificationsSwitcher.isOn = Settings.instance.shouldRemoveNotificationsAfterFewDaysAfterView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func unwindSettings(_ segue:UIStoryboardSegue) {
        if let sourceController = segue.source as? SettingsSelectpickerViewController{
            switch sourceController.selection {
            case .StartPage:
                guard let pageType = SettingsStartPage(rawValue: sourceController.selected) else {
                    break
                }
                Settings.instance.startPageType = pageType
            case .DisplayOrder:
                guard let order = SettingsDisplayOrder(rawValue: sourceController.selected) else {
                    break
                }
                Settings.instance.contactsOrder = order
            case .GroupDisplay:
                guard let type = SettingsGroupDisplay(rawValue: sourceController.selected) else {
                    break
                }
                Settings.instance.groupDisplayType = type
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let controller = segue.destination as? SettingsSelectpickerViewController else {
            return
        }
        
        controller.hidesBottomBarWhenPushed = true
        
        controller.selection = selection
        switch selection {
        case .StartPage:
            controller.selected = Settings.instance.startPageType.rawValue
        case .DisplayOrder:
            controller.selected = Settings.instance.contactsOrder.rawValue
        case .GroupDisplay:
            controller.selected = Settings.instance.groupDisplayType.rawValue
        }
    }

    
    @IBAction func selectStartPageClick(_ sender: Any) {
        selection = .StartPage
        performSegue(withIdentifier: "selectSettingsSegue", sender: self)
    }
    
    @IBAction func selectDisplayOrder(_ sender: Any) {
        selection = .DisplayOrder
        performSegue(withIdentifier: "selectSettingsSegue", sender: self)
    }

    @IBAction func selectGroupDisplay(_ sender: Any) {
        selection = .GroupDisplay
        performSegue(withIdentifier: "selectSettingsSegue", sender: self)
    }
    
    @IBAction func donate(_ sender: Any) {
        router?.showDonateScreen()
    }
    
    @IBAction func videoGuide(_ sender: Any) {
        let proposalRouter = router?.presentProposalRouter(presentWithAlpha: false)
        proposalRouter?.showVideoGuide(
            skipAction: { vc in
                vc.dismiss(animated: true, completion: nil)
        },
            finishAction: nil
        )
    }
    
    @IBAction func allTabSwitcherChanged(_ sender: Any) {
        Settings.instance.showAllTabWhenContactsDidUpdate = allTabSwitcher.isOn
    }
    
    @IBAction func removeOldNotificationsSwitchedChaned(_ sender: Any) {
        Settings.instance.shouldRemoveNotificationsAfterFewDaysAfterView = removeOldNotificationsSwitcher.isOn
    }
    
    @IBAction func unlockAll() {
        purchasesManager?.buy(.unlockAll)
            .take(1)
            .subscribe(
                onNext: { [weak self] result in
                    switch result {
                    case .completed:
                        self?.router?.showThanksScreen(closeAction: nil)
                    default:
                        break
                    }
            },
                onError: { [unowned self] error in
                    let error = error as NSError
                    self.showAlert(
                        title: "in-app-purchase.error-title".localized,
                        message: error.localizedDescription,
                        actions: [UIAlertAction(title: "Close".localized, style: .cancel, handler: nil)]
                    )
            })
        .addDisposableTo(disposeBag)
    }

	@IBAction func restoreAll() {
        purchasesManager?.restore()
            .subscribe(
                onNext: { [unowned self] _ in
                    self.showAlert(
                        title: "in-app-purchase.restore.success-title".localized,
                        message: "in-app-purchase.restore.success-text".localized,
                        actions: [UIAlertAction(title: "Close".localized, style: .cancel, handler: nil)]
                    )
                },
                onError: { [unowned self] error in
                    let error = error as NSError
                    self.showAlert(
                        title: "in-app-purchase.restore.error-title".localized,
                        message: error.localizedDescription,
                        actions: [UIAlertAction(title: "Close".localized, style: .cancel, handler: nil)]
                    )
            })
            .addDisposableTo(disposeBag)
	}
    
    /// Отправить письмо разработчику
    @IBAction func sendEmailToDevelopper() {
        let systemName = UIDevice.current.systemName
        let systemVersion = UIDevice.current.systemVersion
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"]
        let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"]
        let message: String?
        if let version = version as? String, let build = buildNumber as? String {
            message = "\n\n\n\n\n\n\(systemName) \(systemVersion), version \(version) (\(build))"
        } else {
            message = nil
        }
        
        do {
            try communicationHelper?.sendEmail(
                emailAddress: Constants.developerEmail,
                message: message
            )
        } catch {
            print("Ошибка отправки почты разработчику: \(error)")
        }
    }
    
    /// Оценить приложение
    @IBAction func rateApp() {
        communicationHelper?.openInAppStore(forReview: true)
    }

    override func willMove(toParent parent: UIViewController?) {
        if parent == nil { //Уходим с экрана
            delegate?.settingsControllerDidUpdateSettings(self, settings: Settings.instance)
        }
    }
}


extension SettingsViewController: UserNotificationProtocol {}
