//
//  SettingsSelectpickerViewController.swift
//  TemporaryContacts
//
//  Created by User on 22/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SettingsSelectpickerViewController: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    
    var selection: SettingsSelect = .StartPage
    var selected: String = ""
    var data: [String] = []

	override var preferredStatusBarStyle: UIStatusBarStyle {
		get {
			return .lightContent
		}
	}
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 32.0, bottom: 0.0, right: 0.0)
		self.tp_makeGreenBackButton()
        navigationItem.title = selection.rawValue.localized
        if selection == .StartPage{
            data = SettingsStartPage.allValues
        }
        else if selection == .DisplayOrder{
            data = SettingsDisplayOrder.allValues
        }
        else{
            data = SettingsGroupDisplay.allValues
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
     
        let nib = UINib(nibName: "SelectionCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "SelectionCell")
    }
    
    @IBAction func submitButtonClick(_ sender: Any) {
        performSegue(withIdentifier: "unwindSettings", sender: self)
    }
    
    @IBAction func cancelButtonClick(_ sender: Any) {
        let _ = navigationController?.popViewController(animated: true)
    }
}

extension SettingsSelectpickerViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return data.count
    }
    
    private func checkIfSelected(indexPath: IndexPath) -> Bool{
        switch selection {
        case .StartPage:
            if let startPage = SettingsStartPage(rawValue: selected){
                return startPage.rawValue == data[indexPath.row]
            }
        case .GroupDisplay:
            if let groupDisplay = SettingsGroupDisplay(rawValue: selected){
                return groupDisplay.rawValue == data[indexPath.row]
            }
        case .DisplayOrder:
            if let displayOrder = SettingsDisplayOrder(rawValue: selected){
                return displayOrder.rawValue == data[indexPath.row]
            }
        }
        return data[indexPath.row] == selected
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionCell", for: indexPath) as! SelectionCell
        let isSelected = checkIfSelected(indexPath: indexPath)
        cell.prepare(text: data[indexPath.row].localized, checked: isSelected)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected = data[indexPath.row]
        tableView.reloadData()
    }

}

