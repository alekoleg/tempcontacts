//
//  DonateControllerViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 06.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import StoreKit
import RxSwift

/// Вью модель экрана пожертвований
class DonateViewModel {
    
    /// Менеджер покупок
    private let purchaseManager: PurchasesManager
    /// Доступные пожертвования
    fileprivate let purchases: [PurchasesManager.Purchase]
    /// Роутер
    fileprivate let router: ContactsRouter
    /// Диспоуз
    fileprivate let disposeBag = DisposeBag()
    
    init(purchaseManager: PurchasesManager, router: ContactsRouter) {
        self.purchaseManager = purchaseManager
        self.router = router
        purchases = [
            PurchasesManager.Purchase.donate1,
            PurchasesManager.Purchase.donate2,
            PurchasesManager.Purchase.donate3,
        ]
    }
    
    // MARK: - getters
    
    var numberOfButtons: Int {
        return purchases.count
    }
    
    func buttonTitle(at index: Int) -> String {
        let purchase = purchases[index]
        return purchaseManager.localizedPrice(for: purchase) ?? "Покупка №\(index + 1)"
    }
    
    // MARK: - actions
    
    func tapToButton(at index: Int) {
        let purchase = purchases[index]
        purchaseManager.buy(purchase)
            .take(1)
            .subscribe(
                onNext: { [unowned self] result in
                    switch result {
                    case .completed, .restored:
                        // Успешно купили
                        self.showThanks()
                    default:
                        break
                    }
                },
                onError: { [unowned self] error in
                    let error = error as NSError
                    self.router.showAlertController(
                        title: "in-app-purchase.error-title".localized,
                        message: error.localizedDescription,
                        style: .alert,
                        actions: [UIAlertAction(title: "Close".localized, style: .cancel, handler: nil)]
                    )
            })
            .addDisposableTo(disposeBag)
    }
    
    private func showThanks() {
        router.showThanksScreen { vc in
            vc.dismiss(animated: true, completion: nil)
        }
        router.didShowSignal
            .take(1)
            .subscribe(
                onNext: { [unowned self] _ in
                    self.router.removeControllersFromStack(types: [DonateController.self])
            })
            .addDisposableTo(disposeBag)
    }
}
