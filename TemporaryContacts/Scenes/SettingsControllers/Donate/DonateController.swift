//
//  DonateController.swift
//  TemporaryContacts
//
//  Created by Konshin on 06.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

/// Экран пожертвований
class DonateController: UIViewController {
    
    private let backgroundImage: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "donate_background"))
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    private let backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "BackChevron").withRenderingMode(.alwaysOriginal), for: .normal)
        return button
    }()
    
    private let appIconView = UIImageView(image: #imageLiteral(resourceName: "donate_app_icon"))
    
    private let mainLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.appFont(size: 20, typeface: .bold)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = "donate.support-project".localized
        return label
    }()
    
    private let subLabel: UILabel = {
        let label = UILabel()
        if AppConstants.isScreenSizeIsGreaterThanIPhone5 {
            label.font = UIFont.appFont(size: 19, typeface: .medium)
        } else {
            label.font = UIFont.appFont(size: 16, typeface: .medium)
        }
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.setContentHuggingPriority(UILayoutPriority(rawValue: 0), for: .vertical)
        label.text = "donate.only-with-you".localized
        return label
    }()
    
    private let buttonsContainer = UIView()
    
    fileprivate let viewModel: DonateViewModel
    
    init(vm: DonateViewModel) {
        viewModel = vm
        super.init(nibName: nil, bundle: nil)
        
        hidesBottomBarWhenPushed = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    // MARK: - actions
    
    private func setup() {
        view.backgroundColor = ColorHelper.AppTintColor()
        
        view.addSubview(backgroundImage)
        backgroundImage.snp.remakeConstraints { $0.edges.equalToSuperview() }
        
        view.addSubview(backButton)
        backButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        backButton.snp.remakeConstraints { maker in
            maker.width.height.equalTo(40)
            maker.left.equalToSuperview().offset(12)
            maker.top.equalToSuperview().offset(30)
        }
        
        view.addSubview(appIconView)
        appIconView.snp.remakeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.top.equalToSuperview().offset(50)
        }
        
        view.addSubview(mainLabel)
        mainLabel.snp.remakeConstraints { maker in
            maker.left.right.equalToSuperview().inset(20)
            maker.top.equalTo(appIconView.snp.bottom).offset(14)
        }
        
        view.addSubview(subLabel)
        subLabel.snp.remakeConstraints { maker in
            maker.left.right.equalToSuperview().inset(20)
            maker.top.equalTo(mainLabel.snp.bottom).offset(20)
        }
        
        view.addSubview(buttonsContainer)
        buttonsContainer.snp.remakeConstraints { maker in
            maker.left.right.bottom.equalToSuperview().inset(30)
            maker.top.equalTo(subLabel.snp.bottom).offset(8)
        }
        
        setupButtons()
    }
    
    private func setupButtons() {
        let buttonHeight: CGFloat = 44
        
        var lastElement: UIView?
        
        let numberOfItems = viewModel.numberOfButtons
        for i in 0..<numberOfItems {
            let button = UIButton(type: .system)
            button.layer.cornerRadius = buttonHeight / 2
            button.layer.borderWidth = 2
            button.layer.borderColor = UIColor.white.cgColor
            button.titleLabel?.font = UIFont.appFont(size: 15, typeface: .heavy)
            button.setTitle(viewModel.buttonTitle(at: i), for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            button.tag = i
            button.addTarget(self, action: #selector(tapToButton(_:)), for: .touchUpInside)
            
            buttonsContainer.addSubview(button)
            button.snp.remakeConstraints { maker in
                maker.left.right.equalToSuperview()
                maker.height.equalTo(buttonHeight)
                if let last = lastElement {
                    maker.top.equalTo(last.snp.bottom).offset(12)
                } else {
                    maker.top.equalToSuperview()
                }
                if i == numberOfItems - 1 {
                    maker.bottom.equalToSuperview()
                }
            }
            
            lastElement = button
        }
    }
    
    @objc private func tapToButton(_ button: UIButton) {
        viewModel.tapToButton(at: button.tag)
    }
    
    @objc private func back() {
        navigationController?.popViewController(animated: true)
    }
}
