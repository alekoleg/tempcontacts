//
//  ThanksViewController.swift
//  TemporaryContacts
//
//  Created by Konshin on 07.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

/// Экран благодарности
class ThanksViewController: UIViewController {
    
    typealias Action = (ThanksViewController) -> Void
    
    @IBOutlet private var closeButton: UIButton!
    
    /// Действие по закрытию
    let closeAction: Action?
    
    init(closeAction: Action?) {
        self.closeAction = closeAction
        super.init(nibName: "ThanksViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        closeButton.layer.borderColor = UIColor.white.cgColor
        closeButton.layer.cornerRadius = closeButton.bounds.height / 2
        closeButton.layer.borderWidth = 2
        closeButton.titleLabel?.font = UIFont.appFont(size: 15, typeface: .bold)
        closeButton.setTitle("thanks.close".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func close() {
        if let action = closeAction {
            action(self)
            return
        }
        if let navigation = navigationController {
            navigation.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }

}
