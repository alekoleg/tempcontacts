//
//  ThanksScreenRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol ThanksScreenRouter: AlertPresenterRouter {
    /// Отображает экран благодарностей
    func showThanksScreen(closeAction: ThanksViewController.Action?)
}

extension Router where Self: ThanksScreenRouter, RootController == UINavigationController {
    /// Отображает экран благодарностей
    func showThanksScreen(closeAction: ThanksViewController.Action?) {
        let vc = ThanksViewController(closeAction: closeAction)
        rootController.pushViewController(vc, animated: true)
    }
}
