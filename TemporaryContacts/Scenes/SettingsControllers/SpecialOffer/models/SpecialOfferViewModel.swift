//
//  SpecialOfferViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 07.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import RxSwift

class SpecialOfferViewModel {
    
    typealias Action = (SpecialOfferViewModel) -> Void
    
    struct Item {
        let number: String
        let icon: UIImage?
        let text: String
    }
    
    /// Менеджер по покупкам
    fileprivate let purchaseManager: PurchasesManager
    /// Менеджер по доступности
    fileprivate let availabilityManager: AvailabilityManager
    /// Роутер
    fileprivate let router: AlertPresenterRouter
    /// Диспоуз
    fileprivate let disposeBag = DisposeBag()
    /// Доступные итемы, отображающие ограничения
    fileprivate let items: [Item]
    /// Действие после покупки
    var didBuyAction: Action?
    
    init(purchaseManager: PurchasesManager,
         availabilityManager: AvailabilityManager,
         router: AlertPresenterRouter) {
        self.purchaseManager = purchaseManager
        self.availabilityManager = availabilityManager
        self.router = router
        items = [
            AvailabilityManager.Element.tempContacts,
            AvailabilityManager.Element.businessCards,
            AvailabilityManager.Element.groups,
            AvailabilityManager.Element.notifications,
        ]
            .map { element in
                switch element {
                case .businessCards:
                    return Item(
                        number: String(availabilityManager.maxNumber(for: element)),
                        icon: #imageLiteral(resourceName: "special_offer_limit_business_cards"),
                        text: "special-offer.element.business-card".localized
                    )
                case .groups:
                    return Item(
                        number: String(availabilityManager.maxNumber(for: element)),
                        icon: #imageLiteral(resourceName: "special_offer_limit_groups"),
                        text: "special-offer.element.groups".localized
                    )
                case .tempContacts:
                    return Item(
                        number: String(availabilityManager.maxNumber(for: element)),
                        icon: #imageLiteral(resourceName: "special_offer_limit_temp_contacts"),
                        text: "special-offer.element.temporary-contacts".localized
                    )
                case .notifications:
                    return Item(
                        number: String(availabilityManager.maxNumber(for: element)),
                        icon: #imageLiteral(resourceName: "special_offer_limit_notifications"),
                        text: "special-offer.element.notificaions".localized
                    )
                }
        }
    }
    
    // MARK: - getters
    
    var limitationItems: [Item] {
        return items
    }
    
    var highPrice: PurchasesManager.LocalizedPrice {
        return purchaseManager.localizedPriceModel(for: .unlockAll)
            ?? PurchasesManager.LocalizedPrice(price: 0, symbol: nil)
    }
    
    var offerPrice: PurchasesManager.LocalizedPrice {
        return purchaseManager.localizedPriceModel(for: .unlockAllSpecialOffer)
            ?? PurchasesManager.LocalizedPrice(price: 0, symbol: nil)
    }
    
    // MARK: - actions
    
    func buy() {
        purchaseManager.buy(.unlockAllSpecialOffer)
            .subscribe(
                onNext: { [unowned self] result in
                    switch result {
                    case .completed:
                        // Успешно купили
                        self.didBuyAction?(self)
                    default:
                        break
                    }
                },
                onError: { [unowned self] error in
                    let error = error as NSError
                    self.router.showAlertController(
                        title: "in-app-purchase.error-title".localized,
                        message: error.localizedDescription,
                        style: .alert,
                        actions: [UIAlertAction(title: "Close".localized, style: .cancel, handler: nil)]
                    )
            })
            .addDisposableTo(disposeBag)
    }
}
