//
//  SpecialOfferViewController.swift
//  TemporaryContacts
//
//  Created by Konshin on 07.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

/// Экран сособого предложения
class SpecialOfferViewController: UIViewController {
    
    /// Действие по окончанию
    typealias DoneAction = (SpecialOfferViewController) -> Void
    
    @IBOutlet private var buyButton: UIButton!
    @IBOutlet private var limitationsContainer: UIView!
    @IBOutlet private var limitationLabel: UILabel!
    @IBOutlet private var offerLabel: UILabel!
    @IBOutlet private var pricesView: UIView!
    
    fileprivate let viewModel: SpecialOfferViewModel
    
    /// Действие по окончанию
    var doneAction: DoneAction?
    
    init(vm: SpecialOfferViewModel) {
        viewModel = vm
        super.init(nibName: "SpecialOfferViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    // MARK: - getters
    
    fileprivate let screenIsGreaterThan320 = AppConstants.isScreenSizeIsGreaterThanIPhone5
    
    // MARK: - actions

    private func setup() {
        buyButton.layer.cornerRadius = buyButton.bounds.height / 2
        buyButton.layer.masksToBounds = true
        
        let limitationsFont: UIFont
        if screenIsGreaterThan320 {
            limitationsFont = UIFont.appFont(size: 19)
        } else {
            limitationsFont = UIFont.appFont(size: 15)
        }
        limitationLabel.attributedText = "special-offer.your-app-is-limited"
            .localized
            .attributedString(lineHeight: 22,
                              font: limitationsFont,
                              textAlignment: .center)
        offerLabel.attributedText = "special-offer.you-can-disable-limit"
            .localized
            .attributedString(lineHeight: 22,
                              font: UIFont.appFont(size: 15),
                              textAlignment: .center)
        
        setupLimitations()
        setupPrices()
    }
    
    private func setupLimitations() {
        let items = viewModel.limitationItems
        
        let interLineOffset: CGFloat = screenIsGreaterThan320 ? 4 : 0
        
        var lastView: UIView?
        for (index, item) in items.enumerated() {
            let view = UIView()
            limitationsContainer.addSubview(view)
            view.snp.remakeConstraints() { maker in
                maker.top.bottom.equalToSuperview()
                maker.width.equalTo(76)
                if let last = lastView {
                    maker.left.equalTo(last.snp.right).offset(interLineOffset)
                } else {
                    maker.left.equalToSuperview()
                }
                if index == items.count - 1 {
                    maker.right.equalToSuperview()
                }
            }
            
            let imageView = UIImageView(image: item.icon)
            view.addSubview(imageView)
            imageView.snp.remakeConstraints { maker in
                maker.centerX.equalToSuperview().offset(5)
                maker.top.equalToSuperview().offset(6)
                maker.width.height.equalTo(50)
            }
            
            let numberLabel = UILabel()
            numberLabel.text = item.number
            numberLabel.textColor = UIColor.white
            numberLabel.font = UIFont.appFont(size: 19, typeface: .heavy)
            view.addSubview(numberLabel)
            numberLabel.snp.remakeConstraints { maker in
                maker.left.equalTo(imageView).offset(34)
                maker.bottom.equalTo(imageView).offset(-30)
            }
            
            let textLabel = UILabel()
            textLabel.text = item.text
            textLabel.textColor = UIColor.white
            
            
            let fontSize: CGFloat = screenIsGreaterThan320
                ? 13
                : 11
            
            textLabel.font = UIFont.appFont(size: fontSize, typeface: .medium)
            textLabel.textAlignment = .center
            textLabel.numberOfLines = 2
            view.addSubview(textLabel)
            textLabel.snp.remakeConstraints { maker in
                maker.left.right.bottom.equalToSuperview()
                maker.top.equalTo(imageView.snp.bottom)
            }
            
            lastView = view
        }
    }
    
    private func setupPrices() {
        func label(price: PurchasesManager.LocalizedPrice, textColor: UIColor) -> UILabel {
            let label = UILabel()
            
            let string = NSMutableAttributedString(
                string: price.price.stringValue,
                attributes: [
                    NSAttributedString.Key.foregroundColor: textColor,
                    NSAttributedString.Key.font: UIFont.appFont(size: 40, typeface: .black)
                ]
            )
            if let currency = price.symbol {
                string.append(
                    NSAttributedString(
                        string: currency,
                        attributes: [
                            NSAttributedString.Key.foregroundColor: textColor,
                            NSAttributedString.Key.font: UIFont.appFont(size: 20, typeface: .black)
                        ]
                    )
                )
            }
            label.attributedText = string
            
            return label
        }
        
        let oldPriceLabel = label(price: viewModel.highPrice, textColor: UIColor(hex: 0x636278))
        pricesView.addSubview(oldPriceLabel)
        oldPriceLabel.snp.remakeConstraints { maker in
            maker.left.centerY.equalToSuperview()
        }
        
        let strike = StrikeoutView()
        pricesView.addSubview(strike)
        strike.snp.remakeConstraints { maker in
            maker.edges.equalTo(oldPriceLabel)
        }
        
        let offerLabel = label(price: viewModel.offerPrice, textColor: UIColor.white)
        pricesView.addSubview(offerLabel)
        offerLabel.snp.remakeConstraints { maker in
            maker.left.equalTo(oldPriceLabel.snp.right).offset(16)
            maker.right.top.bottom.equalToSuperview()
        }
    }
    
    // MARK: ibactions
    
    @IBAction fileprivate func close() {
        if let action = doneAction {
            action(self)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction fileprivate func buy() {
        viewModel.buy()
    }
}
