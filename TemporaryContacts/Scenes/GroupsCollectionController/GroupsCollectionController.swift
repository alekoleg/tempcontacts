//
//  GroupsCollectionController.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import SnapKit

private let reuseIdentifier = "reuseIdentifier"
private let collectionHeight: CGFloat = 150

/// Экран групп контактов в виде коллекции
class GroupsCollectionController: UIViewController {
    fileprivate let viewModel: GroupsCollectionViewModel
    
    fileprivate let collectionView: UICollectionView
    fileprivate let bottomContentView = UIView()
    
    fileprivate var bottomContentController: UIViewController?
    
    weak var navigationBarUpdater: RootContactsNavigationBarUpdater?
    
    init(viewModel: GroupsCollectionViewModel) {
        self.viewModel = viewModel
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = .horizontal
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: collectionViewLayout
        )
        
        super.init(nibName: nil, bundle: nil)
        
        viewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(
            UINib(nibName: "GroupsCollectionCell", bundle: nil),
            forCellWithReuseIdentifier: reuseIdentifier
        )
        
        view.addSubview(collectionView)
        collectionView.snp.remakeConstraints() { maker in
            maker.top.equalToSuperview()
            maker.left.right.equalToSuperview().inset(4)
            maker.height.equalTo(collectionHeight)
        }
        
        view.addSubview(bottomContentView)
        bottomContentView.backgroundColor = UIColor.white
        bottomContentView.snp.remakeConstraints() { maker in
            maker.top.equalTo(collectionView.snp.bottom)
            maker.left.right.bottom.equalToSuperview()
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap))
        collectionView.addGestureRecognizer(tap)
        
        viewModel.updateData()
    }

    // MARK: - actions
    
    @objc fileprivate func addNewGroup() {
        viewModel.addNewGroup()
    }
    
    @objc fileprivate func showGroupDetail() {
        viewModel.showSelectedGroupDetail()
    }
    
    @objc fileprivate func tap(tap: UITapGestureRecognizer) {
        let location = tap.location(in: collectionView)
        let tappedIndexPath = collectionView.indexPathForItem(at: location)
        viewModel.tappedToIndexPath(tappedIndexPath)
    }
}


extension GroupsCollectionController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems(at: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: reuseIdentifier,
            for: indexPath
        )
        
        if let cell = cell as? GroupsCollectionCell {
            let item = viewModel.item(at: indexPath)
            cell.configure(with: item)
            cell.delegate = self
        }
        
        return cell
    }
}


extension GroupsCollectionController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 105, height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}


extension GroupsCollectionController: GroupsCollectionViewModelDelegate {
    func vmReloadData() {
        collectionView.reloadData()
    }
    
    func vmRemoveBottomContent() {
        for v in bottomContentView.subviews {
            v.removeFromSuperview()
        }
    }
    
    func vmShowContactsListController(with vm: ContactsListViewModel) {
        let vc = ContactsListController(viewModel: vm)
        bottomContentView.addSubview(vc.view)
        vc.view.snp.remakeConstraints() { maker in
            maker.edges.equalToSuperview()
        }
        
        bottomContentController = vc
    }
    
    func vmShowAlert(title: String?, message: String?, actions: [UIAlertAction]) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        for action in actions {
            alertController.addAction(action)
        }
        alertController.view.tintColor = ColorHelper.AppDarkTextColor()
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func vmReloadNavigationBar() {
        navigationBarUpdater?.updateNavigationBarRightItems(for: self)
    }
    
    func updatesConvertorHasReceivedfUpdates(_ convertor: FetchedResultsUpdatesConverter, updates: [FetchedResultUpdate]) {
        collectionView.performBatchUpdates(
            { [unowned self] in
                for update in updates {
                    self.collectionView.applyFetchedResultsUpdate(update)
                }
            },
            completion: nil
        )
    }
    
    func updatesConvertorHasReceivedError(_ convertor: FetchedResultsUpdatesConverter, error: Error) {
        collectionView.reloadData()
    }
}


extension GroupsCollectionController: GroupsCollectionCellDelegate {
    func groupsCollectionCellDidTapDelete(cell: GroupsCollectionCell) {
        guard let indexPath = collectionView.indexPath(for: cell) else {
            return
        }
        viewModel.delete(at: indexPath)
    }
    
    func groupsCollectionCellDidTapNotification(cell: GroupsCollectionCell) {
        guard let indexPath = collectionView.indexPath(for: cell) else {
            return
        }
        viewModel.editNotification(at: indexPath)
    }
}


extension GroupsCollectionController: RootContactsTabViewController {
    var rightNavigationItems: [UIBarButtonItem]? {
        let detail = UIBarButtonItem(
            image: #imageLiteral(resourceName: "Dots"),
            style: .plain,
            target: self,
            action: #selector(showGroupDetail)
        )
        let add = UIBarButtonItem(
            image: #imageLiteral(resourceName: "Add"),
            style: .plain,
            target: self,
            action: #selector(addNewGroup)
        )
        if viewModel.showGroupInfoButton {
            return [add, detail]
        } else {
            return [add]
        }
    }
}
