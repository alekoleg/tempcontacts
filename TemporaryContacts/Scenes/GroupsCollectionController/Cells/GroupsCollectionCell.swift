//
//  GroupsCollectionCell.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol GroupsCollectionCellDelegate: class {
    func groupsCollectionCellDidTapDelete(cell: GroupsCollectionCell)
    func groupsCollectionCellDidTapNotification(cell: GroupsCollectionCell)
}

class GroupsCollectionCell: UICollectionViewCell {
    @IBOutlet private var mainLabel: UILabel!
    @IBOutlet private var borderView: UIView!
    @IBOutlet private var notificationButton: UIButton!
    
    weak var delegate: GroupsCollectionCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        borderView.layer.borderColor = ColorHelper.AppTintColor().cgColor
        borderView.layer.borderWidth = 1.0
        notificationButton.layer.cornerRadius = notificationButton.bounds.size.height / 2.0
    }

    func configure(with item: GroupsCollectionCellItem) {
        mainLabel.text = item.text
        contentView.alpha = item.highlighted ? 1 : 0.5
    }
    
    // MARK: - IBActions
    
    @IBAction func deleteTapped() {
        delegate?.groupsCollectionCellDidTapDelete(cell: self)
    }
    
    @IBAction func notificationTapped() {
        delegate?.groupsCollectionCellDidTapNotification(cell: self)
    }
}
