//
//  GroupsCollectionCellItem.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

struct GroupsCollectionCellItem {
    let text: String
    let highlighted: Bool
}
