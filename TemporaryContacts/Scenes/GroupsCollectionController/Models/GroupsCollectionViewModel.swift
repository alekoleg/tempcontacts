//
//  GroupsCollectionViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData
import PhoneNumberKit

protocol GroupsCollectionViewModelDelegate: FetchedResultsUpdatesConverterDelegate, UserNotificationProtocol {
    func vmReloadData()
    func vmShowContactsListController(with vm: ContactsListViewModel)
    func vmRemoveBottomContent()
    func vmReloadNavigationBar()
}

class GroupsCollectionViewModel {
    fileprivate let router: ContactsRouter
    fileprivate let settings: Settings
    fileprivate let fetchedController: NSFetchedResultsController<Groups>
    fileprivate let updateConverter = FetchedResultsUpdatesConverter()
    fileprivate let communicationHelper: CommunicationHelper
    fileprivate let partialFormatter: PartialFormatter
    fileprivate let availabilityManager: AvailabilityManager
    
    fileprivate var selectedGroup: Groups? {
        didSet {
            if let group = selectedGroup {
                delegate?.vmShowContactsListController(with: contactsListViewModel(for: group))
            } else {
                delegate?.vmRemoveBottomContent()
            }
            delegate?.vmReloadNavigationBar()
        }
    }
    
    weak var delegate: GroupsCollectionViewModelDelegate? {
        didSet {
            updateConverter.delegate = delegate
        }
    }
    
    init(
        fetchedController: NSFetchedResultsController<Groups>,
        router: ContactsRouter,
        settings: Settings,
        communicationHelper: CommunicationHelper,
        partialFormatter: PartialFormatter,
        availabilityManager: AvailabilityManager
        )
    {
        self.fetchedController = fetchedController
        self.router = router
        self.settings = settings
        self.communicationHelper = communicationHelper
        self.partialFormatter = partialFormatter
        self.availabilityManager = availabilityManager
        
        fetchedController.delegate = updateConverter
    }
    
    // MARK: - Getters
    
    func numberOfSections() -> Int {
        return fetchedController.sections?.count ?? 0
    }
    
    func numberOfItems(at section: Int) -> Int {
        guard let sections = fetchedController.sections, sections.count > section else {
            return 0
        }
        
        return sections[section].numberOfObjects
    }
    
    func item(at indexPath: IndexPath) -> GroupsCollectionCellItem {
        let group = fetchedController.object(at: indexPath)
        
        return GroupsCollectionCellItem(
            text: group.name ?? "groups.collection.group.unknown".localized,
            highlighted: selectedGroup == group
        )
    }
    
    /// Отображать ли айтем для углубления в информацию о выбранной группе
    var showGroupInfoButton: Bool {
        return selectedGroup != nil
    }
    
    // MARK: - actions
    
    func updateData() {
        do {
            try fetchedController.performFetch()
        } catch {
            print("Ошибка получения записей контактов")
        }
        delegate?.vmReloadData()
    }
    
    func delete(at indexPath: IndexPath) {
        let group = fetchedController.object(at: indexPath)
        
        let acceptAction = UIAlertAction(
            title: "Yes".localized,
            style: .destructive,
            handler: { alert in
                ContactsManager.instance.delete(group: group,
                                                deleteTempContacts: true,
                                                saveContext: true)
        })
        let cancelAction = UIAlertAction(title: "DeleteGroupCancel".localized, style: .cancel) { [unowned self] (action) in
            if self.settings.showTips {
                self.delegate?.showAlert(
                    title: "",
                    message: "DeleteGroupHint".localized,
                    actions: [
                        UIAlertAction(
                            title: "Close".localized,
                            style: UIAlertAction.Style.cancel,
                            handler: nil
                        )
                    ]
                )
            }
        }
        
        delegate?.showAlert(
            title: "DeleteGroupTitle".localized,
            message: "DeleteGroupMessage".localized,
            actions: [acceptAction, cancelAction]
        )
    }
    
    func addNewGroup() {
        guard availabilityManager.canAddNewElement(.groups) else {
            delegate?.showLimitationAlert(for: .groups)
            return
        }
        router.showGroupCreator()
    }
    
    func showSelectedGroupDetail() {
        guard let group = selectedGroup else {
            return
        }
        router.showGroupDetail(group)
    }
    
    func editNotification(at indexPath: IndexPath) {
        guard availabilityManager.canAddNewElement(.notifications) else {
            delegate?.showLimitationAlert(for: .notifications)
            return
        }
        let group = fetchedController.object(at: indexPath)
        
        router.showNotificationController(
            notification: nil,
            owner: .group(group),
            context: group.managedObjectContext,
            delegate: self
        )
    }
    
    func tappedToIndexPath(_ indexPath: IndexPath?) {
        if let ip = indexPath {
            selectedGroup = fetchedController.object(at: ip)
        } else {
            selectedGroup = nil
        }
        delegate?.vmReloadData()
    }
    
    // MARK: - private
    
    private func contactsListViewModel(for group: Groups) -> ContactsListViewModel {
        let resultsController: NSFetchedResultsController<Contacts> = CoreDataManager.instance
            .fetchedResultsControllerTyped(
                entityName: "Contacts",
                sortingDescriptors: settings.contactsSortingDescriptors(),
                cacheName: nil,
                sectionNameKeyPath: "sectionGroupingKey"
        )
        
        let config = ContactsListViewModelConfiguration(
            fetchedController: resultsController,
            filteringPredicate: NSPredicate(format: "(%@ IN groups)", group),
            isSearchAvailable: false
        )
        let vm = ContactsListViewModel(
            configuration: config,
            settings: settings,
            router: router,
            communicationHelper: communicationHelper,
            partialFormatter: partialFormatter,
            availabilityManager: availabilityManager
        )
        return vm
    }
}


extension GroupsCollectionViewModel: EditNotificationViewControllerDelegate {
    func editNotificationControllerDidComplete(_ controller: EditNotificationViewController, with notificaion: AppNotification?) {
        router.dismissController(controller)
        guard let notification = notificaion, case .group(let group)? = controller.owner else {
            return
        }
        
        group.addToNotifications(notification)
        CoreDataManager.instance.saveContext(group.managedObjectContext)
    }
}
