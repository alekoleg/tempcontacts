//
//  GroupCommunicationRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol GroupCommunicationRouter: AlertPresenterRouter, ContactsListRouter {
    
    /// Отображает экран отправки смс и почты для контактов групп
    func showGroupCommunicationController(group: Groups, action: GroupCommunicationsViewModel.Action)
}

extension Router where Self: GroupCommunicationRouter, RootController == UINavigationController {
    /// Отображает экран отправки смс и почты для контактов групп
    func showGroupCommunicationController(group: Groups, action: GroupCommunicationsViewModel.Action) {
        let vm = GroupCommunicationsViewModel(
            group: group,
            action: action,
            communicationHelper: dependencies.communicationHelper,
            settings: dependencies.settings,
            router: self,
            partialFormatter: dependencies.partialFormatter,
            availabilityManager: dependencies.availabilityManager
        )
        let vc = GroupCommunicationsController(viewModel: vm)
        rootController.pushViewController(vc, animated: true)
    }
}
