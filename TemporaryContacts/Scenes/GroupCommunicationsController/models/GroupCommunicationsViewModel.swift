//
//  GroupCommunicationsViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 25.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData
import PhoneNumberKit

protocol GroupCommunicationsViewModelDelegate: class {
    func vmSetSendButtonAvailable(_ available: Bool)
}

class GroupCommunicationsViewModel {
    enum Action {
        case sms
        case email
    }
    
    fileprivate let contactsListViewModel: ContactsSelectionListViewModel
    fileprivate let action: Action
    fileprivate let communicationHelper: CommunicationHelper
    fileprivate let router: GroupCommunicationRouter
    fileprivate let partialFormatter: PartialFormatter
    
    fileprivate var selectedContacts: [Contacts] {
        didSet {
            delegate?.vmSetSendButtonAvailable(isSendingAvailable)
        }
    }
    
    weak var delegate: GroupCommunicationsViewModelDelegate?
    
    init(group: Groups,
         action: Action,
         communicationHelper: CommunicationHelper,
         settings: Settings,
         router: GroupCommunicationRouter,
         partialFormatter: PartialFormatter,
         availabilityManager: AvailabilityManager)
    {
        self.action = action
        self.router = router
        self.communicationHelper = communicationHelper
        self.partialFormatter = partialFormatter
        
        let tempContext = CoreDataManager.instance.tempContextOnMainTread
        contactsListViewModel = createSelectionViewModel(
            for: action,
            group: group,
            settings: settings,
            router: router,
            in: tempContext,
            partialFormatter: partialFormatter,
            availabilityManager: availabilityManager
        )
        selectedContacts = contactsListViewModel.selectedContacts
        
        contactsListViewModel.selectionDelegate = self
    }
    
    // MARK: - getters
    
    var title: String {
        switch action {
        case .sms:
            return "group-communications.sms".localized
        case .email:
            return "group-communications.email".localized
        }
    }
    
    /// Картинка в навигейшен баре
    var headerImage: UIImage? {
        switch action {
        case .sms:
            return #imageLiteral(resourceName: "group_communication_sms")
        case .email:
            return #imageLiteral(resourceName: "group_communication_email")
        }
    }
    
    func contactsController() -> UIViewController {
        let vc = ContactsSelectionListController(vm: contactsListViewModel)
        return vc
    }
    
    /// Можно отправлять
    var isSendingAvailable: Bool {
        return !selectedContacts.isEmpty
    }
    
    // MARK: - actions
    
    /// Отправить почту
    func send() {
        guard isSendingAvailable else {
            fatalError("Отправка сейчас не доступна")
        }
        switch action {
        case .sms:
            let phones = selectedContacts
                .flatMap({ $0.mainPhone?.digits })
                .map({ communicationHelper.normalizePhone($0) })
            guard !phones.isEmpty else {
                router.showAlertController(
                    title: "group-communications.send-sms.failure".localized,
                    message: "group-communications.send-sms.has-not-phones".localized,
                    style: .alert,
                    actions: [UIAlertAction(title: "Close".localized,
                                            style: .cancel,
                                            handler: nil)]
                )
                return
            }
            communicationHelper.sendMessage(to: phones)
        case .email:
            let emails = selectedContacts
                .flatMap({ $0.mainEmail?.email })
            guard !emails.isEmpty else {
                router.showAlertController(
                    title: "group-communications.send-email.failure".localized,
                    message: "group-communications.send-email.has-not-emailes".localized,
                    style: .alert,
                    actions: [UIAlertAction(title: "Close".localized,
                                            style: .cancel, 
                                            handler: nil)]
                )
                return
            }
            try? communicationHelper.sendEmail(to: emails)
        }
    }
    
    private func flatAndPopulate(with controller: NSFetchedResultsController<Contacts>) {
        
    }
}


extension GroupCommunicationsViewModel: ContactsSelectionListDelegate {
    /// Вызывается при нажатии на Готово
    func selectionListDidFinish(_ vm: ContactsSelectionListViewModel, with selectedContacts: [Contacts]) {
        
    }
    
    func selectionListDidUpdateSelectedContacts(_ vm: ContactsSelectionListViewModel, contacts: [Contacts]) {
        selectedContacts = contacts
    }
}


/// Инициализирует вьюМОдель для работы с выбором контактов для отправки группового сообщения
///
/// - Parameters:
///   - action: Тип сообщения
///   - group: Группа
///   - settings: Насйтройки приложения
///   - router: Роутер
///   - context: Контекст Кор даты
///   - partialFormatter: Обработчик номеров
/// - Returns: ВЬюМодель
private func createSelectionViewModel(for action: GroupCommunicationsViewModel.Action,
                                      group: Groups,
                                      settings: Settings,
                                      router: ContactsListRouter,
                                      in context: NSManagedObjectContext,
                                      partialFormatter: PartialFormatter,
                                      availabilityManager: AvailabilityManager) -> ContactsSelectionListViewModel
{
    let resultsController: NSFetchedResultsController<Contacts> = CoreDataManager.instance
        .fetchedResultsControllerTyped(
            entityName: "Contacts",
            sortingDescriptors: settings.contactsSortingDescriptors(),
            cacheName: nil,
            managedContext: context,
            sectionNameKeyPath: "sectionGroupingKey"
    )
    
    let contactFilterPredicateString: String
    switch action {
    case .email:
        contactFilterPredicateString = "emails.@count != 0"
    case .sms:
        contactFilterPredicateString = "phones.@count != 0"
    }
    
    let predicate = NSPredicate(format: "(%@ IN groups) && \(contactFilterPredicateString)", group)
    resultsController.fetchRequest.predicate = predicate
    
    try? resultsController.performFetch()
    let allContacts = resultsController.fetchedObjects ?? []
    
    // Получаю список контактов по 1 на каждые контактные данные пользователя
    flatContacts(
        allContacts,
        action: action,
        in: context
    )
    
    try? resultsController.performFetch()
    // Выделяем контакты, которые удовлетворяют запросу
    let selectedContacts = resultsController.fetchedObjects ?? []
    
    let config = ContactsListViewModelConfiguration(
        fetchedController: resultsController,
        filteringPredicate: predicate,
        isSearchAvailable: false
    )
    let contactsListViewModel = ContactsSelectionListViewModel(
        configuration: config,
        settings: settings,
        router: router,
        communicationHelper: CommunicationHelper(presenter: nil),
        partialFormatter: partialFormatter,
        availabilityManager: availabilityManager
    )
    contactsListViewModel.selectedContacts = selectedContacts
    contactsListViewModel.isContactsListEditable = false
    contactsListViewModel.setSelectionEnabled(true, animated: false)
    
    switch action {
    case .sms:
        contactsListViewModel.extraInfoType = .mainPhone
    case .email:
        contactsListViewModel.extraInfoType = .mainEmail
    }
    
    return contactsListViewModel
}


/// Расправляет контакты для разделения по типам контактных данных
///
/// Создает несколько контактов для моделей с несколькими контактными данными
///
/// - Parameters:
///   - contacts: Список оригинальных контактов
///   - action: Тип контактных данных
///   - context: Контекст кор даты
private func flatContacts(_ contacts: [Contacts], action: GroupCommunicationsViewModel.Action, in context: NSManagedObjectContext) {
    for contact in contacts {
        let entityName = contact.entity.name ?? ""
        
        switch action {
        case .sms:
            let phones = contact.phones?.allObjects as? [Phones] ?? []
            guard phones.count > 1 else {
                continue
            }
            for index in 1..<phones.count {
                let phone = phones[index]
                let duplicate = NSEntityDescription.insertNewObject(
                    forEntityName: entityName,
                    into: context
                ) as! Contacts
                let attributes = contact.entity.attributesByName
                for (key, _) in attributes {
                    let value = contact.value(forKey: key)
                    duplicate.setValue(value, forKey: key)
                }
                
                duplicate.groups = contact.groups
                duplicate.phones = NSSet(object: phone)
                contact.removeFromPhones(phone)
            }
        case .email:
            let emails = contact.emails?.allObjects as? [Emails] ?? []
            guard emails.count > 1 else {
                continue
            }
            for index in 1..<emails.count {
                let email = emails[index]
                let duplicate = NSEntityDescription.insertNewObject(
                    forEntityName: entityName,
                    into: context
                    ) as! Contacts
                let attributes = contact.entity.attributesByName
                for (key, _) in attributes {
                    let value = contact.value(forKey: key)
                    duplicate.setValue(value, forKey: key)
                }
                
                duplicate.groups = contact.groups
                duplicate.emails = NSSet(object: email)
                contact.removeFromEmails(email)
            }
        }
    }
}
