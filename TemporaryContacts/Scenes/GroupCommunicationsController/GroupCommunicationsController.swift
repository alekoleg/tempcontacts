//
//  GroupCommunicationsController.swift
//  TemporaryContacts
//
//  Created by Konshin on 25.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

/// Экран отправки группового смс/емейл
class GroupCommunicationsController: UIViewController {
    fileprivate let viewModel: GroupCommunicationsViewModel
    
    @IBOutlet private var topView: UIView!
    @IBOutlet private var titleLabel: UILabel!
    
    init(viewModel: GroupCommunicationsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "GroupCommunicationsController", bundle: nil)
        viewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - properties
    
    private(set) fileprivate lazy var sendItem: UIBarButtonItem = {
        return UIBarButtonItem(
            image: #imageLiteral(resourceName: "group_communication_send").withRenderingMode(.alwaysOriginal),
            style: .plain,
            target: self,
            action: #selector(send)
        )
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tp_makeGreenBackButton()

        titleLabel.text = viewModel.title
        let vc = viewModel.contactsController()
        addChild(vc)
        view.addSubview(vc.view)
        vc.view.snp.remakeConstraints() { maker in
            maker.left.bottom.right.equalToSuperview()
            maker.top.equalTo(topView.snp.bottom)
        }
        
        sendItem.isEnabled = viewModel.isSendingAvailable
        navigationItem.rightBarButtonItem = sendItem
        
        let imageView = UIImageView(image: viewModel.headerImage)
        imageView.sizeToFit()
        navigationItem.titleView = imageView
    }
    
    // MARK: - actions
    
    @objc private func send() {
        viewModel.send()
    }
}


extension GroupCommunicationsController: GroupCommunicationsViewModelDelegate {
    func vmSetSendButtonAvailable(_ available: Bool) {
        sendItem.isEnabled = viewModel.isSendingAvailable
    }
}
