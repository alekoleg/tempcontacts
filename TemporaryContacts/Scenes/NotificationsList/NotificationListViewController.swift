//
//  NotificationListViewController.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 21/05/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

class NotificationListViewController: UIViewController {
    
    fileprivate let tableView = UITableView()
    /// Лейбла, отображающаяся при отсутствии данных
    fileprivate let noDataLabel = UILabel()

	let viewModel:NotificationListViewModel

	init(vm:NotificationListViewModel) {
		self.viewModel = vm
		super.init(nibName: nil, bundle: nil)
		initTabBar()
        vm.delegate = self
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "new.notification.title".localized
        let imageView = UIImageView(image: #imageLiteral(resourceName: "notifications_list_title"))
        imageView.sizeToFit()
        navigationItem.titleView = imageView
        
        setupTable()
        setupNoDataLabel()

		self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"Add"),
		                                                         style: .plain,
		                                                         target: self,
		                                                         action: #selector(NotificationListViewController.createNew))
        
        viewModel.startWork()
        dataDidUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.screenDidShow()
    }
    
    // MARK: - actions

	@objc func createNew() {
		viewModel.createNew()
	}
    
    // MARK: - private
    
    /// Отображаемые даннные обновились
    fileprivate func dataDidUpdate() {
        let showNoData = viewModel.showNoDataLabel
        noDataLabel.isHidden = !showNoData
        viewModel.dataDidUpdate()
    }
    
    private func setupTable() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.keyboardDismissMode = .onDrag
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor(hex: 0xF0F0F8)
        tableView.contentInset.top = 18
        tableView.contentInset.bottom = 18
        tableView.contentOffset.y = -tableView.contentInset.top
        
        tableView.register(NotificationsListCell.self, forCellReuseIdentifier: reuseIdentifier)
        view.addSubview(tableView)
        tableView.snp.remakeConstraints() { maker in
            maker.edges.equalToSuperview()
        }
    }
    
    private func setupNoDataLabel() {
        noDataLabel.textColor = UIColor(hex: 0xC7C7CD)
        noDataLabel.numberOfLines = 0
        noDataLabel.textAlignment = .center
        noDataLabel.font = UIFont.systemFont(ofSize: 19)
        noDataLabel.text = "new.notification.no-date".localized
        tableView.backgroundView = noDataLabel
    }

	private func initTabBar() {
		tabBarItem.title = "notifications-list.title".localized
		tabBarItem.image = #imageLiteral(resourceName: "Alarm")
	}

}

private let reuseIdentifier = #file

// MARK: - UITableViewDataSource
extension NotificationListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let number = viewModel.numberOfItems(at: section)
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        if let nCell = cell as? NotificationsListCell {
            let item = viewModel.item(at: indexPath)
            nCell.configure(with: item)
            nCell.cellDelegate = self
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension NotificationListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelect(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel.didList(indexPath: indexPath)
    }
}

// MARK: - NotificationListViewModelDelegate
extension NotificationListViewController: NotificationListViewModelDelegate {
    
    func vmReloadData() {
        tableView.reloadData()
        dataDidUpdate()
    }

	func vmShowActionAlert(title: String?, message: String?, actions: [UIAlertAction]) {
		let vc = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        vc.view.tintColor = ColorHelper.AppDarkTextColor()
		for action in actions {
			vc.addAction(action)
		}
		self.present(vc, animated: true, completion: nil)
	}
    
    func updatesConvertorHasReceivedfUpdates(_ convertor: FetchedResultsUpdatesConverter, updates: [FetchedResultUpdate]) {
        
        if updates.count > 5 {
            tableView.reloadData()
        } else {
            tableView.beginUpdates()
            for update in updates {
                tableView.applyFetchedResultsUpdate(update, with: .fade)
            }
            tableView.endUpdates()
            
            /// Ячейки подлежат обновлению что бы соединить линии
            let extraReloadIndexPaths: Set<IndexPath> = Set(tableView.indexPathsForVisibleRows ?? [])
            for ip in extraReloadIndexPaths {
                if let cell = tableView.cellForRow(at: ip) as? NotificationsListCell {
                    cell.configure(with: viewModel.item(at: ip))
                }
            }
        }
        dataDidUpdate()
    }
    
    func updatesConvertorHasReceivedError(_ convertor: FetchedResultsUpdatesConverter, error: Error) {
        tableView.reloadData()
        dataDidUpdate()
    }
}

// MARK: - NotificationsListCellDelegate
extension NotificationListViewController: NotificationsListCellDelegate {
    func notificationsListCellDidTapToRemove(_ cell: NotificationsListCell) {
        guard let ip = tableView.indexPath(for: cell) else {
            return
        }
        
        viewModel.delete(at: ip)
    }
    
    func notificationsListCellDidTapToDate(_ cell: NotificationsListCell) {
        guard let ip = tableView.indexPath(for: cell) else {
            return
        }
        
        viewModel.showNotificationDetail(at: ip)
    }
}

// MARK: - UpdateBadgesProtocol
extension NotificationListViewController: UpdateBadgesProtocol {
    var badgeNumber: Int? {
        return viewModel.numberOfExpiredNotifications
    }
}
