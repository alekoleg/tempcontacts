//
//  NotificationsListCell.swift
//  TemporaryContacts
//
//  Created by Konshin on 21.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import MGSwipeTableCell
import SnapKit

protocol NotificationsListCellDelegate: class {
    func notificationsListCellDidTapToRemove(_ cell: NotificationsListCell)
    func notificationsListCellDidTapToDate(_ cell: NotificationsListCell)
}

/// Ячейка из списка уведомлений
class NotificationsListCell: MGSwipeTableCell {
    
    /// Вью для отображения маски
    private let cellMaskView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.orange
        return view
    }()
    /// Вью для отображения даты
    private let leftView = UIView()
    /// Лейбла с отображением даты
    fileprivate let dateLabel: UILabel = {
       let label = UILabel()
        label.textColor = ColorHelper.AppDarkTextColor()
        label.font = UIFont.appFont(size: 14, typeface: .medium)
        label.numberOfLines = 0
        return label
    }()
    /// Лейбла с отображением времени
    fileprivate let timeLabel: UILabel = {
        let label = UILabel()
        label.textColor = ColorHelper.AppDarkTextColor()
        label.font = UIFont.appFont(size: 12, typeface: .heavy)
        label.layer.cornerRadius = 12
        label.layer.masksToBounds = true
        label.textAlignment = .center
        label.backgroundColor = UIColor(hex: 0xE3E3EE)
        return label
    }()
    /// Лейбла для имени
    fileprivate let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = ColorHelper.AppDarkTextColor()
        label.font = UIFont.appFont(size: 16, typeface: .heavy)
        label.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .vertical)
        return label
    }()
    /// Лейбла дополнительной инфомрации под именем
    fileprivate let subNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = ColorHelper.AppDarkTextColor()
        label.font = UIFont.appFont(size: 14, typeface: .light)
        label.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .vertical)
        return label
    }()
    /// Лейбла для уведомления
    fileprivate let notificationTextLabel: UILabel = {
        let label = UILabel()
        label.textColor = ColorHelper.AppDarkTextColor()
        label.font = UIFont.appFont(size: 11, typeface: .medium)
        label.numberOfLines = 0
        return label
    }()
    /// вью - точечка
    fileprivate let lineDot: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        view.layer.cornerRadius = 10
        
        let dot = UIView()
        dot.backgroundColor = ColorHelper.AppDarkTextColor()
        dot.layer.cornerRadius = 5
        dot.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .vertical)
        dot.layer.zPosition = 1000
        view.addSubview(dot)
        dot.snp.remakeConstraints { maker in
            maker.width.height.equalTo(10)
            maker.center.equalToSuperview()
        }
        
        return view
    }()
    /// вью - линия выше вью точки
    fileprivate let topLine: UIView = {
        let view = UIView()
        view.backgroundColor = ColorHelper.AppDarkTextColor()
        return view
    }()
    /// вью - линия ниже вью точки
    fileprivate let bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = ColorHelper.AppDarkTextColor()
        return view
    }()
    
    fileprivate var subNameLablVisibleConstraint: Constraint?
    
    private let deleteButton = MGSwipeButton(
        title: "",
        icon: #imageLiteral(resourceName: "BlueDelete"),
        backgroundColor: ColorHelper.AppRedColor(),
        insets: UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 40)
    )
    
    var cellDelegate: NotificationsListCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - actions
    
    func setup() {
        
        deleteButton.callback = { [unowned self] _ in
            self.cellDelegate?.notificationsListCellDidTapToRemove(self)
            return true
        }
        rightButtons = [deleteButton]
        
        layer.addSublayer(maskLayer)
        
        leftView.addSubview(dateLabel)
        dateLabel.snp.remakeConstraints { maker in
            maker.left.equalToSuperview().inset(22)
            maker.right.equalToSuperview().inset(12)
            maker.top.equalToSuperview().offset(7)
        }
        leftView.addSubview(timeLabel)
        timeLabel.snp.remakeConstraints { maker in
            maker.left.equalTo(dateLabel)
            maker.top.equalTo(dateLabel.snp.bottom).offset(7)
            maker.width.equalTo(50)
            maker.height.equalTo(22)
        }
        let tapRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(tapToLeftHalf)
        )
        leftView.addGestureRecognizer(tapRecognizer)
        contentView.addSubview(leftView)
        leftView.snp.remakeConstraints { maker in
            maker.left.equalToSuperview().offset(17)
            maker.top.bottom.equalToSuperview().inset(6)
            maker.width.equalTo(120)
        }
        
        contentView.addSubview(nameLabel)
        nameLabel.snp.remakeConstraints { maker in
            maker.left.equalTo(leftView.snp.right)
            maker.top.equalToSuperview().offset(13)
            maker.right.equalToSuperview().offset(-22)
        }
        
        contentView.addSubview(subNameLabel)
        subNameLabel.snp.remakeConstraints { maker in
            maker.left.right.equalTo(nameLabel)
            maker.top.equalTo(nameLabel.snp.bottom)
        }
        
        contentView.addSubview(notificationTextLabel)
        notificationTextLabel.snp.remakeConstraints { maker in
            maker.top.equalTo(nameLabel.snp.bottom).offset(4).priority(750)
            subNameLablVisibleConstraint = maker.top.equalTo(subNameLabel.snp.bottom).offset(2).constraint
            maker.left.right.equalTo(nameLabel)
            maker.bottom.lessThanOrEqualToSuperview().offset(-8)
        }
        
        addSubview(lineDot)
        lineDot.snp.remakeConstraints { maker in
            maker.top.equalToSuperview().offset(15)
            maker.width.height.equalTo(20)
            maker.left.equalToSuperview().offset(8)
        }
        addSubview(topLine)
        topLine.snp.remakeConstraints { maker in
            maker.top.equalToSuperview()
            maker.bottom.equalTo(lineDot.snp.top)
            maker.width.equalTo(1)
            maker.centerX.equalTo(lineDot)
        }
        addSubview(bottomLine)
        bottomLine.snp.remakeConstraints { maker in
            maker.top.equalTo(lineDot.snp.bottom)
            maker.bottom.equalToSuperview()
            maker.width.equalTo(1)
            maker.centerX.equalTo(lineDot)
        }
    }
    
    private var lastSize: CGSize?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if bounds.size != lastSize {
            updateMask()
            lastSize = bounds.size
        }
    }
    
    /// Особый леер для маски, который не отображается на скриншоте (для MGSwipeTableCell)
    private let maskLayer = MaskLayer()
    
    ///Обновляет маску
    private func updateMask() {
        let layer = maskLayer
        layer.fillColor = UIColor(hex: 0xF0F0F8).cgColor
        let bounds = self.bounds
        let path = CGMutablePath()
        var rect = bounds.insetBy(dx: 10, dy: 6)
        let diff = 17 - rect.origin.x
        rect.origin.x = 17
        rect.size.width -= diff
        
        path.addRect(bounds)
        path.addRoundedRect(in: rect, cornerWidth: 8, cornerHeight: 8)
        
        layer.path = path
        layer.fillRule = CAShapeLayerFillRule.evenOdd
    }

    @IBAction fileprivate func tapToLeftHalf() {
        cellDelegate?.notificationsListCellDidTapToDate(self)
    }
}

extension NotificationsListCell {
    /// Режим отображения линий
    enum LineMode {
        case none
        case toBottom
        case toTop
        case fill
    }
    
    struct Item {
        let date: String?
        let time: String?
        let name: String?
        let subNameText: String?
        let text: String?
        let dateIsExpired: Bool,
        lineMode: LineMode
    }
    
    func configure(with item: Item) {
        dateLabel.text = item.date
        timeLabel.text = item.time
        nameLabel.text = item.name
        subNameLabel.text = item.subNameText
        notificationTextLabel.text = item.text
        
        if subNameLabel.text?.isEmpty == false {
            subNameLablVisibleConstraint?.activate()
        } else {
            subNameLablVisibleConstraint?.deactivate()
        }
        
        if item.dateIsExpired {
            contentView.backgroundColor = UIColor.white
            lineDot.backgroundColor = UIColor.white
        } else {
            contentView.backgroundColor = UIColor(hex: 0xD8D7EC)
            lineDot.backgroundColor = UIColor(hex: 0xD8D7EC)
        }
        setLineMode(item.lineMode)
    }
    
    /// Задает режим отображения линий
    func setLineMode(_ mode: LineMode) {
        switch mode {
        case .fill:
            topLine.isHidden = false
            bottomLine.isHidden = false
        case .none:
            topLine.isHidden = true
            bottomLine.isHidden = true
        case .toBottom:
            topLine.isHidden = true
            bottomLine.isHidden = false
        case .toTop:
            topLine.isHidden = false
            bottomLine.isHidden = true
        }
    }
}

private class MaskLayer: CAShapeLayer {
    override func render(in ctx: CGContext) {
        //SNapshoting is diabled
    }
}
