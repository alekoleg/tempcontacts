//
//  NotificationListViewModel.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 21/05/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import CoreData
import Contacts
import RxSwift

protocol NotificationListViewModelDelegate: FetchedResultsUpdatesConverterDelegate, UserNotificationProtocol {
    func vmReloadData()
    func vmShowActionAlert(title: String?, message: String?, actions: [UIAlertAction])
}

class NotificationListViewModel {
    
    enum Action {
        case createNew
    }

	fileprivate var router: NotificationListRouter
    /// Менеджер возможностей
    fileprivate let availabilityManager: AvailabilityManager
    fileprivate let fetchedController: NSFetchedResultsController<AppNotification>
    /// Конвертатор обновления базы уведомлений
    fileprivate let updateConvertor = FetchedResultsUpdatesConverter()
	private let settings: Settings
    fileprivate let initializationAction: Action?
    
    weak var delegate: NotificationListViewModelDelegate? {
        didSet {
            updateConvertor.delegate = delegate
        }
    }
    
    init(fetchedController: NSFetchedResultsController<AppNotification>,
         initializationAction: Action?,
         router: NotificationListRouter,
         availabilityManager: AvailabilityManager,
         settings: Settings) {
        self.availabilityManager = availabilityManager
        self.fetchedController = fetchedController
        self.router = router
		self.settings = settings
        self.initializationAction = initializationAction
        fetchedController.delegate = updateConvertor
        
        updateData()
        setupObservers()
    }
    
    // MARK: - getters
    
    var showNoDataLabel: Bool {
        for i in 0..<numberOfSections {
            let numberOfRows = numberOfItems(at: i)
            if numberOfRows > 0 {
                return false
            }
        }
        
        return true
    }
    
    var numberOfSections: Int {
        return fetchedController.sections?.count ?? 0
    }
    
    func numberOfItems(at section: Int) -> Int {
        guard let sections = fetchedController.sections, sections.count > section else {
            return 0
        }
        
        return sections[section].numberOfObjects
    }
    
    func item(at indexPath: IndexPath) -> NotificationsListCell.Item {
        let notification = fetchedController.object(at: indexPath)
        let date = notification.date.flatMap(dateFormatter.string)
        let time = notification.date.flatMap(timeFormatter.string)
        let name: String?
        let subNameText: String?
        if let contact = notification.contact, let displayName = contact.displayName {
            name = displayName
            subNameText = contact.displaySpecialization
        } else if let group = notification.group, let groupName = group.name {
            name = groupName
            subNameText = nil
        } else {
            name = nil
            subNameText = nil
        }
        let text = notification.text
        let section = indexPath.section
        return NotificationsListCell.Item(
            date: date,
            time: time,
            name: name,
            subNameText: subNameText,
            text: text,
            dateIsExpired: notification.isExpired,
            lineMode: {
                switch indexPath.row {
                case 0 where numberOfItems(at: section) == 1:
                    return .none
                case 0:
                    return .toBottom
                case let number where number == (numberOfItems(at: section) - 1):
                    return .toTop
                default:
                    return .fill
                }
        }()
        )
    }
    
    /// Количество уведомлений из прошлого
    var numberOfExpiredNotifications: Int {
        let notifications = fetchedController.fetchedObjects ?? []
        return notifications
            .filter { $0.isExpired }
            .count
    }
    
    // MARK: - actions
    
    /// Инициализирует работу вью модели
    func startWork() {
        switch initializationAction {
        case .createNew?:
            createNew()
        default:
            break
        }
    }
    
    func dataDidUpdate() {
        NotificationCenter.default.post(name: .rootBarUpdateBadges, object: nil)
    }
    
    /// Обработать событие - экран уведомлений отобразился
    func screenDidShow() {
        Settings.instance.removeNewNotificationsInfo()
    }
    
    func updateData() {
		do {
            try fetchedController.performFetch()
        } catch {
            print("Ошибка получения записей уведомлений")
        }
        delegate?.vmReloadData()
    }
    
    func didSelect(at indexPath: IndexPath) {
        let notification = fetchedController.object(at: indexPath)
        if let contact = notification.contact {
            try? router.showContactInfo(contact: contact, editable: true, delegate: self)
        } else if let group = notification.group {
            router.showGroupDetail(group)
        }
    }
    
    func delete(at indexPath: IndexPath) {
        let notification = fetchedController.object(at: indexPath)
        
        let title = "DeleteNotificationTitle".localized
        let message = "DeleteNotificationMessage".localized

        let editAction = UIAlertAction(title: "Delete".localized, style: .destructive) { (action) in
            if let context = notification.managedObjectContext {
                context.delete(notification)
                CoreDataManager.instance.saveContext(context)
            }
        }
        let cancel = UIAlertAction(title: "Close".localized, style: UIAlertAction.Style.cancel, handler: nil)
        delegate?.showAlert(
            title: title,
            message: message,
            actions: [editAction, cancel]
        )
    }
    
    func createNew() {
        guard availabilityManager.canAddNewElement(.notifications) else {
            delegate?.showLimitationAlert(for: .notifications)
            return
        }
        
        let pickGroupAction = UIAlertAction(
            title: "new.notification.from.group".localized,
            style: .default
        ) { [unowned self] (_) in
            
            self.router.showSelectPicker(selection: .Groups,
                                         moveToTemp: false,
                                         associatedContact: nil,
                                         context: nil,
                                         delegate: self)
        }
        let contactAction = UIAlertAction(
            title:"new.notification.from.contact".localized,
            style: .default
        ) { [unowned self] (_) in
            
            let resultsController: NSFetchedResultsController<Contacts> = CoreDataManager.instance
                .fetchedResultsControllerTyped(
                    entityName: "Contacts",
                    sortingDescriptors: self.settings.contactsSortingDescriptors(),
                    cacheName: nil,
                    managedContext: CoreDataManager.instance.managedObjectContext,
                    sectionNameKeyPath: "sectionGroupingKey"
            )
            
            self.router.showContactsSelector(
                selectedContacts: [],
                fetchedController: resultsController,
                searchAvailable: true,
                filteringPredicate: nil,
                delegate: self
            )
            
        }
        let cancelAction = UIAlertAction(
            title: "Cancel".localized,
            style: .destructive,
            handler: nil
        )

		self.delegate?.vmShowActionAlert(
            title: "new.notification.from.title".localized,
            message: "new.notification.from.message".localized,
            actions: [pickGroupAction, contactAction, cancelAction]
        )
	}
    
    func showNotificationDetail(at indexPath: IndexPath) {
        let notification = fetchedController.object(at: indexPath)
        let owner: EditNotificationViewController.Owner
        if let c = notification.contact {
            owner = .contact(c)
        } else if let g = notification.group {
            owner = .group(g)
        } else {
            return
        }
        
        router.showNotificationController(
            notification: notification,
            owner: owner,
            context: notification.managedObjectContext,
            delegate: self
        )
    }
    
    func didList(indexPath: IndexPath) {
        let notification = fetchedController.object(at: indexPath)
        if notification.isExpired && notification.viewDate == nil {
            notification.viewDate = NSDate() as Date
            setNeedsSaveContext()
        }
    }
    
    // MARK: - private
    
    private func setupObservers() {
        NotificationCenter.default.addObserver(
            forName: .didReceiveLocalNotification,
            object: nil,
            queue: nil
        ) { [weak self] _ in
            self?.updateData()
        }
    }
    
    /// Форматтер даты
    private let dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "d MMMM yyyy"
        return df
    }()
    /// Форматтер времени
    private let timeFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "HH:mm"
        return df
    }()
    
    private var saveDispose: DisposeBag?
    
    private func setNeedsSaveContext() {
        saveDispose = nil
        
        let dispose = DisposeBag()
        Observable<Int>.timer(1, scheduler: MainScheduler.instance)
            .subscribe(
                onNext: { [weak self] _ in
                    self?.saveContext()
            })
            .disposed(by: dispose)
        saveDispose = dispose
    }
    
    private func saveContext() {
        CoreDataManager.instance.saveContext(fetchedController.managedObjectContext)
    }
}

extension NotificationListViewModel: ContactsSelectionListDelegate {

	/// Обновлены выбранные контакты
	func selectionListDidUpdateSelectedContacts(_ vm: ContactsSelectionListViewModel, contacts: [Contacts]) {
		if let contact = contacts.first {
			self.router.popCurrentViewController()
			router.showNotificationController(
				notification: nil,
				owner: .contact(contact),
				context: contact.managedObjectContext,
				delegate: self
			)

		}
	}

	/// Вызывается при нажатии на Готово
	func selectionListDidFinish(_ vm: ContactsSelectionListViewModel, with selectedContacts: [Contacts]) {

	}

	/// Уведомляет об изменении в данные для отображения
	func selectionListDidUpdateItems(_ vm: ContactsSelectionListViewModel) {

	}
}

// MARK: - SelectpickerViewControllerDelegate
extension NotificationListViewModel: SelectpickerViewControllerDelegate {
	func selectPickerControllerDidSelectGroup(_ controller: SelectpickerViewController, selectedData: [NSFetchRequestResult]) {
		router.dismissController(controller)
		guard let groups = selectedData as? [Groups] else {
			return
		}
		if let group = groups.first {
			router.showNotificationController(
				notification: nil,
				owner: .group(group),
				context: group.managedObjectContext!,
				delegate: self
			)

		}
	}
}

// MARK: - EditNotificationViewControllerDelegate
extension NotificationListViewModel: EditNotificationViewControllerDelegate {
	func editNotificationControllerDidComplete(_ controller: EditNotificationViewController, with notificaion: AppNotification?) {
		router.dismissController(controller)
        
        switch controller.owner {
        case .contact(let contact)?:
            if let originalNotification = controller.originalNotification {
                if notificaion == nil {
                    
                }
                if let newNotification = notificaion {
                    contact.removeFromNotifications(originalNotification)
                    contact.addToNotifications(newNotification)
                } else {
                    // Значит, уведомление было удалено
                    originalNotification.contact = nil
                    originalNotification.managedObjectContext?.delete(originalNotification)
                }
            } else if let notification = notificaion {
                contact.addToNotifications(notification)
            } else {
                return
            }

            CoreDataManager.instance.saveContext(contact.managedObjectContext)
        case .group(let group)?:
            if let originalNotification = controller.originalNotification {
                if notificaion == nil {
                    // Значит, уведомление былоу удалено
                    originalNotification.group = nil
                    originalNotification.managedObjectContext?.delete(originalNotification)
                }
            } else if let notification = notificaion {
                group.addToNotifications(notification)
            } else {
                return
            }
            CoreDataManager.instance.saveContext(group.managedObjectContext)
        default:
            return
        }
	}
}

// MARK: - ContactConverterDelegate
extension NotificationListViewModel: ContactConverterDelegate {
    func contactConverterDidCompleteWithContact(converter: ContactControllerDelegateConverter, contact: CNContact?) {
        guard let contact = contact else {
            return
        }
        switch converter.type {
        case .creation:
            ContactsManager.instance.addNewContactToDB(contact)
            Metrica.instance.send(.commonContactIsCreated)
        case .editing:
            ContactsManager.instance.updateContactInDB(contact)
        }
    }
}
