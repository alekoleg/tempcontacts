//
//  NotificationListRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

protocol NotificationListRouter: OpenDetailRouter, SelectPickerRouter, ContactsSelectionListRouter, EditNotificationRouter, RemovingScreensRouter {
    
}
