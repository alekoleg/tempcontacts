//
//  BusinessCardsListController.swift
//  TemporaryContacts
//
//  Created by Konshin on 20.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import RxSwift
import XMSegmentedControl

private let reuseIdentifier = #file

/// Экран список с визитками
class BusinessCardsListController: UIViewController {
    
    fileprivate let viewModel: BusinessCardsListViewModel
    
    /// Контейнер вьюшек переключения типа сортировки
    fileprivate let sortingContainerView = UIView()
    /// Контрол переключения сортировок
    fileprivate let sortingSegmentedControl = TCSegmentedControll()
    /// Таблицы
    fileprivate let tableView = UITableView()
    /// Лейбла, отображающаяся при отсутствии данных
    fileprivate let noDataLabel = UILabel()
    /// Серчбар
    private let searchBar = CustomSearchBar(frame: .zero)
    /// Диспоуз
    fileprivate let disposeBag = DisposeBag()
    
    init(vm: BusinessCardsListViewModel) {
        viewModel = vm
        super.init(nibName: nil, bundle: nil)
        vm.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = viewModel.screenName

        vmReloadBar()

        setupSortingView()
        setupSeacrhBar()
        setupTableView()
        setupNoDataLabel()
        
        dataDidUpdate()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.reloadData()
    }
    
    // MARK: - getters
    
    /// Возвращает сигнал с заполненным вью от вьюконтроллера визиток
    fileprivate func businessControllerView(at indexPath: IndexPath) -> Observable<UIView> {
        return viewModel.businesCardsViewModel(at: indexPath)
            .subscribeOn(ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global()))
            .observeOn(MainScheduler.instance)
            .map { [unowned self] vm in
                let vc = BusinessCardsCollectionController(viewModel: vm)
                self.addChild(vc)
                return vc.view
        }
    }
    
    // MARK: - actions

    private func setupSortingView() {
        view.addSubview(sortingContainerView)
        sortingContainerView.backgroundColor = ColorHelper.AppTintColor()
        sortingContainerView.snp.remakeConstraints { maker in
            maker.top.left.right.equalToSuperview()
            maker.height.equalTo(41)
        }
        
        let sortingLabel = UILabel()
        sortingLabel.text = "businnes-cards.list.sorting".localized
        sortingLabel.font = UIFont.appFont(size: 15)
        sortingLabel.textColor = UIColor(hex: 0xA3A2BD)
        sortingContainerView.addSubview(sortingLabel)
        sortingLabel.snp.remakeConstraints { maker in
            maker.left.equalToSuperview().offset(26)
            maker.centerY.equalToSuperview()
        }
        
        let sortingArrow = UIImageView(image: #imageLiteral(resourceName: "business_cards_list_sorting_arrow"))
        sortingContainerView.addSubview(sortingArrow)
        sortingArrow.snp.remakeConstraints { maker in
            maker.centerY.equalTo(sortingLabel)
            maker.left.equalTo(sortingLabel.snp.right).offset(8)
        }
        
        sortingSegmentedControl.segmentTitle = viewModel.sortingSegmentTitles
        sortingSegmentedControl.delegate = self
        sortingSegmentedControl.font = UIFont.appFont(size: 15, typeface: .light)
        sortingSegmentedControl.selectedFont = UIFont.appFont(size: 15, typeface: .medium)
        sortingSegmentedControl.selectedItemHighlightStyle = .bottomEdge
        sortingSegmentedControl.backgroundColor = ColorHelper.AppTintColor()
        sortingSegmentedControl.highlightColor = ColorHelper.AppGreenColor()
        sortingSegmentedControl.tint = UIColor.white.withAlphaComponent(0.6)
        sortingSegmentedControl.highlightTint = UIColor.white
        sortingSegmentedControl.selectedSegment = viewModel.sortingSelectedIndex
        sortingContainerView.addSubview(sortingSegmentedControl)
        sortingSegmentedControl.snp.remakeConstraints { maker in
            maker.top.right.bottom.equalToSuperview()
            maker.left.equalTo(sortingArrow.snp.right).offset(30)
        }
    }
    
    private func setupSeacrhBar() {
        searchBar.backgroundColor = UIColor.white
        searchBar.placeholder = "search".localized
        searchBar.font = UIFont.appFont(size: 18, typeface: .light)
        searchBar.delegate = self
        view.addSubview(searchBar)
        searchBar.snp.remakeConstraints() { maker in
            maker.top.equalTo(sortingContainerView.snp.bottom)
            maker.left.right.equalToSuperview()
            maker.height.equalTo(60)
        }
    }
    
    private func setupTableView() {
        tableView.register(BusienssCardsListCell.self, forCellReuseIdentifier: reuseIdentifier)
        
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.keyboardDismissMode = .onDrag
        tableView.rowHeight = 44
        tableView.estimatedRowHeight = 44
        view.addSubview(tableView)
        tableView.snp.remakeConstraints() { maker in
            maker.left.bottom.right.equalToSuperview()
            maker.top.equalTo(searchBar.snp.bottom)
        }
    }
    
    private func setupNoDataLabel() {
        noDataLabel.textColor = UIColor(hex: 0xC7C7CD)
        noDataLabel.numberOfLines = 0
        noDataLabel.textAlignment = .center
        noDataLabel.font = UIFont.systemFont(ofSize: 19)
        noDataLabel.text = "businnes-cards.list.no-data".localized
    }
    
    /// Отображаемые даннные обновились
    fileprivate func dataDidUpdate() {
        let showNoData = viewModel.showNoDataLabel
        noDataLabel.isHidden = !showNoData
    }
    
    @objc fileprivate func tapToRightItem(barItem: UIBarButtonItem) {
        viewModel.changeDisplayType()
    }
}

extension BusinessCardsListController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

// MARK: - UITableViewDataSource
extension BusinessCardsListController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems(at: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? BusienssCardsListCell else {
            return UITableViewCell()
        }
        
        let item = self.viewModel.item(at: indexPath)
        cell.configure(with: item)
        cell.bcDelegate = self
        
        businessControllerView(at: indexPath)
            .observeOn(MainScheduler.instance)
            .takeUntil(cell.rx.methodInvoked(#selector(BusienssCardsListCell.prepareForReuse)))
            .subscribe(
                onNext: { view in
                    cell.setLeftView(view)
            })
            .disposed(by: disposeBag)
        
        cell.selectionStyle = .none
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension BusinessCardsListController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = viewModel.item(at: indexPath)
        let height = BusienssCardsListCell.necessaryHeight(item: item, tableWidth: tableView.bounds.width)
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelect(at: indexPath)
    }
}

// MARK: - BusienssCardsListCellDelegate
extension BusinessCardsListController: BusienssCardsListCellDelegate {
    func businessCardsListCellDidTapToDelete(_ cell: BusienssCardsListCell) {
        guard let ip = tableView.indexPath(for: cell) else {
            return
        }
        
        viewModel.delete(at: ip)
    }
    
    func businessCardsDidTapToContext(_ cell: BusienssCardsListCell, contextButton: UIButton) {
        guard let ip = tableView.indexPath(for: cell) else {
            return
        }
        
        let vc = DataSelectorController(items: viewModel.contextMenuItems(at: ip))
        vc.tableView.bounces = false
        vc.rowHeight = 40
        vc.modalPresentationStyle = .popover
        vc.popoverPresentationController?.sourceView = contextButton.superview
        vc.popoverPresentationController?.sourceRect = contextButton.frame
        vc.popoverPresentationController?.permittedArrowDirections = .up
        vc.popoverPresentationController?.delegate = self
        vc.preferredContentSize = CGSize(width: 200, height: vc.contentHeight)
        present(vc, animated: true, completion: nil)
    }
}

// MARK: - BusinessCardsListViewModelDelegate
extension BusinessCardsListController: BusinessCardsListViewModelDelegate {
    
    func vmReloadData(animated: Bool) {
        if animated && tableView.numberOfSections > 0 {
            tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        } else {
            tableView.reloadData()
        }
        dataDidUpdate()
    }
    
    func vmReloadBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: viewModel.modeIcon,
            style: .plain,
            target: self,
            action: #selector(tapToRightItem)
        )
    }
    
    func updatesConvertorHasReceivedfUpdates(_ convertor: FetchedResultsUpdatesConverter, updates: [FetchedResultUpdate]) {
        guard isViewLoaded && view.window != nil else {
            // Обновимся при viewWillAppear
            return
        }
        
        tableView.layer.removeAllAnimations()
        
        if updates.count > 5 {
            tableView.reloadData()
        } else {
            tableView.beginUpdates()
            let tableViewHasElements = tableView.numberOfSections > 0 && tableView.numberOfRows(inSection: 0) > 0
            for update in updates {
                switch update {
                case .delete, .update, .move:
                    if !tableViewHasElements {
                        tableView.reloadData()
                        dataDidUpdate()
                        return
                    }
                default:
                    break
                }
                tableView.applyFetchedResultsUpdate(update, with: .fade)
            }
            tableView.endUpdates()
        }
        dataDidUpdate()
    }
    
    func updatesConvertorHasReceivedError(_ convertor: FetchedResultsUpdatesConverter, error: Error) {
        tableView.reloadData()
        dataDidUpdate()
    }
}

// MARK: - CustomSearchBarDelegate
extension BusinessCardsListController: CustomSearchBarDelegate {
    func customSearchTextDidChanged(_ searchBar: CustomSearchBar) {
        viewModel.searchWithTerm(searchBar.text ?? "")
    }
}

// MARK: - XMSegmentedControlDelegate
extension BusinessCardsListController: XMSegmentedControlDelegate {
    
    func xmSegmentedControl(_ xmSegmentedControl: XMSegmentedControl, selectedSegment: Int) {
        viewModel.didUpdateSortingSelectedIndex(selectedSegment)
    }
}
