//
//  BusinessCardsListRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData

protocol BusinessCardsListRouter {
    /// Отображает список визиток
    func showBusinessCardsList()
}

extension Router where Self: BusinessCardsListRouter & OpenDetailRouter, RootController == UINavigationController {
    /// Отображает список визиток
    func showBusinessCardsList() {
        let req = NSFetchRequest<Contacts>(entityName: Contacts.entityName)
        req.sortDescriptors = dependencies.settings.contactsSortingDescriptors()
        let controller = NSFetchedResultsController<Contacts>(
            fetchRequest: req,
            managedObjectContext: CoreDataManager.instance.managedObjectContext,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        
        let vm = BusinessCardsListViewModel(
            controller: controller,
            settings: dependencies.settings,
            availabilityManager: dependencies.availabilityManager,
            router: self
        )
        let vc = BusinessCardsListController(vm: vm)
        rootController.pushViewController(vc, animated: true)
    }
}
