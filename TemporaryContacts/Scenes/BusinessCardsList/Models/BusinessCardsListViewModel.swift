//
//  BusinessCardsListViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 20.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import CoreData
import Contacts
import RxSwift

private enum SortingType {
    case name
    case creationDate
    
    var name: String {
        switch self {
        case .creationDate:
            return "businnes-cards.list.sorting.by-date".localized
        case .name:
            return "businnes-cards.list.sorting.by-name".localized
        }
    }
}

protocol BusinessCardsListViewModelDelegate: UserNotificationProtocol, FetchedResultsUpdatesConverterDelegate {
    func vmReloadData(animated: Bool)
    /// Обновляет навигейшен бар
    func vmReloadBar()
}

class BusinessCardsListViewModel {
    
    typealias Mode = BusinessCardsDisplayMode
    
    weak var delegate: BusinessCardsListViewModelDelegate? {
        didSet {
            updateConverter.delegate = delegate
        }
    }
    
    fileprivate let controller: NSFetchedResultsController<Contacts>
    fileprivate let settings: Settings
    fileprivate let availabilityManager: AvailabilityManager
    fileprivate var router: OpenDetailRouter
    fileprivate let updateConverter = FetchedResultsUpdatesConverter()
    fileprivate let originalPredicate = NSPredicate(format: "businessCards.@count > 0")
    
    fileprivate var businesCardsViewModels = [Contacts: BusinessCardsCollectionViewModel]()
    
    /// Выбранный тип сортировки
    fileprivate var sortingType: SortingType = .creationDate
    /// Тип отображения
    fileprivate(set) var displayMode: Mode {
        get { return Settings.instance.businessCardsDisplayMode }
        set { Settings.instance.businessCardsDisplayMode = newValue }
    }
    
    fileprivate var sortingTypes: [SortingType] = [.name, .creationDate]
    
    init(controller: NSFetchedResultsController<Contacts>,
         settings: Settings,
         availabilityManager: AvailabilityManager,
         router: OpenDetailRouter)
    {
        self.controller = controller
        self.settings = settings
        self.availabilityManager = availabilityManager
        self.router = router
        
        controller.fetchRequest.predicate = originalPredicate
        controller.delegate = updateConverter
        
        initialize()
        setSortingType(.creationDate, force: true)
    }
    
    // MARK: - getters
    
    var screenName: String? {
        return "contacts.context-menu.business_cards".localized
    }
    
    var sortingSegmentTitles: [String] {
        return sortingTypes.map { $0.name }
    }
    
    var sortingSelectedIndex: Int {
        return sortingTypes.index(of: sortingType) ?? 0
    }
    
    var modeIcon: UIImage? {
        switch displayMode {
        case .list:
            return #imageLiteral(resourceName: "business_cards_list_mode_cards").withRenderingMode(.alwaysOriginal)
        case .cards:
            return #imageLiteral(resourceName: "business_cards_list_mode_list").withRenderingMode(.alwaysOriginal)
        }
    }
    
    // MARK: - actions
    
    func reloadData() {
        do {
            try controller.performFetch()
        } catch {
            print("Ошибка поулчения записей из кор даты: \(error)")
        }
        delegate?.vmReloadData(animated: false)
    }
    
    func didSelect(at indexPath: IndexPath) {
        let contact = controller.object(at: indexPath)
        try? router.showContactInfo(contact: contact, editable: true, delegate: self)
    }
    
    func delete(at indexPath: IndexPath) {
        let contact = controller.object(at: indexPath)
        
        let acceptAction = UIAlertAction(title: "Delete".localized, style: .destructive) { _ in
            let context = contact.managedObjectContext
            ContactsManager.instance.delete(contact: contact)
            CoreDataManager.instance.saveContext(context)
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
        
        delegate?.showAlert(
            title: "DeleteContactTitle".localized,
            message: "DeleteContactMessage".localized,
            actions: [acceptAction, cancelAction]
        )
    }
    
    func didUpdateSortingSelectedIndex(_ index: Int) {
        setSortingType(sortingTypes[index])
    }
    
    func searchWithTerm(_ term: String) {
        if term.isEmpty {
            controller.fetchRequest.predicate = originalPredicate
        } else {
            let fieldsForSearch = settings.contactSearchFields
            let predicates = fieldsForSearch.map() { field in
                return NSPredicate(format: "\(field) CONTAINS[cd] %@", term)
            }
            let predicate = NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
            let resultsPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [originalPredicate, predicate])
            controller.fetchRequest.predicate = resultsPredicate
        }
        reloadData()
    }
    
    func changeDisplayType() {
        switch displayMode {
        case .cards:
            displayMode = .list
        case .list:
            displayMode = .cards
        }
        delegate?.vmReloadBar()
        delegate?.vmReloadData(animated: true)
    }
    
    // MARK: private
    
    private func initialize() {

    }
    
    private func setSortingType(_ type: SortingType, force: Bool = false) {
        guard sortingType != type || force else {
            return
        }
        
        sortingType = type
        
        let descriptors: [NSSortDescriptor]
        let commonDescriptors = settings.contactSortingProperties()
            .map() {
                NSSortDescriptor(
                    key: $0,
                    ascending: true,
                    selector: #selector(NSString.localizedCompare(_:))
                )
        }
        
        switch type {
        case .name:
            descriptors = commonDescriptors
        case .creationDate:
            let vcCreationDateDescriptor = NSSortDescriptor(
                key: "lastDateOfCreationBusinessCard",
                ascending: false
            )
            descriptors = [vcCreationDateDescriptor] + commonDescriptors
        }
        controller.fetchRequest.sortDescriptors = descriptors
        try? controller.performFetch()
        delegate?.vmReloadData(animated: true)
    }
}

// MARK: - Датасорс
extension BusinessCardsListViewModel {
    var showNoDataLabel: Bool {
        guard controller.fetchRequest.predicate == originalPredicate else {
            return false
        }
        
        for i in 0..<numberOfSections {
            let numberOfRows = numberOfItems(at: i)
            if numberOfRows > 0 {
                return false
            }
        }
        
        return true
    }
    
    var numberOfSections: Int {
        return controller.sections?.count ?? 0
    }
    
    func numberOfItems(at section: Int) -> Int {
        guard let sections = controller.sections, sections.count > section else {
            return 0
        }
        
        return sections[section].numberOfObjects
    }
    
    func item(at indexPath: IndexPath) -> BusienssCardsListCell.Item {
        let contact = controller.object(at: indexPath)
        let displayProperties = settings.contactSortingProperties()
        var name = ""
        let space = " "
        var specialization: String? = contact.specialization
        
        for (index, property) in displayProperties.enumerated() {
            guard let value = contact.value(forKeyPath: property) as? String, !value.isEmpty else {
                continue
            }
            
            if index > 1 {
                if !name.isEmpty {
                    break
                }
                specialization = nil
            }
            
            if !name.isEmpty {
                name.append(space)
            }
            
            name.append(value)
        }
        
        return BusienssCardsListCell.Item(
            text: name,
            subText: specialization,
            avatar: contact.avatarFile.flatMap { AppManager.instance.loadImageJPG(name: $0) },
            mode: displayMode
        )
    }
    
    /// Возвращает сигнал с заполненной вью моделью BusinessCardsCollectionViewModel
    func businesCardsViewModel(at indexPath: IndexPath) -> Observable<BusinessCardsCollectionViewModel> {
        return Observable.create({ [unowned self] observer -> Disposable in
            let contact = self.controller.object(at: indexPath)
            let vm: BusinessCardsCollectionViewModel
            if let cached = self.businesCardsViewModels[contact] {
                vm = cached
                // Удаляю старого делегата
                vm.viewDelegate = nil
            } else {
                vm = BusinessCardsCollectionViewModel()
                vm.isExpandable = false
                vm.delegate = self
                self.businesCardsViewModels[contact] = vm
            }
            
            switch self.displayMode {
            case .cards:
                vm.style = .mini
            case .list:
                vm.style = .miniWithoutPaging
            }
            
            let cards: [BusinessCardsCollectionViewModel.Card] = contact.orderedCards
                .flatMap({ card -> BusinessCardsCollectionViewModel.Card? in
                    if let image = card.image {
                        return (image, card.creationDate as Date?)
                    } else {
                        return nil
                    }
                })
            vm.cards = cards
            observer.onNext(vm)
            
            return Disposables.create()
        })
    }
    
    /// Айтемы для контекст меню
    func contextMenuItems(at indexPath: IndexPath) -> [DataSelectorItem] {
        return [
            DataSelectorItem(
                image: nil,
                text: "Delete".localized,
                action: { [unowned self] in
                    self.delete(at: indexPath)
                }
            )
        ]
    }
}

extension BusinessCardsListViewModel: ContactsSelectionListDelegate {
    func selectionListDidFinish(_ vm: ContactsSelectionListViewModel, with selectedContacts: [Contacts]) {
        
    }
    
    func selectionListDidUpdateItems(_ vm: ContactsSelectionListViewModel) {
        
    }
    
    func selectionListDidUpdateSelectedContacts(_ vm: ContactsSelectionListViewModel, contacts: [Contacts]) {
        guard let contact = contacts.first else {
            return
        }
        
        router.showPhotoContactDetail(contact)
    }
}

// MARK: - BusinessCardsCollectionDelegate
extension BusinessCardsListViewModel: BusinessCardsCollectionDelegate {
    func businessCardsCollectionDidUpdateImages(viewModel: BusinessCardsCollectionViewModel, cards: [BusinessCardsCollectionViewModel.Card]) {
        guard let contact = contactForCardsVM(viewModel), let context = contact.managedObjectContext else {
            return
        }
        let businessCards = cards.map() { image, date in
            return BusinessCard(image: image, creationDate: date, context: context)
        }
        
        contact.setBusinessCards(cards: businessCards)
        CoreDataManager.instance.saveContext(context)
    }
    
    fileprivate func contactForCardsVM(_ vm: BusinessCardsCollectionViewModel) -> Contacts? {
        for (key, value) in businesCardsViewModels {
            if value === vm {
                return key
            }
        }
        return nil
    }
    
    func businessCardsCollectionWillAddNewCard(viewModel: BusinessCardsCollectionViewModel) -> Bool {
        guard availabilityManager.canAddNewElement(.businessCards) else {
            delegate?.showLimitationAlert(for: .businessCards)
            return false
        }
        return true
    }
    
    func businessCardsCollectionDidTapCardWhenNotExpandable(viewModel: BusinessCardsCollectionViewModel) {
        guard let contact = contactForCardsVM(viewModel) else {
            return
        }
        router.showPhotoContactDetail(contact)
    }
}

extension BusinessCardsListViewModel: ContactConverterDelegate {
    func contactConverterDidCompleteWithContact(converter: ContactControllerDelegateConverter, contact: CNContact?) {
        guard let contact = contact else {
            return
        }
        switch converter.type {
        case .creation:
            ContactsManager.instance.addNewContactToDB(contact)
            Metrica.instance.send(.commonContactIsCreated)
        case .editing:
            ContactsManager.instance.updateContactInDB(contact)
        }
    }
}
