//
//  BusienssCardsListCell.swift
//  TemporaryContacts
//
//  Created by Konshin on 20.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import MGSwipeTableCell

protocol BusienssCardsListCellDelegate: class {
    func businessCardsListCellDidTapToDelete(_ cell: BusienssCardsListCell)
    func businessCardsDidTapToContext(_ cell: BusienssCardsListCell, contextButton: UIButton)
}

private let horizontalMargins: CGFloat = 17

/// Ячейка для отображения 2-х полей и списка визиток
class BusienssCardsListCell: MGSwipeTableCell {
    
    typealias Mode = BusinessCardsDisplayMode
    
    struct Item {
        let text: String
        let subText: String?
        let avatar: UIImage?
        let mode: Mode
    }
    
    /// Контейнер для карточек
    private let cardsContainer: UIView = {
        let v = UIView()
        v.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .vertical)
        return v
    }()
    /// Основная лейбла
    private let mainTextLabel: UILabel = {
        let l = UILabel()
        l.textColor = ColorHelper.AppDarkTextColor()
        l.font = UIFont.appFont(size: 18, typeface: .semibold)
        return l
    }()
    /// Лейбла с доп текстом
    private let subTextLabel: UILabel = {
        let l = UILabel()
        l.textColor = ColorHelper.AppDarkTextColor()
        l.font = UIFont.appFont(size: 14, typeface: .light)
        return l
    }()
    /// Лейбла с доп текстом
    private let avatarView: UIImageView = {
        let iv = UIImageView()
        iv.layer.masksToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.setContentCompressionResistancePriority(.required, for: .vertical)
        iv.setContentCompressionResistancePriority(.required, for: .horizontal)
        return iv
    }()
    /// кнопка вызова контекстного меню
    private let contextMenuButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "business_cards_list_context_menu").withRenderingMode(.alwaysOriginal), for: .normal)
        button.setContentCompressionResistancePriority(.required, for: .horizontal)
        return button
    }()
    
    fileprivate var mode: Mode = .list
    /// Делегат ячейки
    weak var bcDelegate: BusienssCardsListCellDelegate?
    
    private let deleteButton = MGSwipeButton(
        title: "",
        icon: UIImage(named: "BlueDelete"),
        backgroundColor: ColorHelper.AppRedColor(),
        insets: UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
    )
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        cardsContainer.subviews.forEach() { $0.removeFromSuperview() }
    }
    
    // MARK: - getters
    
    /// Необходимая высота для айтема
    static func necessaryHeight(item: Item, tableWidth: CGFloat) -> CGFloat {
        switch item.mode {
        case .cards:
            let contentWidth = tableWidth - 2 * horizontalMargins
            return 67 + BusinessCardsCollectionController.necessaryHeight(
                for: contentWidth,
                style: .mini
            )
        case .list:
            return 74
        }
    }
    
    // MARK: - actions
    
    private func setup() {
        contentView.addSubview(mainTextLabel)
        contentView.addSubview(subTextLabel)
        contentView.addSubview(cardsContainer)
        setMode(.list)
        
        contextMenuButton.addTarget(self, action: #selector(showContextMenu(_:)), for: .touchUpInside)
        
        deleteButton.callback = { [unowned self] _ in
            self.bcDelegate?.businessCardsListCellDidTapToDelete(self)
            return true
        }
    }
    
    func configure(with item: Item) {
        mainTextLabel.text = item.text
        subTextLabel.text = item.subText

        if item.mode == .cards {
            avatarView.image = item.avatar ?? #imageLiteral(resourceName: "contacts_list_avatar_placeholder")
        }
        
        setMode(item.mode)
    }
    
    func setLeftView(_ view: UIView) {
        cardsContainer.addSubview(view)
        view.snp.remakeConstraints() { $0.edges.equalToSuperview() }
    }
    
    @objc private func showContextMenu(_ sender: UIButton) {
        bcDelegate?.businessCardsDidTapToContext(self, contextButton: sender)
    }
    
    fileprivate func setMode(_ mode: Mode) {
        self.mode = mode
        let hasSubText = subTextLabel.text?.isEmpty == false
        
        switch mode {
        case .cards:
            separatorInset.left = horizontalMargins
            separatorInset.right = horizontalMargins
            
            rightButtons = []
            
            contentView.addSubview(avatarView)
            contentView.addSubview(contextMenuButton)
            
            avatarView.snp.remakeConstraints { maker in
                maker.left.equalToSuperview().offset(horizontalMargins)
                maker.top.equalToSuperview().offset(15)
                maker.width.height.equalTo(40)
            }
            avatarView.layer.cornerRadius = 20
            cardsContainer.snp.remakeConstraints { maker in
                maker.top.equalTo(avatarView.snp.bottom).offset(10)
                maker.left.right.equalToSuperview().inset(horizontalMargins)
                maker.bottom.equalToSuperview().offset(horizontalMargins).priority(249)
            }
            contextMenuButton.snp.remakeConstraints { maker in
                maker.top.equalTo(avatarView)
                maker.right.equalToSuperview().offset(-15)
                maker.width.height.equalTo(30)
            }
            mainTextLabel.snp.remakeConstraints { maker in
                maker.left.equalTo(avatarView.snp.right).offset(10)
                if hasSubText {
                    maker.centerY.equalTo(avatarView).offset(-9)
                } else {
                    maker.centerY.equalTo(avatarView)
                }
                maker.right.equalTo(contextMenuButton.snp.left).offset(-8)
            }
            subTextLabel.snp.remakeConstraints { maker in
                maker.left.equalTo(mainTextLabel)
                maker.top.equalTo(mainTextLabel.snp.bottom).offset(2)
                maker.right.equalTo(contextMenuButton.snp.left).offset(-8)
            }
        case .list:
            separatorInset.left = 106
            separatorInset.right = 17
            
            rightButtons = [deleteButton]
            
            avatarView.removeFromSuperview()
            contextMenuButton.removeFromSuperview()
            
            cardsContainer.snp.remakeConstraints { maker in
                maker.left.equalToSuperview().offset(horizontalMargins)
                maker.width.equalTo(74)
                maker.centerY.equalToSuperview()
            }
            mainTextLabel.snp.remakeConstraints { maker in
                maker.left.equalTo(cardsContainer.snp.right).offset(horizontalMargins)
                if hasSubText {
                    maker.centerY.equalToSuperview().offset(-9)
                } else {
                    maker.centerY.equalToSuperview()
                }
                maker.right.equalToSuperview().offset(-horizontalMargins)
            }
            subTextLabel.snp.remakeConstraints { maker in
                maker.left.equalTo(mainTextLabel)
                maker.top.equalTo(mainTextLabel.snp.bottom).offset(2)
                maker.right.equalToSuperview().offset(-horizontalMargins)
            }
        }
    }
}
