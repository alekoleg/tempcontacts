//
//  PhotoContactDetailViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 15.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol PhotoContactDetailViewModelDelegate: UserNotificationProtocol {
    func setEnterMode(_ enterMode: PhotoContactDetailViewModel.EnterMode, text: String?)
    func vmShowMinimalNotification(title: String, subTitle: String?)
    func vmTextFieldInLoading(_ inLoading: Bool)
}


class PhotoContactDetailViewModel {
    enum EnterMode: Int {
        case web
        case keys
        case email
    }
    
    fileprivate let contact: Contacts
    fileprivate let cardsManager: BusinessCardsManager
    fileprivate let communiocationHelper: CommunicationHelper
    fileprivate let availabilityManager: AvailabilityManager
    
    weak var vmDelegate: PhotoContactDetailViewModelDelegate?
    
    fileprivate var selectedEnterMode: EnterMode?
    /// Введенные тексты по секциям
    private var enteredTexts = [EnterMode: String]()
    
    init(contact: Contacts,
         cardsManager: BusinessCardsManager,
         communiocationHelper: CommunicationHelper,
         availabilityManager: AvailabilityManager)
    {
        self.contact = contact
        self.cardsManager = cardsManager
        self.communiocationHelper = communiocationHelper
        self.availabilityManager = availabilityManager
    }
    
    // MARK: - getters
    
    func cardsCollection() -> UIViewController {
        let cards: [BusinessCardsCollectionViewModel.Card] = contact.orderedCards
            .flatMap({ card -> BusinessCardsCollectionViewModel.Card? in
                if let image = card.image {
                    return (image, card.creationDate as Date?)
                } else {
                    return nil
                }
            })
        let vm = BusinessCardsCollectionViewModel(cards: cards)
        vm.delegate = self
        vm.isExpandable = true
        vm.isEditingEnabled = true
        return BusinessCardsCollectionController(viewModel: vm)
    }
    
    var screenName: String? {
        let name = [contact.lastName, contact.firstName]
            .flatMap({ $0 })
            .joined(separator: " ")
        return name.isEmpty ? contact.specialization : name
    }
    
    // MARK: - actions
    
    func addInfoToContact() {
        guard let type = selectedEnterMode, let string = enteredTexts[type] else {
            return
        }
        
        vmDelegate?.vmTextFieldInLoading(true)
        
        DispatchQueue.global().async { [unowned self] in
            switch type {
            case .email:
                let email = Emails(context: self.contact.managedObjectContext)
                email.label = EmailLabels.Home.rawValue
                email.email = string
                self.contact.addToEmails(email)
            case .keys:
                let phone = Phones(context: self.contact.managedObjectContext)
                phone.label = PhoneLabels.Mobile.rawValue
                phone.digits = string
                self.contact.addToPhones(phone)
            case .web:
                guard let url = self.communiocationHelper.webURL(from: string) else {
                    return
                }
                let site = Website(context: self.contact.managedObjectContext)
                site.url = url.absoluteString
                self.contact.addToWebsites(site)
                break
            }
            
            CoreDataManager.instance.saveContext(self.contact.managedObjectContext)
            ContactsManager.instance.updateContact(dbContact: self.contact)
            
            DispatchQueue.main.async { [unowned self] in
                self.vmDelegate?.vmTextFieldInLoading(false)
                self.vmDelegate?.vmShowMinimalNotification(
                    title: "action.data-saved".localized,
                    subTitle: nil
                )
            }
        }
    }
    
    /// Пытается выполнить текущее действие с введенным текстом
    func goTo() {
        guard let type = selectedEnterMode, let string = enteredTexts[type] else {
            return
        }
        
        switch type {
        case .email:
            try? communiocationHelper.sendEmail(emailAddress: string)
            Metrica.instance.send(.emailSendedFromBusinessCardsScreen)
        case .keys:
            try? communiocationHelper.call(string)
            Metrica.instance.send(.callFromBusinessCardsScreen)
        case .web:
            guard let url = communiocationHelper.webURL(from: string) else {
                return
            }
            communiocationHelper.goToWebURL(url)
            Metrica.instance.send(.browserIsOpenFromBusinessCardsScreen)
        }
    }
    
    func save() {
        
    }
    
    func didTapToButton(with enterMode: EnterMode) {
        selectedEnterMode = enterMode
        vmDelegate?.setEnterMode(enterMode, text: enteredTexts[enterMode])
    }
    
    func textDidUpdate(_ text: String?) {
        guard let mode = selectedEnterMode else {
            return
        }
        enteredTexts[mode] = text
    }
}


extension PhotoContactDetailViewModel: BusinessCardsCollectionDelegate {
    func businessCardsCollectionDidUpdateImages(viewModel: BusinessCardsCollectionViewModel, cards: [BusinessCardsCollectionViewModel.Card]) {
        guard let context = contact.managedObjectContext else {
            return
        }
        let businessCards = cards.map() { image, date in
            return BusinessCard(image: image, creationDate: date, context: context)
        }
        
        contact.setBusinessCards(cards: businessCards)
        CoreDataManager.instance.saveContext(context)
    }

    /// Запрашивает делегата о возможности создать новую визитку
    ///
    /// - Parameter viewModel: Вьюмодель визиток
    /// - Returns: Если вернуть нет, но карточка не будет создаваться
    func businessCardsCollectionWillAddNewCard(viewModel: BusinessCardsCollectionViewModel) -> Bool {
        guard availabilityManager.canAddNewElement(.businessCards) else {
            vmDelegate?.showLimitationAlert(for: .businessCards)
            return false
        }
        return true
    }
}
