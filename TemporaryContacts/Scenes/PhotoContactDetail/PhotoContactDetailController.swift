//
//  PhotoContactDetailController.swift
//  TemporaryContacts
//
//  Created by Konshin on 15.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import JFMinimalNotifications

class PhotoContactDetailController: UIViewController {
    fileprivate enum LeftViewType {
        case addButton
        case activity
    }
    
    fileprivate let viewModel: PhotoContactDetailViewModel
    
    @IBOutlet private var headerContentView: UIView!
    @IBOutlet fileprivate var webButton: UIButton!
    @IBOutlet fileprivate var buttonsView: UIView!
    @IBOutlet fileprivate var keysButton: UIButton!
    @IBOutlet fileprivate var emailButton: UIButton!
    @IBOutlet fileprivate var textField: UITextField!
    
    weak var navigationDelegate: NavigationDelegate?
    
    init(viewModel: PhotoContactDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "PhotoContactDetailController", bundle: nil)
        viewModel.vmDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialize()
    }
    
    // MARK: - actions
    
    private func initialize() {
        title = viewModel.screenName
        
        let cardsVC = viewModel.cardsCollection()
        addChild(cardsVC)
        headerContentView.addSubview(cardsVC.view)
        cardsVC.view.snp.remakeConstraints() { maker in
            maker.edges.equalToSuperview()
        }
        
        leftViewStyle = .addButton
        textField.leftViewMode = .always
        
        let pan = UIPanGestureRecognizer(target: nil, action: nil)
        pan.delegate = self
        view.addGestureRecognizer(pan)
    }

    // MARK: - IBActions
    
    @IBAction private func selectWeb() {
        viewModel.didTapToButton(with: .web)
    }
    
    @IBAction private func selectKeys() {
        viewModel.didTapToButton(with: .keys)
    }
    
    @IBAction private func selectEmail() {
        viewModel.didTapToButton(with: .email)
    }
    
    @objc private func tapToAdd() {
        view.endEditing(true)
        viewModel.addInfoToContact()
    }
    
    @objc private func save() {
        navigationDelegate?.navigationDelegateSomeControllerWantToDismiss(controller: self)
        viewModel.save()
    }
    
    @IBAction private func textDidChanged(_ tf: UITextField) {
        viewModel.textDidUpdate(tf.text)
    }
    
    fileprivate var leftViewStyle: LeftViewType = .activity {
        didSet {
            guard oldValue != leftViewStyle else {
                return
            }
            
            switch leftViewStyle {
            case .addButton:
                let addButton = UIButton(type: .system)
                addButton.setImage(#imageLiteral(resourceName: "photo_contact_detail_add").withRenderingMode(.alwaysOriginal), for: .normal)
                addButton.setBackgroundImage(nil, for: .normal)
                addButton.addTarget(self, action: #selector(tapToAdd), for: .touchUpInside)
                addButton.sizeToFit()
                textField.leftView = addButton
            case .activity:
                let activity = UIActivityIndicatorView(style: .gray)
                activity.sizeToFit()
                activity.startAnimating()
                textField.leftView = activity
            }
        }
    }
}


extension PhotoContactDetailController: PhotoContactDetailViewModelDelegate {
    func setEnterMode(_ enterMode: PhotoContactDetailViewModel.EnterMode, text: String?) {
        for button in [webButton, keysButton, emailButton] {
            button?.isSelected = button?.tag == enterMode.rawValue
        }
        textField.isUserInteractionEnabled = true
        textField.text = text
        switch enterMode {
        case .email:
            textField.keyboardType = .emailAddress
        case .keys:
            textField.keyboardType = .numbersAndPunctuation
        case .web:
            textField.keyboardType = .URL
        }
        
        textField.reloadInputViews()
        textField.becomeFirstResponder()
    }
    
    func vmShowMinimalNotification(title: String, subTitle: String?) {
        guard let notification = JFMinimalNotification(
            style: .custom,
            title: title,
            subTitle: subTitle,
            dismissalDelay: 2
            ) else {
                return
        }
        view.addSubview(notification)
        notification.delegate = self
        notification.tm_prepate()
        notification.show()
    }
    
    func vmTextFieldInLoading(_ inLoading: Bool) {
        if inLoading {
            view.isUserInteractionEnabled = false
            leftViewStyle = .activity
        } else {
            view.isUserInteractionEnabled = true
            leftViewStyle = .addButton
        }
    }
}


extension PhotoContactDetailController: JFMinimalNotificationDelegate {
    func minimalNotificationDidDismiss(_ notification: JFMinimalNotification!) {
        notification.removeFromSuperview()
    }
}


extension PhotoContactDetailController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        viewModel.goTo()
        return true
    }
}


extension PhotoContactDetailController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let location = touch.location(in: view)
        var textFieldContainsTouch: Bool {
            return textField.frame.contains(location)
        }
        var buttonsContainsTouch: Bool {
            return buttonsView.frame.contains(location)
        }
        if !textFieldContainsTouch && !buttonsContainsTouch {
            view.endEditing(true)
        }
        return false
    }
}
