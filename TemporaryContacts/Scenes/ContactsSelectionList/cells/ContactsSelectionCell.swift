//
//  ContactsSelectionCell.swift
//  TemporaryContacts
//
//  Created by Konshin on 22.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

/// Ячейка, имеет блок, повторяющий логику ContactsListCell
/// Умеет показывать состояние выделенности
class ContactsSelectionCell: ContactsExpandableCell {
    private let selectionMark = UIImageView(image: #imageLiteral(resourceName: "contact_selection_none"))
    
    private var inSelectionMode = false {
        didSet {
            isSwipable = !inSelectionMode
            if inSelectionMode {
                setExpanded(expanded: false)
            }
        }
    }
    
    func configure(with item: ContactsSelectionCellItem, expandableView: UIView?, delegate: ContactsListCellDelegate) {
        super.configure(item, expandedView: expandableView, delegate: delegate)
        
        setSelected(item.isSelected)
    }
    
    private func setSelected(_ selected: Bool) {
        if selected {
            selectionMark.image = #imageLiteral(resourceName: "contact_selection_true")
        } else {
            selectionMark.image = #imageLiteral(resourceName: "contact_selection_none")
        }
    }
    
    func setSelectionMode(_ selectionMode: Bool, animated: Bool) {
        guard inSelectionMode != selectionMode else {
            return
        }
        inSelectionMode = selectionMode
        
        if !selectionMode {
            setSelected(false)
        }
        
        if animated {
            let animationCenterOffset: CGFloat = 50
            let newView: UIView
            if selectionMode {
                newView = selectionMark
            } else {
                newView = rightButton
            }
            let currentViews = rightButtonContainer.subviews
                .filter({ $0 != newView })
            
            rightButtonContainer.addSubview(newView)
            newView.snp.remakeConstraints() { maker in
                maker.centerY.equalToSuperview()
                maker.centerX.equalToSuperview().offset(animationCenterOffset)
            }
            rightButtonContainer.layoutIfNeeded()
            UIView.animate(
                withDuration: 0.2,
                animations: { [unowned self] in
                    currentViews.forEach() { sv in
                        sv.snp.remakeConstraints() { maker in
                            maker.centerY.equalToSuperview()
                            maker.centerX.equalToSuperview().offset(animationCenterOffset)
                        }
                    }
                    newView.snp.updateConstraints() { maker in
                        maker.centerX.equalToSuperview()
                    }
                    self.rightButtonContainer.layoutIfNeeded()
                },
                completion: { fin in
                    guard fin, newView.superview != nil else {
                        return
                    }
                    currentViews.forEach({ $0.removeFromSuperview() })
                    newView.snp.makeConstraints() { maker in
                        maker.width.height.equalToSuperview()
                    }
            })
        } else {
            rightButtonContainer.subviews.forEach({ $0.removeFromSuperview() })
            let newView: UIView
            if selectionMode {
                newView = selectionMark
            } else {
                newView = rightButton
            }
            rightButtonContainer.addSubview(newView)
            newView.snp.remakeConstraints() { maker in
                maker.edges.equalToSuperview()
            }
        }
    }
}
