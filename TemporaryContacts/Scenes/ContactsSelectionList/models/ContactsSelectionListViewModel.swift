//
//  ContactsSelectionListViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 22.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol ContactsSelectionListDelegate: class {
    /// Обновлены выбранные контакты
    func selectionListDidUpdateSelectedContacts(_ vm: ContactsSelectionListViewModel, contacts: [Contacts])
    /// Вызывается при нажатии на Готово
    func selectionListDidFinish(_ vm: ContactsSelectionListViewModel, with selectedContacts: [Contacts])
    /// Уведомляет об изменении в данные для отображения
    func selectionListDidUpdateItems(_ vm: ContactsSelectionListViewModel)
}

extension ContactsSelectionListDelegate {
    func selectionListDidUpdateItems(_ vm: ContactsSelectionListViewModel) {}
}


protocol ContactsSelectionListViewModelDelegate: ContactsListViewModelDelegate {
    func vmSetInEditing(_ inEditing: Bool, animated: Bool)
}


class ContactsSelectionListViewModel: ContactsListViewModel {
    
    /// Делегат вьюмодели - ВьюКонтроллер
    weak var selectionVMDelegate: ContactsSelectionListViewModelDelegate?
    /// Делегат компонента
    weak var selectionDelegate: ContactsSelectionListDelegate?
    
    var inEditing = false {
        didSet {
            isExpandEnabled = !inEditing
        }
    }
    
    var selectedContacts = [Contacts]() {
        didSet {
            selectionDelegate?.selectionListDidUpdateSelectedContacts(self, contacts: selectedContacts)
        }
    }
    
    // MARK: - getters
    
    /// Запрос на есть ли данные для отображения
    var hasData: Bool {
        return self.numberOfSections() > 0
    }

    // MARK: - actions
    
    func setSelectionEnabled(_ enabled: Bool, animated: Bool) {
        guard inEditing != enabled else {
            return
        }
        
        inEditing = enabled
        if !enabled {
            selectedContacts.removeAll()
        }
        selectionVMDelegate?.vmSetInEditing(enabled, animated: animated)
    }
    
    func selectionItem(at indexPath: IndexPath) -> ContactsSelectionCellItem {
        let commonItem = item(at: indexPath)
        
        let contact = fetchedController.object(at: indexPath)
        let selected = selectedContacts.contains(obj: contact)

        return ContactsSelectionCellItem(
            mainText: commonItem.mainText,
            subText: commonItem.subText,
            showSeparator: commonItem.showSeparator,
            rightButtonIcon: commonItem.rightButtonIcon,
            avatarImage: contact.avatarFile.flatMap { AppManager.instance.loadImageJPG(name: $0) },
            isTempObject: commonItem.isTempObject,
            isSwipable: commonItem.isSwipable,
            isExpanded: commonItem.isExpanded,
            isSelected: selected
        )
    }
    
    func selectAll() {
        selectedContacts = fetchedController.fetchedObjects ?? []
        vmDelegate?.vmReloadData()
    }
    
    override func didSelect(indexPath: IndexPath) {
        if inEditing {
            let contact = fetchedController.object(at: indexPath)
            if let index = selectedContacts.index(of: contact) {
                selectedContacts.remove(at: index)
            } else {
                selectedContacts.append(contact)
            }
            selectionVMDelegate?.vmReloadIndexPaths([indexPath])
        } else {
            super.didSelect(indexPath: indexPath)
        }
    }
    
    /// Пытается завершить выбор контактов
    func done() {
        guard let vmDelegate = self.selectionVMDelegate else {
            return
        }
        router.dismissController(vmDelegate.asViewController())
        selectionDelegate?.selectionListDidFinish(self, with: selectedContacts)
    }
    
    /// Отображаемые данные обновились
    func dataDidUpdate() {
        selectionDelegate?.selectionListDidUpdateItems(self)
    }
}
