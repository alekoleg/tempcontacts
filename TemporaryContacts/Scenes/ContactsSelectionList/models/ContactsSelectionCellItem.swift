//
//  ContactsSelectionCellItem.swift
//  TemporaryContacts
//
//  Created by Konshin on 22.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

class ContactsSelectionCellItem: ContactsListCellItem {
    let isSelected: Bool
    
    init(mainText: String?,
         subText: String?,
         showSeparator: Bool,
         rightButtonIcon: UIImage?,
         avatarImage: UIImage?,
         isTempObject: Bool,
         isSwipable: Bool,
         isExpanded: Bool,
         isSelected: Bool)
    {
        self.isSelected = isSelected
        super.init(
            mainText: mainText,
            subText: subText,
            showSeparator: showSeparator,
            rightButtonIcon: rightButtonIcon,
            avatarImage: avatarImage,
            isTempObject: isTempObject,
            isSwipable: isSwipable,
            isExpanded: isExpanded)
    }
}
