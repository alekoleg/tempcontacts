//
//  ContactsSelectionListRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData

protocol ContactsSelectionListRouter {
    func showContactsSelector(selectedContacts: [Contacts],
    fetchedController: NSFetchedResultsController<Contacts>,
    searchAvailable: Bool,
    filteringPredicate: NSPredicate?,
    delegate: ContactsSelectionListDelegate)
}

extension Router where Self: ContactsSelectionListRouter & ContactsListRouter, RootController == UINavigationController {
    func showContactsSelector(selectedContacts: [Contacts],
                              fetchedController: NSFetchedResultsController<Contacts>,
                              searchAvailable: Bool,
                              filteringPredicate: NSPredicate?,
                              delegate: ContactsSelectionListDelegate)
    {
        let config = ContactsListViewModelConfiguration(
            fetchedController: fetchedController,
            filteringPredicate: filteringPredicate,
            isSearchAvailable: searchAvailable
        )
        let vm = ContactsSelectionListViewModel(
            configuration: config,
            settings: dependencies.settings,
            router: self,
            communicationHelper: dependencies.communicationHelper,
            partialFormatter: dependencies.partialFormatter,
            availabilityManager: dependencies.availabilityManager
        )
        vm.selectionDelegate = delegate
        vm.inEditing = true
        vm.selectedContacts = selectedContacts
        let vc = ContactsSelectionListController(vm: vm)
        rootController.pushViewController(vc, animated: true)
    }
}
