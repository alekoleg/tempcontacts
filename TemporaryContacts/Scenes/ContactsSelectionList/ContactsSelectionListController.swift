//
//  ContactsSelectionListController.swift
//  TemporaryContacts
//
//  Created by Konshin on 22.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

private let reuseIdentifier = #file

class ContactsSelectionListController: ContactsListController {
    fileprivate let viewModel: ContactsSelectionListViewModel

    init(vm: ContactsSelectionListViewModel) {
        self.viewModel = vm
        super.init(viewModel: viewModel)
        vm.selectionVMDelegate = self
        
        hidesBottomBarWhenPushed = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tp_makeGreenBackButton()
        
        tableView.register(ContactsSelectionCell.self, forCellReuseIdentifier: reuseIdentifier)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "contacts-selection.done".localized,
            style: .plain,
            target: self,
            action: #selector(done)
        )
    }
    
    // MARK: - actions
    
    @objc private func done() {
        viewModel.done()
    }
    
    override func dataDidUpdate() {
        super.dataDidUpdate()
        viewModel.dataDidUpdate()
    }
    
    // MARK: - tableView
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        if let contactCell = cell as? ContactsSelectionCell {
            let item = viewModel.selectionItem(at: indexPath)
            var expandedView: UIView?
            if item.isExpanded {
                expandedView = businessControllerView(at: indexPath)
            }
            contactCell.configure(with: item, expandableView: expandedView, delegate: self)
            contactCell.setSelectionMode(viewModel.inEditing, animated: false)
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
}


extension ContactsSelectionListController: ContactsSelectionListViewModelDelegate {
    func vmSetInEditing(_ inEditing: Bool, animated: Bool) {
        for cell in tableView.visibleCells {
            if let cell = cell as? ContactsSelectionCell {
                cell.setSelectionMode(inEditing, animated: animated)
            }
        }
    }
}
