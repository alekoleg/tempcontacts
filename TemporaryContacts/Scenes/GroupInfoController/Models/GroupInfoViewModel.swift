//
//  GroupInfoViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 22.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData
import PhoneNumberKit

protocol GroupInfoViewModelDelegate: UserNotificationProtocol {
    func vmSetInEditing(_ inEditing: Bool, animated: Bool)
    func vmSetDeleteFromGroupTitle(_ title: String?)
    func vmController() -> UIViewController
    func vmSetHasNotContactsMarkVisible(_ visible: Bool)
}


class GroupInfoViewModel {
    fileprivate let group: Groups
    fileprivate let contactsListViewModel: ContactsSelectionListViewModel
    fileprivate let partialFormatter: PartialFormatter
    /// Контекст, в котором ведется работа
    fileprivate let context: NSManagedObjectContext
    /// Менеджер по работе с доступностями создавать сущности
    fileprivate let availabilityManager: AvailabilityManager
    
    /// В режиме редактирования
    var inEditing = false
    
    fileprivate let router: GroupInfoRouter
    
    fileprivate let settings: Settings
    
    fileprivate var selectedContacts = [Contacts]() {
        didSet {
            let count = selectedContacts.count
            let title: String?
            if count > 0 {
                title = "group-detail.remove-from-groups".localized + " \(count)"
            } else {
                title = nil
            }
            delegate?.vmSetDeleteFromGroupTitle(title)
        }
    }
    
    weak var delegate: GroupInfoViewModelDelegate? {
        didSet {
            delegate?.vmSetHasNotContactsMarkVisible(!contactsListViewModel.hasData)
        }
    }
    
    /// Экран служит созданием новой группы
    fileprivate let isCreateNew: Bool
    
    /// Создает вьюмодель для экрана детализации и редактирования группы
    ///
    /// - Parameters:
    ///   - group: МОдель группы. Если не передать - создается нвоая группа
    ///   - settings: Настройки приложения
    ///   - router: Модель роутера
    init(group: Groups?,
         settings: Settings,
         router: GroupInfoRouter,
         partialFormatter: PartialFormatter,
         availabilityManager: AvailabilityManager)
    {
        // Создаю дочерний контекст для управления изменениями
        let tempContext = CoreDataManager.instance.tempContextOnMainTread
        
        let detailedGroup: Groups
        if let group = group,
            let currentContextGroup = getGroup(with: group.objectID, context: tempContext)
        {
            detailedGroup = currentContextGroup
            isCreateNew = false
        } else {
            isCreateNew = true
            detailedGroup = Groups(context: tempContext)
        }
        
        self.router = router
        self.group = detailedGroup
        self.settings = settings
        self.partialFormatter = partialFormatter
        self.context = tempContext
        self.availabilityManager = availabilityManager
        
        let resultsController: NSFetchedResultsController<Contacts> = CoreDataManager.instance
            .fetchedResultsControllerTyped(
                entityName: "Contacts",
                sortingDescriptors: settings.contactsSortingDescriptors(),
                cacheName: nil,
                managedContext: tempContext,
                sectionNameKeyPath: "sectionGroupingKey"
        )
        
        let config = ContactsListViewModelConfiguration(
            fetchedController: resultsController,
            filteringPredicate: NSPredicate(format: "(%@ IN groups)", detailedGroup),
            isSearchAvailable: false
        )
        contactsListViewModel = ContactsSelectionListViewModel(
            configuration: config,
            settings: settings,
            router: router,
            communicationHelper: CommunicationHelper(presenter: nil),
            partialFormatter: partialFormatter,
            availabilityManager: availabilityManager
        )
        contactsListViewModel.selectionDelegate = self
    }
    
    // MARK: - getters
    
    func contactsListController() -> UIViewController {
        let vc = ContactsSelectionListController(vm: contactsListViewModel)
        return vc
    }
    
    var groupName: String? {
        get { return group.name }
        set { group.name = newValue }
    }
    
    var hasContactsData: Bool {
        return contactsListViewModel.hasData
    }
    
    // MARK: - actions
    
    /// Задает состояние редактирования
    func setInEditing(_ inEditing: Bool) {
        guard inEditing != self.inEditing else {
            return
        }
        tapToEdit()
    }
    
    func tapToEdit() {
        inEditing = !inEditing
        delegate?.vmSetInEditing(inEditing, animated: true)
        contactsListViewModel.setSelectionEnabled(inEditing, animated: true)
        if !inEditing {
            CoreDataManager.instance.saveContext(group.managedObjectContext)
            if isCreateNew {
                NotificationCenter.default.post(name: .didFinishCreateGroup, object: nil)
                Metrica.instance.send(.groupIsCreated)
            }
        }
    }
    
    func removeFromGroup() {
        let editAction = UIAlertAction(
            title: "Remove".localized,
            style: .destructive) { [unowned self] _ in
                let contactsToDelete = Set(self.selectedContacts)
                self.group.removeFromContacts(contactsToDelete as NSSet)
                self.contactsListViewModel.selectedContacts.removeAll()
        }
        let closeAction = UIAlertAction(
            title: "Close".localized,
            style: UIAlertAction.Style.cancel,
            handler: nil
        )
        delegate?.showAlert(
            title: "RemoveContactsTitle".localized,
            message: "RemoveContactsMessage".localized,
            actions: [closeAction, editAction]
        )
    }
    
    func selectAll() {
        contactsListViewModel.selectAll()
    }
    
    func eraseGroup() {
        let editAction = UIAlertAction(
            title: "Yes".localized,
            style: .destructive
        ) { [unowned self] (action) in
            self.group.managedObjectContext?.delete(self.group)
            CoreDataManager.instance.saveContext(self.group.managedObjectContext)
            if let del = self.delegate {
                self.router.dismissController(del.vmController())
            }
            Metrica.instance.send(.groupIsDisbanded)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel)
        
        delegate?.showAlert(
            title: "DisbandGroupTitle".localized,
            message: "DisbandGroupMessage".localized,
            actions: [cancelAction, editAction]
        )
    }
    
    func deleteGroup() {
        let editAction = UIAlertAction(
            title: "Yes".localized,
            style: .destructive
        ) { [unowned self] (action) in
            ContactsManager.instance.delete(group: self.group,
                                            deleteTempContacts: true,
                                            saveContext: true)
            if let del = self.delegate {
                self.router.dismissController(del.vmController())
            }
        }
        
        let cancelAction = UIAlertAction(
            title: "DeleteGroupCancel".localized,
            style: .cancel
        ) { [unowned self] (action) in
            
            if !Settings.instance.showTips { return }
            
            self.delegate?.showAlert(
                title: "",
                message: "DeleteGroupHint".localized,
                actions: [UIAlertAction(title: "Close".localized, style: UIAlertAction.Style.cancel, handler: nil)]
            )
        }
        
        delegate?.showAlert(
            title: "DeleteGroupTitle".localized,
            message: "DeleteGroupMessage".localized,
            actions: [cancelAction, editAction]
        )
    }
    
    func addContacts() {
        let resultsController: NSFetchedResultsController<Contacts> = CoreDataManager.instance
            .fetchedResultsControllerTyped(
                entityName: "Contacts",
                sortingDescriptors: settings.contactsSortingDescriptors(),
                cacheName: nil,
                managedContext: context,
                sectionNameKeyPath: "sectionGroupingKey"
        )
        resultsController.fetchRequest.predicate = NSPredicate(format: "!(SELF IN %@)", group.contacts ?? NSSet())
        
        router.showContactsSelector(
            selectedContacts: group.contacts?.allObjects as? [Contacts] ?? [],
            fetchedController: resultsController,
            searchAvailable: true,
            filteringPredicate: nil,
            delegate: self
        )
    }
    
    // MARK: - header actions
    
    func editNotification() {
        guard availabilityManager.canAddNewElement(.notifications) else {
            delegate?.showLimitationAlert(for: .notifications)
            return
        }
        router.showNotificationController(
            notification: nil,
            owner: .group(group),
            context: context,
            delegate: self
        )
    }
    
    func sendGroupMessage() {
        router.showGroupCommunicationController(
            group: group,
            action: .sms
        )
    }
    
    func sendGroupEmail() {
        router.showGroupCommunicationController(
            group: group,
            action: .email
        )
    }
}


extension GroupInfoViewModel: ContactsSelectionListDelegate {
    func selectionListDidFinish(_ vm: ContactsSelectionListViewModel, with selectedContacts: [Contacts]) {
        guard vm != contactsListViewModel else {
            // Ничего не делаем на внутренний список
            return
        }

        if let contacts = group.contacts {
            group.contacts = contacts.addingObjects(from: selectedContacts) as NSSet
        } else {
            group.contacts = NSSet(array: selectedContacts)
        }
    }

    func selectionListDidUpdateSelectedContacts(_ vm: ContactsSelectionListViewModel, contacts: [Contacts]) {
        if vm == contactsListViewModel {
            selectedContacts = contacts
        }
    }
    
    func selectionListDidUpdateItems(_ vm: ContactsSelectionListViewModel) {
        delegate?.vmSetHasNotContactsMarkVisible(!vm.hasData)
    }
}


extension GroupInfoViewModel: EditNotificationViewControllerDelegate {
    func editNotificationControllerDidComplete(_ controller: EditNotificationViewController, with notificaion: AppNotification?) {
        router.dismissController(controller)
        guard let notification = notificaion else {
            return
        }
        group.addToNotifications(notification)
        CoreDataManager.instance.saveContext(group.managedObjectContext)
    }
}

// MARK: - helpers

func getGroup(with id: NSManagedObjectID, context: NSManagedObjectContext) -> Groups? {
    return context.object(with: id) as? Groups
}
