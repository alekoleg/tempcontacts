//
//  GroupInfoController.swift
//  TemporaryContacts
//
//  Created by Konshin on 22.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import SnapKit

class GroupInfoController: UIViewController {
    fileprivate let viewModel: GroupInfoViewModel
    
    @IBOutlet fileprivate var tableContentView: UIView!
    @IBOutlet fileprivate var headerView: UIView!
    @IBOutlet fileprivate var contactActionsView: UIView!
    @IBOutlet fileprivate var addContactView: UIView!
    @IBOutlet fileprivate var groupEditView: UIView!
    @IBOutlet fileprivate var nameTextField: UITextField!
    @IBOutlet fileprivate var hasNotContactsMark: UILabel!
    // buttons
    @IBOutlet fileprivate var removeFromGroupButton: UIButton!
    @IBOutlet fileprivate var eraseButton: UIButton!
    @IBOutlet fileprivate var deleteButton: UIButton!
    
    fileprivate var groupEditingViewHiddingConstraint: Constraint!
    fileprivate var groupNameWidthConstraint: Constraint!
    
    fileprivate var inEditing: Bool?
    
    init(viewModel: GroupInfoViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "GroupInfoController", bundle: nil)
        
        viewModel.delegate = self
        hidesBottomBarWhenPushed = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tp_makeGreenBackButton()
        
        for v in [addContactView, contactActionsView] as [UIView] {
            headerView.addSubview(v)
            v.snp.remakeConstraints() { maker in
                maker.edges.equalToSuperview()
            }
        }
        
        view.addSubview(groupEditView)
        groupEditView.snp.remakeConstraints() { maker in
            maker.height.equalTo(170)
            maker.left.right.equalToSuperview()
            maker.bottom.equalToSuperview().priority(750)
            groupEditingViewHiddingConstraint = maker.top.equalTo(view.snp.bottom).constraint
        }
        
        // Внимание
        // Тут какая-то магия. Констрейнты периодически не хотят обновляться и берутся с ксибки
        // На ксибке нижнему констрейнту задано свойство "Убраться при жизни"
        tableContentView.snp.remakeConstraints() { maker in
            maker.top.equalTo(headerView.snp.bottom)
            maker.left.right.equalToSuperview()
            maker.bottom.equalTo(groupEditView.snp.top)
        }
        
        nameTextField.snp.makeConstraints() { maker in
            groupNameWidthConstraint = maker.width.equalTo(240).constraint
        }
        nameTextField.attributedPlaceholder = NSAttributedString(
            string: "group-detail.name.placeholder".localized,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white]
        )

        vmSetInEditing(viewModel.inEditing, animated: false)
        
        let vc = viewModel.contactsListController()
        tableContentView.addSubview(vc.view)
        vc.view.snp.remakeConstraints() { maker in
            maker.edges.equalToSuperview()
        }
        addChild(vc)
        
        deleteButton.layer.cornerRadius = 22.0
        eraseButton.layer.borderWidth = 1.0
        eraseButton.layer.borderColor = ColorHelper.AppTintColor().cgColor
        eraseButton.layer.cornerRadius = 22.0;
        
        nameTextField.addTarget(self, action: #selector(nameChanged(_:)), for: .editingChanged)
        nameTextField.text = viewModel.groupName
        nameChanged(nameTextField)
        
        vmSetHasNotContactsMarkVisible(!viewModel.hasContactsData)
        
        let groupIcon = UIImageView(image: #imageLiteral(resourceName: "groupIcon"))
        groupIcon.sizeToFit()
        navigationItem.titleView = groupIcon
    }
    
    // MARK: - actions
    
    @IBAction private func addContacts() {
        viewModel.addContacts()
    }
    
    @objc fileprivate func tapToRightItem() {
        viewModel.tapToEdit()
    }
    
    @IBAction private func eraseGroup() {
        viewModel.eraseGroup()
    }
    
    @IBAction private func deleteGroup() {
        viewModel.deleteGroup()
    }
    
    @IBAction private func selectAll() {
        viewModel.selectAll()
    }
    
    @IBAction private func removeSelected() {
        viewModel.removeFromGroup()
    }
    
    @objc private func nameChanged(_ textField: UITextField) {
        if textField.text?.isEmpty == false {
            textField.placeholder = nil
        } else {
            textField.attributedPlaceholder = NSAttributedString(
                string: "group-detail.name.placeholder".localized,
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white]
            )
        }
        viewModel.groupName = textField.text
    }
    
    @IBAction private func editNotification() {
        viewModel.editNotification()
    }
    
    @IBAction private func sendGroupEmail() {
        viewModel.sendGroupEmail()
    }
    
    @IBAction private func sendGroupMessage() {
        viewModel.sendGroupMessage()
    }
}


extension GroupInfoController: GroupInfoViewModelDelegate {
    
    func vmController() -> UIViewController {
        return self
    }
    
    func vmSetDeleteFromGroupTitle(_ title: String?) {
        removeFromGroupButton.setTitle(title, for: .normal)
    }

    func vmSetInEditing(_ inEditing: Bool, animated: Bool) {
        guard inEditing != self.inEditing else {
            return
        }
        
        let action = { [unowned self] in
            self.addContactView.isHidden = !inEditing
            self.contactActionsView.isHidden = inEditing
            if inEditing {
                self.groupEditingViewHiddingConstraint.deactivate()
                self.groupNameWidthConstraint.activate()
            } else {
                self.groupEditingViewHiddingConstraint.activate()
                self.groupNameWidthConstraint.deactivate()
            }
            self.view.layoutIfNeeded()
            
            let rightItem: UIBarButtonItem
            if inEditing {
                rightItem = UIBarButtonItem(
                    title: "done".localized,
                    style: .plain,
                    target: self,
                    action: #selector(self.tapToRightItem)
                )
            } else {
                rightItem = UIBarButtonItem(
                    image: #imageLiteral(resourceName: "group_info_edit").withRenderingMode(.alwaysOriginal),
                    style: .plain,
                    target: self,
                    action: #selector(self.tapToRightItem)
                )
            }
            self.navigationItem.setRightBarButton(rightItem, animated: animated)
            
            self.nameTextField.isUserInteractionEnabled = inEditing
        }
        
        if animated {
            UIView.transition(
                with: headerView,
                duration: 0.3,
                options: .transitionCrossDissolve,
                animations: action,
                completion: nil
            )
        } else {
            action()
        }
    }
    
    func vmSetHasNotContactsMarkVisible(_ visible: Bool) {
        hasNotContactsMark?.isHidden = !visible
    }
}


extension GroupInfoController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
