//
//  RootViewController.swift
//  TemporaryContacts
//
//  Created by Konshin on 08.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import XMSegmentedControl
import SnapKit

/// Основной контроллер контактов. СОдержит переключение по табам и работа с шапкой навигации
class RootContactsController: UIViewController {
    fileprivate let viewModel: RootViewModel
    fileprivate let segmentedControl = TCSegmentedControll()
    fileprivate var currentController: RootContactsTabViewController?
    fileprivate let contentView = UIView()
    
    init(viewModel: RootViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        viewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTitle()
        setupNavigationItem()

        segmentedControl.delegate = self
        segmentedControl.font = UIFont.appFont(size: 16, typeface: .light)
        segmentedControl.selectedFont = UIFont.appFont(size: 16, typeface: .bold)
        segmentedControl.selectedItemHighlightStyle = .bottomEdge
        segmentedControl.backgroundColor = ColorHelper.AppTintColor()
        segmentedControl.highlightColor = ColorHelper.AppGreenColor()
        segmentedControl.tint = UIColor.white.withAlphaComponent(0.6)
        segmentedControl.highlightTint = UIColor.white
        segmentedControl.segmentTitle = viewModel.availableSegmentTitles
        segmentedControl.selectedSegment = viewModel.selectedSegmentIndex
        
        view.addSubview(segmentedControl)
        segmentedControl.snp.remakeConstraints() { maker in
            maker.top.left.right.equalToSuperview()
            maker.height.equalTo(44)
        }
        
        view.addSubview(contentView)
        contentView.snp.remakeConstraints() { maker in
            maker.left.right.bottom.equalToSuperview()
            maker.top.equalTo(segmentedControl.snp.bottom)
        }
        
        viewModel.showFirstTab()
        
        
    }
    
    // MARK: - actions
    
    /// Задает заголовок контроллера
    private func setupTitle() {
        guard let semibold = UIFont(name: "MyriadPro-Semibold", size: 19),
            let light = UIFont(name: "MyriadPro-Light", size: 19) else {
                fatalError("Не найдены шрифты")
        }
        let textColor = UIColor.white
        let attTitle = NSMutableAttributedString()
        attTitle.append(
            NSAttributedString(
                string: "Temp",
                attributes: [
                    NSAttributedString.Key.font: semibold,
                    NSAttributedString.Key.foregroundColor: textColor
                ]
            )
        )
        attTitle.append(
            NSAttributedString(
                string: "Phones",
                attributes: [
                    NSAttributedString.Key.font: light,
                    NSAttributedString.Key.foregroundColor: textColor
                ]
            )
        )
        let label = UILabel()
        label.attributedText = attTitle
        label.sizeToFit()
        navigationItem.titleView = label
    }
    
    private func setupNavigationItem() {
        let settings = UIBarButtonItem(
            image: #imageLiteral(resourceName: "Settings").withRenderingMode(.alwaysOriginal),
            style: .plain,
            target: self,
            action: #selector(settingsItemPressed)
        )
        navigationItem.leftBarButtonItem = settings
    }
    
    @objc private func settingsItemPressed() {
        viewModel.settingsItemPressed()
    }
    
    fileprivate func setupController(_ controller: RootContactsTabViewController, asChild: Bool) {
        if asChild {
            addChild(controller.viewController)
            navigationItem.rightBarButtonItems = controller.rightNavigationItems
        } else {
            controller.viewController.removeFromParent()
            navigationItem.rightBarButtonItems = nil
        }
    }
}

extension RootContactsController: RootViewModelDelegate {
    func vmShowController(_ controller: RootContactsTabViewController, animated: Bool) {
        if let current = currentController {
            setupController(current, asChild: false)
        }
        
        controller.navigationBarUpdater = self
        
        for v in contentView.subviews {
            v.removeFromSuperview()
        }
        contentView.addSubview(controller.view)
        controller.view.snp.remakeConstraints() { maker in
            maker.edges.equalToSuperview()
        }
        
        setupController(controller, asChild: true)
        currentController = controller
    }
    
    func vmReloadSegmentedBar() {
        segmentedControl.selectedSegment = viewModel.selectedSegmentIndex
    }
}

// MARK: - RootContactsNavigationBarUpdater
extension RootContactsController: RootContactsNavigationBarUpdater {
    func updateNavigationBarRightItems(for controller: RootContactsTabViewController) {
        navigationItem.setRightBarButtonItems(controller.rightNavigationItems, animated: false)
    }
}

// MARK: - XMSegmentedControlDelegate
extension RootContactsController: XMSegmentedControlDelegate {
    func xmSegmentedControl(_ xmSegmentedControl: XMSegmentedControl, selectedSegment: Int) {
        viewModel.setTabAtIndex(index: selectedSegment)
    }
}

// MARK: - ReloadContentController
extension RootContactsController: ReloadContentController {
    func reloadContentWhenSelectedAgainInBar() {
        if let reloadController = currentController as? ReloadContentController {
            reloadController.reloadContentWhenSelectedAgainInBar()
        }
    }
}

extension RootContactsController: RootBarExpandableProtocol {
    var expandedBarItems: [RootExpandedBarItem] {
        return viewModel.expandedBarItems
    }
}
