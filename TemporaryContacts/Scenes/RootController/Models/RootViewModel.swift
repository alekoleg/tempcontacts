//
//  RootViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 08.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData
import PhoneNumberKit
import RxSwift

protocol RootViewModelDelegate: class {
    func vmShowController(_ controller: RootContactsTabViewController, animated: Bool)
    func vmReloadSegmentedBar()
}

class RootViewModel {
    enum Tab: Int{
        case all
        case temp
        case groups
        
        static let allTabs: [Tab] = [.all, .temp, .groups]
        
        init(settingsTab: SettingsStartPage) {
            switch settingsTab {
            case .all:
                self = .all
            case .temp:
                self = .temp
            case .groups:
                self = .groups
            default:
                self = .all
            }
        }
        
        fileprivate var name: String {
            switch self {
            case .all:
                return "AllTab".localized
            case .groups:
                return "GroupsTab".localized
            case .temp:
                return "TempTab".localized
            }
        }
    }
    
    weak var delegate: RootViewModelDelegate?
    
    /// Роутер
    fileprivate let router: ContactsRouter
    /// Форматтер номеров
    private let partialFormatter: PartialFormatter
    private let communicationHelper: CommunicationHelper
    private let availabilityManager: AvailabilityManager
    fileprivate var settings: Settings
    /// выбранный таб
    fileprivate var tab: Tab
    /// Текущий отображаемый контроллер
    fileprivate var currentController: RootContactsTabViewController?
    /// Диспоуз
    fileprivate let disposeBag = DisposeBag()
    
    init(settings: Settings,
         router: ContactsRouter,
         communicationHelper: CommunicationHelper,
         partialFormatter: PartialFormatter,
         availabilityManager: AvailabilityManager)
    {
        self.communicationHelper = communicationHelper
        self.settings = settings
        self.router = router
        self.partialFormatter = partialFormatter
        self.availabilityManager = availabilityManager
        tab = Tab(settingsTab: settings.startPageType)
        router.delegate = self
    }
    
    // MARK: - getters
    
    var availableSegmentTitles: [String] {
        return Tab.allTabs.map({ $0.name })
    }
    
    var selectedSegmentIndex: Int {
        return tab.rawValue
    }
    
    fileprivate func controller(for tab: Tab) -> RootContactsTabViewController {
        switch tab {
        case .all:
            let resultsController: NSFetchedResultsController<Contacts> = CoreDataManager.instance
                .fetchedResultsControllerTyped(
                    entityName: "Contacts",
                    sortingDescriptors: settings.contactsSortingDescriptors(),
                    cacheName: nil,
                    sectionNameKeyPath: "sectionGroupingKey"
            )
            
            let config = ContactsListViewModelConfiguration(
                fetchedController: resultsController,
                filteringPredicate: nil,
                isSearchAvailable: true
            )
            let vm = ContactsListViewModel(
                configuration: config,
                settings: settings,
                router: router,
                communicationHelper: communicationHelper,
                partialFormatter: partialFormatter,
                availabilityManager: availabilityManager
            )
            vm.delegate = self
            return ContactsListController(viewModel: vm)
        case .temp:
            let resultsController: NSFetchedResultsController<Contacts> = CoreDataManager.instance
                .fetchedResultsControllerTyped(
                    entityName: "Contacts",
                    sortingDescriptors: settings.contactsSortingDescriptors(),
                    cacheName: nil,
                    sectionNameKeyPath: "sectionGroupingKey"
            )
            
            let noDataString = NSMutableAttributedString(string: "advice.temp-contacts.how-to-add".localized)
            let textAttachment = NSTextAttachment()
            textAttachment.image = #imageLiteral(resourceName: "temp_in_advice")
            let attachString = NSAttributedString(attachment: textAttachment)
            noDataString.append(attachString)
            
            let config = ContactsListViewModelConfiguration(
                fetchedController: resultsController,
                filteringPredicate: NSPredicate(format: "isTempContact == true"),
                isSearchAvailable: true,
                noDataAttributedString: noDataString
            )
            let vm = ContactsListViewModel(
                configuration: config,
                settings: settings,
                router: router,
                communicationHelper: communicationHelper,
                partialFormatter: partialFormatter,
                availabilityManager: availabilityManager
            )
            vm.delegate = self
            return ContactsListController(viewModel: vm)
        case .groups:
            let resultsController: NSFetchedResultsController<Groups> = CoreDataManager.instance.fetchedResultsControllerTyped(
                entityName: "Groups",
                sortingDescriptors: [NSSortDescriptor(key: "name", ascending: true)],
                cacheName: nil
            )
            switch settings.groupDisplayType {
            case .List:
                let vm = GroupsListViewModel(
                    router: router,
                    fetchedController: resultsController,
                    settings: settings,
                    availabilityManager: availabilityManager
                )
                return GroupsListController(viewModel: vm)
            case .Bars:
                let vm = GroupsCollectionViewModel(
                    fetchedController: resultsController,
                    router: router,
                    settings: settings,
                    communicationHelper: communicationHelper,
                    partialFormatter: partialFormatter,
                    availabilityManager: availabilityManager
                )
                return GroupsCollectionController(viewModel: vm)
            }
        }
    }
    
    var expandedBarItems: [RootExpandedBarItem] {
        return [
            RootExpandedBarItem(
                text: "root-search.expanded-bar-actions.all".localized,
                image: #imageLiteral(resourceName: "contacts_expanded_all"),
                action: { [weak self] in
                    self?.router.open(tab: .contacts)
                    self?.setTab(.all)
                    self?.delegate?.vmReloadSegmentedBar()
                }
            ),
            RootExpandedBarItem(
                text: "root-search.expanded-bar-actions.temp".localized,
                image: #imageLiteral(resourceName: "contacts_expanded_temp"),
                action: { [weak self] in
                    self?.router.open(tab: .contacts)
                    self?.setTab(.temp)
                    self?.delegate?.vmReloadSegmentedBar()
                }
            ),
            RootExpandedBarItem(
                text: "root-search.expanded-bar-actions.groups".localized,
                image: #imageLiteral(resourceName: "contacts_expanded_groups"),
                action: { [weak self] in
                    self?.router.open(tab: .contacts)
                    self?.setTab(.groups)
                    self?.delegate?.vmReloadSegmentedBar()
                }
            ),
            RootExpandedBarItem(
                text: "root-search.expanded-bar-actions.search".localized,
                image: #imageLiteral(resourceName: "contacts_expanded_search"),
                action: { [weak self] in
                    self?.router.open(tab: .contacts)
                    self?.setTab(.all)
                    self?.delegate?.vmReloadSegmentedBar()
                    if let contactsListController = self?.currentController as? ContactsListController {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            // Даем прогрузиться экрану и отлаяутиться
                            contactsListController.doExpandedBarAction(.search)
                        }
                    }
                }
            ),
            RootExpandedBarItem(
                text: "root-search.expanded-bar-actions.new-photo-contact".localized,
                image: #imageLiteral(resourceName: "contacts_expanded_new_photo"),
                action: { [weak self] in
                    self?.router.open(tab: .contacts)
                    self?.setTab(.all)
                    self?.delegate?.vmReloadSegmentedBar()
                    if let contactsListController = self?.currentController as? ContactsListController {
                        contactsListController.doExpandedBarAction(.newPhotoContact)
                    }
                }
            ),
            RootExpandedBarItem(
                text: "root-search.expanded-bar-actions.new-contact".localized,
                image: #imageLiteral(resourceName: "contacts_expanded_new_contact"),
                action: { [weak self] in
                    self?.router.open(tab: .contacts)
                    self?.setTab(.all)
                    self?.delegate?.vmReloadSegmentedBar()
                    if let contactsListController = self?.currentController as? ContactsListController {
                        contactsListController.doExpandedBarAction(.newContact)
                    }
                }
            ),
        ]
    }
    
    // MARK: - actions
    
    func setTabAtIndex(index: Int) {
        guard let tab = Tab(rawValue: index) else {
            return
        }
        setTab(tab)
    }
    
    /// Делает активным переданный таб и отображает необходимый кнотроллер
    func setTab(_ tab: Tab) {
        guard self.tab != tab else {
            /// Если тот же таб - просто возвращаем до рутового контроллера
            router.popToRootViewController()
            return
        }
        self.tab = tab
        showController(for: tab)
    }
    
    func showFirstTab() {
        showController(for: tab)
    }
    
    func settingsItemPressed() {
        router.showSettings(delegate: self)
    }
    
    /// Отображает контроллер подходящий под Таб
    ///
    /// - Parameter tab: табю контроллера
    /// - Returns: Возвращает показанынй контроллер
    @discardableResult fileprivate func showController(for tab: Tab) -> RootContactsTabViewController {
        let vc = controller(for: tab)
        delegate?.vmShowController(vc, animated: true)
        currentController = vc
        return vc
    }
}


extension RootViewModel: SettingsViewControllerDelegate {
    func settingsControllerDidUpdateSettings(_ settingsController: SettingsViewController, settings: Settings) {
        self.settings = settings
        showController(for: tab)
    }
}


extension RootViewModel: ContactsRouterDelegate {
    /// Роутер собирается отобразить информацию о системном контакте
    ///
    /// - Parameters:
    ///   - router: Модель роутера
    ///   - contact: Модель контакта
    func contactsRouterWillShowContactDetails(router: OpenDetailRouter, contact: Contacts) {
        if contact.isNew {
            contact.isNew = false
            ContactsManager.instance.updateContactDisplayInformation(contact)
        }
    }
}


extension RootViewModel: ContactsListDelegate {
    func contactsListDidSetTemp(_ vm: ContactsListViewModel, temp: Bool, for contact: Contacts) {
        if temp {
            router.showSelectPicker(
                selection: .Groups,
                moveToTemp: true,
                associatedContact: contact,
                context: contact.managedObjectContext,
                delegate: self
            )
        }
    }
}


extension RootViewModel: SelectpickerViewControllerDelegate {
    func selectPickerControllerDidSelectGroup(_ controller: SelectpickerViewController, selectedData: [NSFetchRequestResult]) {
        router.dismissController(controller)
        guard let contact = controller.associatedContact,
            let groups = selectedData as? [Groups] else {
                return
        }
        contact.groups = NSSet(array: groups)
        CoreDataManager.instance.saveContext()
    }
}
