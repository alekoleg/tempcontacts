//
//  RootContactsTabViewController.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

/// Протокол, описывающий подчиняемые контроллеры в RootViewController
protocol RootContactsTabViewController: NSObjectProtocol {
    /// Айтемы для отображения справа в UINavigationBar
    var rightNavigationItems: [UIBarButtonItem]? { get }
    var view: UIView! { get }
    var viewController: UIViewController { get }
    var navigationBarUpdater: RootContactsNavigationBarUpdater? { get set }
}


extension RootContactsTabViewController where Self: UIViewController {
    var viewController: UIViewController {
        return self
    }
    /// Стандартная реализация
    var navigationBarUpdater: RootContactsNavigationBarUpdater? { get { return nil } set {} }
}


/// Протокол для обратного общения подчиненных контроллеров с основным
/// Необходим для уведомления об обновлении списка айтемов в UINavigationBar
protocol RootContactsNavigationBarUpdater: class {
    func updateNavigationBarRightItems(for controller: RootContactsTabViewController)
}
