//
//  RootBarController.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit

private let bottomBarHeight: CGFloat = 60

protocol RootBarControllerDelegate: class {
    func controller(for index: Int) -> UIViewController
}

/// Основной контроллер, упраляющий нижним баром
class RootBarController: UIViewController {
    
    private let contentView = UIView()
    
    /// Бар
    let tabBar: RootTabBar
    /// Делегат
    weak var delegate: RootBarControllerDelegate?
    /// Диспоуз
    fileprivate let disposeBag = DisposeBag()
    /// Контсрейнт отношения низа контента к бару
    fileprivate var bottomToBarConstraint: Constraint?
    
    var selectedIndex: Int {
        get { return tabBar.selectedIndex }
        set { return tabBar.selectedIndex = newValue }
    }
    
    init(items: [RootTabBarItem]) {
        tabBar = RootTabBar(items: items)
        super.init(nibName: nil, bundle: nil)
        tabBar.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.orange
        view.addSubview(tabBar)
        tabBar.snp.remakeConstraints { maker in
            maker.left.right.equalToSuperview()
            maker.height.equalTo(bottomBarHeight)
            if #available(iOS 11.0, *) {
                maker.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            } else {
                maker.bottom.equalToSuperview()
            }
        }
        
        view.addSubview(contentView)
        contentView.snp.remakeConstraints { maker in
            maker.left.top.right.equalToSuperview()
            bottomToBarConstraint = maker.bottom.equalTo(tabBar.snp.top).constraint
            maker.bottom.equalToSuperview().priority(750)
        }
        
        setController(for: selectedIndex)
        self.view.bringSubviewToFront(tabBar)
        
        updateBadges()
        setupObservers()
    }
    
    // MARK: - getters
    
    fileprivate func controller(at index: Int) -> UIViewController {
        let vc: UIViewController
        if let cached = controllers[index] {
            vc = cached
        } else {
            guard let delegate = self.delegate else {
                return UIViewController()
            }
            
            vc = delegate.controller(for: index)
            controllers[index] = vc
        }
        return vc
    }
    
    fileprivate var barIsVisible: Bool {
        return bottomToBarConstraint?.isActive == true
    }

    // MARK: - actions
    
    private var controllers: [Int: UIViewController]  = [:]
    
    func openTab(at index: Int) {
        setController(for: index)
    }
    
    fileprivate func updateBadges() {
        for index in 0..<tabBar.items.count {
            let vc = controllers[index] ?? delegate?.controller(for: index)
            if let badgeProtocol = vc as? UpdateBadgesProtocol, let number = badgeProtocol.badgeNumber {
                tabBar.setBadge(badge: number, for: index)
            }
        }
    }
    
    fileprivate func setupObservers() {
        NotificationCenter.default.addObserver(
            forName: .rootBarUpdateBadges,
            object: nil,
            queue: nil
        ) { [weak self] _ in
            self?.updateBadges()
        }
    }
    
    private var tempBag: DisposeBag?
    
    fileprivate func setController(for index: Int) {
        if index == selectedIndex {
            if let vc = controllers[index] as? ReloadContentController {
                vc.reloadContentWhenSelectedAgainInBar()
            }
        }
        
        contentView.subviews.forEach { $0.removeFromSuperview() }
        
        let vc = controller(at: index)
        contentView.addSubview(vc.view)
        vc.view.snp.remakeConstraints { maker in
            maker.edges.equalToSuperview()
        }
        
        addChild(vc)
        selectedIndex = index
        
        hideBarifNeeded(for: vc, animated: false)
        let disposeBag = DisposeBag()
        if let nc = vc as? UINavigationController {
            nc.rx.willShow
                .subscribe(
                    onNext: { [weak self] (viewController: UIViewController, animated: Bool) in
                        self?.hideBarifNeeded(for: viewController, animated: animated)
                    }
                )
                .disposed(by: disposeBag)
        }
        tempBag = disposeBag
    }
    
    private func hideBarifNeeded(for controller: UIViewController, animated: Bool) {
        setBarVisible(visible: !controller.hidesBottomBarWhenPushed, animated: animated)
    }
    
    private func setBarVisible(visible: Bool, animated: Bool) {
        guard barIsVisible != visible else {
            return
        }
        
        if visible {
            bottomToBarConstraint?.activate()
            tabBar.snp.remakeConstraints { maker in
                maker.left.right.equalToSuperview()
                maker.height.equalTo(bottomBarHeight)
                if #available(iOS 11.0, *) {
                    maker.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
                } else {
                    maker.bottom.equalToSuperview()
                }
            }
        } else {
            bottomToBarConstraint?.deactivate()
            tabBar.snp.remakeConstraints { maker in
                maker.left.right.equalToSuperview()
                maker.top.equalTo(view.snp.bottom).offset(20)
                maker.height.equalTo(bottomBarHeight)
            }
        }
        
        if animated {
            UIView.animate(withDuration: 0.3) { [unowned self] in
                self.view.layoutIfNeeded()
            }
        }
    }
}

extension RootBarController: RootTabBarDelegate {
    func mainTabBarExpandableItems(tabBar: RootTabBar, at index: Int) -> [RootExpandedBarItem] {
        if let vc = controller(at: index) as? RootBarExpandableProtocol {
            return vc.expandedBarItems
        }
        return []
    }


    func mainTabBarDidSelectTab(tabBar: RootTabBar, at index: Int) {
        setController(for: index)
    }
}

extension Notification.Name {
    static let rootBarUpdateBadges = NSNotification.Name(rawValue: "rootBarUpdateBadges")
}
