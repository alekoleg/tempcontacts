//
//  RootExpandedBarItem.swift
//  TemporaryContacts
//
//  Created by Konshin on 22.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

struct RootExpandedBarItem {
    typealias Action = () -> Void
    
    let text: String
    let image: UIImage?
    let action: Action
}
