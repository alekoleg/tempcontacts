//
//  RootTabBarItem.swift
//  TemporaryContacts
//
//  Created by Konshin on 22.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

struct RootTabBarItem {
    let text: String
    let image: UIImage?
    let selectedImage: UIImage?
    let isExpanded: Bool
    
    init(text: String, image: UIImage? = nil, selectedImage: UIImage? = nil, isExpanded: Bool = false) {
        self.text = text
        self.image = image
        self.selectedImage = selectedImage
        self.isExpanded = isExpanded
    }
}
