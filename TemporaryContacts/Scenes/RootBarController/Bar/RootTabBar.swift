//
//  MainTabBar.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol RootTabBarDelegate: class {
    func mainTabBarDidSelectTab(tabBar: RootTabBar, at index: Int)
    func mainTabBarExpandableItems(tabBar: RootTabBar, at index: Int) -> [RootExpandedBarItem]
}

/// Бар основного меню
class RootTabBar: UIView {
    
    let items: [RootTabBarItem]

    /// Контейнер для всех кнопок
    private let buttonsContainer = UIView()
    fileprivate var buttons = [UIButton]()
    
    // MARK: - properties
    
    weak var delegate: RootTabBarDelegate?
    
    /// Выбранный индекс
    var selectedIndex: Int = 0 {
        didSet {
            updateSelection()
        }
    }
    
    // MARK: - initialization
    
    init(items: [RootTabBarItem]) {
        self.items = items
        super.init(frame: .zero)
        setup()
        updateSelection()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - getters
    
    // MARK: - actions
    
    func setBadge(badge: Int, for index: Int) {
        guard buttons.count > index else {
            return
        }
        
        buttons[index].tcBadgeNumber = badge
    }
    
    private func setup() {
        let longTap = UILongPressGestureRecognizer(target: self, action: #selector(handleLongTap(_:)))
        longTap.minimumPressDuration = 0.2
        longTap.delegate = self
        addGestureRecognizer(longTap)
        
        backgroundColor = ColorHelper.lightTintColor
        
        addSubview(buttonsContainer)
        buttonsContainer.snp.remakeConstraints { maker in
            maker.edges.equalToSuperview()
        }
        
        var lastContainer: UIView?
        
        for (index, item) in items.enumerated() {
            let container = UIView()
            buttonsContainer.addSubview(container)
            container.snp.remakeConstraints { maker in
                if !item.isExpanded {
                    // Если айтем расширенный - не ограничиваем его сверху
                    maker.top.equalToSuperview()
                }
   
                maker.top.bottom.equalToSuperview()
                if let last = lastContainer {
                    maker.left.equalTo(last.snp.right)
                    maker.width.equalTo(last)
                } else {
                    maker.left.equalToSuperview()
                }
                if index == items.count - 1 {
                    maker.right.equalToSuperview()
                }
            }
            
            let label = UILabel()
            label.text = item.text
            label.font = UIFont.appFont(size: 11, typeface: .medium)
            label.textColor = UIColor.white
            label.textAlignment = .center
            container.addSubview(label)
            label.snp.remakeConstraints { maker in
                maker.left.bottom.right.equalToSuperview()
                maker.height.equalTo(25)
            }
            
            let button = UIButton(type: .custom)
            container.addSubview(button)
            button.setImage(item.image, for: .normal)
            button.setImage(item.selectedImage, for: .selected)
            button.addTarget(self, action: #selector(tapToButton(_:)), for: .touchUpInside)
            button.adjustsImageWhenHighlighted = false
            
            if item.isExpanded {
                button.setBackgroundImage(#imageLiteral(resourceName: "tab_bar_contacts_background"), for: .normal)
            }
            
            container.addSubview(button)
            button.snp.remakeConstraints { maker in
                maker.centerX.equalToSuperview()
                if item.isExpanded {
                    maker.centerY.equalToSuperview().offset(-16)
                } else {
                    maker.centerY.equalToSuperview().offset(-8)
                }
            }
            
            lastContainer = container
            buttons.append(button)
        }
    }
    
    private func updateSelection() {
        for (index, button) in buttons.enumerated() {
            button.isSelected = index == selectedIndex
        }
    }
    
    @objc private func tapToButton(_ button: UIButton) {
        guard let tappedIndex = buttons.index(of: button) else {
            return
        }
        
        selectedIndex = tappedIndex
        delegate?.mainTabBarDidSelectTab(tabBar: self, at: tappedIndex)
        if let expandedView = expandedBar {
            setExpandedBarVisible(expandedBar: expandedView, visible: false)
            expandedBar = nil
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if let expandedBar = self.expandedBar, expandedBar.frame.contains(point) {
            let covertedPoint = convert(point, to: expandedBar)
            return expandedBar.hitTest(covertedPoint, with: event)
        }
        
        guard let superView = super.hitTest(point, with: event) else {
            if let expandedBar = self.expandedBar {
                setExpandedBarVisible(expandedBar: expandedBar, visible: false)
            }
            return nil
        }
        
        if superView is UIButton {
            return superView
        } else {
            for v in superView.subviews {
                if v is UIButton {
                    return v
                }
            }
        }
        
        return superView
    }

    @objc private func handleLongTap(_ tap: UILongPressGestureRecognizer) {
        switch tap.state {
        case .began where expandedBar == nil:
            let location = tap.location(in: self)
            guard let button = hitTest(location, with: nil) as? UIButton,
                let index = buttons.index(of: button),
                let expandableItems = delegate?.mainTabBarExpandableItems(tabBar: self, at: index),
                !expandableItems.isEmpty else {
                    return
            }
            
            let expandedBar = RootExpandedBar(items: expandableItems)
            expandedBar.delegate = self
            setExpandedBarVisible(expandedBar: expandedBar, visible: true)
            self.expandedBar = expandedBar
        default:
            if let expandedBar = self.expandedBar {
                expandedBar.handleRecognizer(tap)
            }
        }
        
        
    }
    
    private var expandedBar: RootExpandedBar?
    
    fileprivate func setExpandedBarVisible(expandedBar: RootExpandedBar, visible: Bool) {
        if visible {
            insertSubview(expandedBar, at: 0)
            
            expandedBar.alpha = 0
            expandedBar.snp.remakeConstraints { maker in
                maker.left.right.equalToSuperview()
                maker.bottom.equalTo(self.snp.bottom)
            }
            layoutIfNeeded()
            
            expandedBar.snp.remakeConstraints { maker in
                maker.left.right.equalToSuperview()
                maker.bottom.equalTo(self.snp.top)
            }
            setNeedsLayout()
            
            UIView.animate(withDuration: 0.3) { [unowned self] in
                self.layoutIfNeeded()
                expandedBar.alpha = 1
            }
        } else {
            expandedBar.snp.remakeConstraints { maker in
                maker.left.bottom.right.equalToSuperview()
            }
            
            setNeedsLayout()
            
            UIView.animate(
                withDuration: 0.3,
                animations: { [unowned self] in
                    expandedBar.alpha = 0
                    self.layoutIfNeeded()
            },
                completion: { _ in
                    expandedBar.removeFromSuperview()
            })
            self.expandedBar = nil
        }
    }
}

// MARK: - RootExpandedBarDelegate
extension RootTabBar: RootExpandedBarDelegate {
    func rootExpandedBarDidSelectItem(_ bar: RootExpandedBar, item: RootExpandedBarItem) {
        setExpandedBarVisible(expandedBar: bar, visible: false)
    }
}

// MARK: - UIGestureRecognizerDelegate
extension RootTabBar: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let location = touch.location(in: self)
        guard let button = hitTest(location, with: nil) as? UIButton,
            let index = buttons.index(of: button),
            let expandableItems = delegate?.mainTabBarExpandableItems(tabBar: self, at: index),
            !expandableItems.isEmpty else {
                return false
        }
        
        return true
    }
}
