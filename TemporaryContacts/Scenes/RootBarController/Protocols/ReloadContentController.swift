//
//  ReloadContentController.swift
//  TemporaryContacts
//
//  Created by Konshin on 19.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

/// Протокол для контроллеров, умеющих обновить контент при повторном отображении
protocol ReloadContentController {
    /// Обновление контента
    func reloadContentWhenSelectedAgainInBar()
}

extension UINavigationController: ReloadContentController {
    func reloadContentWhenSelectedAgainInBar() {
        if let reloadController = viewControllers.first as? ReloadContentController {
            reloadController.reloadContentWhenSelectedAgainInBar()
        }
    }
}
