//
//  UpdateBadgesProtocol.swift
//  TemporaryContacts
//
//  Created by Konshin on 19.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

/// Протокол объединяющий сущности, умеющие отдавать бюйджик для RootTabBar
protocol UpdateBadgesProtocol {
    var badgeNumber: Int? { get }
}

extension UINavigationController: UpdateBadgesProtocol {
    var badgeNumber: Int? {
        if let badgeProtocol = viewControllers.first as? UpdateBadgesProtocol {
            return badgeProtocol.badgeNumber
        } else {
            return nil
        }
    }
}
