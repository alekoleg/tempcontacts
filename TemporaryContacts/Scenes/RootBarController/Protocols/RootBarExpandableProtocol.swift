//
//  RootBarExpandableProtocol.swift
//  TemporaryContacts
//
//  Created by Konshin on 22.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

/// Протокол, отдаюдий айтемы для RootTabBar для открытия расширенного меню
protocol RootBarExpandableProtocol {
    var expandedBarItems: [RootExpandedBarItem] { get }
}

extension UINavigationController: RootBarExpandableProtocol {
    var expandedBarItems: [RootExpandedBarItem] {
        if let vc = viewControllers.first as? RootBarExpandableProtocol {
            return vc.expandedBarItems
        }
        return []
    }
}
