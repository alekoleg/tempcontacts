//
//  RootExpandedBar.swift
//  TemporaryContacts
//
//  Created by Konshin on 22.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

private let itemHeight: CGFloat = 60
private let maximumYOffset: CGFloat = 25

protocol RootExpandedBarDelegate: class {
    func rootExpandedBarDidSelectItem(_ bar: RootExpandedBar, item: RootExpandedBarItem)
}

class RootExpandedBar: UIView {

    /// Айтемы для отображения
    let items: [RootExpandedBarItem]
    /// Лейер-маска
    private let maskLayer = CAShapeLayer()
    /// Все кнопки
    private var buttons = [UIButton]()
    /// Делегат
    weak var delegate: RootExpandedBarDelegate?
    
    init(items: [RootExpandedBarItem]) {
        self.items = items
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - actions
    
    func handleRecognizer(_ recognizer: UIGestureRecognizer) {
        let location = recognizer.location(in: self)
        switch recognizer.state {
        case .changed:
            guard let button = hitTest(location, with: nil) as? UIButton else {
                for b in buttons {
                    b.isHighlighted = false
                }
                return
            }
            
            if button.isHighlighted {
                return
            }
            
            for b in buttons {
                b.isHighlighted = b == button
            }
        case .ended:
            guard let button = hitTest(location, with: nil) as? UIButton else {
                return
            }
            tapToButton(button)
        default:
            for b in buttons {
                b.isHighlighted = false
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateMask()
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let superView = super.hitTest(point, with: event) else {
            return nil
        }
        
        if superView is UIButton {
            return superView
        } else {
            for v in superView.subviews {
                if v is UIButton {
                    return v
                }
            }
        }
        
        return superView
    }
    
    private func setup() {
        backgroundColor = ColorHelper.lightTintColor.withAlphaComponent(0.95)
        layer.mask = maskLayer
        
        let roundRadius: CGFloat = 31
        let greenRound = UIView()
        greenRound.backgroundColor = ColorHelper.AppGreenColor()
        greenRound.layer.cornerRadius = roundRadius
        greenRound.layer.masksToBounds = true
        addSubview(greenRound)
        greenRound.snp.remakeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.centerY.equalTo(self.snp.bottom).offset(14)
            maker.width.height.equalTo(roundRadius * 2)
        }
        
        configureItems()
    }
    
    /// Обновляет маску
    private func updateMask() {
        let halfWidth = bounds.width / 2
        /// Расчет радиуса, основываясь на Теореме Пифагора
        let radius = (pow(halfWidth, 2) + pow(maximumYOffset, 2)) / (2 * maximumYOffset)
        let katetVertical = radius - maximumYOffset
        let center = CGPoint(x: halfWidth, y: radius)
        
        let triangleAngle = asin(katetVertical / radius)
        let startAngle = triangleAngle + CGFloat.pi
        let endAngle = CGFloat.pi * 2 - triangleAngle
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: bounds.height))
        path.addLine(to: CGPoint(x: 0, y: maximumYOffset))
        path.addArc(withCenter: center,
                    radius: radius,
                    startAngle: startAngle,
                    endAngle: endAngle,
                    clockwise: true)
        path.addLine(to: CGPoint(x: bounds.width, y: bounds.height))
        path.addLine(to: CGPoint(x: 0, y: bounds.height))
        maskLayer.path = path.cgPath
    }
    
    /// Задает айтемы
    private func configureItems() {
        let numberOfContainers = items.count
        let halfSize = Int(ceil(CGFloat(numberOfContainers) / 2))
        let numberOfLevels = halfSize
        let levelOffset = maximumYOffset / CGFloat(numberOfLevels)
        
        func level(for index: Int) -> Int {
            if index < halfSize {
                return index
            } else {
                return numberOfContainers - index - 1
            }
        }
        
        func verticalOffset(for index: Int) -> CGFloat {
            let indexLevel = level(for: index)
            return CGFloat(indexLevel) * levelOffset
        }
        
        var lastContainer: UIView?
        
        for (index, item) in items.enumerated() {
            let container = UIView()
            addSubview(container)
            container.snp.remakeConstraints { maker in
                let offsetFromBottom = verticalOffset(for: index)
                maker.top.greaterThanOrEqualToSuperview().offset(16)
                maker.height.equalTo(itemHeight)
                maker.bottom.equalToSuperview().offset(-offsetFromBottom)
                if let last = lastContainer {
                    maker.left.equalTo(last.snp.right)
                    maker.width.equalTo(last)
                } else {
                    maker.left.equalToSuperview().offset(16)
                }
                if index == items.count - 1 {
                    maker.right.equalToSuperview().offset(-16)
                }
            }
            
            let label = UILabel()
            label.text = item.text
            label.font = UIFont.appFont(size: 9, typeface: .bold)
            label.textColor = UIColor.white
            label.numberOfLines = 2
            label.textAlignment = .center
            container.addSubview(label)
            label.snp.remakeConstraints { maker in
                maker.left.bottom.right.equalToSuperview()
                maker.height.equalTo(23)
            }
            
            let button = UIButton(type: .system)
            container.addSubview(button)
            button.setImage(item.image?.withRenderingMode(.alwaysOriginal), for: .normal)
            button.addTarget(self, action: #selector(tapToButton(_:)), for: .touchUpInside)
            button.adjustsImageWhenHighlighted = true
            
            container.addSubview(button)
            button.snp.remakeConstraints { maker in
                maker.centerX.equalToSuperview()
                maker.bottom.equalTo(label.snp.top)
            }
            
            lastContainer = container
            buttons.append(button)
        }
    }
    
    @objc private func tapToButton(_ button: UIButton) {
        guard let index = buttons.index(of: button) else {
            return
        }
        let item = items[index]
        item.action()
        delegate?.rootExpandedBarDidSelectItem(self, item: item)
    }
}
