//
//  BusinessCardsCollectionCell.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol BusinessCardsCollectionCellDelegate: class {
    func businessCardsCellDidTapToRemove(_ cell: BusinessCardsCollectionCell)
}


class BusinessCardsCollectionCell: UICollectionViewCell {
    /// Режим отображения ячейки
    ///
    /// - common: Стандартный
    /// - focused: С фокусом на ячейке - контент уходит чуть вдаль
    /// - focusedWithRemoveButton: С фокусом и с отображением кнопки удаления
    enum Mode {
        case common
        case focused
        case focusedWithRemoveButton
    }
    
    /// Контейнер, где лежит картинка визитки
    fileprivate let cardContainer = UIView()
    /// Картинка визитки
    fileprivate let cardView = UIImageView()
    /// КНопка удаления ячейки
    fileprivate let removeButton = UIButton(type: .system)
    
    weak var delegate: BusinessCardsCollectionCellDelegate?
    
    /// Возможность интерактивного взаимодействия
    var isInteractive = false

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // MARK: - properties
    
    var style: BusinessCardsCollectionViewModel.Style = .common {
        didSet {
            updateStyle(style)
        }
    }
    
    private var mode: Mode = .common
    
    // MARK: - actions
    
    private func initialize() {
        layer.masksToBounds = true

        contentView.addSubview(cardContainer)
        cardContainer.snp.remakeConstraints() { maker in
            maker.edges.equalToSuperview()
        }
        
        cardView.contentMode = .scaleAspectFill
        cardView.clipsToBounds = true
        cardContainer.addSubview(cardView)
        cardView.snp.remakeConstraints() { maker in
            maker.edges.equalToSuperview()
        }
        
        removeButton.setImage(#imageLiteral(resourceName: "business_cards_remove").withRenderingMode(.alwaysOriginal), for: .normal)
        removeButton.contentEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        removeButton.addTarget(self, action: #selector(remove), for: .touchUpInside)
        contentView.addSubview(removeButton)
        removeButton.snp.remakeConstraints() { maker in
            maker.right.top.equalToSuperview().inset(10)
        }

        updateStyle(style)
    }
    
    private func updateStyle(_ style: BusinessCardsCollectionViewModel.Style) {
        switch style {
        case .common:
            layer.cornerRadius = 0
            removeButton.isHidden = false
        case .mini, .miniWithoutPaging:
            layer.cornerRadius = 8
            removeButton.isHidden = true
        }
    }
    
    func configure(with card: UIImage) {
        cardView.image = card
    }
    
    func setMode(_ mode: Mode, animated: Bool) {
        guard mode != self.mode else {
            return
        }
        let transform: CGAffineTransform
        switch mode {
        case .focused, .focusedWithRemoveButton:
            transform = CGAffineTransform(scaleX: 0.9, y: 0.8)
        default:
            transform = CGAffineTransform.identity
        }
        
        let removeButtonWillAppear = mode == .focusedWithRemoveButton
        
        if animated {
            if removeButtonWillAppear {
                removeButton.alpha = 0
                removeButton.isHidden = false
            }
            
            UIView.animate(
                withDuration: 0.1,
                animations: { [unowned self] in
                    self.cardContainer.transform = transform
                    self.removeButton.alpha = removeButtonWillAppear ? 1 : 0
                },
                completion: { [unowned self] _ in
                    if !removeButtonWillAppear {
                        self.removeButton.isHidden = true
                    }
            })
        } else {
            cardContainer.transform = transform
            removeButton.isHidden = !removeButtonWillAppear
        }
        self.mode = mode
    }
    
    // MARK: private
    
    @objc private func remove() {
        delegate?.businessCardsCellDidTapToRemove(self)
    }
}
