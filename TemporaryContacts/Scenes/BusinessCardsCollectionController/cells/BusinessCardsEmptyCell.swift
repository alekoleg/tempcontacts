//
//  BusinessCardsEmptyCell.swift
//  TemporaryContacts
//
//  Created by Konshin on 16.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

class BusinessCardsEmptyCell: UICollectionViewCell {
    private let emptyView = UIView()
    private let emptyViewImage = UIImageView()
    private let emptyViewLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // MARK: - properties
    
    var style: BusinessCardsCollectionViewModel.Style = .common {
        didSet {
            updateStyle(style)
        }
    }
    
    // MARK: - actions
    
    private func initialize() {
        layer.masksToBounds = true
        
        emptyView.backgroundColor = UIColor(hex: 0xEEEEF4)
        emptyView.layer.masksToBounds = true
        emptyView.layer.cornerRadius = 8
        
        emptyView.addSubview(emptyViewImage)
        emptyViewImage.snp.remakeConstraints() { maker in
            maker.centerX.equalToSuperview()
            maker.centerY.equalToSuperview().offset(-15)
        }
        
        emptyViewLabel.textColor = UIColor(hex: 0xD0CFDB)
        emptyViewLabel.text = "businnes-card.photo-place".localized
        emptyView.addSubview(emptyViewLabel)
        emptyViewLabel.snp.remakeConstraints() { maker in
            maker.centerX.equalTo(emptyViewImage)
            maker.top.equalTo(emptyViewImage.snp.bottom).offset(8)
        }
        
        backgroundView = emptyView

        updateStyle(style)
    }
    
    private func updateStyle(_ style: BusinessCardsCollectionViewModel.Style) {
        switch style {
        case .common:
            emptyViewLabel.font = UIFont.appFont(size: 18, typeface: .semibold)
            emptyViewImage.image = #imageLiteral(resourceName: "business_card_add_photo")
            layer.cornerRadius = 0
        case .mini, .miniWithoutPaging:
            emptyViewLabel.font = UIFont.appFont(size: 15, typeface: .semibold)
            emptyViewImage.image = #imageLiteral(resourceName: "business_card_add_photo_mini")
            layer.cornerRadius = 8
        }
    }
}
