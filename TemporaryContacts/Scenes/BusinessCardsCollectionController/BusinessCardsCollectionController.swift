//
//  BusinessCardsCollectionController.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import JTSImageViewController
import RxSwift
import RxCocoa
import RSKImageCropper

/// Отступы от края коллекции до ячейки
private let cellMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

/// Экран, отображающий визитки в виде коллекции + пагинатор снизу экрана
class BusinessCardsCollectionController: UIViewController {
    fileprivate let viewModel: BusinessCardsCollectionViewModel
    
    fileprivate let collectionView: UICollectionView
    private let pageController = UIPageControl()
    fileprivate var imagePicker: UIImagePickerController?
    
    fileprivate let disposeBag = DisposeBag()

    init(viewModel: BusinessCardsCollectionViewModel) {
        self.viewModel = viewModel
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        )
        
        super.init(nibName: nil, bundle: nil)
        
        viewModel.viewDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        
        view.addSubview(pageController)
        pageController.isUserInteractionEnabled = false
        pageController.pageIndicatorTintColor = UIColor(hex: 0xf1f1f1)
        pageController.currentPageIndicatorTintColor = UIColor(hex: 0xD8D8D8)

        collectionView.register(
            BusinessCardsEmptyCell.self,
            forCellWithReuseIdentifier: reuseIdentifier(for: .addNew)
        )
        let card: BusinessCardsCollectionViewModel.Card = (UIImage(), nil)
        collectionView.register(
            BusinessCardsCollectionCell.self,
            forCellWithReuseIdentifier: reuseIdentifier(for: .card(card))
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        view.addSubview(collectionView)
        
        DispatchQueue.main.async { [unowned self] in
            self.setupLayout(for: self.viewModel.style)
        }
        
        addObserving()
    }
    
    // MARK: - getters
    
    static func necessaryHeight(for width: CGFloat, style: BusinessCardsCollectionViewModel.Style) -> CGFloat {
        let pageControlHeight: CGFloat = self.pageControlHeight(for: style)
        return width / Settings.instance.businesCardsRatio + pageControlHeight
    }
    
    private static func pageControlHeight(for style: BusinessCardsCollectionViewModel.Style) -> CGFloat {
        switch style {
        case .common:
            return 30
        case .mini:
            return 20
        case .miniWithoutPaging:
            return 0
        }
    }
    
    fileprivate func reuseIdentifier(for item: BusinessCardsCollectionViewModel.Item) -> String {
        switch item {
        case .addNew:
            return "EmptyCell"
        case .card:
            return "CardCell"
        }
    }
    
    /// Нахождение в состоянии редактирования визиток
    fileprivate var inEditing = false

    // MARK: - actions
    
    fileprivate func addObserving() {
        self.collectionView.rx.observe(CGSize.self, "contentSize")
            .distinctUntilChanged(==)
            .subscribe(
            onNext: { [unowned self] _ in
                self.updatePages()
                self.scrollToPage(page: self.pageController.currentPage)
                
        })
            .disposed(by: disposeBag)
    }
    
    fileprivate func setupLayout(for style: BusinessCardsCollectionViewModel.Style) {
        switch style {
        case .common, .mini:
            view.addSubview(pageController)
            pageController.snp.remakeConstraints() { maker in
                maker.left.right.equalToSuperview()
                maker.bottom.equalToSuperview()
                maker.height.equalTo(BusinessCardsCollectionController.pageControlHeight(for: style))
            }
            collectionView.isScrollEnabled = true
        case .miniWithoutPaging:
            pageController.removeFromSuperview()
            collectionView.isScrollEnabled = false
            collectionView.setContentOffset(.zero, animated: false)
        }
        collectionView.snp.remakeConstraints() { maker in
            maker.top.equalToSuperview()
            maker.left.right.equalToSuperview()
            maker.width.equalTo(collectionView.snp.height)
                .multipliedBy(Settings.instance.businesCardsRatio)
            switch style {
            case .common, .mini:
                maker.bottom.equalTo(self.pageController.snp.top)
            case .miniWithoutPaging:
                maker.bottom.equalToSuperview()
            }
        }
    }
    
    fileprivate func updatePages() {
        let pageWidth = collectionView.bounds.size.width
        let offset = collectionView.contentOffset.x
        let contentWidth = collectionView.contentSize.width
        
        guard pageWidth > 0 else {
            return
        }
    
        pageController.numberOfPages = Int(contentWidth / pageWidth)
        pageController.currentPage = Int(offset / pageWidth + 0.5)
        pageController.updateCurrentPageDisplay()
    }
    
    fileprivate func scrollToPage(page: Int, animated: Bool = false) {
        pageController.currentPage = page
        let pageWidth = collectionView.bounds.size.width
        var contentOffset = collectionView.contentOffset
        contentOffset.x = pageWidth * CGFloat(page)
        collectionView.setContentOffset(contentOffset, animated: animated)
    }
    
    fileprivate func cropController(for image: UIImage) -> UIViewController {
        let cc = RSKImageCropViewController(image: image, cropMode: .custom)
        cc.isRotationEnabled = true
        cc.dataSource = self
        cc.moveAndScaleLabel.text = "businnes-card.crop.set-card-position".localized
        cc.delegate = self
        return cc
    }
    
    fileprivate func setCardsEditing(_ editing: Bool, animated: Bool) {
        guard inEditing != editing else {
            return
        }
        
        inEditing = editing
        for cell in collectionView.visibleCells {
            if let cell = cell as? BusinessCardsCollectionCell {
                cell.setMode(editing ? .focusedWithRemoveButton : .common, animated: animated)
            }
        }
    }
    
    private var focusedCellInfo: (ip: IndexPath, timerBag: DisposeBag)?
    
    @objc fileprivate func longPress(recognizer: UIGestureRecognizer) {
        let location = recognizer.location(in: collectionView)
        let indexPath = collectionView.indexPathForItem(at: location)
        switch recognizer.state {
        case .began:
            guard let ip = indexPath, let cell = collectionView.cellForItem(at: ip) as? BusinessCardsCollectionCell else {
                if inEditing {
                    setCardsEditing(false, animated: true)
                }
                return
            }
            let disposeBag = DisposeBag()
            let timer = Observable<Int>
                .interval(0.6, scheduler: MainScheduler.instance)
                .take(1)
            timer.subscribe(onNext: { [unowned self] _ in
                self.setCardsEditing(true, animated: true)
                self.focusedCellInfo = nil
            })
            .disposed(by: disposeBag)
            
            focusedCellInfo = (ip, disposeBag)
            cell.setMode(.focused, animated: true)
        case .cancelled, .failed, .ended:
            guard focusedCellInfo != nil,
                let ip = indexPath,
                let cell = collectionView.cellForItem(at: ip) as? BusinessCardsCollectionCell else {
                return
            }
            focusedCellInfo = nil
            cell.setMode(.common, animated: true)
        case .changed:
            if focusedCellInfo != nil && indexPath != focusedCellInfo?.ip {
                focusedCellInfo = nil
                if let ip = indexPath, let cell = collectionView.cellForItem(at: ip) as? BusinessCardsCollectionCell {
                    cell.setMode(.common, animated: true)
                }
            }
        default:
            break
        }
    }
}


extension BusinessCardsCollectionController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard viewModel.isEditingEnabled else {
            return false
        }
        
        if inEditing {
            return false
        }
        let location = touch.location(in: collectionView)
        let prevLocation = touch.previousLocation(in: collectionView)
        let indexPath = collectionView.indexPathForItem(at: location)
        return indexPath != nil && prevLocation == location
    }
}


extension BusinessCardsCollectionController: UICollectionViewDataSource, UICollectionViewDelegate {
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.cellItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = viewModel.cellItems[indexPath.item]
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: reuseIdentifier(for: item),
            for: indexPath
        )
        
        switch item {
        case .card(let card):
            if let cell = cell as? BusinessCardsCollectionCell {
                cell.configure(with: card.image)
                cell.style = viewModel.style
                cell.isInteractive = viewModel.isInteractive
                cell.setMode(inEditing ? .focusedWithRemoveButton : .common, animated: false)
                cell.delegate = self
            }
        case .addNew:
            if let cell = cell as? BusinessCardsEmptyCell {
                cell.style = viewModel.style
            }
            break
        }
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        setCardsEditing(false, animated: true)
        viewModel.didTap(at: indexPath.item)
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updatePages()
    }
}

// MARK: - FlowLayout
extension BusinessCardsCollectionController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return cellMargins
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargins.right + cellMargins.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        let byInsets = bounds.insetBy(
            dx: cellMargins.right,
            dy: cellMargins.top + cellMargins.bottom
        ).size
        return byInsets
    }
}


extension BusinessCardsCollectionController: BusinessCardsCollectionViewModelDelegate {
    func vmReloadData() {
        collectionView.reloadData()
    }
    
    func vmUpdateStyle(_ style: BusinessCardsCollectionViewModel.Style) {
        setupLayout(for: style)
        collectionView.reloadData()
    }
    
    func vmShowImagePicker() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "TakeCameraPhoto".localized, style: .default) { [unowned self] (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                self.showPicker(for: .camera)
            }
        }
        
        let photoLibraryAction = UIAlertAction(title: "LoadFromPhotoLibrary".localized, style: .default) { [unowned self] (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                self.showPicker(for: .photoLibrary)
            }
        }

        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel) { _ in }
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibraryAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = ColorHelper.AppTintColor()
        self.present(alertController, animated: true, completion: nil)
    }
    
    func vmShowFullScreenImage(image: UIImage, fromCellAtIndex: Int) {
        guard let cell = collectionView.cellForItem(at: IndexPath(item: fromCellAtIndex, section: 0)) else {
            return
        }
        
        let imageInfo = JTSImageInfo()
        imageInfo.image = image
        imageInfo.referenceRect = cell.frame
        imageInfo.referenceView = cell.superview
        
        let imageViewer = JTSImageViewController(
            imageInfo: imageInfo,
            mode: .image,
            backgroundStyle: .blurred
        )
        imageViewer?.show(from: self, transition: .fromOriginalPosition)
    }
    
    func vmDoUpdates(updates: [FetchedResultUpdate]) {
        collectionView.performBatchUpdates(
            { [unowned self] in
                for update in updates {
                    self.collectionView.applyFetchedResultsUpdate(update)
                }
            },
            completion: nil
        )
    }
    
    func vmScrollToPage(_ page: Int, animated: Bool) {
        scrollToPage(page: page, animated: animated)
    }
    
    // MARK: - private
    
    private func showPicker(for sourceType: UIImagePickerController.SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = sourceType
        self.present(imagePicker, animated: true, completion: {})
        // Сохраняем чтоб не релизился
        self.imagePicker = imagePicker
    }
}


extension BusinessCardsCollectionController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let vc = cropController(for: image)
            picker.pushViewController(vc, animated: true)
            switch picker.sourceType {
            case .photoLibrary, .savedPhotosAlbum:
                Metrica.instance.send(.businessCardIsCreatedFromGallery)
            case .camera:
                Metrica.instance.send(.businessCardIsCreatedFromCamera)
            @unknown default:
                fatalError()
            }
        } else{
            print("Something went wrong")
            picker.dismiss(animated: true, completion: nil)
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}


extension BusinessCardsCollectionController: RSKImageCropViewControllerDataSource, RSKImageCropViewControllerDelegate {
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        controller.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        viewModel.didSelectNewPhoto(croppedImage)
        controller.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewControllerCustomMaskRect(_ controller: RSKImageCropViewController) -> CGRect {
        let bounds = controller.view.bounds
        let cardWidth = bounds.size.width - 40
        let cardHeight = cardWidth / Settings.instance.businesCardsRatio
        return CGRect(
            x: (bounds.width - cardWidth) / 2,
            y: (bounds.height - cardHeight) / 2,
            width: cardWidth,
            height: cardHeight
        )
    }
    
    func imageCropViewControllerCustomMaskPath(_ controller: RSKImageCropViewController) -> UIBezierPath {
        let rect = imageCropViewControllerCustomMaskRect(controller)
        return UIBezierPath(rect: rect)
    }
    
    func imageCropViewControllerCustomMovementRect(_ controller: RSKImageCropViewController) -> CGRect {
        let rect = controller.maskRect
        return rect
    }
}


extension BusinessCardsCollectionController: BusinessCardsCollectionCellDelegate {
    func businessCardsCellDidTapToRemove(_ cell: BusinessCardsCollectionCell) {
        guard let ip = collectionView.indexPath(for: cell) else {
            return
        }
        
        viewModel.remove(at: ip)
    }
}
