//
//  BusinessCardsCollectionViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol BusinessCardsCollectionDelegate: class {
    func businessCardsCollectionDidUpdateImages(viewModel: BusinessCardsCollectionViewModel, cards: [BusinessCardsCollectionViewModel.Card])
    /// Нажали на ячейку при недоступности раскрытия визиток
    ///
    /// - Parameter viewModel: Ссылка на вьюмодель
    func businessCardsCollectionDidTapCardWhenNotExpandable(viewModel: BusinessCardsCollectionViewModel)
    
    /// Запрашивает делегата о возможности создать новую визитку
    ///
    /// - Parameter viewModel: Вьюмодель визиток
    /// - Returns: Если вернуть нет, но карточка не будет создаваться
    func businessCardsCollectionWillAddNewCard(viewModel: BusinessCardsCollectionViewModel) -> Bool
}


extension BusinessCardsCollectionDelegate {
    func businessCardsCollectionDidTapCardWhenNotExpandable(viewModel: BusinessCardsCollectionViewModel) {}
}


protocol BusinessCardsCollectionViewModelDelegate: UserNotificationProtocol {
    func vmReloadData()
    func vmUpdateStyle(_ style: BusinessCardsCollectionViewModel.Style)
    func vmShowImagePicker()
    func vmShowFullScreenImage(image: UIImage, fromCellAtIndex: Int)
    func vmDoUpdates(updates: [FetchedResultUpdate])
    func vmScrollToPage(_ page: Int, animated: Bool)
}


class BusinessCardsCollectionViewModel {
    typealias Card = (image: UIImage, date: Date?)
    
    /// Стиль отображения
    ///
    /// - common: Стандартный
    /// - mini: Уменьшенный шрифт и картинка загрушки
    /// - mini: mini + отображается только 1-я визитка
    enum Style {
        case common
        case mini
        case miniWithoutPaging
    }
    
    enum Item {
        case addNew
        case card(Card)
    }
    
    private var items: [Item] = []
    
    weak var delegate: BusinessCardsCollectionDelegate?
    weak var viewDelegate: BusinessCardsCollectionViewModelDelegate?
    
    // MARK: - properties
    
    /// Возможность раскрыть визитку на весь размер
    var isExpandable = false
    /// Возможность удалять визитки
    var isEditingEnabled = false
    /// Возможность интерактивного взаимодействия с визитками
    var isInteractive = false
    
    var cards: [Card] {
        get {
            return items.reduce([]) { acc, item in
                switch item {
                case .card(let card):
                    return acc + [card]
                default:
                    return acc
                }
            }
        }
        set {
            items = newValue.map({ Item.card($0) }) + [.addNew]
            viewDelegate?.vmReloadData()
        }
    }
    
    var style: Style = .common {
        didSet {
            guard style != oldValue else {
                return
            }
            viewDelegate?.vmUpdateStyle(style)
        }
    }
    
    // MARK: - initialization
    
    init(cards: [Card] = []) {
        items = cards.map({ Item.card($0) }) + [.addNew]
    }
    
    // MARK: - getters
    
    var cellItems: [Item] {
        return items
    }
    
    // MARK: - actions
    
    func didTap(at index: Int) {
        let item = items[index]
        switch item {
        case .addNew:
            guard delegate?.businessCardsCollectionWillAddNewCard(viewModel: self) != false else {
                return
            }
            viewDelegate?.vmShowImagePicker()
        case .card(let card):
            guard isExpandable && false else {
                delegate?.businessCardsCollectionDidTapCardWhenNotExpandable(viewModel: self)
                return
            }
            viewDelegate?.vmShowFullScreenImage(image: card.image, fromCellAtIndex: index)
        }
    }
    
    func didSelectNewPhoto(_ photo: UIImage) {
        let date = Date()
        cards.append((photo, date))
        let newIndex = cards.count - 1
        viewDelegate?.vmScrollToPage(newIndex, animated: true)
        delegate?.businessCardsCollectionDidUpdateImages(viewModel: self, cards: cards)
        NotificationCenter.default.post(name: .didLoadBusinessCard, object: nil)
    }
    
    func remove(at indexPath: IndexPath) {
        viewDelegate?.showAlert(
            title: "businnes-card.delete-alert.title".localized,
            message: "businnes-card.delete-alert.message".localized,
            actions: [
                UIAlertAction(
                    title: "Cancel".localized,
                    style: .cancel,
                    handler: nil
                ),
                UIAlertAction(
                    title: "Delete".localized,
                    style: .default,
                    handler: { [unowned self] _ in
                        let index = indexPath.item
                        self.items.remove(at: index)
                        self.viewDelegate?.vmDoUpdates(updates: [
                            .delete(indexPath)
                            ])
                        self.delegate?.businessCardsCollectionDidUpdateImages(viewModel: self, cards: self.cards)
                        Metrica.instance.send(.businessCardIsDeleted)
                })
            ]
        )
    }
}
