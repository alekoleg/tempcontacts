
//
//  FavoriteRouter.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 25/04/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import ContactsUI

class FavoritesRouter: NSObject, Router {
    
    typealias RootController = UINavigationController
    
    /// Зависимости
    let dependencies: DependenciesStorage
	/// Основной контроллер
	let rootViewController: UINavigationController

	private let favoriteVC:FavoritesController

	fileprivate var contactsVC:CNContactViewController?

	init(dependencies: DependenciesStorage) {
        self.dependencies = dependencies
        
		let vm = FavoriteViewModel()
		favoriteVC = FavoritesController(vm: vm)
		rootViewController = UINavigationController.init(rootViewController: favoriteVC)
		rootViewController.navigationBar.isTranslucent = false
		super.init()
		vm.router = self
		vm.delegate = favoriteVC
	}

	func showContact(favorite:Favorite) {
		guard let identifier = favorite.contact?.systemIdentifier,
			let systemContact = ContactsManager.instance.getContact(systemIdentifier: identifier) else {
				return
		}

		let vc = CNContactViewController(for: systemContact)
		let item = UIBarButtonItem(image: UIImage(named: "BackChevron"), style: .plain, target: self, action: #selector(FavoritesRouter.hideFromModal))
		item.tintColor = ColorHelper.AppGreenColor()
		vc.view.tintColor = ColorHelper.AppTintColor()
		vc.navigationItem.leftBarButtonItem = item
		let nc = UINavigationController(rootViewController: vc)
		nc.view.backgroundColor = ColorHelper.AppTintColor()
		nc.navigationBar.isTranslucent = false
		rootViewController.present(nc, animated: true, completion: nil)
		vc.delegate = self
	}
    
    // MARK: - Router
    
    var rootController: UINavigationController {
        return rootViewController
    }
}

extension FavoritesRouter: CNContactViewControllerDelegate {
	func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
		viewController.dismiss(animated: true, completion: nil)
	}

	func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool {
		return true
	}
}

extension FavoritesRouter {

	@objc func hideFromModal() {
		rootViewController.dismiss(animated: true, completion: nil)
	}
}

