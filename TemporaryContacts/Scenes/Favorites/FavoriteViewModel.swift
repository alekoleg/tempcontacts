//
//  FavoriteViewModel.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 25/04/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import Contacts
import CoreData
import UIKit

protocol FavoriteViewModelDelegate {

	func vmShowAlert(actions:[UIAlertAction])
	func vmCall(phone:String)
	func vmReload()
}

class FavoriteViewModel {

	var router:FavoritesRouter!

	fileprivate var dbController:NSFetchedResultsController<NSFetchRequestResult>?
	var delegate:FavoriteViewModelDelegate?

	init() {
		
	}

	func didSelected(contact:CNContact) {
		if contact.phoneNumbers.count == 1 {
			saveFavorite(contact: contact, phone: contact.phoneNumbers.first!)
		}

		if contact.phoneNumbers.count > 1 {
			askNumberForUse(contact: contact)
		}
        Metrica.instance.send(.contactIsAddedToFavorites)
	}

	func didSelected(index:Int) {
		let item = self.item(at: index)
		if var phone = item.phone?.digits {
			phone = phone.replacingOccurrences(of: " ", with: "")
			self.delegate?.vmCall(phone: phone)
		}
	}

	func delete(favorite:Favorite) {
		CoreDataManager.instance.managedObjectContext.delete(favorite)
		CoreDataManager.instance.saveContext()
		refetch()
	}

	func askNumberForUse(contact:CNContact) {

		let actions = contact.phoneNumbers.map { (phone) -> UIAlertAction in
			return UIAlertAction(title: phone.value.stringValue, style: .default, handler: { (_) in
				self.saveFavorite(contact: contact, phone: phone)
			})
		}

		self.delegate?.vmShowAlert(actions: actions)
	}

    /// обновить данные
	func refetch() {
		let fetchResultController = CoreDataManager.instance.fetchedResultsController(entityName: "Favorite", keyForSort: "order")
		do {
			try fetchResultController.performFetch()
			self.dbController = fetchResultController

            var shouldUpdate = false
            for object in dbController?.fetchedObjects as? [Favorite] ?? [] {
                guard let phoneIdentifier = object.phone?.systemIdentifier, let contact = object.contact else {
                    object.managedObjectContext?.delete(object)
                    shouldUpdate = true
                    continue
                }
                let updatedPhone = contact.phones?.filtered(using: NSPredicate(format: "systemIdentifier == %@", phoneIdentifier)).first as? Phones
                if updatedPhone?.digits != object.phone?.digits {
                    object.phone = updatedPhone
                    shouldUpdate = true
                }
            }

            if shouldUpdate {
                try fetchResultController.managedObjectContext.save()
                try fetchResultController.performFetch()
            }
		} catch {
			self.dbController = nil
		}

		self.delegate?.vmReload()
	}

	private func saveFavorite(contact:CNContact, phone:CNLabeledValue<CNPhoneNumber>) {

		let predicate = NSPredicate(format: "systemIdentifier == %@", contact.identifier)
		let request = NSFetchRequest<Contacts>(entityName: "Contacts")
		request.predicate = predicate

		let context = CoreDataManager.instance.managedObjectContext
		let result = try? context.fetch(request)

		if let contact = result?.first {
			let phoneSearched = contact.phones?.first(where: { (o:Any) -> Bool in
				let object = o as! Phones
				return object.systemIdentifier == phone.identifier
			}) as? Phones

			if let phoneSearched = phoneSearched {
				let favorite = NSEntityDescription.insertNewObject(forEntityName: "Favorite", into: context) as! Favorite
				favorite.phone = phoneSearched
				favorite.contact = contact
				favorite.order = 0
				CoreDataManager.instance.saveContext()
				refetch()
			}
		}
	}
    
    /// Перемещает объект избранного в сортировке
    ///
    /// - Parameters:
    ///   - fromIndex: Изначальный индекс объекта
    ///   - toIndex: Индекс, куда переместить
    func move(from fromIndex: Int, to toIndex: Int) {
        guard var favorites = dbController?.fetchedObjects as? [Favorite] else {
            return
        }
        
        let favorite = favorites.remove(at: fromIndex)
        favorites.insert(favorite, at: toIndex)
        for (index, f) in favorites.enumerated() {
            f.order = Int16(index)
        }

        CoreDataManager.instance.saveContext()
        try? dbController?.performFetch()
    }
}

/// TableView Data Source
extension FavoriteViewModel {

	func numberOfItems() -> Int {
		return dbController?.sections?[0].numberOfObjects ?? 0
	}

	func item(at index:Int) -> Favorite {
		let indexPath = IndexPath(row: index, section: 0)
		let object = self.dbController?.object(at: indexPath) as! Favorite
		return object
	}
}

