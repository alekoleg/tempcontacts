//
//  FavoritesController.swift
//  TemporaryContacts
//
//  Created by Konshin on 01.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import SnapKit
import ContactsUI

/// Экран избранных контактов/групп
class FavoritesController: UIViewController {

	let vm:FavoriteViewModel
	let tableView = UITableView()
	fileprivate lazy var communicationHelper: CommunicationHelper = CommunicationHelper(presenter: self)

	init(vm:FavoriteViewModel) {
		self.vm = vm
		super.init(nibName: nil, bundle: nil)
		initTabBar()
	}
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		self.isEditing = false
		self.title = NSLocalizedString("favorites.vc.title", comment: "")
		self.view.backgroundColor = UIColor.white

		self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Add"), style: .plain, target: self, action: #selector(FavoritesController.addNewContact))

		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.register(FavoriteCell.self, forCellReuseIdentifier: "FavoriteCell")
		self.tableView.tableFooterView = UIView()
		self.view.addSubview(self.tableView)
		self.tableView.snp.makeConstraints { (make) in
			make.edges.equalToSuperview()
		}
		self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        vm.refetch()
    }

	@objc func addNewContact() {
		let vc = CNContactPickerViewController()
		vc.delegate = self
		self.present(vc, animated: true, completion: nil)
	}
    
    //MARK: - tabBar
    
    private func initTabBar() {
        tabBarItem.title = "favorites.vc.tab-bar.title".localized
        tabBarItem.image = #imageLiteral(resourceName: "tab_bar_star")
    }

	override func setEditing(_ editing: Bool, animated: Bool) {
		super.setEditing(editing, animated: animated)
		self.tableView.setEditing(editing, animated: animated)
		let item = isEditing ?
			UIBarButtonItem(image: #imageLiteral(resourceName: "FavoriteDone"), style: .plain, target: self, action: #selector(FavoritesController.saveEditing)) :
			UIBarButtonItem(image: #imageLiteral(resourceName: "FavoriteEdit"), style: .plain, target: self, action: #selector(FavoritesController.enterEditing))
		navigationItem.rightBarButtonItem = item
	}

	@objc func enterEditing() {
		self.setEditing(true, animated: true)
	}

	@objc func saveEditing() {
		self.setEditing(false, animated: true)
	}
}

extension FavoritesController:FavoriteViewModelDelegate {

	func vmShowAlert(actions: [UIAlertAction]) {
		let vc = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		for action in actions {
			vc.addAction(action)
		}
		self.present(vc, animated: true, completion: nil)
	}

	func vmReload() {
		self.tableView.reloadData()
	}

	func vmCall(phone: String) {
		try? self.communicationHelper.normalizeAndCall(to: phone)
        Metrica.instance.send(.calledFromFavoritesScreen)
	}
}

extension FavoritesController: CNContactPickerDelegate {

	func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
		picker.dismiss(animated: true, completion: nil)
	}

	func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
		picker.dismiss(animated: true) { 
			self.vm.didSelected(contact: contact)
		}
	}
}

extension FavoritesController: UITableViewDelegate, UITableViewDataSource {

	// MARK: - Table view data source
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return vm.numberOfItems()
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 64
	}

	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		return 64
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let object = self.vm.item(at: indexPath.row)

		let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteCell", for: indexPath) as! FavoriteCell
		cell.delegate = self
		cell.configure(object: object)

		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		self.vm.didSelected(index: indexPath.row)
	}

    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
		return .none
	}

	func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
		return true
	}

	func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        vm.move(from: sourceIndexPath.row, to: destinationIndexPath.row)
	}

	func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
		return false
	}

	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		return true
	}

}

extension FavoritesController: FavoriteCellDelegate {

	func cellWantsOpen(favorite: Favorite) {

		self.vm.router.showContact(favorite: favorite)
	}

	func cellWantsDelete(favorite: Favorite) {
		self.vm.delete(favorite:favorite)
	}
}
