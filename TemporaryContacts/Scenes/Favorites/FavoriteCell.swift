//
//  FavoriteCell.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 28/04/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import SnapKit
import PhoneNumberKit

protocol FavoriteCellDelegate:class {

	func cellWantsOpen(favorite:Favorite)
	func cellWantsDelete(favorite:Favorite)
}

/// Размер стороны аватарки
private let avatarSide: CGFloat = {
    let screenWidth = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
    return screenWidth > 320 ? 42 : 36
}()

class FavoriteCell: UITableViewCell {

	let titleLabel = UILabel()
    let subTitleLabel = UILabel()
	let avatar = UIImageView()
	let	deleteButton = UIButton()
	let showContactButton = UIButton()
	let temporaryImage = UIImageView()

	private var favorite:Favorite?
	weak var delegate:FavoriteCellDelegate?
    
    fileprivate let partialFormatter = PartialFormatter()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)

		contentView.addSubview(avatar)
		avatar.layer.masksToBounds = true
		avatar.layer.cornerRadius = avatarSide / 2
        avatar.contentMode = .scaleAspectFill

		contentView.addSubview(showContactButton)
		showContactButton.setImage(#imageLiteral(resourceName: "FavoriteArrow"), for: .normal)
		showContactButton.addTarget(self, action: #selector(FavoriteCell.showContact), for: .touchUpInside)

		contentView.addSubview(deleteButton)
		deleteButton.setImage(#imageLiteral(resourceName: "removeRed"), for: .normal)
		deleteButton.addTarget(self, action: #selector(FavoriteCell.deleteFavorite), for: .touchUpInside)

		contentView.addSubview(temporaryImage)
		temporaryImage.image = #imageLiteral(resourceName: "RemoveFromTemp")
		temporaryImage.contentMode = .center

		contentView.addSubview(titleLabel)
		titleLabel.textColor = ColorHelper.AppTintColor()
        titleLabel.font = UIFont.appFont(size: 18, typeface: .semibold)
        
        contentView.addSubview(subTitleLabel)
        subTitleLabel.textColor = ColorHelper.AppTintColor()
        subTitleLabel.font = UIFont.appFont(size: 14, typeface: .light)
        
		self.shouldIndentWhileEditing = false
        
        separatorInset.left = avatarSide + 18 + 15

		self.setNeedsUpdateConstraints()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

    // MARK: - actions

	func configure(object:Favorite) {

		var title = object.contact?.firstName ?? ""
		title.append(" ")
		title.append(object.contact?.lastName ?? "")
		titleLabel.text = title

        subTitleLabel.text = object.phone?.digits.flatMap { digits in
            partialFormatter.formatPartial(digits)
        }

		if let avatarName = object.contact?.avatarFile {
			avatar.image = AppManager.instance.loadImageJPG(name: avatarName)
		}
		temporaryImage.isHidden = object.contact?.isTempContact == false
		self.favorite = object

		if avatar.image == nil {
			avatar.image = #imageLiteral(resourceName: "userPlaceholder")
		}
	}

	override func prepareForReuse() {
		super.prepareForReuse()
		avatar.image = #imageLiteral(resourceName: "userPlaceholder")
		temporaryImage.isHidden = true
	}

	override func updateConstraintsIfNeeded() {

		avatar.alpha = self.isEditing ? 0.0 : 1.0
		deleteButton.alpha = self.isEditing ? 1.0 : 0.0
		showContactButton.alpha = self.isEditing ? 0.0 : 1.0

		avatar.snp.remakeConstraints { (make) in
			make.left.equalTo(18)
			make.width.height.equalTo(avatarSide)
			make.centerY.equalToSuperview()
		}

		titleLabel.snp.remakeConstraints { (make) in
			make.top.equalTo(12)
			if isEditing {
				make.left.equalTo(deleteButton.snp.right)
			} else {
				make.left.equalTo(avatar.snp.right).offset(15)
			}
		}
        
        subTitleLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.right.equalTo(titleLabel)
        }

		deleteButton.snp.remakeConstraints { (make) in
			make.left.top.bottom.equalToSuperview()
			make.width.equalTo(49)
		}

		showContactButton.snp.remakeConstraints { (make) in
			make.top.right.bottom.equalToSuperview()
			make.width.equalTo(44)
		}

		temporaryImage.snp.remakeConstraints { (make) in
			make.top.bottom.equalToSuperview()
			if isEditing {
				make.right.equalToSuperview().offset(-10)
			} else {
				make.right.equalTo(showContactButton.snp.left)
			}
		}

		super.updateConstraints()
	}

	@objc func showContact() {
		if let f = favorite {
			self.delegate?.cellWantsOpen(favorite: f)
		}
	}

	@objc func deleteFavorite() {
		if let f = favorite {
			self.delegate?.cellWantsDelete(favorite: f)
		}
	}

	override func setEditing(_ editing: Bool, animated: Bool) {
		super.setEditing(editing, animated: animated)
		self.setNeedsUpdateConstraints()
		self.setNeedsLayout()
		UIView.animate(withDuration: 0.3) { 
			self.updateConstraintsIfNeeded()
			self.layoutIfNeeded()
		}
	}
}
