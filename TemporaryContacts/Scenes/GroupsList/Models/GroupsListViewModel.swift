//
//  GroupsListViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData

protocol GroupsListViewModelDelegate: FetchedResultsUpdatesConverterDelegate, UserNotificationProtocol {
    func vmReloadData()
}

class GroupsListViewModel {
    fileprivate let router: ContactsRouter
    private let fetchedController: NSFetchedResultsController<Groups>
    private let settings: Settings
    fileprivate let updateConverter = FetchedResultsUpdatesConverter()
    fileprivate let availabilityManager: AvailabilityManager
    
    weak var delegate: GroupsListViewModelDelegate? {
        didSet {
            updateConverter.delegate = delegate
        }
    }
    
    init(router: ContactsRouter,
         fetchedController: NSFetchedResultsController<Groups>,
         settings: Settings,
         availabilityManager: AvailabilityManager) {
        self.router = router
        self.fetchedController = fetchedController
        self.settings = settings
        self.availabilityManager = availabilityManager
        
        fetchedController.delegate = updateConverter
    }
    
    // MARK: - Getters
    
    func numberOfSections() -> Int {
        return fetchedController.sections?.count ?? 0
    }
    
    func numberOfItems(at section: Int) -> Int {
        guard let sections = fetchedController.sections, sections.count > section else {
            return 0
        }
        
        return sections[section].numberOfObjects
    }
    
    func item(at indexPath: IndexPath) -> GroupsListCellViewItem {
        let group = fetchedController.object(at: indexPath)
        let numberOfRows = numberOfItems(at: indexPath.section)
        let isLast = indexPath.row == numberOfRows - 1
        let hasNotifications = group.notifications.flatMap { $0.count > 0 } ?? false
        
        return GroupsListCellViewItem(
            text: group.name ?? "groups.list.group.unknown".localized,
            separatorIsVisible: !isLast,
            notificationIcon: hasNotifications ? #imageLiteral(resourceName: "group_cell_notification") : #imageLiteral(resourceName: "group_cell_notification_dim")
        )
    }
    
    // MARK: - actions
    
    func updateData() {
        do {
            try fetchedController.performFetch()
        } catch {
            print("Ошибка получения записей контактов")
        }
        delegate?.vmReloadData()
    }
    
    func didSelect(indexPath: IndexPath) {
        let group = fetchedController.object(at: indexPath)
        router.showGroupDetail(group)
    }
    
    func delete(at indexPath: IndexPath) {
        let group = fetchedController.object(at: indexPath)
        
        let acceptAction = UIAlertAction(
            title: "Yes".localized,
            style: .destructive,
            handler: { alert in
                ContactsManager.instance.delete(group: group,
                                                deleteTempContacts: true,
                                                saveContext: true)
        })
        let cancelAction = UIAlertAction(title: "DeleteGroupCancel".localized, style: .cancel) { [unowned self] (action) in
            if self.settings.showTips {
                self.delegate?.showAlert(
                    title: "",
                    message: "DeleteGroupHint".localized,
                    actions: [
                        UIAlertAction(
                            title: "Close".localized,
                            style: UIAlertAction.Style.cancel,
                            handler: nil
                        )
                    ]
                )
            }
        }
        
        delegate?.showAlert(
            title: "DeleteGroupTitle".localized,
            message: "DeleteGroupMessage".localized,
            actions: [acceptAction, cancelAction]
        )
    }
    
    func editNotification(at indexPath: IndexPath) {
        guard availabilityManager.canAddNewElement(.notifications) else {
            delegate?.showLimitationAlert(for: .notifications)
            return
        }
        let group = fetchedController.object(at: indexPath)
        
        router.showNotificationController(
            notification: nil,
            owner: .group(group),
            context: group.managedObjectContext,
            delegate: self
        )
    }
    
    func addNewGroup() {
        guard availabilityManager.canAddNewElement(.groups) else {
            delegate?.showLimitationAlert(for: .groups)
            return
        }
        router.showGroupCreator()
    }
}


extension GroupsListViewModel: EditNotificationViewControllerDelegate {
    func editNotificationControllerDidComplete(_ controller: EditNotificationViewController, with notificaion: AppNotification?) {
        router.dismissController(controller)
        guard let notification = notificaion, case .group(let group)? = controller.owner else {
            return
        }
        group.addToNotifications(notification)
        CoreDataManager.instance.saveContext(group.managedObjectContext)
    }
}
