//
//  GroupsListCellViewItem.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

struct GroupsListCellViewItem {
    let text: String
    let separatorIsVisible: Bool
    let notificationIcon: UIImage?
}
