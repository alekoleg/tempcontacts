//
//  GroupsListCell.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import MGSwipeTableCell

protocol GroupsListCellDelegate: class {
    func groupsListCellDidTapRemoveButton(_ cell: GroupsListCell)
    func groupsListCellDidTapNotification(cell: GroupsListCell)
}

class GroupsListCell: MGSwipeTableCell {
    @IBOutlet private var mainLabel: UILabel!
    @IBOutlet private var separator: UIView!
    @IBOutlet private var notificationButton: UIButton!
    
    weak var groupsListDelegate: GroupsListCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let deleteButton = MGSwipeButton(
            title: "",
            icon: #imageLiteral(resourceName: "BlueDelete"),
            backgroundColor: ColorHelper.AppRedColor(),
            insets: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 30)
        )
        deleteButton.callback = { [unowned self] _ in
            self.groupsListDelegate?.groupsListCellDidTapRemoveButton(self)
            return true
        }
        
        rightButtons = [deleteButton]
        rightSwipeSettings.transition = MGSwipeTransition.clipCenter
    }
    
    func configure(item: GroupsListCellViewItem) {
        mainLabel.text = item.text
        separator.isHidden = !item.separatorIsVisible
        notificationButton.setImage(item.notificationIcon, for: .normal)
    }
    
    @IBAction private func tapToNotification() {
        groupsListDelegate?.groupsListCellDidTapNotification(cell: self)
    }
}
