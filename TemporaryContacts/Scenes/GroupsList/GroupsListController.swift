//
//  GroupsListController.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

private let reuseIdentifier = "reuseIdentifier"

/// Экран групп контактов в виде списка
class GroupsListController: UITableViewController {
    fileprivate let viewModel: GroupsListViewModel
    
    init(viewModel: GroupsListViewModel) {
        self.viewModel = viewModel
        super.init(style: .plain)
        
        viewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = 58
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "GroupsListCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        
        viewModel.updateData()
    }
    
    // MARK: - actions
    
    @objc fileprivate func addNewGroup() {
        viewModel.addNewGroup()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return viewModel.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.numberOfItems(at: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        if let cell = cell as? GroupsListCell {
            let item = viewModel.item(at: indexPath)
            cell.configure(item: item)
            cell.groupsListDelegate = self
        }
        
        cell.selectionStyle = .none

        return cell
    }
    
    // MARK: - UITableView delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelect(indexPath: indexPath)
    }
}

extension GroupsListController: RootContactsTabViewController {
    var rightNavigationItems: [UIBarButtonItem]? {
        let add = UIBarButtonItem(
            image: #imageLiteral(resourceName: "Add"),
            style: .plain,
            target: self,
            action: #selector(addNewGroup)
        )
        return [add]
    }
}

// MARK: - GroupsListViewModelDelegate
extension GroupsListController: GroupsListViewModelDelegate {
    
    func vmReloadData() {
        tableView.reloadData()
    }
    
    func updatesConvertorHasReceivedfUpdates(_ convertor: FetchedResultsUpdatesConverter, updates: [FetchedResultUpdate]) {
        tableView.beginUpdates()
        for update in updates {
            tableView.applyFetchedResultsUpdate(update, with: .fade)
        }
        tableView.endUpdates()
    }
    
    func updatesConvertorHasReceivedError(_ convertor: FetchedResultsUpdatesConverter, error: Error) {
        tableView.reloadData()
    }
}


extension GroupsListController: GroupsListCellDelegate {
    func groupsListCellDidTapRemoveButton(_ cell: GroupsListCell) {
        guard let ip = tableView.indexPath(for: cell) else {
            return
        }
        
        viewModel.delete(at: ip)
    }
    
    func groupsListCellDidTapNotification(cell: GroupsListCell) {
        guard let ip = tableView.indexPath(for: cell) else {
            return
        }
        
        viewModel.editNotification(at: ip)
    }
}
