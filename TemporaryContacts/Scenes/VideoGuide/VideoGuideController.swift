//
//  VideoGuideController.swift
//  TemporaryContacts
//
//  Created by Aleksey on 07.10.2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

/// Экран с видео гидом
class VideoGuideController: UIViewController {
    
    typealias Action = (VideoGuideController) -> Void
    
    /// Кнопка завершения
    @IBOutlet fileprivate var youtubePlayer: YTPlayerView!
    /// Кнопка завершения
    @IBOutlet fileprivate var doneButton: UIButton!
    
    /// Действие по нажатию на пропустить
    var skipAction: Action?
    
    /// Действие по завершению просмотра
    var finishAction: Action?
    
    init() {
        super.init(nibName: "VideoGuideController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        doneButton.layer.borderWidth = 1
        doneButton.layer.borderColor = UIColor.white.cgColor
        doneButton.layer.cornerRadius = doneButton.bounds.height / 2
        
        youtubePlayer.delegate = self
        
        let playerVars = [
            "playsinline": 1,
            "rel": 0,
            "controls": 1
        ]
        youtubePlayer.load(withVideoId: "deCoLiYzWZw",
                           playerVars: playerVars)
        youtubePlayer.webView!.isOpaque = false
        youtubePlayer.webView?.backgroundColor = youtubePlayer.backgroundColor
    }
    
    /// Пропустит
    @IBAction func skip() {
        skipAction?(self)
    }

}

// MARK: - YTPlayerViewDelegate
extension VideoGuideController: YTPlayerViewDelegate {
    
    /// Видео загрузилось
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        if view.window != nil {
            playerView.playVideo()
        }
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        switch state {
        case .ended:
            finishAction?(self)
        default:
            break
        }
    }
}
