//
//  PhotoContactCreateControler.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import SnapKit
import JFMinimalNotifications

class PhotoContactCreateControler: UIViewController {
    private let firstNameField = UITextField()
    private let lastNameField = UITextField()
    private let specializationField = UITextField()
    
    fileprivate let viewModel: PhotoContactCreateViewModel
    fileprivate let cardsCollectionController: UIViewController
    
    weak var navigationDelegate: NavigationDelegate?
    
    init(viewModel: PhotoContactCreateViewModel) {
        self.viewModel = viewModel
        cardsCollectionController = viewModel.cardsCollectionController()
        super.init(nibName: nil, bundle: nil)
        
        hidesBottomBarWhenPushed = true
        viewModel.vmDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        initializeUI()
        addChild(cardsCollectionController)
    }

    // MARK: - actions
    
    private func initializeUI() {
        let titleImage = UIImageView(image: #imageLiteral(resourceName: "business_cards_title_icon"))
        titleImage.sizeToFit()
        navigationItem.titleView = titleImage
        
        view.backgroundColor = UIColor.white
        view.addSubview(cardsCollectionController.view)
        cardsCollectionController.view.snp.remakeConstraints() { maker in
            maker.left.top.right.equalToSuperview()
        }
        
        configureTextField(firstNameField, placeHolder: "photo-contact-creator.firstName".localized)
        view.addSubview(firstNameField)
        firstNameField.snp.remakeConstraints() { maker in
            maker.top.equalTo(cardsCollectionController.view.snp.bottom).offset(16)
            maker.left.right.equalToSuperview().inset(26)
        }
        createUnderline(under: firstNameField)
        
        configureTextField(lastNameField, placeHolder: "photo-contact-creator.lastName".localized)
        view.addSubview(lastNameField)
        lastNameField.snp.remakeConstraints() { maker in
            maker.top.equalTo(firstNameField.snp.bottom).offset(36)
            maker.left.right.equalToSuperview().inset(26)
        }
        createUnderline(under: lastNameField)
        
        configureTextField(specializationField, placeHolder: "photo-contact-creator.specialization".localized)
        view.addSubview(specializationField)
        specializationField.snp.remakeConstraints() { maker in
            maker.top.equalTo(lastNameField.snp.bottom).offset(36)
            maker.left.right.equalToSuperview().inset(26)
        }
        createUnderline(under: specializationField)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "photo-contact-creator.done".localized,
            style: .plain,
            target: self,
            action: #selector(done)
        )
    }
    
    private func configureTextField(_ tf: UITextField, placeHolder: String) {
        tf.font = UIFont.appFont(size: 18, typeface: .regular)
        tf.textColor = ColorHelper.AppDarkTextColor()
        tf.placeholder = placeHolder
        tf.delegate = self
        tf.addTarget(self, action: #selector(textFieldDidChangeText), for: .editingChanged)
    }
    
    private func createUnderline(under view: UIView) {
        let line = UIView()
        line.backgroundColor = UIColor(hex: 0x3F3D53)
        self.view.addSubview(line)
        line.snp.remakeConstraints() { maker in
            maker.left.right.equalTo(view)
            maker.top.equalTo(view.snp.bottom).offset(4)
            maker.height.equalTo(1)
        }
    }
    
    @objc private func textFieldDidChangeText(_ tf: UITextField) {
        switch tf {
        case firstNameField:
            viewModel.didUpdateName(tf.text ?? "")
        case lastNameField:
            viewModel.didUpdateLastName(tf.text ?? "")
        case specializationField:
            viewModel.didUpdateSpecialization(tf.text ?? "")
        default:
            return
        }
    }
    
    @objc private func done() {
        view.endEditing(true)
        viewModel.complete()
    }
    
    @objc override func tp_back () {
        viewModel.exit()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        
        guard let touch = touches.first else {
            return
        }
        let location = touch.location(in: view)
        if !(view.hitTest(location, with: event) is UITextField) {
            view.endEditing(true)
        }
    }
}


extension PhotoContactCreateControler: PhotoContactCreateViewModelDelegate {
    func vmAsController() -> UIViewController {
        return self
    }
    
    func vmShowMinimalNotification(title: String, subTitle: String?) {
        guard let notification = JFMinimalNotification(
            style: .custom,
            title: title,
            subTitle: subTitle,
            dismissalDelay: 2
            ) else {
                return
        }
        view.addSubview(notification)
        notification.delegate = self
        notification.tm_prepate()
        notification.show()
    }
}


extension PhotoContactCreateControler: JFMinimalNotificationDelegate {
    func minimalNotificationDidDismiss(_ notification: JFMinimalNotification!) {
        notification.removeFromSuperview()
    }
}


extension PhotoContactCreateControler: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
