//
//  PhotoContactCreateRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol PhotoContactCreateRouter: RemovingScreensRouter, AlertPresenterRouter {
    
}
