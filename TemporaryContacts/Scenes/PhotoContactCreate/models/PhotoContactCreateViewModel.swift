//
//  PhotoContactCreateViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData

protocol PhotoContactCreateDelegate: class {
    func photoContactDidCreate(contact: Contacts, businessCards: [BusinessCard])
}

protocol PhotoContactCreateViewModelDelegate: UserNotificationProtocol {
    func vmAsController() -> UIViewController
    func vmShowMinimalNotification(title: String, subTitle: String?)
}

class PhotoContactCreateViewModel {
    private let router: PhotoContactCreateRouter
    private let cardsCollectionViewModel: BusinessCardsCollectionViewModel
    fileprivate let contact: Contacts
    fileprivate let availabilityManager: AvailabilityManager
    fileprivate var cards: [BusinessCard] = []
    
    fileprivate let context = CoreDataManager.instance.tempContextOnMainTread
    
    weak var delegate: PhotoContactCreateDelegate?
    weak var vmDelegate: PhotoContactCreateViewModelDelegate?
    
    init(router: PhotoContactCreateRouter, availabilityManager: AvailabilityManager) {
        self.router = router
        self.cardsCollectionViewModel = BusinessCardsCollectionViewModel()
        self.availabilityManager = availabilityManager
        cardsCollectionViewModel.isExpandable = true
        // Создаем модель контакта
        contact = Contacts(context: context)
        cardsCollectionViewModel.delegate = self
    }
    
    // MARK: - getters
    
    func cardsCollectionController() -> UIViewController {
        return BusinessCardsCollectionController(viewModel: cardsCollectionViewModel)
    }
    
    // MARK: - actions
    
    func didUpdateName(_ name: String) {
        contact.firstName = name
    }
    
    func didUpdateLastName(_ lastName: String) {
        contact.lastName = lastName
    }
    
    func didUpdateSpecialization(_ text: String) {
        contact.specialization = text
    }
    
    func complete() {
        let contactIsEmpty = contact.lastName?.isEmpty != false
            && contact.firstName?.isEmpty != false
            && contact.specialization?.isEmpty != false
        guard let vmDelegate = vmDelegate else {
            return
        }
        if contactIsEmpty {
            vmDelegate.vmShowMinimalNotification(
                title: "photo-contact-creator.warning.populate".localized,
                subTitle: nil
            )
        } else {
            delegate?.photoContactDidCreate(contact: contact, businessCards: cards)
            router.dismissController(vmDelegate.vmAsController())
        }
    }
    
    func exit() {
        guard let delegate = vmDelegate else {
            return
        }
        let isEmpty = contact.lastName?.isEmpty != false
            && contact.firstName?.isEmpty != false
            && contact.specialization?.isEmpty != false
            && cards.isEmpty
        if isEmpty {
            router.dismissController(delegate.vmAsController())
        } else {
            let actions: [UIAlertAction] = [
                UIAlertAction(
                    title: "No".localized,
                    style: .cancel,
                    handler: nil
                ),
                UIAlertAction(
                    title: "Yes".localized,
                    style: .default,
                    handler: { [unowned self] _ in
                        self.router.dismissController(delegate.vmAsController())
                    }
                )
            ]
            router.showAlertController(
                title: "quit".localized,
                message: "photo-contact-creator.exit-without-save".localized,
                style: .alert,
                actions: actions
            )
        }
    }
    
}


extension PhotoContactCreateViewModel: BusinessCardsCollectionDelegate {
    func businessCardsCollectionDidUpdateImages(viewModel: BusinessCardsCollectionViewModel, cards: [BusinessCardsCollectionViewModel.Card]) {
        self.cards = cards.map() { card in
            return BusinessCard(image: card.image, creationDate: card.date, context: contact.managedObjectContext)
        }
    }

    /// Запрашивает делегата о возможности создать новую визитку
    ///
    /// - Parameter viewModel: Вьюмодель визиток
    /// - Returns: Если вернуть нет, но карточка не будет создаваться
    func businessCardsCollectionWillAddNewCard(viewModel: BusinessCardsCollectionViewModel) -> Bool {
        guard availabilityManager.canAddNewElement(.businessCards) else {
            vmDelegate?.showLimitationAlert(for: .businessCards)
            return false
        }
        return true
    }
}
