//
//  ContactsListRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol ContactsListRouter: CreateEntityRouter, BusinessCardsListRouter, NotificationListRouter {
    
}

extension Router where Self: ContactsListRouter, RootController == UINavigationController {
    
}
