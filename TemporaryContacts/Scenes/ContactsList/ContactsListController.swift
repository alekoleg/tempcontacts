//
//  ContactsListController.swift
//  TemporaryContacts
//
//  Created by Konshin on 08.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import JFMinimalNotifications

private let reuseIdentifier = "reuseIdentifier"
private let headerReuseIdentifier = "headerReuseIdentifier"

/// Экран списка контактов
class ContactsListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    fileprivate let viewModel: ContactsListViewModel
    
    let tableView = UITableView()
    /// Лейбла, отображающаяся при отсутствии данных
    fileprivate let noDataLabel = UILabel()
    fileprivate let searchBar = CustomSearchBar(frame: .zero)
    
    /// Делегат на обновление навигешйен бара
    weak var navigationBarUpdater: RootContactsNavigationBarUpdater?
    
    fileprivate var noDataLabelMode = ContactsListViewModel.NoDataLabelMode.hidden {
        didSet {
            guard oldValue != noDataLabelMode else {
                return
            }
            updateNoDataLabelMode()
        }
    }
    
    init(viewModel: ContactsListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        
        viewModel.vmDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if viewModel.isSearchAvailable {
            searchBar.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: 84)
            searchBar.backgroundColor = UIColor.white
            searchBar.placeholder = "search".localized
            searchBar.font = UIFont.appFont(size: 18, typeface: .light)
            searchBar.delegate = self
            view.addSubview(searchBar)
            searchBar.snp.remakeConstraints() { maker in
                maker.left.top.right.equalToSuperview()
                maker.height.equalTo(60)
            }
        }

        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 120
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.sectionIndexColor = UIColor(hex: 0x5E5D72)
        tableView.sectionIndexBackgroundColor = UIColor.clear
        tableView.keyboardDismissMode = .onDrag
        
        tableView.register(ContactsExpandableCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.register(
            UINib(nibName: "ContactsListHeaderView", bundle: nil),
            forHeaderFooterViewReuseIdentifier: headerReuseIdentifier
        )
        view.addSubview(tableView)
        
        noDataLabel.textColor = UIColor(hex: 0xC7C7CD)
        noDataLabel.numberOfLines = 0
        noDataLabel.textAlignment = .center
        noDataLabel.font = UIFont.systemFont(ofSize: 19)
        noDataLabel.attributedText = viewModel.configuration.noDataAttributedString
        
        updateNoDataLabelMode()
        
        viewModel.updateData()
    }
    
    fileprivate var isNeedReloadDataOnAppear = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isNeedReloadDataOnAppear {
            isNeedReloadDataOnAppear = false
            vmReloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.updateData()
    }
    
    // MARK: - getters

    internal func businessControllerView(at indexPath: IndexPath) -> UIView {
        let vm = viewModel.businesCardsViewModel(at: indexPath)
        let vc = BusinessCardsCollectionController(viewModel: vm)
        addChild(vc)
        return vc.view
    }
    
    // MARK: - actions
    
    @objc fileprivate func addNewContact(barItem: UIBarButtonItem) {
        let vc = DataSelectorController(items: viewModel.addNewContactSelectionItems())
        vc.tableView.bounces = false
        vc.rowHeight = 40
        vc.modalPresentationStyle = .popover
        vc.popoverPresentationController?.barButtonItem = barItem
        vc.popoverPresentationController?.permittedArrowDirections = .up
        vc.popoverPresentationController?.delegate = self
        vc.preferredContentSize = CGSize(width: 200, height: vc.contentHeight)
        present(vc, animated: true, completion: nil)
    }

    /// Отображаемые даннные обновились
    func dataDidUpdate() {
        noDataLabelMode = viewModel.noDataLabelMode
    }
    
    private func updateNoDataLabelMode() {
        switch noDataLabelMode {
        case .atCenter:
            tableView.tableFooterView = nil
            tableView.backgroundView = noDataLabel
            tableView.snp.remakeConstraints() { maker in
                maker.left.bottom.right.equalToSuperview()
                if viewModel.isSearchAvailable {
                    maker.top.equalTo(searchBar.snp.bottom)
                } else {
                    maker.top.equalToSuperview()
                }
            }
        case .atBottom:
            tableView.backgroundView = nil
            noDataLabel.frame.size.height = 180
            tableView.tableFooterView = noDataLabel
            tableView.snp.remakeConstraints() { maker in
                maker.left.bottom.right.equalToSuperview()
                if viewModel.isSearchAvailable {
                    maker.top.equalTo(searchBar.snp.bottom)
                } else {
                    maker.top.equalToSuperview()
                }
            }
        case .hidden:
            tableView.backgroundView = nil
            tableView.tableFooterView = nil
            tableView.snp.remakeConstraints() { maker in
                maker.left.bottom.right.equalToSuperview()
                if viewModel.isSearchAvailable {
                    maker.top.equalTo(searchBar.snp.bottom)
                } else {
                    maker.top.equalToSuperview()
                }
            }
        }
    }
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return viewModel.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let number = viewModel.numberOfItems(at: section)
        return number
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        if let contactCell = cell as? ContactsExpandableCell {
            let item = viewModel.item(at: indexPath)
            var expandedView: UIView?
            if item.isExpanded {
                expandedView = businessControllerView(at: indexPath)
            }
            contactCell.configure(item, expandedView: expandedView, delegate: self)
        }
        
        cell.selectionStyle = .none

        return cell
    }

    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelect(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerReuseIdentifier) as? ContactsListHeaderView else {
            return nil
        }
        let item = viewModel.sectionNameItem(at: section)
        header.configure(item: item)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
        
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return viewModel.sectionIndexTitles()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let baseHeight: CGFloat = 76
        
        if !viewModel.itemIsExpanded(at: indexPath) {
            return baseHeight
        } else {
            let businesCardsWidth = tableView.bounds.size.width - 15 - 64
            let height = businesCardsWidth / Settings.instance.businesCardsRatio + 10
            return height + baseHeight
        }
    }
}


extension ContactsListController: ContactsListViewModelDelegate {
    func vmReloadData() {
        tableView.reloadData()
        dataDidUpdate()
    }
    
    func vmChangeExpandState(_ indexPath: IndexPath, toOpen: Bool) {
        tableView.reloadRows(at: [indexPath], with: .automatic)
        if toOpen {
            tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
        }
    }
    
    func vmReloadIndexPaths(_ indexPaths: [IndexPath]) {
        tableView.reloadRows(at: indexPaths, with: .automatic)
        dataDidUpdate()
    }

    func updatesConvertorHasReceivedfUpdates(_ convertor: FetchedResultsUpdatesConverter, updates: [FetchedResultUpdate]) {

        if updates.count > 5 {
            tableView.reloadData()
        } else {
            tableView.beginUpdates()
            for update in updates {
                tableView.applyFetchedResultsUpdate(update, with: .fade)
            }
            tableView.endUpdates()
        }
        dataDidUpdate()
    }
    
    func updatesConvertorHasReceivedError(_ convertor: FetchedResultsUpdatesConverter, error: Error) {
        tableView.reloadData()
        dataDidUpdate()
    }
    
    func vmShowAlert(title: String?, message: String?, actions: [UIAlertAction]) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        for action in actions {
            alertController.addAction(action)
        }
        alertController.view.tintColor = ColorHelper.AppDarkTextColor()
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func vmShowMinimalNotification(title: String, subTitle: String?) {
        guard let notification = JFMinimalNotification(
            style: .custom,
            title: title,
            subTitle: subTitle,
            dismissalDelay: 2
            ) else {
                return
        }
        view.addSubview(notification)
        notification.delegate = self
        notification.tm_prepate()
        notification.show()
    }
    
    func vmIsVisible() -> Bool {
        return isViewLoaded && view.window != nil
    }
    
    func vmSetNeedsReloadOnAppear() {
        isNeedReloadDataOnAppear = true
    }
    
    func vmReloadBar() {
        navigationBarUpdater?.updateNavigationBarRightItems(for: self)
    }
    
    func vmSetSearchBarFirstResponder() {
        _ = searchBar.becomeFirstResponder()
    }
}


// MARK: - RootContactsTabViewController
extension ContactsListController: RootContactsTabViewController {
    var rightNavigationItems: [UIBarButtonItem]? {
        let add = UIBarButtonItem(
            image: #imageLiteral(resourceName: "Add"),
            style: .plain,
            target: self,
            action: #selector(addNewContact)
        )
        return [add]
    }
}


// MARK: - ContactsListCellDelegate
extension ContactsListController: ContactsListCellDelegate {
    func contactsListCellDidTapToActionButton(_ cell: ContactsExpandableCell, action: ContactsExpandableCell.ContactListAction) {
        guard let ip = tableView.indexPath(for: cell) else {
            return
        }
        viewModel.doCellAction(action, at: ip)
    }

    func baseContactCellDidTapAction(_ cell: BaseContactCell, action: BaseContactCell.BaseAction) {
        guard let ip = tableView.indexPath(for: cell) else {
            return
        }
        viewModel.doBaseCellACtion(action, at: ip)
    }
}


// MARK: - CustomSearchBarDelegate
extension ContactsListController: CustomSearchBarDelegate {
    func customSearchTextDidChanged(_ searchBar: CustomSearchBar) {
        viewModel.searchWithTerm(searchBar.text ?? "")
    }
}


// MARK: - JFMinimalNotificationDelegate
extension ContactsListController: JFMinimalNotificationDelegate {
    func minimalNotificationDidDismiss(_ notification: JFMinimalNotification!) {
        notification.removeFromSuperview()
    }
}


// MARK: - UIPopoverPresentationControllerDelegate
extension ContactsListController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

// MARK: - ReloadContentController
extension ContactsListController: ReloadContentController {
    func reloadContentWhenSelectedAgainInBar() {
        searchBar.text = ""
        viewModel.searchWithTerm("")
    }
}

// MARK: - ExpandedBarACtions
extension ContactsListController {
    func doExpandedBarAction(_ action: ContactsListViewModel.ExpandedBarAction) {
        viewModel.doExpandedBarAction(action)
    }
}
