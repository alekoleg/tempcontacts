//
//  BaseContactCellItem.swift
//  TemporaryContacts
//
//  Created by Konshin on 23.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

class BaseContactCellItem {
    let mainText: String?
    let subText: String?
    let showSeparator: Bool
    let rightButtonIcon: UIImage?
    let isTempObject: Bool
    let isSwipable: Bool
    let avatarImage: UIImage?
    
    init(mainText: String?,
         subText: String?,
         showSeparator: Bool,
         rightButtonIcon: UIImage?,
         avatarImage: UIImage?,
         isTempObject: Bool,
         isSwipable: Bool) {
        self.mainText = mainText
        self.subText = subText
        self.showSeparator = showSeparator
        self.rightButtonIcon = rightButtonIcon
        self.isTempObject = isTempObject
        self.isSwipable = isSwipable
        self.avatarImage = avatarImage
    }
}

