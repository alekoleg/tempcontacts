//
//  ContactsListCellItem.swift
//  TemporaryContacts
//
//  Created by Konshin on 08.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import RxSwift

class ContactsListCellItem: BaseContactCellItem {
    let isExpanded: Bool
    
    init(mainText: String?,
         subText: String?,
         showSeparator: Bool,
         rightButtonIcon: UIImage?,
         avatarImage: UIImage?,
         isTempObject: Bool,
         isSwipable: Bool,
         isExpanded: Bool)
    {
        self.isExpanded = isExpanded
        super.init(
            mainText: mainText,
            subText: subText,
            showSeparator: showSeparator,
            rightButtonIcon: rightButtonIcon,
            avatarImage: avatarImage,
            isTempObject: isTempObject,
            isSwipable: isSwipable
        )
    }
}
