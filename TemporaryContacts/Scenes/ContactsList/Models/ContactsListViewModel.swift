//
//  ContactsListViewModel.swift
//  TemporaryContacts
//
//  Created by Konshin on 08.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData
import Contacts
import RxSwift
import PhoneNumberKit

protocol ContactsListDelegate: class {
    func contactsListDidSetTemp(_ vm: ContactsListViewModel, temp: Bool, for contact: Contacts)
}

protocol ContactsListViewModelDelegate: ControllerProtocol, FetchedResultsUpdatesConverterDelegate, UserNotificationProtocol {
    /// Обновляет весь список контактов
    func vmReloadData()
    /// Обновляет опеределенные индексы
    func vmReloadIndexPaths(_ indexPaths: [IndexPath])
    /// Обновляет ячейку ради открытия/закрытия
    func vmChangeExpandState(_ indexPath: IndexPath, toOpen: Bool)
    /// Показыает уведомлялку сверху
    func vmShowMinimalNotification(title: String, subTitle: String?)
    /// Задает необходимость обновить данные при отображении
    func vmSetNeedsReloadOnAppear()
    /// Возвращает true, если контроллер загружен и отображен
    func vmIsVisible() -> Bool
    /// Обновляет айтемы в навигейшен баре
    func vmReloadBar()
    /// Задает фоокус редактирвоания на серч бар
    func vmSetSearchBarFirstResponder()
}

class ContactsListViewModel: NSObject {
    enum ExtraInfoType {
        case specialization
        case mainPhone
        case mainEmail
    }
    
    let fetchedController: NSFetchedResultsController<Contacts>
    /// Настройки для вьюмодели
    let configuration: ContactsListViewModelConfiguration
    /// Конвертатор обновления базы контактов
    fileprivate let updateConvertor = FetchedResultsUpdatesConverter()
    /// Роутер
    internal var router: ContactsListRouter
    fileprivate let partialFormatter: PartialFormatter
    /// Помощник работы с URLами
    let communicationHelper: CommunicationHelper
    
    fileprivate let availabilityManager: AvailabilityManager
    
    fileprivate var businesCardsViewModels = [Contacts: BusinessCardsCollectionViewModel]()
    
    /// Делегат вьюмодели
    weak var vmDelegate: ContactsListViewModelDelegate? {
        didSet {
            updateConvertor.delegate = vmDelegate
        }
    }
    
    /// Делегат экрана
    weak var delegate: ContactsListDelegate?
    
    /// Настройки приложения
    private(set) var settings: Settings
    
    /// Список идентификаторов, которые раскрыты
    private var expandedIdentifiers = Set<SystemIdentifier>()
    
    /// Создает вьюмодель для работы со списком контактов
    ///
    /// - Parameters:
    ///   - configuration: Настройки вьюмодели
    ///   - settings: Настройки приложения
    ///   - router: Роутер
    ///   - communicationHelper: Менеджер по работе с коммуникациями
    ///   - partialFormatter: Помощник для работы с номерами
    ///   - availabilityManager: Менеджер по работе с доступностями пользователя
    init(configuration: ContactsListViewModelConfiguration,
        settings: Settings,
        router: ContactsListRouter,
        communicationHelper: CommunicationHelper,
        partialFormatter: PartialFormatter,
        availabilityManager: AvailabilityManager)
    {
        self.router = router
        self.isSearchAvailable = configuration.isSearchAvailable
        self.fetchedController = configuration.fetchedController
        self.configuration = configuration
        self.settings = settings
        self.communicationHelper = communicationHelper
        self.partialFormatter = partialFormatter
        self.availabilityManager = availabilityManager
        super.init()
        
        fetchedController.delegate = updateConvertor
        fetchedController.fetchRequest.predicate = originalPredicate
        
        setupObservers()
        updateData()
    }
    
    // MARK: - properties
    
    let isSearchAvailable: Bool
    
    /// Возможность удалять контакты и задавать временность контакта
    var isContactsListEditable = true
    
    /// Тип инфомрации на ячейках под именем контакта
    var extraInfoType: ExtraInfoType = .specialization
    
    /// Возможность раскрывать информацию о визитках
    var isExpandEnabled: Bool = true {
        didSet {
            if !isExpandEnabled && !expandedIdentifiers.isEmpty {
                expandedIdentifiers.removeAll()
                vmDelegate?.vmReloadData()
            }
        }
    }
    
    // MARK: - Getters
    
    func numberOfSections() -> Int {
        return fetchedController.sections?.count ?? 0
    }
    
    func numberOfItems(at section: Int) -> Int {
        guard let sections = fetchedController.sections, sections.count > section else {
            return 0
        }
        
        return sections[section].numberOfObjects
    }
    
    func identifier(at indexPath: IndexPath) -> SystemIdentifier? {
        return fetchedController.object(at: indexPath).systemIdentifier
    }
    
    func item(at indexPath: IndexPath) -> ContactsListCellItem {
        let contact = fetchedController.object(at: indexPath)
        let numberOfRows = numberOfItems(at: indexPath.section)
        let isLast = indexPath.row == numberOfRows - 1

        let expanded: Bool
        if isExpandEnabled, let contactIdentifier = contact.systemIdentifier, expandedIdentifiers.contains(contactIdentifier) {
            expanded = true
        } else {
            expanded = false
        }
        
        let (name, specialization, isTemp) = contactMainProperties(contact: contact)
        
        let subText: String?
        switch extraInfoType {
        case .specialization:
            subText = specialization
        case .mainPhone:
            subText = contact.mainPhone?.digits
                .flatMap({ partialFormatter.formatPartial($0) })
        case .mainEmail:
            subText = contact.mainEmail?.email
        }
        
        return ContactsListCellItem(
            mainText: name,
            subText: subText,
            showSeparator: !isLast,
            rightButtonIcon: expanded ? #imageLiteral(resourceName: "business_card_cell_compress") : #imageLiteral(resourceName: "business_card_cell_expand"),
            avatarImage: contact.avatarFile.flatMap { AppManager.instance.loadImageJPG(name: $0) },
            isTempObject: isTemp,
            isSwipable: isContactsListEditable && !expanded,
            isExpanded: expanded
        )
    }
    
    func contactMainProperties(contact: Contacts) -> (name: String, subText: String?, isTemp: Bool) {
        return (contact.displayName ?? "", contact.displaySpecialization ?? "", contact.isTempContact)
    }
    
    func sectionNameItem(at index: Int) -> ContactsListHeaderView.Item {
        return fetchedController.sections?[index].name ?? ""
    }
    
    func sectionIndexTitles() -> [String]? {
        return fetchedController.sectionIndexTitles.filter({ $0.count == 1 })
    }
    
    func itemIsExpanded(at indexPath: IndexPath) -> Bool {
        let contact = fetchedController.object(at: indexPath)
        if let contactIdentifier = contact.systemIdentifier {
            return expandedIdentifiers.contains(contactIdentifier)
        } else {
            return false
        }
    }
    
    func businesCardsViewModel(at indexPath: IndexPath) -> BusinessCardsCollectionViewModel {
        let contact = fetchedController.object(at: indexPath)
        let vm: BusinessCardsCollectionViewModel
        if let cached = businesCardsViewModels[contact] {
            vm = cached
        } else {
            vm = BusinessCardsCollectionViewModel()
            vm.isExpandable = false
            vm.style = .mini
            vm.delegate = self
            businesCardsViewModels[contact] = vm
        }
        
        let cards: [BusinessCardsCollectionViewModel.Card] = contact.orderedCards
            .compactMap({ card -> BusinessCardsCollectionViewModel.Card? in
                if let image = card.image {
                    return (image, card.creationDate as Date?)
                } else {
                    return nil
                }
            })
        vm.cards = cards

        return vm
    }
    
    func addNewContactSelectionItems() -> [DataSelectorItem] {
        return [
            DataSelectorItem(
                image: #imageLiteral(resourceName: "contact_create_photo"),
                text: "contacts-list.create-contact.photo".localized,
                action: { [unowned self] in
                    self.router.showPhotoContactCreator(delegate: self)
                }
            ),
            DataSelectorItem(
                image: #imageLiteral(resourceName: "contact_create_common"),
                text: "contacts-list.create-contact.standart".localized,
                action: { [unowned self] in
                    self.router.showContactCreator(delegate: self)
                }
            )
        ]
    }
    
    fileprivate var originalPredicate: NSPredicate? {
        return configuration.filteringPredicate
    }
    
    // MARK: - actions
    
    func updateData() {
        do {
            try fetchedController.performFetch()
        } catch {
            print("Ошибка получения записей контактов")
        }
        vmDelegate?.vmReloadData()
    }
    
    func didSelect(indexPath: IndexPath) {
        let contact = fetchedController.object(at: indexPath)
        try? router.showContactInfo(contact: contact, editable: true, delegate: self)
    }
    
    func searchWithTerm(_ term: String) {
        if term.isEmpty {
            fetchedController.fetchRequest.predicate = originalPredicate
        } else {
            let fieldsForSearch = settings.contactSearchFields
            let predicates = fieldsForSearch.map() { field in
                return NSPredicate(format: "\(field) CONTAINS[cd] %@", term)
            }
            let predicate = NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
            if let mainPredicate = originalPredicate {
                fetchedController.fetchRequest.predicate = NSCompoundPredicate(
                    andPredicateWithSubpredicates: [mainPredicate, predicate]
                )
            } else {
                fetchedController.fetchRequest.predicate = predicate
            }
        }
        updateData()
    }
    
    // MARK: - cell actions
    
    func doBaseCellACtion(_ action: BaseContactCell.BaseAction, at indexPath: IndexPath) {
        switch action {
        case .remove:
            delete(at: indexPath)
        case .tempSwitch:
            switchTempState(at: indexPath)
        case .rightButtonTapped:
            guard let systemIdentifier = fetchedController.object(at: indexPath).systemIdentifier else {
                return
            }
            let toOpen: Bool
            if expandedIdentifiers.contains(systemIdentifier) {
                expandedIdentifiers.remove(systemIdentifier)
                toOpen = false
            } else {
                expandedIdentifiers.insert(systemIdentifier)
                toOpen = true
            }
            vmDelegate?.vmChangeExpandState(indexPath, toOpen: toOpen)
        }
    }
    
    func doCellAction(_ action: ContactsExpandableCell.ContactListAction, at indexPath: IndexPath) {
        switch action {
        case .message:
            let contact = fetchedController.object(at: indexPath)
            guard let phones = contact.phones?.allObjects as? [Phones] else {
                return
            }
            try? communicationHelper.selectPhone(from: phones, andMakeAction: .message)
        case .notification:
            guard availabilityManager.canAddNewElement(.notifications) else {
                vmDelegate?.showLimitationAlert(for: .notifications)
                return
            }
            let contact = fetchedController.object(at: indexPath)
            router.showNotificationController(
                notification: nil,
                owner: .contact(contact),
                context: contact.managedObjectContext,
                delegate: self
            )
        }
    }
    
    // MARK: private
    
    /// Настройка подписки на нотификации
    private func setupObservers() {
        NotificationCenter.default.addObserver(
            forName: Notification.Name.contantsDidUpdateFromPhoneBook,
            object: nil,
            queue: nil
        ) { _ in
            DispatchQueue.main.async { [weak self] in
                self?.updateData()
            }
        }
        
        NotificationCenter.default.addObserver(
            forName: Notification.Name.businessCardsDidUpdate,
            object: nil,
            queue: nil
        ) { _ in
            // Небольшая задержка чтоб понять, находимся ли мы на
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                guard let delegate = self?.vmDelegate, !delegate.vmIsVisible() else {
                    return
                }
                delegate.vmSetNeedsReloadOnAppear()
            }
        }
        
        NotificationCenter.default.addObserver(
            forName: Notification.Name.didChangeNewNotificationsState,
            object: nil,
            queue: nil
        ) { [weak self] _ in
            self?.vmDelegate?.vmReloadBar()
        }
        
        NotificationCenter.default.addObserver(
            forName: UIApplication.willEnterForegroundNotification,
            object: nil,
            queue: nil
        ) { [weak self] _ in
            self?.vmDelegate?.vmReloadBar()
        }
        
        NotificationCenter.default.addObserver(
            forName: Notification.Name.didSelectShortCutItemSearchContact,
            object: nil,
            queue: nil
        ) { [weak self] _ in
            guard self?.isSearchAvailable == true && self?.vmDelegate?.vmIsVisible() == true else {
                return
            }
            self?.vmDelegate?.vmSetSearchBarFirstResponder()
        }
    }
    
    private func delete(at indexPath: IndexPath) {
        let contact = fetchedController.object(at: indexPath)
        
        let acceptAction = UIAlertAction(title: "Delete".localized, style: .destructive) { _ in
            ContactsManager.instance.delete(contact: contact)
            
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
        
        vmDelegate?.showAlert(
            title: "DeleteContactTitle".localized,
            message: "DeleteContactMessage".localized,
            actions: [acceptAction, cancelAction]
        )
    }
    
    private func switchTempState(at indexPath: IndexPath) {
        let contact = fetchedController.object(at: indexPath)
        let temp = !contact.isTempContact
        if temp && !availabilityManager.canAddNewElement(.tempContacts) {
            vmDelegate?.showLimitationAlert(for: .tempContacts)
            return
        }
        
        if temp {
            Metrica.instance.send(.markContatAsTemporary)
        }
        
        contact.isTempContact = temp
        CoreDataManager.instance.saveContext(fetchedController.managedObjectContext)
        delegate?.contactsListDidSetTemp(self, temp: temp, for: contact)
        
        if !temp && settings.showTips {
            vmDelegate?.vmShowMinimalNotification(
                title: "\("TempDeleteHintTitle".localized)\n\("TempDeleteHintSubtitle".localized)",
                subTitle: nil
            )
        }
    }
}

// MARK: - Контекстное меню
extension ContactsListViewModel {
    
    var contextMenuBadgeIcon: UIImage? {
        if Settings.instance.numberOfNewNotifications > 0 {
            return #imageLiteral(resourceName: "contacts_context_menu_badge").withRenderingMode(.alwaysOriginal)
        } else {
            return nil
        }
    }
}

// MARK: - ContactConverterDelegate
extension ContactsListViewModel: ContactConverterDelegate {
    func contactConverterDidCompleteWithContact(converter: ContactControllerDelegateConverter, contact: CNContact?) {
        guard let contact = contact else {
            return
        }
        switch converter.type {
        case .creation:
            ContactsManager.instance.addNewContactToDB(contact)
            Metrica.instance.send(.commonContactIsCreated)
        case .editing:
            ContactsManager.instance.updateContactInDB(contact)
        }
        
    }
}

// MARK: - PhotoContactCreateDelegate
extension ContactsListViewModel: PhotoContactCreateDelegate {
    func photoContactDidCreate(contact: Contacts, businessCards: [BusinessCard]) {
        let context = contact.managedObjectContext
        // Создаем новый системный контакт
        let newSystemContact = ContactsManager.instance.addContact(dbContact: contact)
        // Удаляем созданный объект
        context?.delete(contact)
        
        guard let systemContact = newSystemContact else {
            return
        }
        // Сохраняем системный контакт в базу
        ContactsManager.instance.addNewContactToDB(systemContact, businessCards: businessCards, context: context)
        Metrica.instance.send(.photoContactIsCreated)
    }
}

// MARK: - EditNotificationViewControllerDelegate
extension ContactsListViewModel: EditNotificationViewControllerDelegate {
    func editNotificationControllerDidComplete(_ controller: EditNotificationViewController, with notificaion: AppNotification?) {
        router.dismissController(controller)
        // На этом экране мы только создаем новые, поэтому уведомление обьязано быть
        guard let notification = notificaion, case .contact(let contact)? = controller.owner else {
            return
        }

        contact.addToNotifications(notification)
        CoreDataManager.instance.saveContext(contact.managedObjectContext)
    }
}

// MARK: - BusinessCardsCollectionDelegate
extension ContactsListViewModel: BusinessCardsCollectionDelegate {
    /// Запрашивает делегата о возможности создать новую визитку
    ///
    /// - Parameter viewModel: Вьюмодель визиток
    /// - Returns: Если вернуть нет, но карточка не будет создаваться
    func businessCardsCollectionWillAddNewCard(viewModel: BusinessCardsCollectionViewModel) -> Bool {
        guard availabilityManager.canAddNewElement(.businessCards) else {
            vmDelegate?.showLimitationAlert(for: .businessCards)
            return false
        }
        return true
    }

    fileprivate func contactForCardsVM(_ vm: BusinessCardsCollectionViewModel) -> Contacts? {
        for (key, value) in businesCardsViewModels {
            if value === vm {
                return key
            }
        }
        return nil
    }
    
    func businessCardsCollectionDidUpdateImages(viewModel: BusinessCardsCollectionViewModel, cards: [BusinessCardsCollectionViewModel.Card]) {
        guard let contact = contactForCardsVM(viewModel),
            let context = contact.managedObjectContext else
        {
            return
        }
        let businessCards = cards.map() { image, date in
            return BusinessCard(image: image, creationDate: date, context: context)
        }
        
        contact.setBusinessCards(cards: businessCards)
        CoreDataManager.instance.saveContext(context)
    }
    
    func businessCardsCollectionDidTapCardWhenNotExpandable(viewModel: BusinessCardsCollectionViewModel) {
        guard let contact = contactForCardsVM(viewModel) else {
            return
        }
        router.showPhotoContactDetail(contact)
    }
}

// MARK: - Expanded bar actions
extension ContactsListViewModel {
    
    enum ExpandedBarAction {
        case search
        case newPhotoContact
        case newContact
    }
    
    func doExpandedBarAction(_ action: ExpandedBarAction) {
        switch action {
        case .search:
            vmDelegate?.vmSetSearchBarFirstResponder()
        case .newContact:
            self.router.showContactCreator(delegate: self)
        case .newPhotoContact:
            self.router.showPhotoContactCreator(delegate: self)
        }
    }
}

// MARK: - No data label
extension ContactsListViewModel {
    
    enum NoDataLabelMode {
        case hidden
        case atBottom
        case atCenter
    }
    
    /// Режим отображения лейблы с инфомрацией об отсутсвии данных
    var noDataLabelMode: NoDataLabelMode {
        guard configuration.noDataAttributedString?.string.isEmpty == false else {
            return .hidden
        }
        
        let hasData = numberOfSections() != 0
        let hasNotOriginalPredicate = fetchedController.fetchRequest.predicate != originalPredicate
        if !hasData && !hasNotOriginalPredicate {
            return .atCenter
        }
        
        let allContacts = fetchedController.fetchedObjects ?? []
        let allDataIsExamples = allContacts.reduce(true) { acc, contact in
            return acc && contact.isExample
        }
        if hasData && allDataIsExamples {
            return .atBottom
        }
        
        return .hidden
    }
}

extension String {
    fileprivate func transformToPredicate() -> NSPredicate {
        return NSPredicate(format: self)
    }
}
