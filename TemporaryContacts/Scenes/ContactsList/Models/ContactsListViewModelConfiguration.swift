//
//  ContactsListViewModelConfiguration.swift
//  TemporaryContacts
//
//  Created by Konshin on 23.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import CoreData

/// Настройки для вьюмодели экрана списка контактов
struct ContactsListViewModelConfiguration {
    /// Контроллер контактов CoreData
    let fetchedController: NSFetchedResultsController<Contacts>
    /// Фильтрующий предикат
    let filteringPredicate: NSPredicate?
    /// Доступность поиска по контактам
    let isSearchAvailable: Bool
    /// Текст при отсутствии данных
    let noDataAttributedString: NSAttributedString?
    
    init(fetchedController: NSFetchedResultsController<Contacts>,
         filteringPredicate: NSPredicate?,
         isSearchAvailable: Bool,
         noDataAttributedString: NSAttributedString? = nil) {
        self.fetchedController = fetchedController
        self.filteringPredicate = filteringPredicate
        self.isSearchAvailable = isSearchAvailable
        self.noDataAttributedString = noDataAttributedString
    }
}
