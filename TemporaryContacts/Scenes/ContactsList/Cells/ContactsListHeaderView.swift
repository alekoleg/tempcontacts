//
//  ContactsListHeaderView.swift
//  TemporaryContacts
//
//  Created by Konshin on 08.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

class ContactsListHeaderView: UITableViewHeaderFooterView {
    
    typealias Item = String

    @IBOutlet private var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = UIColor.white
    }
    
    func configure(item: Item) {
        label.text = item
    }
}
