//
//  ContactsListCell.swift
//  TemporaryContacts
//
//  Created by Konshin on 08.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import SnapKit
import MGSwipeTableCell
import RxSwift

protocol ContactsListCellDelegate: BaseContactCellDelegate {
    func contactsListCellDidTapToActionButton(_ cell: ContactsExpandableCell, action: ContactsExpandableCell.ContactListAction)
}

/// Расширяемая ячейка для списка контактов
class ContactsExpandableCell: BaseContactCell {
    enum ContactListAction {
        case message
        case notification
    }
    
    private let expandedView = UIView()
    private let expandedContentView = UIView()
    private let messageButton = UIButton()
    private let notificationButton = UIButton()
    
    private var isExpanded = true

    weak var listDelegate: ContactsListCellDelegate? {
        didSet {
            baseDelegate = listDelegate
        }
    }
    
    // MARK: - actions
    
    override func initialize() {
        super.initialize()
        
        setExpanded(expanded: false)
        
        messageButton.setImage(#imageLiteral(resourceName: "business_card_cell_message"), for: .normal)
        messageButton.addTarget(self, action: #selector(messageTapped), for: .touchUpInside)
        expandedView.addSubview(messageButton)
        messageButton.snp.remakeConstraints() { maker in
            maker.top.equalToSuperview().offset(9)
            maker.right.equalToSuperview().offset(-13)
        }
        
        notificationButton.setImage(#imageLiteral(resourceName: "business_card_cell_notification"), for: .normal)
        notificationButton.addTarget(self, action: #selector(notificationTapped), for: .touchUpInside)
        expandedView.addSubview(notificationButton)
        notificationButton.snp.remakeConstraints() { maker in
            maker.top.equalTo(messageButton.snp.bottom).offset(9)
            maker.right.equalToSuperview().offset(-13)
        }
        
        expandedView.addSubview(expandedContentView)
        expandedContentView.snp.remakeConstraints() { maker in
            maker.left.equalToSuperview().offset(15)
            maker.right.lessThanOrEqualToSuperview().offset(-64)
            maker.top.bottom.equalToSuperview()
        }
        
        contentView.addSubview(expandedView)
        expandedView.snp.remakeConstraints() { maker in
            maker.left.right.equalToSuperview()
            maker.top.equalTo(mainContainer.snp.bottom)
            maker.bottom.equalTo(separatorView.snp.top)
        }
    }

    func configure(_ item: ContactsListCellItem, expandedView: UIView?, delegate: ContactsListCellDelegate) {
        super.configure(with: item, delegate: delegate)
        
        listDelegate = delegate
        setExpanded(expanded: item.isExpanded)
        if isExpanded {
            for v in expandedContentView.subviews {
                v.removeFromSuperview()
            }
            if let view = expandedView {
                expandedContentView.addSubview(view)
                view.snp.remakeConstraints() { maker in
                    maker.edges.equalToSuperview()
                }
            }
        }
    }

    internal func setExpanded(expanded: Bool) {
        guard isExpanded != expanded else {
            return
        }

        expandedView.isHidden = !expanded
        isExpanded = expanded
    }
    
    // MARK: IBActions

    @objc private func messageTapped() {
        listDelegate?.contactsListCellDidTapToActionButton(self, action: .message)
    }
    
    @objc private func notificationTapped() {
        listDelegate?.contactsListCellDidTapToActionButton(self, action: .notification)
    }
    
    // MARK: - private

}
