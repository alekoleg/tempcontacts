//
//  BaseContactCell.swift
//  TemporaryContacts
//
//  Created by Konshin on 23.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import MGSwipeTableCell
import SnapKit

protocol BaseContactCellDelegate: class {
    func baseContactCellDidTapAction(_ cell: BaseContactCell, action: BaseContactCell.BaseAction)
}

/// Размер стороны аватарки
private let avatarSide: CGFloat = {
   let screenWidth = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
    return screenWidth > 320 ? 42 : 36
}()

/// Базовая ячейка контакта
/// Умеет отображать контакт, состояние временного контакта и любую кнопку справа
/// Поддерживает действия: Изменения состояния временного контакта, удаление
/// Пришлось написать все кодом, т.к. имеются наследники
class BaseContactCell: MGSwipeTableCell {
    enum BaseAction {
        case tempSwitch
        case remove
        case rightButtonTapped
    }
    
    internal let mainContainer = UIView()
    internal let avatarImageView = UIImageView()
    internal let mainLabel = UILabel()
    internal let subLabel = UILabel()
    internal let separatorView = UIView()
    internal let tempIconView = UIImageView()
    internal let rightButtonContainer = UIView()
    internal let rightButton = UIButton()
    internal private(set) var mainTextCenterYConstraint: Constraint?
    
    weak var baseDelegate: BaseContactCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // MARK: - MGSwipeTableCell buttons
    
    private let tempButton = MGSwipeButton(
        title: "",
        backgroundColor: ColorHelper.AppTintColor(),
        insets: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    )
    private let deleteButton = MGSwipeButton(
        title: "",
        icon: UIImage(named: "BlueDelete"),
        backgroundColor: ColorHelper.AppRedColor(),
        insets: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 30)
    )
    
    // MARK: - actions
    
    func initialize() {
        contentView.addSubview(mainContainer)
        mainContainer.snp.remakeConstraints() { maker in
            maker.left.right.top.equalToSuperview()
            maker.height.equalTo(75)
        }
        
        let avatarSize = CGSize(width: avatarSide, height: avatarSide)
        avatarImageView.layer.cornerRadius = avatarSize.height / 2
        avatarImageView.layer.masksToBounds = true
        avatarImageView.contentMode = .scaleAspectFill
        mainContainer.addSubview(avatarImageView)
        avatarImageView.snp.remakeConstraints { maker in
            maker.size.equalTo(avatarSize)
            maker.centerY.equalToSuperview()
            maker.left.equalToSuperview().offset(21)
        }
        
        rightButtonContainer.addSubview(rightButton)
        rightButton.addTarget(self, action: #selector(tapToRightButton), for: .touchUpInside)
        rightButton.snp.remakeConstraints() { maker in
            maker.edges.equalToSuperview()
            maker.width.height.equalTo(40)
        }
        
        mainContainer.addSubview(rightButtonContainer)
        rightButtonContainer.setContentHuggingPriority(.required, for: .horizontal)
        rightButtonContainer.setContentCompressionResistancePriority(.required, for: .horizontal)
        rightButtonContainer.snp.remakeConstraints() { maker in
            maker.centerY.equalToSuperview()
            maker.centerX.equalTo(mainContainer.snp.right).offset(-22)
        }
        
        mainContainer.addSubview(tempIconView)
        tempIconView.setContentHuggingPriority(.required, for: .horizontal)
        tempIconView.setContentCompressionResistancePriority(.required, for: .horizontal)
        tempIconView.snp.remakeConstraints() { maker in
            maker.centerY.equalToSuperview()
            maker.right.equalToSuperview().offset(-46)
        }
        
        let labelsContainer = UIView()
        labelsContainer.addSubview(mainLabel)
        mainLabel.font = UIFont.appFont(size: 18, typeface: .semibold)
        mainLabel.textColor = ColorHelper.AppDarkTextColor()
        mainLabel.snp.remakeConstraints() { maker in
            maker.left.top.right.equalToSuperview()
            mainTextCenterYConstraint = maker.centerY.equalToSuperview().constraint
        }
        
        labelsContainer.addSubview(subLabel)
        subLabel.font = UIFont.appFont(size: 14, typeface: .light)
        subLabel.textColor = ColorHelper.AppDarkTextColor()
        subLabel.snp.remakeConstraints() { maker in
            maker.top.equalTo(mainLabel.snp.bottom).offset(2).priority(750)
            maker.bottom.left.right.equalToSuperview()
        }
        
        mainContainer.addSubview(labelsContainer)
        labelsContainer.snp.remakeConstraints() { maker in
            maker.centerY.equalToSuperview()
            maker.left.equalTo(avatarImageView.snp.right).offset(10)
            maker.right.equalTo(tempIconView.snp.left).offset(-15)
        }
        
        contentView.addSubview(separatorView)
        separatorView.backgroundColor = UIColor(hex: 0xE7E7E7)
        separatorView.snp.remakeConstraints() { maker in
            maker.right.equalToSuperview().offset(-14)
            maker.left.equalTo(labelsContainer)
            maker.bottom.equalToSuperview()
            maker.height.equalTo(1)
        }
        
        leftSwipeSettings.transition = MGSwipeTransition.rotate3D
        rightSwipeSettings.transition = MGSwipeTransition.rotate3D
        tempButton.callback = { [unowned self] _ in
            self.baseDelegate?.baseContactCellDidTapAction(self, action: .tempSwitch)
            return true
        }
        deleteButton.callback = { [unowned self] _ in
            self.baseDelegate?.baseContactCellDidTapAction(self, action: .remove)
            return true
        }
    }
    
    func configure(with item: BaseContactCellItem, delegate: BaseContactCellDelegate?) {
        self.baseDelegate = delegate
        
        mainLabel.text = item.mainText
        subLabel.text = item.subText
        separatorView.isHidden = !item.showSeparator
        
        let subTextIsEmpty = item.subText?.isEmpty != false
        setMainTextOnCenterVertically(subTextIsEmpty)
        
        let tempIcon: UIImage? = item.isTempObject ? #imageLiteral(resourceName: "RemoveFromTemp") : nil
        tempIconView.image = tempIcon
        
        let tempButtonImage: UIImage? = item.isTempObject
            ? #imageLiteral(resourceName: "RemoveFromTemp")
            : #imageLiteral(resourceName: "MoveToTemp")
        tempButton.setImage(tempButtonImage, for: .normal)
        tempButton.sizeToFit()
        
        avatarImageView.image = item.avatarImage ?? #imageLiteral(resourceName: "contacts_list_avatar_placeholder")
        
        rightButton.setImage(item.rightButtonIcon, for: .normal)
        isSwipable = item.isSwipable
    }

    private func setMainTextOnCenterVertically(_ onCenter: Bool) {
        if onCenter {
            mainTextCenterYConstraint?.activate()
        } else {
            mainTextCenterYConstraint?.deactivate()
        }
        
    }
    
    @objc private func tapToRightButton(sender: UIButton) {
        baseDelegate?.baseContactCellDidTapAction(self, action: .rightButtonTapped)
    }
    
    /// Поддерживает ли ячейка свайпы
    var isSwipable: Bool = false {
        didSet {
            guard oldValue != isSwipable else {
                return
            }
            
            if isSwipable {
                leftButtons = [tempButton]
                rightButtons = [deleteButton]
            } else {
                leftButtons = []
                rightButtons = []
            }
        }
    }
}
