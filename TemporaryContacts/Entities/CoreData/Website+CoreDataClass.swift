//
//  Website+CoreDataClass.swift
//  
//
//  Created by Konshin on 15.04.17.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


public class Website: NSManagedObject {
    @available(*, unavailable)
    convenience init() {
        fatalError()
    }
    
    convenience init(context: NSManagedObjectContext? = nil) {
        let context = context ?? CoreDataManager.instance.managedObjectContext
        self.init(
            entity: CoreDataManager.instance.entityForName(entityName: "Website", context: context),
            insertInto: context
        )
    }
}
