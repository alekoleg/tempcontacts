//
//  Groups+CoreDataClass.swift
//  TemporaryContacts
//
//  Created by User on 21/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import CoreData


public class Groups: NSManagedObject {
    static let entityName: String = "Groups"
    
    @available(*, unavailable)
    convenience init() {
        fatalError()
    }
    
    convenience init(context: NSManagedObjectContext? = nil) {
        let context = context ?? CoreDataManager.instance.managedObjectContext
        self.init(
            entity: CoreDataManager.instance.entityForName(entityName: Groups.entityName, context: context),
            insertInto: context
        )
    }
}
