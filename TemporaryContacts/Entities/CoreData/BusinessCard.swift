//
//  BusinessCard.swift
//  TemporaryContacts
//
//  Created by Konshin on 21.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData

public class BusinessCard: NSManagedObject {
    static let entityName: String = "BusinessCard"
    
    @available(*, unavailable)
    convenience init() {
        fatalError()
    }
    
    convenience init(image: UIImage, creationDate: Date?, context: NSManagedObjectContext? = nil) {
        let data = image.jpegData(compressionQuality: 0.5) ?? Data()
        self.init(imageData: data, creationDate: creationDate, context: context)
    }
    
    convenience init(imageData: Data, creationDate: Date?, context: NSManagedObjectContext? = nil) {
        let context = context ?? CoreDataManager.instance.managedObjectContext
        self.init(
            entity: CoreDataManager.instance.entityForName(entityName: BusinessCard.entityName, context: context),
            insertInto: context
        )
        self.imageData = imageData as NSData as Data
        self.creationDate = creationDate as NSDate? as Date?
    }
}


// MARK: - Custom logic
extension BusinessCard {
    var image: UIImage? {
        return imageData.flatMap({ UIImage(data: $0 as Data) })
    }
}
