//
//  Phones+CoreDataClass.swift
//  TemporaryContacts
//
//  Created by User on 27/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import CoreData


public class Phones: NSManagedObject {
    @available(*, unavailable)
    convenience init() {
        fatalError()
    }
    
    convenience init(context: NSManagedObjectContext? = nil) {
        let context = context ?? CoreDataManager.instance.managedObjectContext
        self.init(
            entity: CoreDataManager.instance.entityForName(entityName: "Phones", context: context),
            insertInto:context
        )
    }
}
