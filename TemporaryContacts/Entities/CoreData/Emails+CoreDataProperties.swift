//
//  Emails+CoreDataProperties.swift
//  TemporaryContacts
//
//  Created by User on 09/01/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import CoreData


extension Emails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Emails> {
        return NSFetchRequest<Emails>(entityName: "Emails");
    }

    @NSManaged public var email: String?
    @NSManaged public var label: String?
    @NSManaged public var systemIdentifier: String?
    @NSManaged public var contact: Contacts?

}
