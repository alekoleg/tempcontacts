//
//  Migration.swift
//  TemporaryContacts
//
//  Created by Konshin on 21.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData

class CoreDataMigration: NSManagedObject {
    static let entityName: String = "CoreDataMigration"
    
    @available(*, unavailable)
    convenience init() {
        fatalError()
    }
    
    convenience init(name: String, context: NSManagedObjectContext? = nil) {
        let context = context ?? CoreDataManager.instance.managedObjectContext
        self.init(
            entity: CoreDataManager.instance.entityForName(entityName: CoreDataMigration.entityName, context: context),
            insertInto: context
        )
        self.name = name
    }
}
