//
//  UserNotification.swift
//  TemporaryContacts
//
//  Created by Konshin on 30.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData

public class AppNotification: NSManagedObject {
    @available(*, unavailable)
    convenience init() {
        fatalError()
    }
    
    convenience init(context: NSManagedObjectContext? = nil) {
        let context = context ?? CoreDataManager.instance.managedObjectContext
        self.init(
            entity: CoreDataManager.instance.entityForName(entityName: AppNotification.entityName, context: context),
            insertInto: context
        )
    }
}


extension AppNotification {
    enum `Type`: Int16 {
        case other = 0
        case remove = 1
        case call = 2
    }
    
    static let entityName: String = "AppNotification"
    
    var notificationType: Type {
        return Type(rawValue: type) ?? .other
    }
    
    /// Уведомление из прошлого
    var isExpired: Bool {
        return date.flatMap { $0.timeIntervalSinceNow < 0 } ?? false
    }
}

extension Date {
    
    /// Инициализирует дату на день после текущего
    func dateOfMorningAtDatSince(days: UInt) -> Date {
        let oneDayDuration: TimeInterval = 24 * 60 * 60
        let date = Date(timeInterval: oneDayDuration * TimeInterval(days), since: self)
        
        let calendar = Calendar.current
        let dateComponents: [Calendar.Component] = [.day, .month, .year, .hour, .minute]
        var components = calendar.dateComponents(Set(dateComponents), from: date)
        components.hour = 10
        components.minute = 0
        return calendar.date(from: components) ?? Date()
    }
}
