//
//  Phones+CoreDataProperties.swift
//  TemporaryContacts
//
//  Created by User on 27/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import CoreData


extension Phones {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Phones> {
        return NSFetchRequest<Phones>(entityName: "Phones");
    }

    @NSManaged public var systemIdentifier: String?
    @NSManaged public var digits: String?
    @NSManaged public var label: String?
    @NSManaged public var contact: Contacts?

}
