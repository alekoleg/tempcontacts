//
//  Emails+CoreDataClass.swift
//  TemporaryContacts
//
//  Created by User on 09/01/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import CoreData


public class Emails: NSManagedObject {
    @available(*, unavailable)
    convenience init() {
        fatalError()
    }
    
    convenience init(context: NSManagedObjectContext? = nil) {
        let context = context ?? CoreDataManager.instance.managedObjectContext
        self.init(
            entity: CoreDataManager.instance.entityForName(entityName: "Emails", context: context),
            insertInto: context
        )
    }
}
