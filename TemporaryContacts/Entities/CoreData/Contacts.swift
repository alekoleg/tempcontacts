//
//  Contacts+CoreDataClass.swift
//  TemporaryContacts
//
//  Created by User on 21/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import CoreData

public typealias SystemIdentifier = String

public class Contacts: NSManagedObject {
    static let entityName: String = "Contacts"
    
    @available(*, unavailable)
    convenience init() {
        fatalError()
    }
    
    convenience init(context: NSManagedObjectContext? = nil) {
        let context = context ?? CoreDataManager.instance.managedObjectContext
        self.init(
            entity: CoreDataManager.instance.entityForName(entityName: Contacts.entityName, context: context),
            insertInto: context
        )
    }
}


// MARK: - Custom logic
extension Contacts {
    enum KnownPhonesType {
        case main(Phones)
        case multiple([Phones])
        case unknown
    }
    
    /// Тип доступных номеров для контакта
    var knownPhonesType: KnownPhonesType {
        guard let phones = phones as? Set<Phones>, !phones.isEmpty else {
            return .unknown
        }
        
        switch phones.count {
        case 1:
            return .main(phones.first!)
        default:
            let sorted = phones.sorted() { l, r in
                let lKey = l.systemIdentifier ?? ""
                let rKey = r.systemIdentifier ?? ""
                return lKey < rKey
            }
            return .multiple(sorted)
        }
    }
    
    /// Основной телефон. Возвращает первый для контактов с несколькими телефонами
    var mainPhone: Phones? {
        switch knownPhonesType {
        case .main(let phone):
            return phone
        case .multiple(let phones):
            return phones.first
        case .unknown:
            return nil
        }
    }
    
    /// Основной емейл. Возвращает первый для контактов с несколькими телефонами
    var mainEmail: Emails? {
        let emails = self.emails?.allObjects as? [Emails] ?? []
        let sorted = emails.sorted() { l, r in
            let lKey = l.systemIdentifier ?? ""
            let rKey = r.systemIdentifier ?? ""
            return lKey < rKey
        }
        return sorted.first
    }
    
    /// Ключ группировки
    @objc var sectionGroupingKey: String {
        if let identifier = sectionIdentifier, !identifier.isEmpty {
            return identifier
        }
        return "#"
    }
    
    var orderedCards: [BusinessCard] {
        let cards = businessCards?.allObjects as? [BusinessCard] ?? []
        return cards.sorted() { l, r in
            let lInterval = l.creationDate?.timeIntervalSince1970 ?? 0
            let rInterval = r.creationDate?.timeIntervalSince1970 ?? 0
            return lInterval < rInterval
        }
    }
    
    func setBusinessCards(cards: [BusinessCard]) {
        businessCards = NSSet(array: cards)
        updateLastDateOfCreationBusinessCard()
    }
    
    /// Обновляет запись о дате последней визитки
    /// Необходимо вызыватьк аждый раз, когда обновлены визитки
    func updateLastDateOfCreationBusinessCard() {
        self.lastDateOfCreationBusinessCard = orderedCards.last?.creationDate
    }
    
    /// Формирует имя и специализация контакта (напрмиер, для отображения в списке)
    /// на основании настроек
    ///
    /// - Parameter settings: Модель настроек
    /// - Returns: Имя и специализацию
    func nameAndSpecialization(settings: Settings = Settings.instance) -> (name: String, specialization: String?) {
        let displayProperties = settings.contactSortingProperties()
        var name = ""
        let space = " "
        var specialization: String? = self.specialization
        
        for (index, property) in displayProperties.enumerated() {
            guard let value = value(forKeyPath: property) as? String, !value.isEmpty else {
                continue
            }
            
            if index > 1 {
                if !name.isEmpty {
                    break
                }
                specialization = nil
            }
            
            if !name.isEmpty {
                name.append(space)
            }
            
            name.append(value)
        }
        
        return (name, specialization)
    }
}
