//
//  Website+CoreDataProperties.swift
//  
//
//  Created by Konshin on 15.04.17.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Website {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Website> {
        return NSFetchRequest<Website>(entityName: "Website")
    }

    @NSManaged public var url: String?
    @NSManaged public var contact: Contacts?

}
