//
//  EmailLabels.swift
//  TemporaryContacts
//
//  Created by User on 09/01/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
enum EmailLabels: String{
    case Home = "_$!<Home>!$_",
    Work = "_$!<Work>!$_",
    iCloud = "iCloud",
    Other = "_$!<Other>!$_"
    
    func getLabel() -> String{
        switch self {
        case .Home:
            return "home".localized
        case .Work:
            return "work".localized
        case .iCloud:
            return "iCloud".localized
        case .Other:
            return "other".localized
        }
    }
    
    static let allValues:[String] = [Home.getLabel(),Work.getLabel(),iCloud.getLabel(),Other.getLabel()]
}
