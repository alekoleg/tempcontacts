//
//  PhoneLabels.swift
//  TemporaryContacts
//
//  Created by User on 27/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
enum PhoneLabels: String{
    case Mobile = "_$!<Mobile>!$_",
    Home = "_$!<Home>!$_",
    Work = "_$!<Work>!$_",
    iPhone = "iPhone",
    Main = "_$!<Main>!$_",
    HomeFax = "_$!<HomeFAX>!$_",
    WorkFax = "_$!<WorkFAX>!$_",
    Pager = "_$!<Pager>!$_",
    Other = "_$!<Other>!$_"
    
    func getLabel() -> String{
        switch self {
        case .Mobile:
            return "mobile".localized
        case .Home:
            return "home".localized
        case .Work:
            return "work".localized
        case .iPhone:
            return "iPhone".localized
        case .Main:
            return "main".localized
        case .HomeFax:
            return "homeFax".localized
        case .WorkFax:
            return "workFax".localized
        case .Pager:
            return "pager".localized
        case .Other:
            return "other".localized
        }
    }
    
    static let allValues:[String] = [Mobile.getLabel(), Home.getLabel(),Work.getLabel(),iPhone.getLabel(),Main.getLabel(),HomeFax.getLabel(),WorkFax.getLabel(),Pager.getLabel(),Other.getLabel()]
}
