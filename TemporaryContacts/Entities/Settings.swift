//
//  Settings.swift
//  TemporaryContacts
//
//  Created by Konshin on 08.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

/// Настройки приложения
class Settings {
    /// shared
    static let instance = Settings()
    
    /// Тип сортировки контактов по всему приложению
    var contactsOrder: SettingsDisplayOrder {
        get {
            if let raw = UserDefaults.standard.string(forKey: "displayOrder"),
                let order = SettingsDisplayOrder(rawValue: raw) {
                return order
            }
            return .FamilyFirst
        }
        set {
            guard newValue != contactsOrder else {
                return
            }
            UserDefaults.standard.set(newValue.rawValue, forKey: "displayOrder")
            NotificationCenter.default.post(name: .contactsSortingTypeDidUpdate, object: nil)
        }
    }
    
    /// Переключаться на основную вкладку при добавлении контактов через системную книгу
    var showAllTabWhenContactsDidUpdate: Bool {
        get {
            return !UserDefaults.standard.bool(forKey: "dontShowAllTab")
        } set {
            UserDefaults.standard.set(!newValue, forKey: "dontShowAllTab")
        }
    }
    
    /// Отображать подсказки
    var showTips: Bool {
        get {
            return !UserDefaults.standard.bool(forKey: "disableHints")
        } set {
            UserDefaults.standard.set(!newValue, forKey: "disableHints")
        }
    }
    
    /// Тип отображения групп
    var groupDisplayType: SettingsGroupDisplay {
        get {
            if let raw = UserDefaults.standard.string(forKey: "groupDisplay"),
                let type = SettingsGroupDisplay(rawValue: raw) {
                return type
            } else {
                return .List
            }
        } set {
            guard newValue != groupDisplayType else {
                return
            }
            UserDefaults.standard.set(newValue.rawValue, forKey: "groupDisplay")
            NotificationCenter.default.post(name: .groupDisplayModeDidUpdate, object: nil)
        }
    }
    
    /// Стартовая страница
    var startPageType: SettingsStartPage {
        get {
            if let raw = UserDefaults.standard.string(forKey: "startPage"),
                let type = SettingsStartPage(rawValue: raw) {
                return type
            } else {
                return .all
            }
        } set {
            UserDefaults.standard.set(newValue.rawValue, forKey: "startPage")
        }
    }
    
    /// Автоматически удалять напоминание через сутки и после просмотра
    var shouldRemoveNotificationsAfterFewDaysAfterView: Bool {
        get { return UserDefaults.standard.bool(forKey: "shouldRemoveNotificationsAfterOneDayAfterView") }
        set {
            guard shouldRemoveNotificationsAfterFewDaysAfterView != newValue else {
                return
            }
            UserDefaults.standard.set(newValue, forKey: "shouldRemoveNotificationsAfterOneDayAfterView")
            NotificationCenter.default.post(name: .didChangeNotificationsRemoveSettings, object: nil)
        }
    }
    
    // MARK: - special offer
    
    private let specialOfferKey = "ShouldSHowSpecialOffer"
    
    /// Состояние специального отображения
    var specialOfferState: Bool? {
        get { return UserDefaults.standard.value(forKey: specialOfferKey) as? Bool }
        set { UserDefaults.standard.setValue(newValue, forKey: specialOfferKey) }
    }
    
    /// Не показывалось специальное предложение
    var specialOfferIsNotShowed: Bool {
        return UserDefaults.standard.bool(forKey: specialOfferKey)
    }
    
    /// Соотношение длины и ширины визитки
    var businesCardsRatio: CGFloat = 9 / 5
    
    /// Возвращает да, если старый вариант туторила уже был показан
    var oldTutorialIsShowed: Bool {
        return UserDefaults.standard.bool(forKey: "SkipTutorial")
    }
    
    /// Показывать туториал при старте
    var showTutorial: Bool {
        get { return !UserDefaults.standard.bool(forKey: "SkipUpdatedTutorial") }
        set { UserDefaults.standard.set(!newValue, forKey: "SkipUpdatedTutorial") }
    }
    
    /// Видео гид уже показывали (автоматически)
    var videoGuideShowed: Bool {
        get { return !UserDefaults.standard.bool(forKey: "VideoGuideShowed_part2") }
        set { UserDefaults.standard.set(!newValue, forKey: "VideoGuideShowed_part2") }
    }
    
    /// необходимо при след запуске показатьс пец оффер
    var shouldShowSpecialOffer: Bool {
        get { return !UserDefaults.standard.bool(forKey: "shouldShowSpecialOffer") }
        set { UserDefaults.standard.set(!newValue, forKey: "shouldShowSpecialOffer") }
    }
    
    /// Стиль отображения в визитнице
    var businessCardsDisplayMode: BusinessCardsDisplayMode {
        get {
            return UserDefaults.standard.string(forKey: "businessCardsDisplayMode")
                .flatMap { BusinessCardsDisplayMode(rawValue: $0) } ?? .list
        }
        set { UserDefaults.standard.set(newValue.rawValue, forKey: "businessCardsDisplayMode") }
    }
    
    /// Создавали пример контактов
    var exampleContactsDidCreated: Bool {
        get { return UserDefaults.standard.bool(forKey: "exampleContactsDidCreated") }
        set { UserDefaults.standard.set(newValue, forKey: "exampleContactsDidCreated") }
    }
}

extension Settings {
    
    /// Количество непросмотренных(новых) уведомлений
    var numberOfNewNotifications: Int {
        get {
            return UIApplication.shared.applicationIconBadgeNumber
        }
        set {
            UIApplication.shared.applicationIconBadgeNumber = newValue
            NotificationCenter.default.post(name: .didChangeNewNotificationsState, object: nil)
        }
    }
    
    /// Удаляет инфомрацию о непросмотренныху ведомлениях
    func removeNewNotificationsInfo() {
        numberOfNewNotifications = 0
    }
}

// MARK: - Contacts sorting
extension Settings {
    
    /// Поля для поиска у контакта
    var contactSearchFields: [String] {
        return [
            "firstName",
            "lastName",
            "specialization",
            "departmentName",
            "jobTitle",
            "middleName",
            "nickname",
            "phoneticFamilyName",
            "phoneticGivenName",
            "phoneticMiddleName",
            "phoneticOrganizationName",
            "previousFamilyName",
            "nameSuffix",
            "namePrefix"
        ]
    }
    
    /// Поля для сортировки контактов
    ///
    /// - Parameter forOrfder: Тип сортировки. Если nil - используется свойство contactsOrder
    func contactSortingProperties(forOrfder: SettingsDisplayOrder? = nil) -> [String] {
        let order = forOrfder ?? contactsOrder
        switch order {
        case .NameFirst:
            return ["firstName", "lastName", "specialization", "nickname"]
        case .FamilyFirst:
            return ["lastName", "firstName", "specialization", "nickname"]
        }
    }
    
    func contactsSortingDescriptors() -> [NSSortDescriptor] {
        let descriptors =  [
            NSSortDescriptor(
                key: "isNew",
                ascending: false
            ),
            NSSortDescriptor(
                key: "hasSectionIdentifier",
                ascending: false
            ),
            NSSortDescriptor(
                key: "sectionIdentifier",
                ascending: true,
                selector: #selector(NSString.localizedCompare(_:))
            ),
            NSSortDescriptor(
                key: "displayName",
                ascending: true,
                selector: #selector(NSString.localizedCompare(_:))
            )
        ]
        return descriptors
    }
}

// MARK: - Расширение для уведомления о настройках
extension NSNotification.Name {
    /// Изменилась настройка удаления уведомлений
    static let didChangeNotificationsRemoveSettings = NSNotification.Name(rawValue: "didChangeNotificationsRemoveSettings")
    
    /// Изменилось состояние новыйху ведомлений
    static let didChangeNewNotificationsState = NSNotification.Name(rawValue: "didChangeNewNotificationsState")
}
