//
//  AppDelegate.swift
//  TemporaryContacts
//
//  Created by User on 20/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import IQKeyboardManagerSwift
import Contacts
import RxSwift
import YandexMobileMetricaPush

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    fileprivate var appNotificationsManager: AppNotificationsManager?
    fileprivate lazy var tabsRouter: TabsRouter = TabsRouter()
    fileprivate let disposeBag = DisposeBag()
    fileprivate let universalDelegateHandler = UniverslDelegateHandler()
    
    /// Роутер предложений
    weak var proposalRouter: ProposalRouter?
    
    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Metrica.instance.initialize()

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enabledToolbarClasses = [PhotoContactCreateControler.self]
        
        // Инициализируем контекст CoreData
        _ = CoreDataManager.instance.managedObjectContext
        
        // Проводим все миграции CoreData
        migrate()

		// work around для людей которые уже раньше установили приложение
		if !UserDefaults.standard.bool(forKey: "Old Users") {
			UserDefaults.standard.set(true, forKey: "Old Users")
			if Settings.instance.oldTutorialIsShowed {
				PurchasesManager.instance.setOldUsersPurchased()
			}
        }
        
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: "AppDidStared")
        
        let showTutorial = Settings.instance.showTutorial
        if !showTutorial {
            // Обновление контактов
            updateContactsInDBIfNeeded()
                .subscribe(
                    onNext: { [unowned self] in
                        self.workWithReceipt()
                })
                .disposed(by: disposeBag)
        }
        
        registerForNotifications(application)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contactBookDidChange),
            name: NSNotification.Name.CNContactStoreDidChange,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contactsSortingTypeDidUpdate),
            name: .contactsSortingTypeDidUpdate,
            object: nil
        )
        
        let nManager = AppNotificationsManager(context: CoreDataManager.instance.managedObjectContext)
        self.appNotificationsManager = nManager
        nManager.removeOldNotifications()
        nManager.startObserving()

        setupAppereance()
        
        window = initializeWindow()
        window?.makeKeyAndVisible()
        
        appActive = true

        if !showTutorial, let localNotification = launchOptions?[UIApplication.LaunchOptionsKey.localNotification] as? UILocalNotification {
            self.parseLocalNotification(localNotification)
        }
        
        if showTutorial {
            let router = tabsRouter.presentProposalRouter(presentWithAlpha: true)
            router.showTour() { [weak self] tour in
                self?.showVideoGuide(router)
            }
            proposalRouter = router
        } else if !Settings.instance.videoGuideShowed {
            let router = tabsRouter.presentProposalRouter(presentWithAlpha: true)
            showVideoGuide(router)
            proposalRouter = router
        } else if Settings.instance.shouldShowSpecialOffer {
            showSpecialOffer(nil)
        }

		// инициализуем покупки
		_ = PurchasesManager.instance
        
        setupReviewManager()
        
        YMPYandexMetricaPush.handleApplicationDidFinishLaunching(options: launchOptions)
        
        return true
    }
    
    // MARK: - actions
    
    /// Отобразить видеоинструкцию
    private func showVideoGuide(_ router: ProposalRouter) {
        router.showVideoGuide(
            skipAction: { vc in
                router.showSkipVideoAlert { [weak self] in
                    vc.dismiss(animated: true, completion: nil)
                    self?.startWorkAfterProposal()
                    // Делаем отметку отобразить спец предложение на след запуск
                    Settings.instance.shouldShowSpecialOffer = true
                }
        },
            finishAction: { [weak self] _ in
                self?.showSpecialOffer(router)
        })
    }
    
    /// Отображает спец предложение
    /// Если не передать роутер - значит, он не отображен
    private func showSpecialOffer(_ router: ProposalRouter?) {
        /// И так все куплено
        guard !tabsRouter.dependencies.availabilityManager.isFullUnlocked() else {
            if let router = router {
                router.rootController.dismiss(animated: true, completion: nil)
                startWorkAfterProposal()
            }
            return
        }
        
        let waitDuration: TimeInterval = router == nil ? 2 : 0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + waitDuration) { [unowned self] in
            // Пытаемчя восстановить покупки
            if PurchasesManager.instance.isPurchasesLoaded {
                let router = router ?? self.tabsRouter.presentProposalRouter(presentWithAlpha: false)
                router.showSpecialOffer(
                    doneAction: { [weak self] vc in
                        vc.dismiss(animated: true, completion: nil)
                        self?.startWorkAfterProposal()
                    },
                    didBuyAction: { _ in
                        router.showThanksScreen { [weak self] vc in
                            vc.dismiss(animated: true, completion: nil)
                            self?.startWorkAfterProposal()
                        }
                })
                
                Settings.instance.shouldShowSpecialOffer = false
            } else if let router = router {
                router.rootController.dismiss(animated: true, completion: nil)
                self.startWorkAfterProposal()
            }
        }
    }
    
    private var appStoreReviewManager: AppStoreReviewManager?
    
    fileprivate func workWithReceipt() {
        guard !Settings.instance.exampleContactsDidCreated else {
            return
        }
        
        AppReceiptManager.instance.getReceipt()
            .subscribe(
                onNext: { receipt in
                    // Скачали приложение не позднее 24 часов назад
                    if abs(receipt.originalPurchaseDate.timeIntervalSinceNow) < 60 * 60 * 24 {
                        ContactsManager.instance.deleteTestContacts()
                        ContactsManager.instance.addTestContacts()
                    }
                    Settings.instance.exampleContactsDidCreated = true
            },
                onError: { error in
                    NSLog("Ошибка получения квитанции: \(error)")
            })
            .disposed(by: disposeBag)
    }
    
    private func registerForNotifications(_ application: UIApplication) {
        var notificationTypes: UIUserNotificationType = UIUserNotificationType()
        notificationTypes.insert(.alert)
        notificationTypes.insert(.sound)
        notificationTypes.insert(.badge)
        
        let category = UIMutableUserNotificationCategory()
        category.identifier = "TemporaryContactsNotification"
        
        let notificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: Set([category]))
        application.registerUserNotificationSettings(notificationSettings)
        
        application.registerForRemoteNotifications()
    }
    
    private func setupReviewManager() {
        appStoreReviewManager = AppStoreReviewManager()
        appStoreReviewManager?.setup()
    }
    
    /// Приложение работает в фореграунде
    private var appActive = false
    
    @objc func contactBookDidChange(notification: NSNotification){
        guard !appActive else {
            return
        }
        
        updateContactsInDBIfNeeded()
            .subscribe(
                onNext: { _ in
                    NotificationCenter.default.post(
                        name: .systemContactsDidUpdateInAppBackground,
                        object: nil
                    )
            })
            .disposed(by: disposeBag)
    }
    
    @objc private func contactsSortingTypeDidUpdate() {
        DispatchQueue.global().async {
            ContactsManager.instance.synchronizeWithSystem()
        }
    }
    
    private func initializeWindow() -> UIWindow {
        let window = UIWindow(frame: UIScreen.main.bounds)
        
        window.rootViewController = tabsRouter.rootController
        return window
    }
    
    private let dbUpdateQueue = DispatchQueue.global(qos: .background)
    
    fileprivate func updateContactsInDBIfNeeded() -> Observable<Void> {
        guard !ContactsManager.instance.contactUpdateInProcess else {
            return Observable.just(Void())
        }
        
        // ПРоисходит миграция
        if migrationSignal != nil {
            updateContactsAfterMigrations = true
        }
        
        return ContactsManager.instance.deleteRemovedSystemContacts()
            .flatMap { return ContactsManager.instance.synchronizeWithSystem() }
            .subscribeOn(ConcurrentDispatchQueueScheduler(queue: dbUpdateQueue))
    }
    
    fileprivate var migrationSignal: Observable<Void>?
    /// Обновить контакты после миграции
    fileprivate var updateContactsAfterMigrations = false
    
    /// Выполняет все нестандартные миграции базы данных
    fileprivate func migrate() {
        migrationSignal = CoreDataManager.instance.migrate()
            .subscribeOn(ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global()))
        migrationSignal?
            .subscribe(
                onNext: {
                    print("Выполнили все миграции")
            },
                onCompleted: { [unowned self] in
                    self.migrationSignal = nil
                    if self.updateContactsAfterMigrations {
                        self.updateContactsInDBIfNeeded()
                            .subscribe(
                                onNext: { _ in }
                            )
                            .disposed(by: self.disposeBag)
                        self.updateContactsAfterMigrations = false
                    }
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: - delegate

    func applicationWillResignActive(_ application: UIApplication) {
        DispatchQueue.global().async {
            AppManager.instance.cancelNotifications()
            AppManager.instance.createNotifications()
        }
        appActive = false
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("scheduled notifications: \(application.scheduledLocalNotifications?.count ?? 0)")
        appActive = true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        CoreDataManager.instance.updateContactsBeforeTerminate()
        CoreDataManager.instance.saveContext()
        NotificationCenter.default.removeObserver(self)
    }
}

private enum ShortcutAction: String {
    case createPhotoContact = "CreatePhotoContact"
    case newNotification = "NewNotification"
    case businessCardsList = "BusinessCardsList"
    case contactsSearch = "ContactsSearch"
}

extension AppDelegate {
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        guard let lastComponent = shortcutItem.type.components(separatedBy: ".").last,
            let action = ShortcutAction(rawValue: lastComponent),
            !Settings.instance.showTutorial else {
                completionHandler(false)
                return
        }
        
        switch action {
        case .createPhotoContact:
            tabsRouter.open(tab: .contacts)
            tabsRouter.contactsRouter.showPhotoContactCreator(delegate: universalDelegateHandler)
        case .businessCardsList:
            tabsRouter.open(tab: .businessCards)
        case .newNotification:
            tabsRouter.open(tab: .notifications)
        case .contactsSearch:
            tabsRouter.open(tab: .contacts)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                NotificationCenter.default.post(name: .didSelectShortCutItemSearchContact, object: nil)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) { [unowned self] in
            self.tabsRouter.contactsRouter.removeAllIntermediateControllers()
        }
        
        completionHandler(true)
    }
}

// MARK: - Notifications
extension AppDelegate {
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        NSLog("\n\n\n\n\n DID RECEIVE LOCAL NOTIFICATION \(notification)\n\n")

        switch application.applicationState {
        case .inactive:
            // Если нажали на системное ведомление 
            parseLocalNotification(notification)
        default:
            if notification.applicationIconBadgeNumber > 0 {
                Settings.instance.numberOfNewNotifications = notification.applicationIconBadgeNumber
            }
        }
        NotificationCenter.default.post(name: .didReceiveLocalNotification, object: notification)
    }
    
    fileprivate func parseLocalNotification(_ notification: UILocalNotification) {
        tabsRouter.open(tab: .notifications)
        // Удаляем уведомление о новых напоминаниях - ведьм ы отправили на экран отображения контакта
        Settings.instance.removeNewNotificationsInfo()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        YMPYandexMetricaPush.setDeviceTokenFrom(deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        YMPYandexMetricaPush.handleRemoteNotification(userInfo)
    }
}

extension NSNotification.Name {
    /// Синхроназация контактов с книгой вот-вот начнется
    static let didReceiveLocalNotification = NSNotification.Name(rawValue: "didReceiveLocalNotification")
    /// Нажали на шорткут - найти контакт
    static let didSelectShortCutItemSearchContact = NSNotification.Name(rawValue: "didSelectShortCutItemSearchContact")
}

import MessageUI

// MARK: - Appearance
extension AppDelegate {

	fileprivate func setupAppereance() {
        AppAppearance.setup()
    }
    
}

// MARK: - WelcomeTourDelegate
extension AppDelegate {
    func startWorkAfterProposal() {
        Settings.instance.showTutorial = false
        updateContactsInDBIfNeeded()
            .subscribe(
                onNext: { [unowned self] in
                    self.workWithReceipt()
            })
            .disposed(by: disposeBag)
    }
}

