//
//  CoreDataManager.swift
//  TemporaryContacts
//
//  Created by User on 21/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

class CoreDataManager {
    
    // Singleton
    static let instance = CoreDataManager()
    
    /// Старый менеджер по работе с карточками
    private lazy var businessCardsManager: BusinessCardsManager = BusinessCardsManager(adapter: FileAdapter())
    
    private init() {}
    
    // Entity for Name
    func entityForName(entityName: String, context: NSManagedObjectContext? = nil) -> NSEntityDescription {
        let context = context ?? self.managedObjectContext
        return NSEntityDescription.entity(forEntityName: entityName, in: context)!
    }

    
    // Fetched Results Controller for Entity Name
    func fetchedResultsController(
        entityName: String,
        keyForSort: String,
        context: NSManagedObjectContext? = nil
        ) -> NSFetchedResultsController<NSFetchRequestResult> {
        let context = context ?? self.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let sortDescriptor = NSSortDescriptor(key: keyForSort, ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: context,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        
        return fetchedResultsController
    }
    
    // Fetched Results Controller for Entity Name
    func fetchedResultsController(
        entityName: String,
        keysForSort: [String],
        cacheName: String?,
        newFirst: Bool = false,
        sectionNameKeyPath: String? = nil,
        context: NSManagedObjectContext? = nil
        ) -> NSFetchedResultsController<NSFetchRequestResult> {
        var sortDescriptors = [NSSortDescriptor]()
        if newFirst {
            sortDescriptors.append(NSSortDescriptor(key: "isNew", ascending: false))
        }
        for keyForSort in keysForSort{
            sortDescriptors.append(
                NSSortDescriptor(
                    key: keyForSort,
                    ascending: true,
                    selector: #selector(NSString.localizedCompare(_:))
                )
            )
        }
        
        return fetchedResultsController(
            entityName: entityName,
            sortingDescriptors: sortDescriptors,
            cacheName: cacheName,
            sectionNameKeyPath: sectionNameKeyPath,
            context: context
        )
    }
    
    // Fetched Results Controller for Entity Name
    func fetchedResultsController(
        entityName: String,
        sortingDescriptors: [NSSortDescriptor],
        cacheName: String?,
        sectionNameKeyPath: String? = nil,
        context: NSManagedObjectContext? = nil
        ) -> NSFetchedResultsController<NSFetchRequestResult> {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.sortDescriptors = sortingDescriptors
        
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: context ?? managedObjectContext,
            sectionNameKeyPath: sectionNameKeyPath,
            cacheName: cacheName
        )
        
        return fetchedResultsController
    }
    
    // Fetched Results Controller for Entity Name
    func fetchedResultsControllerTyped<T: NSFetchRequestResult>(
        entityName: String,
        sortingDescriptors: [NSSortDescriptor],
        cacheName: String? = nil,
        managedContext: NSManagedObjectContext? = nil,
        sectionNameKeyPath: String? = nil
        ) -> NSFetchedResultsController<T> {
        
        let fetchRequest = NSFetchRequest<T>(entityName: entityName)
        fetchRequest.sortDescriptors = sortingDescriptors
        
        let fetchedResultsController = NSFetchedResultsController<T>(
            fetchRequest: fetchRequest,
            managedObjectContext: managedContext ?? managedObjectContext,
            sectionNameKeyPath: sectionNameKeyPath,
            cacheName: cacheName
        )
        
        return fetchedResultsController
    }
    
    // MARK: - Core Data stack
    
    private let persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let url = applicationDocumentsDirectory.appendingPathComponent("TemporaryContacts.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(
                ofType: NSSQLiteStoreType,
                configurationName: nil,
                at: url,
                options: [
                    NSMigratePersistentStoresAutomaticallyOption: true,
                    NSInferMappingModelAutomaticallyOption: true
                ]
            )
        } catch {
            Metrica.instance.report(error,
                                    description: "Ошибка инициализации кордаты")
            
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    /// Приватный контекст для работы с данными
    private lazy var privateMOC: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    lazy private(set) var managedObjectContext: NSManagedObjectContext = {
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.parent = self.privateMOC
        return managedObjectContext
    }()
    
    /// Создает временный контекст с concurrencyType: .mainQueueConcurrencyType
    /// Необходим для создания новых сущностей и передания их в основную базу только по запросу
    var tempContextOnMainTread: NSManagedObjectContext {
        let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.parent = self.managedObjectContext
        return context
    }
    
    // MARK: - Core Data Saving support
    
    /// Сохраняет контекст и все родительские контексты, если есть изменения
    ///
    /// - Parameter context: Контаекст для сохранения.
    /// Если не передать - используется self.managedObjectContext
    func saveContext(_ context: NSManagedObjectContext? = nil, completion: (() -> Void)? = nil) {
        let context = context ?? self.managedObjectContext
        if context.hasChanges {
            context.performAndWait { [unowned self] in
                do {
                    try context.save()
                    if let parent = context.parent {
                        self.saveContext(parent, completion: completion)
                    } else {
                        completion?()
                    }
                } catch {
                    Metrica.instance.report(error,
                                            description: "Ошибка сохранения данных в базу")
                    abort()
                }
            }
        }
    }

    /// Сохраняет контекст в фоне
    ///
    /// - Parameters:
    ///   - context: Контекст. Если не передать, используется managedObjectContext
    ///   - saveBlock: Блок обработки данных перед сохранением
    func saveContextInBackground(
        _ context: NSManagedObjectContext? = nil,
        saveBlock: @escaping (NSManagedObjectContext) throws -> Void,
        completion: (() -> Void)? = nil)
    {
        let mainMoc = context ?? managedObjectContext
        let bgMoc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        bgMoc.parent = mainMoc
        bgMoc.performAndWait { [unowned self] in
            do {
                try saveBlock(bgMoc)
                self.saveContext(bgMoc, completion: completion)
            } catch {
                Metrica.instance.report(error,
                                        description: "Ошибка сохранения фонового контекста данных")
            }
        }
    }
    
    func deleteAllData(entity: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedObjectContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedObjectContext.delete(managedObjectData)
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
    func updateContactsBeforeTerminate(){
        let dbController = CoreDataManager.instance.fetchedResultsController(entityName: "Contacts", keyForSort: "firstName")
        do {
            try dbController.performFetch()
        } catch {
            NSLog(error.localizedDescription)
        }
        guard let contacts = dbController.fetchedObjects as? [Contacts] else { return }
        var i = 0
        while i < contacts.count{
            let contact = contacts[i]
            contact.isNew = false
            i += 1
            if i % 50 == 0{
                CoreDataManager.instance.saveContext()
            }
            
        }
        CoreDataManager.instance.saveContext()
    }
    
    // Старый метод формирования групп
    func prepareSections(sections: [NSFetchedResultsSectionInfo], nameDisplayOrder: SettingsDisplayOrder, checkNew: Bool) -> [Section]{
        
        var array: [Contacts] = []
        for section in sections{
            if let sectionContacts = section.objects as? [Contacts]{
                array.append(contentsOf: sectionContacts)
            }
        }

        var sections: [Section] = []
        var contacts: [Contacts]
        
        if checkNew{
            //Секция "Новые"
            let newContacts = array.filter({ (contact:Contacts) -> Bool in
                return contact.isNew
            })
            if newContacts.count > 0{
                sections.append(Section(name: "NewContactsSectionTitle".localized, data: newContacts))
            }
            
            contacts = array.filter({ (contact:Contacts) -> Bool in
                return !contact.isNew
            })
            
        }else{
            //Все контакты по секциям, и новые и старые
            contacts = array
        }
        
        //Секции по буквам
        let chars = contacts.map({ (contact: Contacts) -> Character in
            let name = nameDisplayOrder == .NameFirst ? contact.firstName: contact.lastName
            if name == nil || name!.isEmpty{
                return "-"
            }
            else{
                return name!.uppercased().first ?? "-"
            }
            
        })
        let unique = Array(Set(chars))
        
        var enSections:[Section] = []
        var ruSections:[Section] = []
        var smileSections:[Section] = []
        
        for sectionChar in unique.sorted(by: { $0 < $1}){
            var newSection = Section(name: "\(sectionChar)", data: [])
            let filteredContacts = array.filter({ (contact:Contacts) -> Bool in
                let name = nameDisplayOrder == .NameFirst ? contact.firstName: contact.lastName
                if name == nil || name!.isEmpty{
                    return sectionChar == "-" && !contact.isNew
                }
                else{
                    return (name?.uppercased().first ?? "-" ) == sectionChar && !contact.isNew
                }
            })
            
            if filteredContacts.count == 0 {
                continue
            }
            
            newSection.data = filteredContacts
            
            let enSet = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ")
            let ruSet = CharacterSet(charactersIn: "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя")
            
            if "\(sectionChar)".rangeOfCharacter(from: enSet.inverted) != nil {
                if "\(sectionChar)".rangeOfCharacter(from: ruSet.inverted) != nil {
                    smileSections.append(newSection)
                }else{
                    ruSections.append(newSection)
                }
            }else{
                enSections.append(newSection)
            }
        }
        sections.append(contentsOf: ruSections)
        sections.append(contentsOf: enSections)
        sections.append(contentsOf: smileSections)
        
        return sections
    }
    
    // MARK: - actions
    
    typealias MigrationAction = (NSManagedObjectContext) -> Bool
    
    /// Регистрирует все миграции
    func migrate() -> Observable<Void> {
        let migrateBusinessCards = registerMigration(name: "Move business cards to db") { [weak self] context in
            guard let sSelf = self else {
                return false
            }
            let request = NSFetchRequest<Contacts>(entityName: Contacts.entityName)
            let contacts: [Contacts]
            do {
                contacts = try context.fetch(request)
            } catch {
                Metrica.instance.report(error,
                                        description: "Ошибка поулчения контактов")
                return false
            }
            
            for contact in contacts {
                guard let identifier = contact.systemIdentifier else {
                    break
                }
                
                let data = sSelf.businessCardsManager.cardsAndCreationDates(for: identifier)
                for (imageData, creationDate) in data {
                    // Создаем запись визитки в базе
                    let card = BusinessCard(
                        imageData: imageData,
                        creationDate: creationDate,
                        context: context
                    )
                    card.contact = contact
                }
                sSelf.businessCardsManager.removeAllCards(for: identifier)
            }
            
            return true
        }
        
        return migrateBusinessCards
    }
    
    fileprivate func registerMigration(name: String, migration: @escaping MigrationAction) -> Observable<Void> {
        return Observable.create() { [weak self] subscribe in
            let disposable = Disposables.create()
            
            guard let sSelf = self else {
                subscribe.onNext(Void())
                return disposable
            }
            
            let migrationContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            migrationContext.parent = sSelf.managedObjectContext
            
            let migrationsRequest = NSFetchRequest<CoreDataMigration>(entityName: CoreDataMigration.entityName)
            migrationsRequest.predicate = NSPredicate(format: "name = %@", name)
            let registeredMigrationsWithSomeName: [CoreDataMigration]
            do {
                registeredMigrationsWithSomeName = try migrationContext.fetch(migrationsRequest)
            } catch {
                Metrica.instance.report(error,
                                        description: "Ошибка поулчения списка мигарций")
                registeredMigrationsWithSomeName = []
            }
            
            guard registeredMigrationsWithSomeName.isEmpty else {
                // Миграция уже выполнялась
                return disposable
            }
            
            // Выполняем миграция
            let success = migration(migrationContext)
            if success {
                // Сохраняем запись о миграции
                _ = CoreDataMigration(name: name, context: migrationContext)
                // Сохраняем контекст
                sSelf.saveContext(migrationContext)
            }
            
            return disposable
        }
        .take(1)
    }
}

// MARK: - coordinator

private let applicationDocumentsDirectory: NSURL = {
    let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return urls[urls.count-1] as NSURL
}()

private let managedObjectModel: NSManagedObjectModel = {
    let modelURL = Bundle.main.url(forResource: "TemporaryContacts", withExtension: "momd")!
    let url = NSSearchPathForDirectoriesInDomains(.documentDirectory, .allDomainsMask, false)
    print(url)
    return NSManagedObjectModel(contentsOf: modelURL)!
}()
