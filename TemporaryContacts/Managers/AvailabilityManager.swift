//
//  AvailabilityManager.swift
//  TemporaryContacts
//
//  Created by Konshin on 02.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData
import RxSwift

private struct Constants {
    /// Максимальное количество визиток
    static let limitedNumberOfBusinessCards = 15
    /// Максимальное количество груп
    static let limitedNumberOfGroups = 5
    /// Максимальное количество временных контактов
    static let limitedNumberOfTempContacts = 19
    /// Максимальное количество уведомлений
    static let limitedNumberOfNotifications = 9
}

class AvailabilityManager {
    
    enum Element {
        case businessCards
        case groups
        case tempContacts
        case notifications
    }
    
    /// Менеджер встроенных покупок
    fileprivate let purchasesManager: PurchasesManager
    /// Состояние приложения
    fileprivate let appState: AppState
    
    
    init(appState: AppState,
         purchasesManager: PurchasesManager = PurchasesManager.instance)
    {
        self.purchasesManager = purchasesManager
        self.appState = appState
    }
    
    // MARK: - getters
    
    /// Максимальное количество элмеентов, доступное для создания
    ///
    /// - Parameter element: Тип элемента
    /// - Returns: Максимальное количество
    func maxNumber(for element: Element) -> Int {
        if isFullUnlocked() {
            return Int.max
        } else {
            switch element {
            case .businessCards:
                return Constants.limitedNumberOfBusinessCards
            case .groups:
                return Constants.limitedNumberOfGroups
            case .tempContacts:
                return Constants.limitedNumberOfTempContacts
            case .notifications:
                return Constants.limitedNumberOfNotifications
            }
        }
    }
    
    func canAddNewElement(_ element: Element) -> Bool {
        let maxNumber = self.maxNumber(for: element)
        let currentNumber = currentNumberOf(elements: element)
        return maxNumber > currentNumber
    }
    
    func currentNumberOf(elements: Element) -> Int {
        switch elements {
        case .businessCards:
            return appState.numberOfCreatedBusinessCards
        case .groups:
            return appState.numberOfCreatedGroups
        case .tempContacts:
            return appState.numberOfCreatedTempContacts
        case .notifications:
            return appState.numberOfCreatedNotifications
        }
    }
    
    // MARK: - actions

    // MARK: private
    
    func isFullUnlocked() -> Bool {
        #if DEBUG
            return true
        #else
            return purchasesManager.isPurchased(.unlockAll) || purchasesManager.isPurchased(.unlockAllSpecialOffer)
        #endif
    }
}

/// Инстанс менеджера покупок
private let purchasesManager: PurchasesManager = PurchasesManager.instance

private var disposeBagKey: UInt8 = 0

extension UserNotificationProtocol {
    
    /// Объект DisposeBag
    private var p_disposeBag: DisposeBag {
        if let bag = objc_getAssociatedObject(self, &disposeBagKey) as? DisposeBag {
            return bag
        } else {
            let bag = DisposeBag()
            objc_setAssociatedObject(self, &disposeBagKey, bag, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            return bag
        }
    }
    
    /// Отображает уведомление о превышении лимита бесплатной версии и предлагает купить полную
    ///
    /// - Parameter availabilityElement: Тип элемента с превышением лимита (Визитка, группа и тд)
    func showLimitationAlert(for availabilityElement: AvailabilityManager.Element) {
        let elementTitle: String = {
            switch availabilityElement {
            case .businessCards:
                return "availability.alert.cannot-create.business-card".localized
            case .groups:
                return "availability.alert.cannot-create.group".localized
            case .tempContacts:
                return "availability.alert.cannot-create.temp-contact".localized
            case .notifications:
                return "availability.alert.cannot-create.notification".localized
            }
        }()
        showAlert(
            title: "availability.alert.cannot-create".localized + " " + elementTitle,
            message: "availability.alert.unload-message".localized,
            actions: [
                UIAlertAction(title: "Close".localized, style: .cancel, handler: nil),
                UIAlertAction(title: "buy".localized, style: .default) { [unowned self] _ in
                    purchasesManager.buy(.unlockAll)
                        .take(1)
                        .subscribe(
                            onNext: { [weak self] result in
                                switch result {
                                case .completed:
                                    self?.showThanksScreen()
                                default:
                                    break
                                }
                        })
                        .addDisposableTo(self.p_disposeBag)
                }
            ]
        )
    }
}
