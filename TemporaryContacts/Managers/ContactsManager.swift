//
//  ContactsManager.swift
//  TemporaryContacts
//
//  Created by User on 22/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import Contacts
import CoreData
import AddressBook
import UIKit
import ContactsUI
import RxSwift
import RxCocoa

/// Менеджер по работе с контактами (системными и контактами приложения)
class ContactsManager {
    
    enum ErrorType: Error {
        case cantGetDataFromDB
        case isAlreadyInUpdate
    }
    
    // Singleton
    static let instance = ContactsManager()
    
    private init() {
        
    }
    
    private let businessCardsManager: BusinessCardsManager = BusinessCardsManager(adapter: FileAdapter())

    private(set) var contactUpdateInProcess = false
    
    // MARK: - actions
    
    // MARK: - work with system contacts
    
    /// Получает список системных контактов
    ///
    /// - Parameter fetchingKeys: Список необходимых ключей от контактов. 
    /// По дефолту получает все необходимое для отображения списка
    /// - Returns: Список системных контактов
    private func getContactsFromSystem(fetchingKeys: [Any]? = nil) -> Set<CNContact> {
        let keysToFetch = fetchingKeys ?? [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactViewController.descriptorForRequiredKeys(),
            CNContactNicknameKey,
            CNContactEmailAddressesKey,
            CNContactUrlAddressesKey,
            CNContactImageDataKey,
            CNContactPhoneNumbersKey,
            CNContactOrganizationNameKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey,
        ]
        let contactStore = CNContactStore()
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results = Set<CNContact>()
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            let predicate = fetchPredicate
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: predicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.formUnion(containerResults)
            } catch {
                print("Error fetching results for container: \(error)")
            }
        }
        
        return results
    }
    
    /// Получает системный контакт по идентификатору
    ///
    /// - Parameters:
    ///   - systemIdentifier: Системный идентификатор контакта
    ///   - fetchingKeys: Необходимые поля контакта
    /// По дуфолту получает все поля для отображения в CNContactViewController
    /// - Returns: Возможная модель контакта
    func getContact(systemIdentifier: String, fetchingKeys: [Any]? = nil) -> CNContact? {
        let store = CNContactStore()
        let keysToFetch = fetchingKeys ?? [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactViewController.descriptorForRequiredKeys(),
            CNContactEmailAddressesKey,
            CNContactImageDataKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactOrganizationNameKey,
            CNContactThumbnailImageDataKey] as [Any]
        
        var storeContact: CNContact? = nil
        do {
            storeContact = try store.unifiedContact(withIdentifier: systemIdentifier, keysToFetch: keysToFetch as! [CNKeyDescriptor])
        } catch {
            print("Error fetching containers")
            return nil
        }
        return storeContact
    }
    
    /// Удаляет контакт из системной книги
    ///
    /// - Parameter systemIdentifier: Системный идентификатор контакта
    func deleteContact(systemIdentifier: String?) {
        guard let identifier = systemIdentifier,
            let contact = getContact(
                systemIdentifier: identifier,
                fetchingKeys: [CNContactFormatter.descriptorForRequiredKeys(for: .fullName)]
            ) else
        {
            return
        }

        let name = contact.givenName
        
        let store = CNContactStore()
        let req = CNSaveRequest()
        let mutableContact = contact.mutableCopy() as! CNMutableContact
        req.delete(mutableContact)
        
        do {
            try store.execute(req)
            print("Success, You deleted the user named: \(name)")
        } catch let e{
            print("Error = \(e)")
        }
    }
    
    /// Подготавливает модель системного контакта из модели Contacts
    ///
    /// - Parameters:
    ///   - dbContact: Модель контакта
    ///   - storeContact: Модель для обновления
    /// - Returns: Обновленная модель системного контакта
    private func prepareContact(dbContact: Contacts, storeContact: CNMutableContact) -> CNMutableContact {
        let contact = storeContact
        
        contact.givenName = dbContact.firstName ?? ""
        contact.familyName = dbContact.lastName ?? ""
        contact.organizationName = dbContact.specialization ?? ""
        
        //        var bugseeString = "\n\nИмопрт контакта: \(contact.givenName) \(contact.familyName)"
        //        bugseeString += "\nСпециальность: \(contact.organizationName) _"
        //        Bugsee.log(bugseeString)
        
        if let image = AppManager.instance.loadImageJPG(name: dbContact.avatarFile ?? ""){
            contact.imageData = image.jpegData(compressionQuality: 1)
        }
        else{
            contact.imageData = nil
        }

        if let phones = dbContact.phones?.allObjects as? [Phones]{
            contact.phoneNumbers = []
            for phone in phones{
                let labeledPhone = CNLabeledValue<CNPhoneNumber>(label: phone.label ?? CNLabelPhoneNumberMobile, value: CNPhoneNumber(stringValue: phone.digits ?? ""))
                contact.phoneNumbers.append(labeledPhone)
            }
        }
        
        if let emails = dbContact.emails?.allObjects as? [Emails]{
            contact.emailAddresses = []
            for email in emails{
                let labeledEmail = CNLabeledValue<NSString>(label: email.label ?? CNLabelHome, value: NSString(string: email.email ?? ""))
                contact.emailAddresses.append(labeledEmail)
            }
        }
        
        if let webSites = dbContact.websites?.allObjects as? [Website] {
            contact.urlAddresses = []
            for webSite in webSites{
                let labeledSite = CNLabeledValue<NSString>(
                    label: CNLabelURLAddressHomePage,
                    value: (webSite.url as NSString?) ?? ""
                )
                contact.urlAddresses.append(labeledSite)
            }
        }
        
        return contact
    }
    
    /// Создает новую модель системного контакта на основании локалького контакта
    ///
    /// - Parameter dbContact: Локальный контиакт
    /// - Returns: Системный контакт
    func addContact(dbContact: Contacts) -> CNMutableContact? {
        let store = CNContactStore()
        
        let contact = prepareContact(dbContact: dbContact, storeContact: CNMutableContact())
        
        let req = CNSaveRequest()
        req.add(contact, toContainerWithIdentifier: nil)
        
        do{
            try store.execute(req)
            print("Success, You added the user named: \(contact.givenName)")
        } catch let e{
            print("Error = \(e)")
            return nil
        }
        return contact
    }
    
    /// Обновляет модель локального контакта из системного контакта, если такой есть в системной книге
    ///
    /// - Parameter dbContact: Локальная модель контакта для обновления
    func updateContact(dbContact: Contacts) {
        
        guard let systemIdentifier = dbContact.systemIdentifier else{ return }
        
        let storeContact = getContact(systemIdentifier: systemIdentifier)
        
        if storeContact != nil{
            let req = CNSaveRequest()
            let mutableContact = storeContact!.mutableCopy() as! CNMutableContact
            
            let contact = prepareContact(dbContact: dbContact, storeContact: mutableContact)
            
            req.update(contact)
            
            let store = CNContactStore()
            do{
                try store.execute(req)
                print("Success, You updated the user named: \(storeContact!.givenName)")
            } catch let e{
                print("Error = \(e)")
            }
        }
    }
    
    // MARK: - work with DB
    
    /// Добавляет нового системного контакта в базу
    /// Работает асинхронно
    ///
    /// - Parameter contact: Контакт для добавления
    func addNewContactToDB(_ contact: CNContact, businessCards: [BusinessCard] = [], context: NSManagedObjectContext? = nil) {
        let context = context ?? CoreDataManager.instance.managedObjectContext
        let newContact = Contacts(context: context)
        self.populateContact(
            newContact,
            from: contact,
            isNew: true,
            context: context
        )
        newContact.setBusinessCards(cards: businessCards)
        CoreDataManager.instance.saveContext(context)
    }
    
    // MARK: - work with DB
    
    /// Обновляет модель контакта в базе данных, если найдет
    /// Если не найдет - добавляет нового пользователя
    /// Работает асинхронно
    ///
    /// - Parameter contact: Контакт для обновления
    func updateContactInDB(_ contact: CNContact) {
        CoreDataManager.instance.saveContextInBackground(saveBlock: { [unowned self] context in
            let dbController = CoreDataManager.instance.fetchedResultsController(
                entityName: "Contacts",
                keyForSort: "firstName",
                context: context
            )
            dbController.fetchRequest.predicate = NSPredicate(
                format: "systemIdentifier == %@", contact.identifier
            )
            
            do {
                try dbController.performFetch()
            } catch {
                print("error search contact for update: \(error)")
                //Контакт не найден
            }
            if let findet = dbController.fetchedObjects?.first as? Contacts {
                self.populateContact(
                    findet,
                    from: contact,
                    isNew: false,
                    context: context
                )
            } else {
                self.addNewContactToDB(contact)
            }
        })
    }
    
    //Загрузка контактов из системной ТК в приложение
    @discardableResult func synchronizeWithSystem() -> Observable<Void> {
        return Observable.create { [unowned self] observer in
            let disposable = Disposables.create()
            
            guard !self.contactUpdateInProcess else {
                observer.onError(ErrorType.isAlreadyInUpdate)
                return disposable
            }
            self.contactUpdateInProcess = true
            
            let status = CNContactStore.authorizationStatus(for: .contacts)
            switch status{
            case .authorized:
                print("Contacts access - Already authorized")
                /* Access the address book */
            case .denied:
                print("Denied access to address book")
            case .notDetermined:
                let store = CNContactStore()
                store.requestAccess(for: .contacts) { (success:Bool, error:Error?) in
                    if success{
                        print("access granted")
                    }
                    else{
                        if error != nil{
                            print(error!.localizedDescription)
                        }
                    }
                }
            case .restricted:
                print("Access restricted")
            @unknown default:
                fatalError()
            }
            
            CoreDataManager.instance.saveContextInBackground(
                saveBlock: { [unowned self] context in
                    let dbController = CoreDataManager.instance.fetchedResultsController(
                        entityName: Contacts.entityName,
                        keyForSort: "firstName",
                        context: context
                    )
                    do {
                        try dbController.performFetch()
                    } catch {
                        observer.onError(error)
                        throw error
                    }
                    
                    //Все контакты, которые храняться в DB приложения
                    
                    guard let dbContacts = dbController.fetchedObjects else {
                        self.contactUpdateInProcess = false
                        let error = ErrorType.cantGetDataFromDB
                        observer.onError(error)
                        throw error
                    }
                    
                    let contactsMap = dbContacts.reduce([String: Contacts]()) { (acc, object) in
                        guard let contact = object as? Contacts, let identifier = contact.systemIdentifier else {
                            return acc
                        }
                        
                        var mAcc = acc
                        mAcc[identifier] = contact
                        return mAcc
                    }
                    
                    NotificationCenter.default.post(
                        name: NSNotification.Name.contantsWillUpdateFromPhoneBook,
                        object: nil
                    )
                    
                    //Первый импорт после установки приложения
                    let defaults = UserDefaults.standard
                    let isFirstImport = !defaults.bool(forKey: "checkNewContacts")
                    
                    //Все контакты, которые храняться в телефонной книги iphone
                    let systemContacts = self.getContactsFromSystem()
                    
                    //Загружаем контакты в DB приложения, сохраняем dbContext каждые 50 записей
                    for importContact in systemContacts {
                        
                        if let contact = contactsMap[importContact.identifier] {
                            self.populateContact(
                                contact,
                                from: importContact,
                                isNew: false,
                                context: context
                            )
                            contact.updateLastDateOfCreationBusinessCard()
                        } else {
                            /// Новый контакт
                            let isNew = !isFirstImport
                            
                            let newContact = Contacts(context: context)
                            newContact.isTempContact = false
                            newContact.systemIdentifier = importContact.identifier
                            self.populateContact(
                                newContact,
                                from: importContact,
                                isNew: isNew,
                                context: context
                            )
                        }
                    }
                    
                    defaults.set(true, forKey: "checkNewContacts")
                    self.contactUpdateInProcess = false
                },
                completion: {
                    NotificationCenter.default.post(name: NSNotification.Name.contantsDidUpdateFromPhoneBook, object: nil)
                    observer.onNext(Void())
            })
            
            return disposable
        }
    }
    
    //Импортируемый контакт
    /// Заполняет поля контакта из модели системного контакта
    ///
    /// - Parameters:
    ///   - contact: Болванка контакта для заполнения
    ///   - importContact: Системная модель контакта
    ///   - isNew: Состояние нового контакта
    ///   - context: Контекст работы с CoreData
    fileprivate func populateContact(
        _ contact: Contacts,
        from importContact: CNContact,
        isNew: Bool,
        context: NSManagedObjectContext
        )
    {
        contact.isNew = isNew
        contact.firstName = importContact.givenName
        contact.lastName = importContact.familyName
        contact.specialization = importContact.organizationName
        contact.nickname = importContact.nickname
        contact.systemIdentifier = importContact.identifier
        contact.middleName = importContact.middleName
        contact.previousFamilyName = importContact.previousFamilyName
        contact.departmentName = importContact.departmentName
        contact.jobTitle = importContact.jobTitle
        contact.phoneticGivenName = importContact.phoneticGivenName
        contact.phoneticMiddleName = importContact.phoneticMiddleName
        contact.phoneticFamilyName = importContact.phoneticFamilyName
        contact.namePrefix = importContact.namePrefix
        contact.nameSuffix = importContact.nameSuffix
        
        if #available(iOS 10.0, *) {
            contact.phoneticOrganizationName = importContact.phoneticOrganizationName
        }

        let phones = importContact.phoneNumbers.map({ (phone: CNLabeledValue<CNPhoneNumber>) -> Phones in
            let result = Phones(context: context)
            result.systemIdentifier = phone.identifier
            result.label = phone.label ?? "phone".localized
            result.digits = phone.value.stringValue
            return result
        })
        let emails = importContact.emailAddresses.map({ (email: CNLabeledValue<NSString>) -> Emails in
            let result = Emails(context: context)
            result.systemIdentifier = email.identifier
            result.label = email.label ?? "email".localized
            result.email = "\(email.value)"
            return result
        })
        let sites = importContact.urlAddresses.map({ (url: CNLabeledValue<NSString>) -> Website in
            let result = Website(context: context)
            result.url = url.value as String
            return result
        })
        
        //Если контакт новый
        if !isNew {
            //Удаление старых телефонов
            if let phonesSet = contact.phones{
                contact.removeFromPhones(phonesSet)
            }
            if let emailsSet = contact.emails{
                contact.removeFromEmails(emailsSet)
            }
        }
        
        if contact.avatarFile?.isEmpty != false {
            //Создание имени файла аватарки
            let nameHash = "\(importContact.givenName) \(importContact.familyName))".hash
            let avatarFile = "\(nameHash)"+TextHelper.randomHash()
            contact.avatarFile = avatarFile
        }
        
        //обновление аватарки
        if let avatarFile = contact.avatarFile{
            if let imageData = importContact.thumbnailImageData, let image = UIImage(data: imageData) {
                AppManager.instance.saveImageJPG(image: image, name: avatarFile)
            }
            else{
                AppManager.instance.deleteFile(name: avatarFile, type: "jpg")
            }
        }
        
        contact.phones = NSSet(array: phones)
        contact.emails = NSSet(array: emails)
        contact.websites = NSSet(array: sites)
        
        updateContactDisplayInformation(contact)
    }
    
    /// Обновляет ключ сортировки для контакта в базе
    ///
    /// - Parameter contact: Модель контакта
    func updateContactDisplayInformation(_ contact: Contacts) {
        let (name, specialization) = contact.nameAndSpecialization()
        contact.displayName = name
        contact.displaySpecialization = specialization
        
        if contact.isNew {
            contact.sectionIdentifier = "NewContactsSectionTitle".localized
        } else if let firstChar = name.first.flatMap({ String($0) }), firstChar.isLetter {
            contact.sectionIdentifier = firstChar.uppercased()
            contact.hasSectionIdentifier = true
        } else {
            contact.sectionIdentifier = ""
            contact.hasSectionIdentifier = false
        }
    }
    
    //Удаление из DB контактов, которые были удалены из системной ТК, пока приложение было в бэкграунде
    func deleteRemovedSystemContacts() -> Observable<Void> {
        let observable = Observable<Void>.create { [unowned self] observer in
            /// Результат блока
            let disposable = Disposables.create()

            self.contactUpdateInProcess = true
            
            let systemContacts = self.getContactsFromSystem()
            let systemContactIdsMap = systemContacts.reduce([String: CNContact]()) { acc, contact in
                var mAcc = acc
                mAcc[contact.identifier] = contact
                return mAcc
            }
            
            CoreDataManager.instance.saveContextInBackground(
                saveBlock: { [unowned self] context in
                    
                    let fetchedRequest = NSFetchRequest<Contacts>(entityName: Contacts.entityName)
                    
                    //Все контакты, которые храняться в DB приложения
                    let dbContacts: [Contacts]
                    do {
                        dbContacts = try context.fetch(fetchedRequest)
                    } catch {
                        observer.onError(ErrorType.cantGetDataFromDB)
                        return
                    }
                    
                    //Все контакты, которые есть в DB, но которые были удалены из системной ТК
                    let contacts = dbContacts.filter({ (contact: Contacts) -> Bool in
                        guard let identifier = contact.systemIdentifier else {
                            return true
                        }
                        return systemContactIdsMap[identifier] == nil
                    })
                    
                    guard !contacts.isEmpty else {
                        self.contactUpdateInProcess = false
                        observer.onNext(Void())
                        observer.onCompleted()
                        return
                    }
                    
                    var i = 0
                    while i < contacts.count{
                        let c = contacts[i]
                        self.delete(contact: c, deleteFromSystem: false, saveContext: false)
                        if i > 0 && i % 100 == 0{
                            CoreDataManager.instance.saveContext(context)
                        }
                        i += 1
                    }
                    self.contactUpdateInProcess = false
                    print("Удаление старых контактов завершено")
                },
                completion: {
                    observer.onNext(Void())
            })
            
            return disposable
        }
        
        return observable
    }
    
    /// Удаляет модель контакта из контекста
    ///
    /// Заодно удаляет все визитки и модель из СКК
    /// - Parameter contact: Модель контакта для удаления
    func delete(contact: Contacts, deleteFromSystem: Bool = true, saveContext: Bool = true) {
        if let cards = contact.businessCards?.allObjects as? [NSManagedObject] {
            // Удаляем все визитки
            cards.forEach() { card in
                card.managedObjectContext?.delete(card)
            }
        }
        
        if let notifications = contact.notifications?.allObjects as? [NSManagedObject] {
            // Удаляем все напоминания
            notifications.forEach() { n in
                n.managedObjectContext?.delete(n)
            }
        }
        
        let identifier = contact.systemIdentifier
        let context = contact.managedObjectContext
        context?.delete(contact)
        if let id = identifier {
            // Старый метод удаления визиток
            businessCardsManager.removeAllCards(for: id)
            if deleteFromSystem {
                deleteContact(systemIdentifier: id)
            }
        }
        if saveContext {
            try? context?.save()
        }
    }
    
    // MARK: - groups
    
    /// Удаляет группу из базы. Заодно удаляет всю дополнительную инфомрацию о ней (Напоминания)
    ///
    /// - Parameters:
    ///   - group: Группа на удаление
    ///   - deleteTempContacts: Удалить все временные контакты группы
    ///   - saveContext: Сохранять после удаления контекст
    func delete(group: Groups, deleteTempContacts: Bool, saveContext: Bool = true) {
        if let notifications = group.notifications?.allObjects as? [NSManagedObject] {
            // Удаляем все напоминания
            notifications.forEach() { n in
                n.managedObjectContext?.delete(n)
            }
        }
        
        if let contacts = group.contacts?.allObjects as? [Contacts] {
            for contact in contacts {
                if contact.isTempContact{
                    delete(contact: contact, deleteFromSystem: true, saveContext: false)
                }
            }
        }
        
        let context = group.managedObjectContext
        context?.delete(group)
        if saveContext {
            try? context?.save()
        }
        Metrica.instance.send(.groupIsDeleted)
    }
    
    // MARK: - test
    
    func addTestContacts() {
        let now = Date()
        
        let context = CoreDataManager.instance.managedObjectContext
        
        let lawyer = Contacts()
        lawyer.firstName = "test-contacts.lawyer".localized
        lawyer.specialization = "test-contacts.example-card".localized
        lawyer.isTempContact = true
        lawyer.isExample = true
        
        let lawyerNotification = AppNotification()
        lawyerNotification.text = "test-contacts.lawyer.notification".localized
        lawyerNotification.date = now.dateOfMorningAtDatSince(days: 1) as NSDate as Date
        lawyer.addToNotifications(lawyerNotification)
        
        let lawyerCard1 = BusinessCard(image: #imageLiteral(resourceName: "business_cards_example_1"), creationDate: Date())
        lawyer.addToBusinessCards(lawyerCard1)
        let lawyerCard2 = BusinessCard(image: #imageLiteral(resourceName: "business_cards_example_2"), creationDate: Date())
        lawyer.addToBusinessCards(lawyerCard2)
        
        let buildMaterials = Contacts()
        buildMaterials.firstName = "test-contacts.build-materials".localized
        buildMaterials.specialization = "test-contacts.example-billboard".localized
        buildMaterials.isTempContact = true
        buildMaterials.isExample = true
        
        let buildMaterialsNotification = AppNotification()
        buildMaterialsNotification.text = "test-contacts.build-materials.notification".localized
        buildMaterialsNotification.date = now.dateOfMorningAtDatSince(days: 2) as NSDate as Date
        buildMaterials.addToNotifications(buildMaterialsNotification)
        
        let buildMaterialsCard = BusinessCard(image: #imageLiteral(resourceName: "business_cards_example_3"), creationDate: Date())
        buildMaterials.addToBusinessCards(buildMaterialsCard)
        
        let sellAuto = Contacts()
        sellAuto.firstName = "test-contacts.sell-auto".localized
        sellAuto.specialization = "test-contacts.example-photo".localized
        sellAuto.isTempContact = true
        sellAuto.isExample = true
        
        let sellAutoNotification = AppNotification()
        sellAutoNotification.text = "test-contacts.sell-auto.notification".localized
        sellAutoNotification.date = now.dateOfMorningAtDatSince(days: 3) as NSDate as Date
        sellAuto.addToNotifications(sellAutoNotification)
        
        let sellAutoCard = BusinessCard(image: #imageLiteral(resourceName: "business_cards_example_4"), creationDate: Date())
        sellAuto.addToBusinessCards(sellAutoCard)
        
        let website = Contacts()
        website.firstName = "test-contacts.website".localized
        website.specialization = "test-contacts.example-website".localized
        website.isTempContact = true
        website.isExample = true
        
        let websiteNotification = AppNotification()
        websiteNotification.text = "test-contacts.website.notification".localized
        websiteNotification.date = now.dateOfMorningAtDatSince(days: 2) as NSDate as Date
        website.addToNotifications(websiteNotification)
        
        let websiteCard = BusinessCard(image: #imageLiteral(resourceName: "business_cards_example_5"), creationDate: Date())
        website.addToBusinessCards(websiteCard)
        
        for contact in [lawyer, buildMaterials, sellAuto, website] as [Contacts] {
            // Добавляем контакты в СКК
            if let systemContact = addContact(dbContact: contact) {
                populateContact(contact, from: systemContact, isNew: false, context: context)
            }
        }
        
        CoreDataManager.instance.saveContext(context)
    }
    
    /// Удаляет тестовые контакты
    func deleteTestContacts() {
        let context = CoreDataManager.instance.managedObjectContext
        let dbController = CoreDataManager.instance.fetchedResultsController(
            entityName: "Contacts",
            keyForSort: "firstName"
        )
        dbController.fetchRequest.predicate = NSPredicate(format: "isExample == true")
        
        do {
            try dbController.performFetch()
        } catch {
            NSLog(error.localizedDescription)
        }
        
        //Все контакты, которые храняться в DB приложения
        guard let dbContacts = dbController.fetchedObjects as? [Contacts] else { return }
        
        dbContacts.forEach { [weak self] contact in
            self?.delete(contact: contact, deleteFromSystem: true, saveContext: false)
        }
        CoreDataManager.instance.saveContext(context)
    }
}
