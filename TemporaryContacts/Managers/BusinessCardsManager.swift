//
//  BusinessCardsManager.swift
//  TemporaryContacts
//
//  Created by Алексей Коньшин on 12.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

private struct Constants {
    static let imagePathExtension = "img"
}

/// Менеджер по работе с визитками
/// Хранение визиток переписано на кор дату, менеджер нужен только для разовой миграции
class BusinessCardsManager {
    private let fileAdapter: FileAdapter
    
    typealias BusinessCard = UIImage
    
    init(adapter: FileAdapter) {
        fileAdapter = adapter
    }
    
    /// Возвращает визитки пользователя по идентификатору
    ///
    /// - Parameter contactIdentifier: Идентификатор пользователя
    @available(*, unavailable)
    func cards(for contactIdentifier: SystemIdentifier) -> [BusinessCard] {
        guard !contactIdentifier.isEmpty else {
            return []
        }
        let components = pathComponents(for: contactIdentifier)
        let fileNames = fileAdapter.content(of: cardsDirectory, pathComponents: components)
        return fileNames.compactMap() { fileName in
            guard let data = fileAdapter.data(
                at: cardsDirectory,
                pathComponents: components,
                fileName: fileName
                ) else {
                    return nil
            }
            
            return UIImage(data: data)
        }
    }
    
    /// Возвращает визитки пользователя по идентификатору
    ///
    /// - Parameter contactIdentifier: Идентификатор пользователя
    func cardsAndCreationDates(for contactIdentifier: SystemIdentifier) -> [(cardData: Data, creationDate: Date?)] {
        guard !contactIdentifier.isEmpty else {
            return []
        }
        let components = pathComponents(for: contactIdentifier)
        let fileNames = fileAdapter.content(of: cardsDirectory, pathComponents: components)
        return fileNames.compactMap() { fileName in
            guard let data = fileAdapter.data(
                at: cardsDirectory,
                pathComponents: components,
                fileName: fileName
                ) else {
                    return nil
            }
            let creationDate = fileAdapter.creationDate(
                at: cardsDirectory,
                pathComponents: components,
                fileName: fileName
            )
            
            return (data, creationDate)
        }
    }
    
    /// Задает новый список визиток для контакта
    ///
    /// - Parameters:
    ///   - cards: Визитки
    ///   - contactIdentifier: Идентификатор контакта
    @available(*, unavailable)
    func setCards(_ cards: [BusinessCard], for contactIdentifier: SystemIdentifier) {
        guard !contactIdentifier.isEmpty else {
            return
        }
        removeAllCards(for: contactIdentifier)
        let components = pathComponents(for: contactIdentifier)
        for (index, card) in cards.enumerated() {
            guard let data = card.jpegData(compressionQuality: 0.5) else {
                continue
            }
            let fileName = "\(index).\(Constants.imagePathExtension)"
            do {
                try fileAdapter.setData(
                    data,
                    directory: cardsDirectory,
                    pathComponents: components,
                    fileName: fileName
                )
            } catch {
                print("Ошибка сохранения визитки")
            }
        }
        NotificationCenter.default.post(name: .businessCardsDidUpdate, object: nil)
    }
    
    /// Удаляет все визитики контакта
    ///
    /// - Parameter contactIdentifier: Идентификатор контакта
    func removeAllCards(for contactIdentifier: SystemIdentifier) {
        let components = pathComponents(for: contactIdentifier)
        do {
            try fileAdapter.deleteContent(of: cardsDirectory, pathComponents: components)
            NotificationCenter.default.post(name: .businessCardsDidUpdate, object: nil)
        } catch {
            print("Ошибка удаления визиток: \(error)")
        }
    }
    
    /// Возвращает информацию о том, если ли визитки у контакта
    ///
    /// - Parameter contactIdentifier: Идентификатор кнотакта
    /// - Returns: Есть ли визитки
    func hasCards(for contactIdentifier: SystemIdentifier) -> Bool {
        let components = pathComponents(for: contactIdentifier)
        return !fileAdapter.content(of: cardsDirectory, pathComponents: components, includeDirectories: false, recoursive: false).isEmpty
    }
    
    /// Возвращает количество визиток всего
    func numberOfAllCards() -> Int {
        let content = fileAdapter.subpaths(of: cardsDirectory, pathComponents: ["BusinessCards"])
        let filtered = content.filter({ $0.hasSuffix(Constants.imagePathExtension) })
        return filtered.count
    }
    
    // MARK: - private
    
    private let cardsDirectory = FileAdapter.Directory.caches
    
    private func pathComponents(for identifier: String) -> [String] {
        return ["BusinessCards", identifier]
    }
}


extension Notification.Name {
    /// Отправляется при изменении визиток
    static let businessCardsDidUpdate = NSNotification.Name(rawValue: "businessCardsDidUpdate")
}
