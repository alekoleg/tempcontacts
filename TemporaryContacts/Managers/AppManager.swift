//
//  AppManager.swift
//  TemporaryContacts
//
//  Created by User on 23/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AppManager {
    
    // Singleton
    static let instance = AppManager()
    
    private init() {}
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func saveImagePNG(image: UIImage, name: String){
        if let data = image.pngData() {
            let filename = getDocumentsDirectory().appendingPathComponent("\(name).png")
            try? data.write(to: filename)
        }
    }
    
    func saveImageJPG(image: UIImage, name: String){
        if let data = image.jpegData(compressionQuality: 0.5) {
            let filename = getDocumentsDirectory().appendingPathComponent("\(name).jpg")
            try? data.write(to: filename)
        }
    }
    
    func loadImagePNG(name:String) -> UIImage?{
        let imageURL =  getDocumentsDirectory().appendingPathComponent("\(name).png")
        return UIImage(contentsOfFile: imageURL.path)
    }
    
    func loadImageJPG(name:String) -> UIImage?{
        let imageURL =  getDocumentsDirectory().appendingPathComponent("\(name).jpg")
        return UIImage(contentsOfFile: imageURL.path)
    }
    
    func deleteFile(name: String, type:String){
        let fileManager = FileManager.default
        let filePath = getDocumentsDirectory().appendingPathComponent("\(name).\(type)")
        if !fileManager.fileExists(atPath: filePath.path) {
            return
        }
        
        do {
            try fileManager.removeItem(atPath: filePath.path)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }
    
    
    /// Создает UILocalNotification для объектов в базе, для которых они нужны
    func createNotifications() {
//        let contactsDBController = CoreDataManager.instance
//            .fetchedResultsController(entityName: "Contacts", keyForSort: "firstName")
//        contactsDBController.fetchRequest.predicate = NSPredicate(
//            format: "notificationDate != NULL AND notificationText != NULL"
//        )
//        
//        do {
//            try contactsDBController.performFetch()
//        } catch {
//            print("Ошибка получения контактов с уведомлениями")
//        }
//        guard let contacts = contactsDBController.fetchedObjects as? [Contacts] else { return }
//        
//        for contact in contacts{
//            if contact.notificationDate! as Date > Date(){
//                createContactNotification(contact: contact)
//            }
//        }
//        
//        let groupsDBController = CoreDataManager.instance
//            .fetchedResultsController(entityName: "Groups", keyForSort: "name")
//        groupsDBController.fetchRequest.predicate = NSPredicate(
//            format: "notificationDate != NULL AND notificationText != NULL"
//        )
//        
//        do {
//            try groupsDBController.performFetch()
//        } catch {
//            print(error)
//        }
//
//        guard let groups = groupsDBController.fetchedObjects as? [Groups] else { return }
//        
//        for group in groups{
//            if group.notificationDate! as Date > Date(){
//                createGroupNotification(group: group)
//            }
//        }
    }
    
    func createContactNotification(contact: Contacts){
//        let id = "\(contact.objectID.hash)"
//        let name = "\(contact.firstName ?? "")\(contact.lastName != nil ? " "+contact.lastName! : "")"
//        let text = "\(name) - \(contact.notificationText!)"
//        createNotification(identifier: id, text: text, fireDate: contact.notificationDate! as Date)
    }
    
    func createGroupNotification(group: Groups){
//        let id = "\(group.objectID.hash)"
//        let name = "Group".localized + " \(group.name ?? "")"
//        let text = "\(name) - \(group.notificationText!)"
//        createNotification(identifier: id, text: text, fireDate: group.notificationDate! as Date)
    }
    
    private func createNotification(identifier: String, text: String, fireDate: Date) {
        let notification = UILocalNotification()
        let userInfo :  Dictionary<String,String> = ["notificationIdentifier" : identifier]
        notification.userInfo = userInfo
        notification.alertBody = text
        notification.alertAction = "Close".localized
        notification.fireDate = fireDate
        notification.category = "TemporaryContactsNotification"
        notification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    func removeNotification(identifier: String){
//        let app:UIApplication = UIApplication.shared
//        if let events = app.scheduledLocalNotifications{
//            for event in events {
//                let notification = event as UILocalNotification
//                let userInfoCurrent = notification.userInfo as! [String:AnyObject]
//                let uid = userInfoCurrent["notificationIdentifier"] as! String
//                if uid == identifier {
//                    //Cancelling local notification
//                    app.cancelLocalNotification(notification)
//                    break;
//                }
//            }
//        }
        
    }
    
    func cancelNotifications() {
//        if let scheduledLocalNotifications = UIApplication.shared.scheduledLocalNotifications{
//            for notification in scheduledLocalNotifications as [UILocalNotification] {
//                UIApplication.shared.cancelLocalNotification(notification)
//            }
//        }
        
    }
    
    // MARK: - settings
}
