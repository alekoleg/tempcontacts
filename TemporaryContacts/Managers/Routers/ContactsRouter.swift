//
//  ContactsRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import CoreData
import PhoneNumberKit
import RxSwift

/// Роутер для навигации по вкладке контактов
class ContactsRouter: Router {
    
    typealias RootController = UINavigationController
    
    /// Контроллер навигации
    fileprivate let navigation: UINavigationController = UINavigationController()
    /// Зависимости
    let dependencies: DependenciesStorage
    /// convertors
    var contactInfoConverter: ContactControllerDelegateConverter?
    var contactsInfoController: CNContactViewController?
    var contactCreateConverter: ContactControllerDelegateConverter?
    
    /// Делегат
    weak var delegate: ContactsRouterDelegate?
    /// Ссылка на TabsRouter
    weak var tabsSelector: TabsSelector?
    
    init(dependencies: DependenciesStorage) {
        self.dependencies = dependencies
        
        let contactsRootVM = RootViewModel(
            settings: Settings.instance,
            router: self,
            communicationHelper: dependencies.communicationHelper,
            partialFormatter: dependencies.partialFormatter,
            availabilityManager: dependencies.availabilityManager
        )
        let contactsRootVC = RootContactsController(viewModel: contactsRootVM)
        navigation.viewControllers = [contactsRootVC]
        navigation.navigationBar.isTranslucent = false
        navigation.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigation.navigationBar.shadowImage = UIImage()
        navigation.view.backgroundColor = ColorHelper.AppTintColor()
        navigation.delegate = dependencies.navDelegate
        
        navigation.tabBarItem.title = "Contacts".localized
        navigation.tabBarItem.image = #imageLiteral(resourceName: "tab_bar_person")
    }
    
    // MARK: - Router
    
    var rootController: RootController {
        return navigation
    }
    
    // MARK: - Actions
    
    // MARK: - contact
    
    func showOneContactList(with contact: Contacts) {
        let resultsController: NSFetchedResultsController<Contacts> = CoreDataManager.instance
            .fetchedResultsControllerTyped(
                entityName: "Contacts",
                sortingDescriptors: dependencies.settings.contactsSortingDescriptors(),
                cacheName: nil,
                sectionNameKeyPath: "sectionGroupingKey"
        )
        let config = ContactsListViewModelConfiguration(
            fetchedController: resultsController,
            filteringPredicate: NSPredicate(format: "SELF == %@", contact),
            isSearchAvailable: false
        )
        let vm = ContactsListViewModel(
            configuration: config,
            settings: dependencies.settings,
            router: self,
            communicationHelper: dependencies.communicationHelper,
            partialFormatter: dependencies.partialFormatter,
            availabilityManager: dependencies.availabilityManager
        )
        let vc = ContactsListController(viewModel: vm)
        vc.tp_makeGreenBackButton()
        navigation.pushViewController(vc, animated: true)
    }
    
    // MARK: - group
    
    
    
    // MARK: - other routing
    
    func showSettings(delegate: SettingsViewControllerDelegate?) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        vc.delegate = delegate
        vc.communicationHelper = dependencies.communicationHelper
        vc.purchasesManager = dependencies.purchaseManager
        vc.router = self
        navigation.pushViewController(vc, animated: true)
    }

    // MARK: - Purchases
    
    /// Отображает экран пожертвований
    func showDonateScreen() {
        let vm = DonateViewModel(purchaseManager: dependencies.purchaseManager, router: self)
        let vc = DonateController(vm: vm)
        navigation.pushViewController(vc, animated: true)
    }

    // MARK: - other actions

    /// Убирает из стека все контроллеры кроме рутового и отображаемого
    func removeAllIntermediateControllers() {
        let numberOfController = navigation.viewControllers.count
        guard numberOfController > 2 else {
            return
        } 
        navigation.viewControllers = [navigation.viewControllers[0], navigation.viewControllers[numberOfController-1]]
    }
    
    // MARK: - private
}

extension ContactsRouter: EditNotificationRouter {}

extension ContactsRouter: OpenDetailRouter {}

extension ContactsRouter: GroupInfoRouter {}

extension ContactsRouter: ContactsListRouter {}

extension ContactsRouter: ContactsSelectionListRouter {}

extension ContactsRouter: RemovingScreensRouter {}

extension ContactsRouter: ThanksScreenRouter {}

extension ContactsRouter: PhotoContactCreateRouter {}

extension ContactsRouter: GroupCommunicationRouter {}

extension ContactsRouter: SelectPickerRouter {}

extension ContactsRouter: ObservableRouter {}

extension ContactsRouter: NavigationDelegate {}

extension ContactsRouter: ShowProposalRouter {}

// MARK: - TabsSelector
extension ContactsRouter: TabsSelector {
    func open(tab: TabsRouter.Tab) {
        tabsSelector?.open(tab: tab)
    }
}
