//
//  ProposalRouter.swift
//  TemporaryContacts
//
//  Created by Aleksey on 26.10.2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

/// Роутер для отображения экранов при запуске приложения (видеогид и тд)
class ProposalRouter: Router {
    
    typealias RootController = UINavigationController
    
    /// Основной контроллер роутера, отобразив который, начинается работа роутера
    unowned private(set) var rootController: UINavigationController
    
    /// Хранилище зависимостей
    let dependencies: DependenciesStorage
    
    init(dependencies: DependenciesStorage, nc: UINavigationController) {
        self.dependencies = dependencies
        rootController = nc
        nc.setNavigationBarHidden(true, animated: false)
        nc.p_router = self
    }
    
    // MARK: - route
    
    /// Отобразить экран с видео
    func showVideoGuide(skipAction: VideoGuideController.Action?, finishAction: VideoGuideController.Action?) {
        let videoGuideController = VideoGuideController()
        
        videoGuideController.skipAction = skipAction
        videoGuideController.finishAction = finishAction
        /// Отобразили видеогид
        Settings.instance.videoGuideShowed = true
        
        pushOrShow(vc: videoGuideController)
    }
    
    /// Отобразить экран с туром
    func showTour(doneAction: @escaping WelcomeTourViewController.DoneAction) {
        guard let tour = UIStoryboard(name: "WelcomeTourViewController", bundle: nil).instantiateInitialViewController() as? WelcomeTourViewController else {
            return
        }
        tour.doneAction = doneAction
        pushOrShow(vc: tour)
    }
    
    /// Отобразить экран спец предложения
    func showSpecialOffer(doneAction: @escaping SpecialOfferViewController.DoneAction,
                          didBuyAction: @escaping SpecialOfferViewModel.Action) {
        let vm = SpecialOfferViewModel(
            purchaseManager: dependencies.purchaseManager,
            availabilityManager: dependencies.availabilityManager,
            router: self
        )
        vm.didBuyAction = didBuyAction
        let vc = SpecialOfferViewController(vm: vm)
        vc.doneAction = doneAction
        pushOrShow(vc: vc)
    }
    
    /// Отобразить алерт с уведомлением о скрытии видео контроллера
    func showSkipVideoAlert(action: @escaping () -> Void) {
        let alert = UIAlertController(title: "video-tour.skip".localized,
                                      message: "video-tour.skip.message".localized,
                                      preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(
                title: "Cancel".localized,
                style: .cancel,
                handler: nil
            )
        )
        alert.addAction(
            UIAlertAction(
                title: "video-tour.skip-action".localized,
                style: .default,
                handler: { _ in
                    action()
            })
        )
        rootController.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - private
    
    /// Отображает без анимации (если первый ВК) или пушит
    private func pushOrShow(vc: UIViewController) {
        if rootController.viewControllers.isEmpty {
            rootController.viewControllers = [vc]
        } else {
            rootController.pushViewController(vc,
                                              animated: true)
        }
    }
}

extension ProposalRouter {
    
    enum RootType {
        case videoGuide
        case tour
        case specialOffer
    }
}

extension ProposalRouter: AlertPresenterRouter {}

extension ProposalRouter: ThanksScreenRouter {}

/// Ключ для хранения роутера
private var routerKey: UInt8 = 0

extension UIViewController {
    
    /// ссылка на роутер
    fileprivate var p_router: ProposalRouter? {
        get {
            return objc_getAssociatedObject(self, &routerKey) as? ProposalRouter
        } set {
            objc_setAssociatedObject(self, &routerKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}
