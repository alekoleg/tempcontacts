//
//  CreateEntityRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import ContactsUI

protocol CreateEntityRouter {
    /// Открывает экран создания контакта
    mutating func showContactCreator(delegate: ContactConverterDelegate?)
    /// Открывает экран создания группы
    func showGroupCreator()
    /// Отображает экран создания фотоконтакта
    func showPhotoContactCreator(delegate: PhotoContactCreateDelegate?)
    
    var contactCreateConverter: ContactControllerDelegateConverter? { get set }
}

extension Router where Self: CreateEntityRouter & NavigationDelegate & ContactsListRouter & RemovingScreensRouter & ContactsSelectionListRouter & GroupCommunicationRouter & GroupInfoRouter & PhotoContactCreateRouter, RootController == UINavigationController {
    
    mutating func showContactCreator(delegate: ContactConverterDelegate?) {
        let converter = ContactControllerDelegateConverter(type: .creation)
        converter.navigationDelegate = self
        converter.converterDelegate = delegate
        let vc = CNContactViewController(forNewContact: nil)
        vc.view.tintColor = ColorHelper.AppTintColor()
        vc.delegate = converter
        
        let nc = UINavigationController(rootViewController: vc)
        configureContactsNC(nc)
        rootController.present(nc, animated: true, completion: nil)
        
        contactCreateConverter = converter
    }
    
    func showGroupCreator() {
        let vm = GroupInfoViewModel(
            group: nil,
            settings: dependencies.settings,
            router: self,
            partialFormatter: dependencies.partialFormatter,
            availabilityManager: dependencies.availabilityManager
        )
        vm.setInEditing(true)
        let vc = GroupInfoController(viewModel: vm)
        rootController.pushViewController(vc, animated: true)
    }
    
    func showPhotoContactCreator(delegate: PhotoContactCreateDelegate?) {
        let vm = PhotoContactCreateViewModel(router: self, availabilityManager: dependencies.availabilityManager)
        vm.delegate = delegate
        let vc = PhotoContactCreateControler(viewModel: vm)
        vc.navigationDelegate = self
        vc.tp_makeGreenBackButton()
        rootController.pushViewController(vc, animated: true)
    }
    
    private func configureContactsNC(_ nc: UINavigationController) {
        nc.view.backgroundColor = ColorHelper.AppTintColor()
        nc.navigationBar.isTranslucent = false
    }
}
