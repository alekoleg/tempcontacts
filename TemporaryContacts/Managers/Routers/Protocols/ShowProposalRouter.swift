//
//  ShowProposalRouter.swift
//  TemporaryContacts
//
//  Created by Aleksey on 26.10.2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

/// Роутер, умеющий отображать роутер предложений
protocol ShowProposalRouter {
    /// Возвращает стек навигации до первого контроллера
    func presentProposalRouter(presentWithAlpha: Bool) -> ProposalRouter
}

extension Router where Self: ShowProposalRouter {
    /// Возвращает стек навигации до первого контроллера
    func presentProposalRouter(presentWithAlpha: Bool) -> ProposalRouter {
        let nc = UINavigationController()
        let router = ProposalRouter(dependencies: self.dependencies,
                                    nc: nc)
        
        let whiteView = UIView()
        whiteView.backgroundColor = UIColor.white
        if presentWithAlpha {
            rootController.view.addSubview(whiteView)
            whiteView.snp.remakeConstraints({ $0.edges.equalToSuperview() })
        }
        
        rootController.present(router.rootController,
                               animated: true,
                               completion: { 
                                whiteView.removeFromSuperview()
        })
        return router
    }
}
