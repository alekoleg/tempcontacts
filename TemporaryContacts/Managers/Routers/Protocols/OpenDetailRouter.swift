//
//  OpenDetailRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import ContactsUI
import Contacts
import RxSwift

protocol ContactsRouterDelegate: class {
    /// Роутер собирается отобразить информацию о системном контакте
    ///
    /// - Parameters:
    ///   - router: Модель роутера
    ///   - contact: Модель контакта
    func contactsRouterWillShowContactDetails(router: OpenDetailRouter, contact: Contacts)
}

enum OpenDetailRouterError: Error {
    case cantFindSystemContact
}

protocol OpenDetailRouter {
    /// Отображает детализацию контакта
    mutating func showContactInfo(contact: Contacts, editable: Bool, delegate: ContactConverterDelegate?) throws
    /// Отображает детализацию группы
    func showGroupDetail(_ group: Groups)
    /// Отображает детализацию фото контакта
    func showPhotoContactDetail(_ contact: Contacts)
    
    var delegate: ContactsRouterDelegate? { get }
    
    var contactInfoConverter: ContactControllerDelegateConverter? { get set }
}

extension Router where Self: OpenDetailRouter & NavigationDelegate & GroupInfoRouter, RootController == UINavigationController {
    
    mutating func showContactInfo(contact: Contacts, editable: Bool, delegate: ContactConverterDelegate?) throws {
        guard let identifier = contact.systemIdentifier,
            let systemContact = ContactsManager.instance.getContact(systemIdentifier: identifier) else {
                throw OpenDetailRouterError.cantFindSystemContact
        }
        self.delegate?.contactsRouterWillShowContactDetails(router: self, contact: contact)
        
        let converter = ContactControllerDelegateConverter(type: .editing)
        converter.navigationDelegate = self
        converter.converterDelegate = delegate

        let vc = CNContactViewController(for: systemContact)
        vc.allowsEditing = editable
        vc.view.tintColor = ColorHelper.AppTintColor()
        vc.delegate = converter
        vc.hidesBottomBarWhenPushed = true
        rootController.pushViewController(vc, animated: true)
        
        var disposeBag = DisposeBag()
        let nc = rootController
        
        vc.rx.methodInvoked(#selector(UIViewController.viewWillAppear(_:)))
            .subscribe(
                onNext: { _ in
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
                    nc.navigationBar.isTranslucent = true
                    nc.navigationBar.tintColor = ColorHelper.AppTintColor()
            })
        .addDisposableTo(disposeBag)
        vc.rx.methodInvoked(#selector(UIViewController.viewWillDisappear(_:)))
            .subscribe(
                onNext: { _ in
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
                    vc.navigationController?.navigationBar.isTranslucent = false
                    vc.navigationController?.navigationBar.tintColor = UIColor.white
            })
            .addDisposableTo(disposeBag)
        
        vc.rx.deallocated
            .subscribe(
                onNext: { _ in
                    disposeBag = DisposeBag()
            })
            .addDisposableTo(disposeBag)
        
        contactInfoConverter = converter
    }
    
    func showGroupDetail(_ group: Groups) {
        let vm = GroupInfoViewModel(
            group: group,
            settings: dependencies.settings,
            router: self,
            partialFormatter: dependencies.partialFormatter,
            availabilityManager: dependencies.availabilityManager
        )
        let vc = GroupInfoController(viewModel: vm)
        rootController.pushViewController(vc, animated: true)
    }
    
    func showPhotoContactDetail(_ contact: Contacts) {
        let vm = PhotoContactDetailViewModel(
            contact: contact,
            cardsManager: dependencies.cardsManager,
            communiocationHelper: dependencies.communicationHelper,
            availabilityManager: dependencies.availabilityManager
        )
        let vc = PhotoContactDetailController(viewModel: vm)
        vc.tp_makeGreenBackButton()
        rootController.pushViewController(vc, animated: true)
    }
}
