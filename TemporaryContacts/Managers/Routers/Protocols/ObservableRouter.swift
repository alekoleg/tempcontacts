//
//  ObservableRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import RxSwift

protocol ObservableRouter {
    /// Сигнал открытия нового контроллера
    var didShowSignal: Observable<(viewController: UIViewController, animated: Bool)> { get }
}

extension Router where Self: ObservableRouter, RootController == UINavigationController {
    /// Сигнал об окончании анимации отображения контроллера
    var didShowSignal: Observable<(viewController: UIViewController, animated: Bool)> {
        return rootController.rx.didShow.asObservable()
    }
}
