//
//  RemovingScreensRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol RemovingScreensRouter {
    /// Убирает контроллер и всех его последющих контроллеров в стеке навигации из контроллера навигации
    ///
    /// - Parameter controller: Контроллер для убирания
    func dismissController(_ controller: UIViewController)
    /// Удаляет контроллеры определенных типов из стека
    ///
    /// - Parameter types: Типы контроллеров для удаления
    func removeControllersFromStack(types: [AnyClass])
    /// Прячет текущий контроллер
    func popCurrentViewController()
    /// Возвращает стек навигации до первого контроллера
    func popToRootViewController()
}

extension Router where Self: RemovingScreensRouter, RootController == UINavigationController {
    /// Убирает контроллер и всех его последющих контроллеров в стеке навигации из контроллера навигации
    ///
    /// - Parameter controller: Контроллер для убирания
    func dismissController(_ controller: UIViewController) {
        guard let index = rootController.viewControllers.index(of: controller),
            index > 0 else {
                return
        }
        let newTopController = rootController.viewControllers[index - 1]
        rootController.popToViewController(newTopController, animated: true)
    }
    /// Удаляет контроллеры определенных типов из стека
    ///
    /// - Parameter types: Типы контроллеров для удаления
    func removeControllersFromStack(types: [AnyClass]) {
        rootController.viewControllers = rootController.viewControllers.filter { vc in
            let type = Swift.type(of: vc)
            if types.contains(where: { $0 == type }) {
                return false
            }
            return true
        }
    }
    /// Прячет текущий контроллер
    func popCurrentViewController() {
        rootController.popViewController(animated: true)
    }
    /// Возвращает стек навигации до первого контроллера
    func popToRootViewController() {
        rootController.popToRootViewController(animated: true)
    }
}
