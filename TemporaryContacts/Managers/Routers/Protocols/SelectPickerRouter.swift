//
//  SelectPickerRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import CoreData

protocol SelectPickerRouter {
    
    func showSelectPicker(
        selection: Selection,
        moveToTemp: Bool,
        associatedContact: Contacts?,
        context: NSManagedObjectContext?,
        delegate: SelectpickerViewControllerDelegate?
    )
}

extension Router where Self: SelectPickerRouter, RootController == UINavigationController {
    func showSelectPicker(
        selection: Selection,
        moveToTemp: Bool,
        associatedContact: Contacts?,
        context: NSManagedObjectContext?,
        delegate: SelectpickerViewControllerDelegate?
        )
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SelectpickerViewController") as! SelectpickerViewController
        controller.selection = selection
        controller.availabilityManager = dependencies.availabilityManager
        controller.moveToTemp = moveToTemp
        controller.delegate = delegate
        if let context = context {
            controller.context = context
        }
        controller.associatedContact = associatedContact
        rootController.pushViewController(controller, animated: true)
    }
}
