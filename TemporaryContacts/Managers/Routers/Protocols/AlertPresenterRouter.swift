//
//  AlertPresenterRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

protocol AlertPresenterRouter {
    
    func showAlertController(title: String?,
        message: String?,
        style: UIAlertController.Style,
        actions: [UIAlertAction])
}

extension Router where Self: AlertPresenterRouter {
    func showAlertController(
        title: String?,
        message: String?,
        style: UIAlertController.Style,
        actions: [UIAlertAction]
        )
    {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: style
        )
        for action in actions {
            alert.addAction(action)
        }
        rootController.asViewController().present(alert, animated: true, completion: nil)
    }
}
