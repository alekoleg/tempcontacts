//
//  NotificationListRouter.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 21/05/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import CoreData

class NotificationsRouter {

	fileprivate let rootViewController: UINavigationController
    
    let dependencies: DependenciesStorage
    
    var contactInfoConverter: ContactControllerDelegateConverter?
    
    var contactCreateConverter: ContactControllerDelegateConverter?
    
    var delegate: ContactsRouterDelegate?

	init(dependencies: DependenciesStorage) {
        self.dependencies = dependencies
		rootViewController = UINavigationController()
        
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: true)
        let controller: NSFetchedResultsController<AppNotification> = CoreDataManager.instance.fetchedResultsControllerTyped(
            entityName: AppNotification.entityName,
            sortingDescriptors: [sortDescriptor],
            cacheName: nil
        )
        let vm = NotificationListViewModel(
            fetchedController: controller,
            initializationAction: nil,
            router: self,
            availabilityManager: dependencies.availabilityManager,
            settings: dependencies.settings
        )
        let vc = NotificationListViewController(vm: vm)
        rootViewController.viewControllers = [vc]
        rootViewController.navigationBar.isTranslucent = false
        rootViewController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        rootViewController.navigationBar.shadowImage = UIImage()
        rootViewController.view.backgroundColor = ColorHelper.AppTintColor()
        rootViewController.delegate = dependencies.navDelegate
	}
}

extension NotificationsRouter: NotificationListRouter, OpenDetailRouter, GroupInfoRouter, ContactsListRouter, RemovingScreensRouter, ContactsSelectionListRouter, GroupCommunicationRouter, PhotoContactCreateRouter, CreateEntityRouter {
}

extension NotificationsRouter: NavigationDelegate {}

extension NotificationsRouter: Router {

    typealias RootController = UINavigationController

    var rootController: UINavigationController {
        return rootViewController
    }
}
