//
//  TabsRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 30.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol TabsSelector: class {
    func open(tab: TabsRouter.Tab)
}

/// Роутер переключения табов
class TabsRouter: Router, TabsSelector {
    
    typealias RootController = RootBarController

    enum Tab {
        case favorites
        case keys
        case contacts
        case businessCards
        case notifications
        
        init(settings: SettingsStartPage) {
            switch settings {
            case .groups, .temp, .all:
                self = .contacts
            case .businessCards:
                self = .businessCards
            case .favorites:
                self = .favorites
            case .keys:
                self = .keys
            case .notifications:
                self = .notifications
            }
        }
    }
    
    let tabs: [Tab]
    
    fileprivate let tabsController: RootBarController
    
    let dependencies: DependenciesStorage
    
    fileprivate(set) lazy var favoritesRouter: FavoritesRouter = FavoritesRouter(dependencies: self.dependencies)
    fileprivate(set) lazy var contactsRouter: ContactsRouter = ContactsRouter(dependencies: self.dependencies)
    fileprivate(set) lazy var keyboardRouter: KeyboardRouter = KeyboardRouter(dependencies: self.dependencies)
    fileprivate(set) lazy var notificationsRouter: NotificationsRouter = NotificationsRouter(dependencies: self.dependencies)
    fileprivate(set) lazy var businessCardsRouter: BusinessCardsRouter = BusinessCardsRouter(dependencies: self.dependencies)
    
    init() {
        let tabs: [Tab] = [
            .favorites,
            .keys,
            .contacts,
            .businessCards,
            .notifications
        ]
        let items: [RootTabBarItem] = tabs.map { tab in
            switch tab {
            case .businessCards:
                return RootTabBarItem(text: "Визитница",
                                      image: #imageLiteral(resourceName: "tab_bar_business_cards_unselected"),
                                      selectedImage: #imageLiteral(resourceName: "tab_bar_business_cards"))
            case .contacts:
                return RootTabBarItem(text: "Контакты",
                                      image: #imageLiteral(resourceName: "tab_bar_person_unselected"),
                                      selectedImage: #imageLiteral(resourceName: "tab_bar_person"),
                                      isExpanded: true)
            case .favorites:
                return RootTabBarItem(text: "Избранные",
                                      image: #imageLiteral(resourceName: "tab_bar_star_unselected"),
                                      selectedImage: #imageLiteral(resourceName: "tab_bar_star"))
            case .keys:
                return RootTabBarItem(text: "Клавиши",
                                      image: #imageLiteral(resourceName: "tab_bar_keys_unselected"),
                                      selectedImage: #imageLiteral(resourceName: "tab_bar_keys"))
            case .notifications:
                return RootTabBarItem(text: "Напомнить",
                                      image: #imageLiteral(resourceName: "tab_bar_notifications_unselected"),
                                      selectedImage: #imageLiteral(resourceName: "tab_bar_notifications"))
            }
        }
        let tabsController = RootBarController(items: items)
        self.tabsController = tabsController
        self.tabs = tabs
        dependencies = DependenciesStorage(rootController: tabsController)
        initialize()
    }
    
    // MARK: - router
    
    var rootController: RootBarController {
        return tabsController
    }
    
    // MARK: - getters
    
    // MARK: - actions
    
    func open(tab: Tab) {
        guard let index = tabs.index(of: tab) else {
            return
        }
        tabsController.openTab(at: index)
    }
    
    private func initialize() {
        tabsController.delegate = self
        tabsController.view.backgroundColor = ColorHelper.lightTintColor
        let selectedTab = Tab(settings: Settings.instance.startPageType)
        if let tabIndex = tabs.index(of: selectedTab) {
            tabsController.openTab(at: tabIndex)
        }
        
        contactsRouter.tabsSelector = self
    }
}

extension TabsRouter: RootBarControllerDelegate {
    func controller(for index: Int) -> UIViewController {
        switch tabs[index] {
        case .businessCards:
            return businessCardsRouter.rootController
        case .contacts:
            return contactsRouter.rootController
        case .favorites:
            return favoritesRouter.rootController
        case .keys:
            return keyboardRouter.rootController
        case .notifications:
            return notificationsRouter.rootController
        }
    }
}

extension TabsRouter: ShowProposalRouter {}
