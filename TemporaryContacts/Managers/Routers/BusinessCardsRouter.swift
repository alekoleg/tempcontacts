//
//  BusinessCardsRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData

class BusinessCardsRouter {
    
    fileprivate let rootViewController: UINavigationController
    
    let dependencies: DependenciesStorage
    
    var delegate: ContactsRouterDelegate?
    
    var contactInfoConverter: ContactControllerDelegateConverter?
    
    var contactCreateConverter: ContactControllerDelegateConverter?
    
    init(dependencies: DependenciesStorage) {
        self.dependencies = dependencies
        rootViewController = UINavigationController()
        
        let req = NSFetchRequest<Contacts>(entityName: Contacts.entityName)
        req.sortDescriptors = dependencies.settings.contactsSortingDescriptors()
        let controller = NSFetchedResultsController<Contacts>(
            fetchRequest: req,
            managedObjectContext: CoreDataManager.instance.managedObjectContext,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        let vm = BusinessCardsListViewModel(
            controller: controller,
            settings: dependencies.settings,
            availabilityManager: dependencies.availabilityManager,
            router: self
        )
        let vc = BusinessCardsListController(vm: vm)
        rootViewController.viewControllers = [vc]
        rootViewController.navigationBar.isTranslucent = false
        rootViewController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        rootViewController.navigationBar.shadowImage = UIImage()
        rootViewController.view.backgroundColor = ColorHelper.AppTintColor()
    }
}

extension BusinessCardsRouter: OpenDetailRouter, NavigationDelegate, GroupInfoRouter, ContactsListRouter, RemovingScreensRouter, ContactsSelectionListRouter, GroupCommunicationRouter, PhotoContactCreateRouter {}

// MARK: - Router
extension BusinessCardsRouter: Router {
    
    typealias RootController = UINavigationController
    
    var rootController: UINavigationController {
        return rootViewController
    }
}
