//
//  AppReceiptManager.swift
//  TemporaryContacts
//
//  Created by Konshin on 27.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import RxSwift
import StoreKit

/// Менеджер по работе с квитанцией Apple
class AppReceiptManager: NSObject {
    /// Шейред инстанс
    static let instance = AppReceiptManager()
    
    /// Сабжект на поулчение квитанции
    fileprivate var receiptSubject: ReplaySubject<Receipt>?
    
    override private init() {
        super.init()
        getReceipt()
    }
    
    // MARK: - actions
    
    /// Получение квитанции
    @discardableResult func getReceipt() -> Observable<Receipt> {
        if let variable = receiptSubject {
            return variable.asObservable()
        }
        
        let subject = ReplaySubject<Receipt>.create(bufferSize: 1)
        
        let request = SKReceiptRefreshRequest(receiptProperties: nil)
        request.delegate = self
        request.start()

        receiptSubject = subject
        
        return subject.asObservable()
    }
    
    private func parseReceipt(json: [String: Any]) -> Receipt? {
        print(json)
        guard let receiptJSON = json["receipt"] as? [String: Any],
            let unixTimeString = receiptJSON["original_purchase_date_ms"] as? String,
            let unixMsTime = Int(unixTimeString),
            let originalVersion = receiptJSON["original_application_version"] as? String else {
                return nil
        }
        
        return Receipt(
            originalPurchaseDate: Date(timeIntervalSince1970: TimeInterval(unixMsTime / 1000)),
            originalPurchaseVersion: originalVersion
        )
    }
    
    // MARK: - private
    
    /// Валидация квитанции
    fileprivate func verifyReceipt() {
        guard let url = Bundle.main.appStoreReceiptURL else {
            sendError(Errors.receiptFileIsMissed)
            return
        }
        
        do {
            let data = try Data(contentsOf: url)
            let dataString = data.base64EncodedString(options: .init(rawValue: 0))
            let bodyJSON = ["receipt-data": dataString]
            let body = try JSONSerialization.data(withJSONObject: bodyJSON, options: .prettyPrinted)
            
            guard let url = URL(string: "https://buy.itunes.apple.com/verifyReceipt") else {
                sendError(Errors.cannotRequest)
                return
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = body
            
            let task = URLSession.shared.dataTask(with: request) { [unowned self] (data, response, error) in
                if let error = error {
                    self.sendError(error)
                } else {
                    guard let data = data,
                        let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                        let receiptJSON = json as? [String: Any],
                        let receipt = self.parseReceipt(json: receiptJSON) else {
                            self.sendError(Errors.cannotParse)
                            return
                    }
                    
                    self.sendComplete(receipt: receipt)
                }
            }
            task.resume()
        } catch {
            sendError(error)
        }
    }
    
    // MARK: - sends
    
    fileprivate func sendComplete(receipt: Receipt) {
        receiptSubject?.onNext(receipt)
    }
    
    fileprivate func sendError(_ error: Error) {
        receiptSubject?.onError(error)
        receiptSubject = nil
    }
}

// MARK: - SKRequestDelegate
extension AppReceiptManager: SKRequestDelegate {
    
    func requestDidFinish(_ request: SKRequest) {
        verifyReceipt()
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        sendError(error)
    }
}

// MARK: - Errors
extension AppReceiptManager {
    enum Errors: Error {
        case receiptFileIsMissed
        case cannotRequest
        case cannotParse
    }
    
    struct Receipt {
        let originalPurchaseDate: Date
        let originalPurchaseVersion: String
    }
}
