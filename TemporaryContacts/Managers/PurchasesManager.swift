//
//  PurchasesManager.swift
//  TemporaryContacts
//
//  Created by Konshin on 02.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import StoreKit
import RxSwift
import Crashlytics

/// Менеджер покупок
class PurchasesManager: NSObject {
    
    /// Локализованная цена
    struct LocalizedPrice {
        let price: NSNumber
        let symbol: String?
    }
    
    enum Result {
        case completed
        case restored
        case canceled
    }
    
    enum Purchase: String {
        case unlockAll = "com.temporarycontacts.app.unlockAll"
        case unlockAllSpecialOffer = "com.temporarycontacts.app.unlockAll.specialOffer"
        case donate1 = "com.temporarycontacts.app.donate___1"
        case donate2 = "com.temporarycontacts.app.donate___2"
        case donate3 = "com.temporarycontacts.app.donate___3"
        
        static let all: [Purchase] = [.unlockAll, .unlockAllSpecialOffer, .donate1, .donate2, .donate3]
        
        fileprivate var id: String {
            return rawValue
        }
        
        fileprivate var purchasedKey: String {
            switch self {
            case .unlockAll:
                return "com.temporarycontacts.app.unlockAll.status"
            case .unlockAllSpecialOffer:
                return "com.temporarycontacts.app.unlockAll.specialOffer.status"
            default:
                return id
            }
        }
    }

    static let instance = PurchasesManager()
	fileprivate let defaults = UserDefaults.standard
    
    private override init() {
        super.init()
        SKPaymentQueue.default().add(self)
        requestProductsData()
    }
    
    // MARK: - getters
    
    /// Локализованная стоимость продукта
    ///
    /// - Parameter purchase: Тип продукта
    /// - Returns: Стоимость
    func localizedPrice(for purchase: Purchase) -> String? {
        guard let product = products[purchase] else {
            return nil
        }
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = product.priceLocale
        return formatter.string(from: product.price)
    }
    
    /// МОдель локализованной стоимости продукта
    ///
    /// - Parameter purchase: Тип продукта
    /// - Returns: Модель стоимости
    func localizedPriceModel(for purchase: Purchase) -> LocalizedPrice? {
        guard let product = products[purchase] else {
            return nil
        }
        return LocalizedPrice(
            price: product.price,
            symbol: product.priceLocale.currencySymbol
        )
    }
    
    /// Покупки загружены
    var isPurchasesLoaded: Bool {
        return !products.isEmpty
    }
    
    // MARK: - actions
    
    /// Делает запрос на приобретение товара
    ///
    /// - Parameter purchase: Тип товара
    /// - Returns: Сигнал с результатом покупки
    @discardableResult func buy(_ purchase: Purchase) -> Observable<Result> {
        if let signal = signalsForPurchase[purchase] {
            // Если уже выполняется покупка - возвращаем текущий сигнал
            return signal
        }
        
        let signal = PublishSubject<Result>()
        signalsForPurchase[purchase] = signal
        let payment = SKMutablePayment()
        payment.productIdentifier = purchase.id
        SKPaymentQueue.default().add(payment)
        return signal
    }
    
    fileprivate var restoreSignal: PublishSubject<Void>?

	func restore() -> Observable<Void> {
        if let signal = restoreSignal {
            return signal
        }
        
        let signal = PublishSubject<Void>()
        restoreSignal = signal
		SKPaymentQueue.default().restoreCompletedTransactions()
        return signal
	}

    func isPurchased(_ purchase: Purchase) -> Bool {
        switch purchase {
        case .unlockAll, .unlockAllSpecialOffer:
            return defaults.bool(forKey: purchase.purchasedKey)
        default:
            return false
        }
    }

	func setOldUsersPurchased() {
		defaults.set(true, forKey: Purchase.unlockAll.purchasedKey)
		defaults.synchronize()
	}
    
    // MARK: - private 
    
    fileprivate var signalsForPurchase = [Purchase: PublishSubject<Result>]()
    
    fileprivate var products = [Purchase: SKProduct]()
    
    fileprivate func requestProductsData() {
        let identifiers = Set(Purchase.all.map({ $0.id }))
        let req = SKProductsRequest(productIdentifiers: identifiers)
        req.delegate = self
        req.start()
    }
    
    fileprivate func provideContent(for purchase: Purchase) {
        switch purchase {
        case .unlockAll, .unlockAllSpecialOffer:
			defaults.set(true, forKey: purchase.purchasedKey)
			NotificationCenter.default.post(name: Notification.Name.updatedPurchaseStatus, object: nil, userInfo: nil)
            break
        default:
            break
        }
    }
    
    fileprivate func purchase(from transaction: SKPaymentTransaction) -> Purchase? {
        return Purchase(rawValue: transaction.payment.productIdentifier)
    }
}


// MARK: - work with purchases
extension PurchasesManager {
    
    fileprivate func complete(transaction: SKPaymentTransaction, purchase: Purchase?) {
        if let purchase = self.purchase(from: transaction) {
            provideContent(for: purchase)
            signalsForPurchase[purchase]?.onNext(.completed)
        }
        
        finish(transaction: transaction, purchase: purchase)
    }
    
    fileprivate func restore(transaction: SKPaymentTransaction, purchase: Purchase?) {
        if let purchase = self.purchase(from: transaction) {
            provideContent(for: purchase)
            signalsForPurchase[purchase]?.onNext(.restored)
        }
        finish(transaction: transaction, purchase: purchase)
    }
    
    fileprivate func failed(transaction: SKPaymentTransaction, purchase: Purchase?) {
        print("Ошибка встроенной покупки: \(transaction.error!)")
        
        if let purchase = purchase, let error = transaction.error {
            switch transaction.error {
            case SKError.paymentCancelled?:
                signalsForPurchase[purchase]?.onNext(.canceled)
            default:
                signalsForPurchase[purchase]?.onError(error)
            }
        }
        finish(transaction: transaction, purchase: purchase)
    }
    
    fileprivate func finish(transaction: SKPaymentTransaction, purchase: Purchase?) {
        SKPaymentQueue.default().finishTransaction(transaction)
        if let purchase = purchase {
            signalsForPurchase[purchase] = nil
        }
    }
}


// MARK: - SKPaymentTransactionObserver
extension PurchasesManager: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for t in transactions {
            let purchase = self.purchase(from: t)
            
            switch t.transactionState {
            case .purchased:
                complete(transaction: t, purchase: purchase)
                if let purchase = purchase, let product = products[purchase] {
                    Answers.logPurchase(withPrice: product.price,
                                        currency: "Rub",
                                        success: true,
                                        itemName: product.localizedTitle,
                                        itemType: product.localizedDescription,
                                        itemId: product.productIdentifier,
                                        customAttributes: [:])
                }
            case .restored:
                restore(transaction: t, purchase: purchase)
            case .failed:
                failed(transaction: t, purchase: purchase)
            default:
                break
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        restoreSignal?.onNext(Void())
        restoreSignal = nil
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        restoreSignal?.onError(error)
        restoreSignal = nil
    }
}


// MARK: - SKProductsRequestDelegate
extension PurchasesManager: SKProductsRequestDelegate {
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let products = response.products
        for product in products {
            if let purchase = Purchase(rawValue: product.productIdentifier) {
                self.products[purchase] = product
            }
        }
        print("Успешно получили продукты: \(products)\n Первый: \(products.first?.productIdentifier ?? "")")
    }
}

extension Notification.Name {

	static let updatedPurchaseStatus = Notification.Name.businessCardsDidUpdate
}
