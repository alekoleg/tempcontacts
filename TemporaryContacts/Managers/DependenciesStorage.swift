//
//  DependenciesStorage.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import PhoneNumberKit

/// Хранилище всех зависимостей
class DependenciesStorage {
    
    private let rootController: UIViewController
    
    init(rootController: UIViewController) {
        self.rootController = rootController
    }
    
    private(set) lazy var fileAdapter: FileAdapter = FileAdapter()
    private(set) lazy var cardsManager: BusinessCardsManager = BusinessCardsManager(adapter: self.fileAdapter)
    private(set) lazy var communicationHelper: CommunicationHelper = CommunicationHelper(presenter: self.rootController)
    private(set) lazy var settings: Settings = Settings()
    private(set) lazy var partialFormatter: PartialFormatter = PartialFormatter()
    private(set) lazy var appState: AppState = AppState(context: CoreDataManager.instance.managedObjectContext)
    private(set) lazy var availabilityManager: AvailabilityManager = AvailabilityManager(
        appState: self.appState,
        purchasesManager: self.purchaseManager
    )
    private(set) lazy var purchaseManager: PurchasesManager = PurchasesManager.instance
    private(set) lazy var navDelegate: ApplicationNavDelegate = ApplicationNavDelegate()
}
