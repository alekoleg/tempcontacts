//
//  AppStoreManager.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import RxSwift
import StoreKit

private let dateOfLastProposalKey = "app-store.review.last.proposal.date"

/// Класс для работы с автоматическими отзывами в магазине
class AppStoreReviewManager {

    fileprivate let disposeBag = DisposeBag()
    
    // MARK: - getters
    
    /// Дата последнего предложения
    private(set) var dateOfLastProposal: Date? {
        get { return UserDefaults.standard.value(forKey: dateOfLastProposalKey) as? Date }
        set { UserDefaults.standard.set(newValue, forKey: dateOfLastProposalKey) }
    }
    
    // MARK: - actions
    
    /// Настраивает менеджер
    func setup() {
        return
    }
    
    /// Отображает пользователю системный алерт с предложением оценить приложение
    private func showReviewController() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            // Fallback on earlier versions
        }
        dateOfLastProposal = Date()
    }
}
