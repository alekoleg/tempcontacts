//
//  AppState.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import RxSwift
import CoreData

/// Класс, отображающий состояние приложения
/// - количество созданных визиток, временных контактов, групп
class AppState {
    
    /// Контекст, по которому следим за количеством элементов
    fileprivate let context: NSManagedObjectContext
    /// Диспоуз
    fileprivate let disposeBag = DisposeBag()
    
    init(context: NSManagedObjectContext) {
        self.context = context
        initialize()
    }
    
    // MARK: - variables
    
    /// Количество созданных визиток
    fileprivate let numberOfCreatedBusinessCardsVariable = Variable(0)
    /// Количество созданных групп
    fileprivate let numberOfCreatedGroupsVariable = Variable(0)
    /// Количество созданных временных контактов
    fileprivate let numberOfCreatedTempContactsVariable = Variable(0)
    /// Количество созданных уведомлений
    fileprivate let numberOfCreatedNotificationsVariable = Variable(0)
    
    // MARK: - stats observable
    /// Количество созданных визиток
    var numberOfCreatedBusinessCardsUpdateSignal: Observable<Int> {
        return numberOfCreatedBusinessCardsVariable.asObservable()
    }
    /// Количество созданных групп
    var numberOfCreatedGroupsUpdateSignal: Observable<Int> {
        return numberOfCreatedGroupsVariable.asObservable()
    }
    /// Количество созданных временных контактов
    var numberOfCreatedTempContactsUpdateSignal: Observable<Int> {
        return numberOfCreatedTempContactsVariable.asObservable()
    }
    /// Количество созданных уведомлений
    var numberOfCreatedNotificationsUpdateSignal: Observable<Int> {
        return numberOfCreatedNotificationsVariable.asObservable()
    }
    
    // MARK: - stats value
    /// Количество созданных визиток
    var numberOfCreatedBusinessCards: Int {
        return numberOfCreatedBusinessCardsVariable.value
    }
    /// Количество созданных групп
    var numberOfCreatedGroups: Int {
        return numberOfCreatedGroupsVariable.value
    }
    /// Количество созданных временных контактов
    var numberOfCreatedTempContacts: Int {
        return numberOfCreatedTempContactsVariable.value
    }
    /// Количество созданных временных контактов
    var numberOfCreatedNotifications: Int {
        return numberOfCreatedNotificationsVariable.value
    }
}


extension AppState {
    // MARK: - actions
    
    fileprivate func initialize() {
        updateNumberOfBusinessCards()
        updateNumberOfGroups()
        updateNumberOfTempContacts()
        updateNumberOfNotifications()
        
        NotificationCenter.default.rx.notification(Notification.Name.NSManagedObjectContextDidSave, object: context)
            .debounce(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(
                onNext: { [weak self] _ in
                    self?.updateNumberOfGroups()
                    self?.updateNumberOfTempContacts()
                    self?.updateNumberOfBusinessCards()
                    self?.updateNumberOfNotifications()
            })
            .addDisposableTo(disposeBag)
    }
    
    // MARK: private
    
    fileprivate var allGroupsRequest: NSFetchRequest<Groups> {
        let r = NSFetchRequest<Groups>(entityName: Groups.entityName)
        return r
    }
    
    fileprivate var tempContactsRequest: NSFetchRequest<Contacts> {
        let r = NSFetchRequest<Contacts>(entityName: Contacts.entityName)
        r.predicate = NSPredicate(format: "isTempContact == true")
        return r
    }
    
    fileprivate var businesscardsRequest: NSFetchRequest<BusinessCard> {
        let r = NSFetchRequest<BusinessCard>(entityName: BusinessCard.entityName)
        return r
    }
    
    fileprivate var notificationsRequest: NSFetchRequest<AppNotification> {
        let r = NSFetchRequest<AppNotification>(entityName: AppNotification.entityName)
        return r
    }
    
    fileprivate func updateNumberOfBusinessCards() {
        let numberOfEntities = (try? context.count(for: businesscardsRequest)) ?? 0
        // Считаем максимальное число из того что сейчас в базе, и максимально зафиксированным количеством элементов. 
        // То есть теперь удаление элемента не увеличивает количество допустимых новых элементов
        if numberOfEntities > numberOfCreatedBusinessCardsForAllTime {
            numberOfCreatedBusinessCardsForAllTime = numberOfEntities
            numberOfCreatedBusinessCardsVariable.value = numberOfEntities
        } else {
            numberOfCreatedBusinessCardsVariable.value = numberOfCreatedBusinessCardsForAllTime
        }
    }
    
    fileprivate func updateNumberOfGroups() {
        let numberOfEntities = (try? context.count(for: allGroupsRequest)) ?? 0
        // Считаем максимальное число из того что сейчас в базе, и максимально зафиксированным количеством элементов.
        // То есть теперь удаление элемента не увеличивает количество допустимых новых элементов
        if numberOfEntities > numberOfCreatedGroupsForAllTime {
            numberOfCreatedGroupsForAllTime = numberOfEntities
            numberOfCreatedGroupsVariable.value = numberOfEntities
        } else {
            numberOfCreatedGroupsVariable.value = numberOfCreatedGroupsForAllTime
        }
    }
    
    fileprivate func updateNumberOfTempContacts() {
        let numberOfEntities = (try? context.count(for: tempContactsRequest)) ?? 0
        // Считаем максимальное число из того что сейчас в базе, и максимально зафиксированным количеством элементов.
        // То есть теперь удаление элемента не увеличивает количество допустимых новых элементов
        if numberOfEntities > numberOfCreatedTempContactsForAllTime {
            numberOfCreatedTempContactsForAllTime = numberOfEntities
            numberOfCreatedTempContactsVariable.value = numberOfEntities
        } else {
            numberOfCreatedTempContactsVariable.value = numberOfCreatedTempContactsForAllTime
        }
    }
    
    fileprivate func updateNumberOfNotifications() {
        let numberOfEntities = (try? context.count(for: notificationsRequest)) ?? 0
        // Считаем максимальное число из того что сейчас в базе, и максимально зафиксированным количеством элементов.
        // То есть теперь удаление элемента не увеличивает количество допустимых новых элементов
        if numberOfEntities > numberOfCreatedNotificationsForAllTime {
            numberOfCreatedNotificationsForAllTime = numberOfEntities
            numberOfCreatedNotificationsVariable.value = numberOfEntities
        } else {
            numberOfCreatedNotificationsVariable.value = numberOfCreatedNotificationsForAllTime
        }
    }
}

extension AppState {
    private struct Keys {
        static let businessCardsKey = "numberOfCreatedBusinessCardsForAllTime"
        static let groups = "numberOfCreatedGroupsForAllTime"
        static let tempContacts = "numberOfCreatedTempContactsForAllTime"
        static let notifications = "numberOfCreatedNotificationsForAllTime"
    }
    
    /// Количество созданных визиток за все время
    fileprivate var numberOfCreatedBusinessCardsForAllTime: Int {
        get { return UserDefaults.standard.integer(forKey: Keys.businessCardsKey) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.businessCardsKey) }
    }
    
    /// Количество созданных групп за все время
    fileprivate var numberOfCreatedGroupsForAllTime: Int {
        get { return UserDefaults.standard.integer(forKey: Keys.groups) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.groups) }
    }
    
    /// Количество созданных временных контактов за все время
    fileprivate var numberOfCreatedTempContactsForAllTime: Int {
        get { return UserDefaults.standard.integer(forKey: Keys.tempContacts) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.tempContacts) }
    }
    
    /// Количество созданных уведомлений за все время
    fileprivate var numberOfCreatedNotificationsForAllTime: Int {
        get { return UserDefaults.standard.integer(forKey: Keys.notifications) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.notifications) }
    }
}
