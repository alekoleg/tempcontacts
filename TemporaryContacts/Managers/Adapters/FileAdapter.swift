//
//  FileAdapter.swift
//  TemporaryContacts
//
//  Created by Алексей Коньшин on 12.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

/// Адаптер для работы с файловой системой
class FileAdapter {
    
    enum ErrorType: Error {
        case cantCreateFile
    }
    
    /// Корневая директория
    enum Directory {
        case document
        case caches
    }
    
    // MARK: - private
    /// Файловый менеджер
    private let manager = FileManager.default
    /// Директория документов
    private let documentsDirectory: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    /// Директория кэша
    private let cachesDirectory: URL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
    
    private func pathToFile(at directory: Directory, pathComponents: [String]) -> URL {
        var baseURL: URL
        switch directory {
        case .document:
            baseURL = documentsDirectory
        case .caches:
            baseURL = cachesDirectory
        }
        
        for c in pathComponents {
            baseURL.appendPathComponent(c)
        }
        return baseURL
    }
    
    // MARK: - work with cache
    
    func data(at directory: Directory, pathComponents: [String], fileName: String) -> Data? {
        let url = pathToFile(at: directory, pathComponents: pathComponents + [fileName])
        return manager.contents(atPath: url.path)
    }
    
    /// Дата создания файла
    ///
    /// - Parameters:
    ///   - directory: Основная диркетория
    ///   - pathComponents: Элементы пути до файла
    ///   - fileName: Название файла
    /// - Returns: Возможно, дату создания
    func creationDate(at directory: Directory, pathComponents: [String], fileName: String) -> Date? {
        let url = pathToFile(at: directory, pathComponents: pathComponents + [fileName])
        let path = url.path
        do {
            let attributes = try manager.attributesOfItem(atPath: path)
            return attributes[FileAttributeKey.creationDate] as? Date
        } catch {
            return nil
        }
    }
    
    func setData(_ data: Data, directory: Directory, pathComponents: [String], fileName: String) throws {
        let directoryURL = pathToFile(at: directory, pathComponents: pathComponents)
        let fileURL = pathToFile(at: directory, pathComponents: pathComponents + [fileName])
        try manager.createDirectory(atPath: directoryURL.path, withIntermediateDirectories: true, attributes: nil)
        let success = manager.createFile(atPath: fileURL.path, contents: data, attributes: nil)
        if !success {
            throw ErrorType.cantCreateFile
        }
    }
    
    /// Возвращает дочерний контент
    ///
    /// - Parameters:
    ///   - directory: Корневая директория
    ///   - pathComponents: Компоненты пути до папки
    ///   - includeDirectories: Включая объекты подпапок этого же уровня
    /// - Returns: Список имен файлов
    func content(of directory: Directory, pathComponents: [String], includeDirectories: Bool = false, recoursive: Bool = false) -> [String] {
        let directoryURL = pathToFile(at: directory, pathComponents: pathComponents)
        
        do {
            let content = try manager.contentsOfDirectory(
                at: directoryURL,
                includingPropertiesForKeys: nil,
                options: .init(rawValue: 0)
            )
            return content
                .filter({ URL in
                    if includeDirectories {
                        return true
                    }
                    var isDirectory: ObjCBool = false
                    manager.fileExists(atPath: URL.path, isDirectory: &isDirectory)
                    return !isDirectory.boolValue
                })
                .map() { url in
                    return url.lastPathComponent
            }
        } catch {
            print("Ошибка получения контента директории: \(error)")
        }

        return []
    }
    
    func subpaths(of directory: Directory, pathComponents: [String]) -> [String] {
        let directoryURL = pathToFile(at: directory, pathComponents: pathComponents)
        return manager.subpaths(atPath: directoryURL.path) ?? []
    }
    
    /// Удаляет контент в папке
    ///
    /// - Parameters:
    ///   - directory: Корневая директория
    ///   - pathComponents: Компоненты пути до папки
    /// - Throws: Возможна неудача
    func deleteContent(of directory: Directory, pathComponents: [String]) throws {
        let directoryURL = pathToFile(at: directory, pathComponents: pathComponents)
        try manager.removeItem(at: directoryURL)
    }
}
