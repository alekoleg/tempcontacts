//
//  StringExtension.swift
//  TemporaryContacts
//
//  Created by User on 18/01/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }

	var capitalizeFirstLetter: String {
		return "\(self.substring(to:1).uppercased())\(self.substring(from:1))"
    }
    
    /// Возвращает true, если строка состоит из 1 символа - буквы
    var isLetter: Bool {
        guard let firstLetter = unicodeScalars.first,
            CharacterSet.letters.contains(firstLetter) else {
                return false
        }
        return true
    }
}
