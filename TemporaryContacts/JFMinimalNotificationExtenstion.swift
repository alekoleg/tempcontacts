//
//  JFMinimalNotificationExtenstion.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 21/03/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import JFMinimalNotifications

extension JFMinimalNotification {

	func tm_prepate() {

		self.backgroundColor = ColorHelper.AppRedColor()
		self.titleLabel.font = UIFont.museoSansCyrl(weight: .w500, size: 15)
		self.titleLabel.textColor = UIColor.white
		self.titleLabel.textAlignment = .center
        self.edgePadding = UIEdgeInsets(top: -10, left: 0, bottom: -10, right: 0)
	}
}
