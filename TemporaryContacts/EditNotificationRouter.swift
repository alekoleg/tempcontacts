//
//  EditNotificationRouter.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData

protocol EditNotificationRouter {
    /// Отобразить экран редактирования уведомления
    func showNotificationController(
        notification: AppNotification?,
        owner: EditNotificationViewController.Owner,
        context: NSManagedObjectContext?,
        delegate: EditNotificationViewControllerDelegate?
    )
}

extension Router where Self: EditNotificationRouter, RootController == UINavigationController {
    func showNotificationController(
        notification: AppNotification?,
        owner: EditNotificationViewController.Owner,
        context: NSManagedObjectContext?,
        delegate: EditNotificationViewControllerDelegate?
        )
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EditNotificationViewController") as!EditNotificationViewController
        controller.originalNotification = notification
        controller.notification = notification
        controller.delegate = delegate
        controller.owner = owner
        if let context = context {
            controller.context = context
        }
        rootController.pushViewController(controller, animated: true)
    }
}
