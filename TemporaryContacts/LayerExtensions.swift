//
//  ViewExtensions.swift
//  TemporaryContacts
//
//  Created by User on 23/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        mask = nil
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        
        
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        mask = shape
    }
}
