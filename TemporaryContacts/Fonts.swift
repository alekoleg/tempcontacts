//
//  Fonts.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 06/03/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import UIKit

enum AppFontTypeface {
    case regular
    case bold
    case semibold
    case heavy
    case light
    case medium
    case black
}

extension UIFont {

	enum FontWeight {
		case w100, w300, w500, w700, w900

		fileprivate func stringRepresentation() -> String {
			switch self {
			case .w100:
				return "100"
			case .w300:
				return "300"
			case .w500:
				return "500"
			case .w700:
				return "700"
			case .w900:
				return "900"
			}
		}
	}

	static func museoSansCyrl(weight: FontWeight, size:CGFloat) -> UIFont {
		let name = "MuseoSansCyrl-\(weight.stringRepresentation())"
		return UIFont(name: name, size: size)!
	}
    
    /// Возвращает стандартный шрифт приложения
    ///
    /// - Parameters:
    ///   - size: Размер шрифта
    ///   - typeface: Начертание
    /// - Returns: Модель UIFont
    static func appFont(size: CGFloat = UIFont.systemFontSize, typeface: AppFontTypeface = .regular) -> UIFont {
        switch typeface {
        case .regular:
            return UIFont.systemFont(ofSize: size)
        case .bold:
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.bold)
        case .semibold:
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
        case .light:
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.light)
        case .medium:
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.medium)
        case .heavy:
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.heavy)
        case .black:
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.black)
        }
    }
}
