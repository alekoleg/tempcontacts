//
//  StrikeoutView.swift
//  TemporaryContacts
//
//  Created by Konshin on 07.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

class StrikeoutView: UIView {

    private let line = CAShapeLayer()
    
    init() {
        super.init(frame: .zero)
        
        line.lineWidth = lineWidth
        line.strokeColor = lineColor.cgColor
        layer.addSublayer(line)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - proeprties
    
    var lineWidth: CGFloat = 4 {
        didSet {
            line.lineWidth = lineWidth
        }
    }
    
    var lineColor = UIColor(hex: 0x525164) {
        didSet {
            line.strokeColor = lineColor.cgColor
        }
    }
    
    // MARK: - actions
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath()
        path.move(to: .zero)
        path.addLine(to: CGPoint(x: bounds.width, y: bounds.height))
        line.path = path.cgPath
    }
}
