//
//  SectionHeader.swift
//  TemporaryContacts
//
//  Created by User on 23/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit

class SectionHeader: UITableViewCell{
    @IBOutlet weak var headerTextLabel: UILabel!
}
