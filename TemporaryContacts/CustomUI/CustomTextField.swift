//
//  CustomTextField.swift
//  TemporaryContacts
//
//  Created by User on 26/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit

class AppTextField: UITextField{
    var insets: (left: CGFloat, right: CGFloat) = (10, 10)
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        prepare()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        
        prepare()
    }
    
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.textRect(forBounds: bounds)
        rect.origin.x += insets.left
        rect.size.width -= (insets.left + insets.right)
        
        return rect
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.leftViewRect(forBounds: bounds)
        return rect.offsetBy(dx: insets.left, dy: 0)
    }
    
    override func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.clearButtonRect(forBounds: bounds)
        return rect.offsetBy(dx: -insets.left, dy: 0)
    }

    func prepare(){
        self.layer.masksToBounds = false
		self.layer.borderWidth = 1.0
        self.layer.borderColor = ColorHelper.AppTintColor().cgColor
        self.layer.cornerRadius = 20
    }
}
