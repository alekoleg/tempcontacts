//
//  AddContactCell.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 09/03/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import UIKit

class AddGroupContactCell: UITableViewCell {

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var iconView: UIImageView!
	var action: (()->Void)?


	func prepare(addAction: @escaping ()->Void){
		self.action = addAction
	}

	@IBAction func actionClick(_ sender: Any) {
		if action != nil{
			action!()
		}
	}

}
