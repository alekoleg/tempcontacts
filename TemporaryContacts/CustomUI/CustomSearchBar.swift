//
//  CustomSearchBar.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import SnapKit

protocol CustomSearchBarDelegate: class {
    func customSearchBarDidBeginEditing(_ searchBar: CustomSearchBar)
    func customSearchBarDidEndEditing(_ searchBar: CustomSearchBar)
    func customSearchTextDidChanged(_ searchBar: CustomSearchBar)
}


extension CustomSearchBarDelegate {
    func customSearchBarDidBeginEditing(_ searchBar: CustomSearchBar) {}
    func customSearchBarDidEndEditing(_ searchBar: CustomSearchBar) {}
}


class CustomSearchBar: UIView {
    fileprivate let textField = AppTextField()
    private let cancelButton = UIButton(type: .system)
    private let clearButton = UIButton(type: .system)
    
    weak var delegate: CustomSearchBarDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: - properties
    
    @objc dynamic var cancelButtonTitle: String {
        get { return cancelButton.title(for: .normal) ?? "" }
        set { cancelButton.setTitle(newValue, for: .normal) }
    }
    
    var text: String? {
        get { return textField.text }
        set { textField.text = newValue }
    }
    
    @objc dynamic var placeholder: String? {
        get { return textField.placeholder }
        set { textField.placeholder = newValue }
    }
    
    var font: UIFont? {
        get { return textField.font }
        set { textField.font = newValue }
    }
    
    override dynamic var tintColor: UIColor! {
        get { return super.tintColor }
        set {
            super.tintColor = newValue
            textField.layer.borderColor = newValue?.cgColor
        }
    }
    
    var insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14) {
        didSet {
            relayout(cancelIsVisible: textField.isFirstResponder)
        }
    }
    
    // MARK: - actions
    
    private func commonInit() {
        textField.clearButtonMode = .always
        textField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        textField.layer.masksToBounds = true
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 20
        textField.layer.borderColor = tintColor.cgColor
        textField.autocorrectionType = .no
        textField.delegate = self
        addSubview(textField)
        
        addSubview(cancelButton)
        cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        
        relayout(cancelIsVisible: textField.isFirstResponder)
    }
    
    private func relayout(cancelIsVisible: Bool) {
        textField.snp.remakeConstraints() { maker in
            maker.centerY.equalToSuperview()
            maker.height.equalTo(44)
            maker.left.equalToSuperview().offset(insets.left)
            maker.top.greaterThanOrEqualToSuperview().offset(insets.top)
            maker.bottom.lessThanOrEqualToSuperview().offset(-insets.bottom)
            if cancelIsVisible {
                maker.right.equalTo(cancelButton.snp.left).offset(-14)
            } else {
                maker.right.equalToSuperview().offset(-insets.right)
            }
        }
        
        cancelButton.snp.remakeConstraints() { maker in
            maker.centerY.equalToSuperview()
            if cancelIsVisible {
                maker.right.equalToSuperview().offset(-insets.right)
            } else {
                maker.left.equalTo(self.snp.right)
            }
        }
    }
    
    fileprivate func setCancelButtonVisible(_ visible: Bool, animated: Bool) {
        relayout(cancelIsVisible: visible)
        if animated {
            UIView.animate(withDuration: 0.3) { [unowned self] in
                self.layoutIfNeeded()
            }
        }
    }
    
    @objc private func cancel() {
        textField.text = nil
        delegate?.customSearchTextDidChanged(self)
        endEditing(true)
    }
}

// MARK: - UITextFieldDelegate
extension CustomSearchBar: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setCancelButtonVisible(true, animated: true)
        delegate?.customSearchBarDidBeginEditing(self)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        setCancelButtonVisible(false, animated: true)
        delegate?.customSearchBarDidEndEditing(self)
    }
    
    @objc fileprivate func textDidChanged(_ textField: UITextField) {
        delegate?.customSearchTextDidChanged(self)
    }
}

// MARK: - First Responder
extension CustomSearchBar {
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func becomeFirstResponder() -> Bool {
        return textField.becomeFirstResponder()
    }
}
