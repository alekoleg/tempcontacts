//
//  paddingsLabel.swift
//  MGRDashboard
//
//  Created by Коньшин Алексей on 27.11.15.
//

import UIKit

/// Лейбла с отступами текста от краев
/// Умеет подсчитывать размер для AutoLayout
class PaddingsLabel: UILabel {
    /// Отступы от краев
    var insets: UIEdgeInsets {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    convenience init() {
        self.init(insets: UIEdgeInsets())
    }
    

    init(insets: UIEdgeInsets, frame: CGRect = .zero) {
        self.insets = insets
        
        super.init(frame: frame)
    }

    
    required init?(coder aDecoder: NSCoder) {
        self.insets = UIEdgeInsets()
        
        super.init(coder: aDecoder)
    }

    
    override var intrinsicContentSize: CGSize {
        let superSize = super.intrinsicContentSize
        let extraWidth = insets.left + insets.right
        let extraHeight = insets.top + insets.bottom
        return CGSize(width: superSize.width + extraWidth, height: superSize.height + extraHeight)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: insets))
    }
}
