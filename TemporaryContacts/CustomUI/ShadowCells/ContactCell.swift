//
//  ContactCell.swift
//  TemporaryContacts
//
//  Created by User on 23/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit
import MGSwipeTableCell
//import Bugsee

protocol ContactCellDelegate: class {
    func contactCellDidTapCall(_ cell: ContactCell)
}

class ContactCell: MGSwipeTableCell{

    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specializationLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var callButton: UIButton!
    
    @IBOutlet weak var tempMark: UIImageView!
    
    weak var contactCellDelegate: ContactCellDelegate?
    
    var isFirst = false
    var isLast = false
    
    func prepare(contact: Contacts, isFirst: Bool, isLast: Bool, nameDisplayOrder: SettingsDisplayOrder){
        
        tempMark.isHidden = !contact.isTempContact
        
        self.isFirst = isFirst
        self.isLast = isLast

        switch contact.knownPhonesType {
        case .main(_):
            callButton?.isHidden = false
            callButton?.setImage(#imageLiteral(resourceName: "call_icon_filled"), for: .normal)
        case .multiple(_):
            callButton?.isHidden = false
            callButton?.setImage(#imageLiteral(resourceName: "call_icon_contour"), for: .normal)
        case .unknown:
            callButton?.isHidden = true
        }
        
        let firstName = NSAttributedString(string: contact.firstName ?? "",
                                           attributes: [NSAttributedString.Key.font : UIFont.museoSansCyrl(weight:.w100, size:18)])
        let lastName = NSAttributedString(string: contact.lastName ?? "",
                                          attributes: [NSAttributedString.Key.font : UIFont.museoSansCyrl(weight:.w700, size:18)])
        let space = NSAttributedString(string:" ")

		let name:NSMutableAttributedString = NSMutableAttributedString()
		if nameDisplayOrder == .NameFirst {
			name.append(firstName)
			name.append(space)
			name.append(lastName)
		} else {
			name.append(lastName)
			name.append(space)
			name.append(firstName)
		}
        nameLabel.attributedText = name

        let check = contact.specialization == nil || contact.specialization!.trimmingCharacters(in: NSCharacterSet.whitespaces).isEmpty
		specializationLabel.text = check ? nil : contact.specialization!
        
        separatorView.isHidden = isLast
    }
    
    @IBAction func call() {
        contactCellDelegate?.contactCellDidTapCall(self)
    }
}
