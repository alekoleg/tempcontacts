//
//  ContactSelectCell.swift
//  TemporaryContacts
//
//  Created by User on 16/01/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import UIKit
//import M13Checkbox

class ContactSelectCell: ContactCell{

    @IBOutlet weak var checkbox: UIImageView!

	override func awakeFromNib() {
		super.awakeFromNib()
		self.checkbox.layer.masksToBounds = true;
		self.checkbox.layer.cornerRadius = self.checkbox.frame.size.height / 2.0
	}
    
    func prepareSelectCell(contact: Contacts, isFirst: Bool, isLast: Bool, nameDisplayOrder: SettingsDisplayOrder, isChecked: Bool ){
        super.prepare(contact: contact, isFirst: isFirst, isLast: isLast, nameDisplayOrder: nameDisplayOrder)
        checkbox.image = isChecked ? UIImage(named:"checkmark") : nil
        
        if !contact.isTempContact{
            tempMark.alpha = 1
        }
    }
    
}
