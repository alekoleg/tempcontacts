//
//  UIButton+Badge.swift
//  TemporaryContacts
//
//  Created by Konshin on 19.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

private var badgeNumberAssociatedKey: UInt8 = 0
private var badgeLabelAssociatedKey: UInt8 = 0

extension UIButton {
    /// количество на бейджике
    var tcBadgeNumber: Int {
        get {
            if let number = objc_getAssociatedObject(self, &badgeNumberAssociatedKey) as? Int {
                return number
            }
            return 0
        }
        set {
            objc_setAssociatedObject(self, &badgeNumberAssociatedKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_ASSIGN)
            let label = tcBadgeLabel
            label.isHidden = newValue == 0
            label.text = String(newValue)
        }
    }
    
    /// количество на бейджике
    private var tcBadgeLabel: PaddingsLabel {
        if let label = objc_getAssociatedObject(self, &badgeLabelAssociatedKey) as? PaddingsLabel {
            return label
        } else {
            let label = PaddingsLabel()
            label.insets = UIEdgeInsets(top: 1, left: 4, bottom: 0, right: 4)
            label.font = UIFont.systemFont(ofSize: 10)
            label.layer.cornerRadius = 5
            label.layer.masksToBounds = true
            label.backgroundColor = ColorHelper.AppRedColor()
            label.textColor = UIColor.white
            label.isUserInteractionEnabled = false
            addSubview(label)
            label.snp.remakeConstraints { maker in
                maker.centerY.equalTo(self.snp.top)
                maker.centerX.equalTo(self.snp.right)
            }
            objc_setAssociatedObject(self, &badgeLabelAssociatedKey, label, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            return label
        }
    }
}
