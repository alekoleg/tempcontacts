//
//  SelectionCell.swift
//  TemporaryContacts
//
//  Created by User on 22/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit
//import M13Checkbox

class SelectionCell: UITableViewCell{
    
    
    @IBOutlet weak var cellTextLabel: UILabel!
    @IBOutlet weak var checkbox: UIImageView!

	override func awakeFromNib() {
		super.awakeFromNib()
		self.checkbox.layer.masksToBounds = true;
		self.checkbox.layer.cornerRadius = self.checkbox.frame.size.height / 2.0
	}
    
    func prepare(text: String, checked: Bool){
        cellTextLabel.text = text
		checkbox.image = checked ? UIImage(named:"checkmark") : nil
    }
}
