//
//  TCSegmentedControll.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 07/03/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import XMSegmentedControl

class TCSegmentedControll : XMSegmentedControl {

	var selectedFont:UIFont = UIFont.appFont(size: 16, typeface: .semibold)

	override var selectedSegment: Int {
		didSet {
			self.updateFontForTabs()
		}
	}

	override func update() {
		super.update()
		self.updateFontForTabs()
	}

	private func updateFontForTabs() {
		func isUIButton(_ view: UIView) -> Bool {
			return view is UIButton ? true : false
		}

		switch(self.contentType) {
		case .text:
			((self.subviews.filter(isUIButton)) as! [UIButton]).forEach {
				if $0.tag == self.selectedSegment {
					$0.titleLabel?.font = self.selectedFont
				} else {
					$0.titleLabel?.font = self.font
				}
			}
		default:
			return
		}

	}
}
