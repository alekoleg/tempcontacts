//
//  ColorHelper.swift
//  TemporaryContacts
//
//  Created by User on 20/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit

class ColorHelper {
    
    static let tint: Int = 0x4A4959
    static let lines: Int = 0x97a9bd
    static let background: Int = 0xe9edf2 //remove
    static let lightBackground: Int = 0xf9fafc //remove
    static let darkTextColor: Int = 0x4A4959 // remove
    static let purple: Int = 0x4E95E8//0x836a9b // remove
	static let greenColor: Int = 0x6CFFB6
	static let redColor:Int = 0xFF6C7B;
    
    class func getHexColor(netHex:Int) -> UIColor{
        let red = CGFloat((netHex >> 16) & 0xff) / 255.0
        let green = CGFloat((netHex >> 8) & 0xff) / 255.0
        let blue = CGFloat(netHex & 0xff) / 255.0
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
    
    class func AppTintColor() -> UIColor{
        return getHexColor(netHex: tint)
    }
    
    class func AppBackgroundColor() -> UIColor{
        return getHexColor(netHex: background)
    }
    
    class func AppLightBackgroundColor() -> UIColor{
        return getHexColor(netHex: lightBackground)
    }
    
    class func AppDarkTextColor() -> UIColor{
        return getHexColor(netHex: darkTextColor)
    }
    
    class func AppLinesColor() -> UIColor{
        return getHexColor(netHex: lines).withAlphaComponent(0.2)
    }
    
    class func AppPurpleColor() -> UIColor{
        return getHexColor(netHex: purple)
    }

	class func AppGreenColor() -> UIColor {
		return getHexColor(netHex: greenColor)
	}

	class func AppRedColor() -> UIColor {
		return getHexColor(netHex: redColor)
	}
    
    /// Легчайший серый цвет для шрифтов
    static let lightTextColor = UIColor(hex: 0xB6B6B6)
    /// Легкий цвет тинт
    static let lightTintColor = UIColor(hex: 0x444259)
}
