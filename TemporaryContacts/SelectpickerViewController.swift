//
//  SelectpickerViewController.swift
//  TemporaryContacts
//
//  Created by User on 22/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import JFMinimalNotifications

enum Selection: String{
    case Contacts = "SelectContactsTitle",
         Groups = "SelectGroupsTitle"
}

protocol SelectpickerViewControllerDelegate: class {
    func selectPickerControllerDidSelectGroup(_ controller: SelectpickerViewController, selectedData: [NSFetchRequestResult])
}

/// Секция контактов в списке.
/// Старая модель
typealias Section = (name: String, data: [NSFetchRequestResult])

class SelectpickerViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: SelectpickerViewControllerDelegate?
    
    /// Контекст для работы с Кор Датой
    var context: NSManagedObjectContext = CoreDataManager.instance.managedObjectContext
    /// Связанный с выбором контакт
    var associatedContact: Contacts?
    /// Менеджер по доступностям
    var availabilityManager: AvailabilityManager?
    
    var selection: Selection = .Contacts
    var selectedData: [NSFetchRequestResult] = []
    
    var unwindSegueId = "unwindSelectGroup"
    
    var dbController = CoreDataManager.instance.fetchedResultsController(entityName: "Contacts", keyForSort: "firstName")
    var sections: [Section] = []
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    @IBOutlet weak var noGroupLabel: UILabel!
    @IBOutlet weak var noGroupButton: UIButton!
    @IBOutlet weak var noGroupSeparator: UIView!
    
    var moveToTemp = false
    
    @IBOutlet weak var searchBar: AppTextField!
    @IBOutlet weak var searchTableViewHeader: UIView!
    @IBOutlet weak var searchViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var searchRemoveButton: UIButton!
    @IBOutlet weak var searchTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchCancelWidthConstraint: NSLayoutConstraint!
    
    var mn:JFMinimalNotification?

	override var preferredStatusBarStyle: UIStatusBarStyle {
		get {
			return .lightContent
		}
	}
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        hidesBottomBarWhenPushed = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        hidesBottomBarWhenPushed = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = selection.rawValue.localized
		self.view.backgroundColor = UIColor.white
        
        if selection == .Contacts {
            
            tableView.separatorColor = .clear
            tableView.separatorStyle = .none

            dbController = CoreDataManager.instance.fetchedResultsController(
                entityName: "Contacts",
                keysForSort: Settings.instance.contactSortingProperties(),
                cacheName: nil,
                context: context
            )
            if let index = navigationItem.rightBarButtonItems?.index(of: addButton){
                navigationItem.rightBarButtonItems?.remove(at: index)
            }
            
            noGroupLabel.removeFromSuperview()
            noGroupButton.removeFromSuperview()
            noGroupSeparator.removeFromSuperview()
            
            searchBar.prepare()
            let btnRemoveIcon = TextHelper.getAwesomeIcon(iconName: "remove", size: 12, color: .white)
            searchRemoveButton.setAttributedTitle(btnRemoveIcon, for: .normal)
            searchRemoveButton.alpha = 0
            setSearchView(isSearching: false)
            searchBar.delegate = self
            searchBar.addTarget(self, action: #selector(self.searchTextDidChanged), for: UIControl.Event.editingChanged)
        }
        else if selection == .Groups{
            searchTableViewHeader.removeFromSuperview()
            dbController = CoreDataManager.instance.fetchedResultsController(
                entityName: "Groups",
                keyForSort: "name",
                context: context
            )
            self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 32, bottom: 0, right: 0)
        }
        do {
            try dbController.performFetch()
        } catch {
            print(error)
        }
        
        if selection == .Contacts{
            if let dbSections = dbController.sections {
                sections = CoreDataManager.instance.prepareSections(
                    sections: dbSections,
                    nameDisplayOrder: Settings.instance.contactsOrder,
                    checkNew: false
                )
            }
            
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 160
        
        let nib = UINib(nibName: "SelectionCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "SelectionCell")
        
        let contactNib = UINib(nibName: "ContactSelectCell", bundle: nil)
        tableView.register(contactNib, forCellReuseIdentifier: "ContactSelectCell")
        
        
        self.mn = JFMinimalNotification(style: JFMinimalNotificationStyle.custom, title: "MarkTempHintTitle".localized, subTitle: "", dismissalDelay: 2)
        self.mn?.tm_prepate()
        self.view.addSubview(mn!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Settings.instance.showTips { return }
        
        if moveToTemp{
            mn?.show()
        }
    }
    
    func reloadContacts(){
        sections = []
        do {
            try dbController.performFetch()
        } catch {
            print(error)
        }
        
        guard let dbSections = dbController.sections else { return }
        
        sections = CoreDataManager.instance.prepareSections(
            sections: dbSections,
            nameDisplayOrder: Settings.instance.contactsOrder,
            checkNew: true
        )
        
        tableView.reloadData()
    }

    
    
    @IBAction func submitButtonClick(_ sender: Any) {
        performSegue(withIdentifier: unwindSegueId, sender: self)
        delegate?.selectPickerControllerDidSelectGroup(self, selectedData: selectedData)
        if selection == .Groups {
            NotificationCenter.default.post(name: .didSelectTempGroup, object: nil)
        }
    }
    
    @IBAction func cancelButtonClick(_ sender: Any) {
        let _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func noGroupClick(_ sender: Any) {
        if selection == .Groups{
            selectedData = []
            tableView.reloadData()
            performSegue(withIdentifier: unwindSegueId, sender: self)
            delegate?.selectPickerControllerDidSelectGroup(self, selectedData: selectedData)
            NotificationCenter.default.post(name: .didSelectTempGroup, object: nil)
        }
    }
    
    @IBAction func addClick(_ sender: Any) {
        if selection == .Contacts { return }
        guard let availabilityManager = availabilityManager,
        availabilityManager.canAddNewElement(.groups) else {
            showLimitationAlert(for: .groups)
            return
        }
        
        let alertController = UIAlertController(title: "EnterGroupName".localized, message: nil, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "Accept".localized, style: .default, handler: {
            alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            textField.endEditing(true)
            if let groupName = textField.text{
                if !groupName.isEmpty{
                    
                    //Создание новой группы
                    let newGroup = Groups()
                    newGroup.name = groupName
                    CoreDataManager.instance.saveContext()
                    self.selectedData.append(newGroup)
                    do {
                        try self.dbController.performFetch()
                    } catch {
                        print(error)
                    }
                    Metrica.instance.send(.groupIsCreated)
                    self.tableView.reloadData()
                }
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.text = ""
            textField.placeholder = "EnterGroupNamePlaceholder".localized
            textField.autocapitalizationType = .words
        }
        alertController.addAction(acceptAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = ColorHelper.AppDarkTextColor()
        self.present(alertController, animated: true, completion: nil)

    }
    
    var searchKeyboardShow = false
    
    @IBAction func cancelSearch(_ sender: Any) {
        searchBar.text = ""
        search(searchText: "", changeConstraints: false)
        self.view.endEditing(true)
    }
    
    
    @IBAction func searchRemoveClick(_ sender: Any) {
        searchBar.text = ""
        search(searchText: "", changeConstraints: true)
    }
    
    fileprivate var textBefore = ""
}

extension SelectpickerViewController: UITableViewDelegate, UITableViewDataSource{
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.selection == .Groups{
            return dbController.sections?.count ?? 0
        }
        else{
            return sections.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.selection == .Groups{
            if let sections = dbController.sections {
                return sections[section].numberOfObjects
            } else {
                return 0
            }
        }
        else{
            return sections[section].data.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var obj: NSFetchRequestResult
        if selection == .Contacts{
            obj = sections[indexPath.section].data[indexPath.row]
        }
        else{
            obj = dbController.object(at: indexPath)
        }
        
        let containsCheck = selectedData.first(where: { (fetch: NSFetchRequestResult) -> Bool in
            return fetch.isEqual(obj)
        })
        
        if let contact = obj as? Contacts{
            let isFirst = indexPath.row == 0
            let isLast = indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1
            let contactCell = tableView.dequeueReusableCell(withIdentifier: "ContactSelectCell", for: indexPath) as! ContactSelectCell
            
            contactCell.prepareSelectCell(
                contact: contact,
                isFirst: isFirst,
                isLast: isLast,
                nameDisplayOrder: Settings.instance.contactsOrder,
                isChecked: containsCheck != nil
            )
            contactCell.selectionStyle = .none
            
            return contactCell
            
        }
        else if let group = obj as? Groups{
            let text = "\(group.name ?? "")"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionCell", for: indexPath) as! SelectionCell
            cell.prepare(text: text, checked: containsCheck != nil)
            return cell
        }
        else{
            let cell = UITableViewCell()
            cell.isHidden = true
            cell.selectionStyle = .none
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj: NSFetchRequestResult
        if selection == .Groups{
            obj = dbController.object(at: indexPath)
        }
        else{
            obj = sections[indexPath.section].data[indexPath.row]
        }
        
        let containsCheck = selectedData.first(where: { (fetch: NSFetchRequestResult) -> Bool in
            return fetch.isEqual(obj)
        })
        if let check = containsCheck{
            var newSelected: [NSFetchRequestResult] = []
            for selected in selectedData{
                if !selected.isEqual(check){
                    newSelected.append(selected)
                }
            }
            selectedData = newSelected
        }
        else{
            selectedData.append(obj)
        }
        tableView.reloadData()
    }

    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if sections.count == 0 { return nil }
        
        let sectionLetters = sections.filter({ (section: (name: String, data: [NSFetchRequestResult])) -> Bool in
            return section.name.count == 1
        })
        .map { (section: (name: String, data: [NSFetchRequestResult])) -> String in
                return section.name
        }
        return sectionLetters
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "SectionHeader") as! SectionHeader
        header.headerTextLabel.text = sections[section].name
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if selection == .Groups{
            return 0
        }
        else{
            return 35
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.searchKeyboardShow { return }
        self.view.endEditing(true)
    }
    
}

extension SelectpickerViewController: UITextFieldDelegate{
    
    @objc func searchTextDidChanged(){
        search(searchText: searchBar.text ?? "", changeConstraints: true)
    }
    
    func setSearchView(isSearching: Bool){
        var check = false
        if isSearching{
            searchRemoveButton.alpha = 1
            if searchTrailingConstraint.constant != 12{
                searchTrailingConstraint.constant = 12
                searchTableViewHeader.setNeedsLayout()
                check = true
            }
            if searchCancelWidthConstraint.constant != 63{
                searchCancelWidthConstraint.constant = 63
                searchTableViewHeader.setNeedsLayout()
                check = true
            }
            self.searchTableViewHeader.layoutIfNeeded()
        }
        else{
            searchRemoveButton.alpha = 0
            if searchTrailingConstraint.constant != 0{
                searchTrailingConstraint.constant = 0
                searchTableViewHeader.setNeedsLayout()
                check = true
            }
            if searchCancelWidthConstraint.constant != 0{
                searchCancelWidthConstraint.constant = 0
                searchTableViewHeader.setNeedsLayout()
                check = true
            }
            if check {
                UIView.animate(withDuration: 0.25, animations: {
                    self.searchTableViewHeader.layoutIfNeeded()
                })
            }
        }
    }
    
    func search(searchText: String, changeConstraints: Bool){
        searchKeyboardShow = true
        if (searchText.isEmpty){
            dbController.fetchRequest.predicate = nil
        }else {
            let text = searchText.lowercased()
            dbController.fetchRequest.predicate = NSPredicate(format: "firstName CONTAINS[cd] %@ OR lastName CONTAINS[cd] %@ OR specialization CONTAINS[cd] %@", text, text, text)
        }
        
        if changeConstraints{
            if !searchText.isEmpty && textBefore.count != 1{
                setSearchView(isSearching: !searchText.isEmpty)
            }
        }
        else{
            setSearchView(isSearching: !searchText.isEmpty)
        }

        reloadContacts()
        textBefore = searchText
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.searchKeyboardShow = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        search(searchText: textField.text ?? "", changeConstraints: true)
        return false
    }
}


extension SelectpickerViewController: UserNotificationProtocol {}
