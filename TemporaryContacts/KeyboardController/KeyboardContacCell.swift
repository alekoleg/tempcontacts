//
//  KeyboardContacCell.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 13/04/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import SnapKit

struct KeyboardContacCellItem {
	struct StringItems {
		let title: String
		var hightlitedRange:NSRange?
	}

	let contact:Contacts
	let title:StringItems
	let detail:[StringItems]
}

class KeyboardContacCell: UITableViewCell {

	let titleLabel:UILabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		self.contentView.addSubview(titleLabel)
		self.titleLabel.snp.makeConstraints { (make) in
			make.edges.equalTo(UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 5))
			make.height.equalTo(58.0)
		}
		titleLabel.numberOfLines = 0
		titleLabel.lineBreakMode = .byWordWrapping
		titleLabel.snp.makeConstraints { (make) in
			make.edges.equalTo(UIEdgeInsets(top: 5,left: 15,bottom: 5,right: 5))
		}
		titleLabel.preferredMaxLayoutWidth = self.contentView.frame.size.width - 20;
	}


	func configure(item:KeyboardContacCellItem) {

		let string = NSMutableAttributedString()

		if !item.title.title.isEmpty {
			let title = NSMutableAttributedString(string: item.title.title,
                                                  attributes: [NSAttributedString.Key.foregroundColor : ColorHelper.AppTintColor(),
                                                               NSAttributedString.Key.font : UIFont.appFont(size: 18)])
            if let range = item.title.hightlitedRange {
                title.setAttributes([NSAttributedString.Key.foregroundColor : ColorHelper.AppTintColor(),
                                     NSAttributedString.Key.font : UIFont.appFont(size: 18, typeface: .semibold)],
                                    range: range)
            }
            string.append(title)
        }

		for detail in item.detail {

			if string.length > 0 {
				let newLine = NSMutableAttributedString(string: "\n", attributes: nil)
				string.append(newLine)
			}

			let detailString = NSMutableAttributedString(string: detail.title,
                                                         attributes: [NSAttributedString.Key.foregroundColor : ColorHelper.getHexColor(netHex: 0xB6B6B6),
                                                                      NSAttributedString.Key.font : UIFont.appFont(size: 16)])
			if let range = detail.hightlitedRange {
                detailString.setAttributes([NSAttributedString.Key.foregroundColor : ColorHelper.AppTintColor()], range: range)
			}
			string.append(detailString)
		}

		self.titleLabel.attributedText = string
	}

	override func layoutSubviews() {
		super.layoutSubviews()
		titleLabel.preferredMaxLayoutWidth = self.contentView.frame.size.width - 20;
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
