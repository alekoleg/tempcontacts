//
//  KeyboardViewModel.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 11/04/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import PhoneNumberKit
import Contacts
import CoreData

protocol KeyboardViewModelDelegate {

	func vmUpdatePhone(phone:String)
	func vmReloadData()
	func vmShowAlertActions(actions:[UIAlertAction])
}

class KeyboardViewModel {

	let buttonsToLetterMap:[Int:String] = [ 2 : "абвгabc",
	                                        3 : "дежзdef",
	                                        4 : "ийклghi",
	                                        5 : "мнопjkl",
	                                        6 : "рстуmno",
	                                        7 : "фхцчpqrs",
	                                        8 : "шщъыtuv",
	                                        9 : "ьэюяwxyz",]
    
    fileprivate var selectedDigits: [Int] = [] {
        didSet {
            originalInput = originalInput(from: selectedDigits, addPlus: showPlus)
            selectedLetters = selectedLetters(from: selectedDigits)
        }
    }
    fileprivate var originalInput: String = "" {
        didSet {
            formattedInput = formattedInput(from: originalInput)
        }
    }
	fileprivate var selectedLetters: [String] = []
    fileprivate var formattedInput = "" {
        didSet {
            delegate?.vmUpdatePhone(phone: formattedInput)
        }
    }
    /// Отображать ли плюс в начале номера
    fileprivate var showPlus: Bool = false {
        didSet {
            guard showPlus != oldValue else {
                return
            }
            originalInput = originalInput(from: selectedDigits, addPlus: showPlus)
        }
    }
    
	fileprivate let partialFormater = PartialFormatter()
	var delegate:KeyboardViewModelDelegate?
	fileprivate let router: KeyboardRouter
    fileprivate let communicationHelper: CommunicationHelper

	fileprivate var dbController:NSFetchedResultsController<NSFetchRequestResult>?
    
    init(router: KeyboardRouter, communicationHelper: CommunicationHelper) {
        self.router = router
        self.communicationHelper = communicationHelper
    }

	func didTaped(button:UIButton) {

		// под 11 тегом скрывается 0
        let digit = button.tag == 11 ? 0 : button.tag
        selectedDigits.append(digit)
		findSuggestions()
	}

	func pasteString(str:String) {
        let digits = str.compactMap({ Int(String($0)) })
        selectedDigits.append(contentsOf: digits)
		findSuggestions()
	}


	func insetPlus() {
		if originalInput.count == 0 {
            showPlus = true
		}
	}

	func clearInput() {
        showPlus = false
        selectedDigits.removeAll()
		findSuggestions()
	}

	func deleteLastCharacter() {
		if !selectedDigits.isEmpty {
            selectedDigits.removeLast()
			findSuggestions()
        } else {
            // Если нажали после удаления всех цифр - удаляем плюс с 1-го места
            showPlus = false
        }
	}

	func call() {
		self.call(number: originalInput)
	}

	func sendSMS() {
		if !originalInput.isEmpty {
            communicationHelper.sendMessage(originalInput)
		}
	}

	func addNewContact() {
		self.router.showContactCreator(delegate: self)
	}

	func didSelectCell(at index:Int) {
		// перевернутое отображение
		let item = self.item(at: self.numberOfItems() - index - 1)
		if let phones = item.contact.phones {
			if phones.count == 1 {
				let number = phones.anyObject() as? Phones
				self.call(number: (number?.digits ?? ""))
			}

			if phones.count > 1 {
				var actions:[UIAlertAction] = []
				for p in phones {
					let number = p as! Phones
					let action = UIAlertAction(title: number.digits, style: .default, handler: {  [unowned self] (action)  in
						self.call(number: (number.digits ?? ""))
					})
					actions.append(action)
				}
				self.delegate?.vmShowAlertActions(actions: actions)
			}
		}
	}

	private func findSuggestions() {

		if !self.originalInput.isEmpty {
			let number = self.originalInput.replacingOccurrences(of: "+", with: "")
			let numberRegexp = self.regularExpresionForCoreData(for: number)
			let wordRegexp = self.wordsRegularExpresionCoreData(for: selectedLetters)
			let numberPredicate = NSPredicate(format: "ANY phones.digits MATCHES[c] %@", numberRegexp)
			let wordFirstNamePredicate = NSPredicate(format: "firstName MATCHES[c] %@", wordRegexp)
			let wordLastNamePredicate = NSPredicate(format: "lastName MATCHES[c] %@", wordRegexp)
			let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [numberPredicate, wordLastNamePredicate, wordFirstNamePredicate])
			let fetchResultController = CoreDataManager.instance.fetchedResultsController(entityName: "Contacts", keyForSort: "firstName")
			fetchResultController.fetchRequest.predicate = predicate
			let sort = NSSortDescriptor(key: "firstName", ascending: false)
			fetchResultController.fetchRequest.sortDescriptors = [sort]
			do{
				try fetchResultController.performFetch()
				self.dbController = fetchResultController
			} catch {
				self.dbController = nil
			}
		} else {
			self.dbController = nil
		}

		self.delegate?.vmReloadData()
	}

	private func call(number:String) {
		if !number.isEmpty {
            try? communicationHelper.normalizeAndCall(to: number)
            Metrica.instance.send(.calledFromKeyboardScreen)
		}
	}

	fileprivate func regularExpresionForCoreData(for phone:String) -> String {
		let format = regularExpresionString(for: phone)
		let result = ".*" + format + ".*"
		return result
	}

	fileprivate func regularExpresionString(for phone:String) -> String {
		let validSeparators = "[+() -]{0,}"
		var format = ""
		for letter in phone {
			format += "\(letter)\(validSeparators)"
		}
		return format
	}

	fileprivate func wordsRegularExpresionString(for phoneLetters:[String]) -> String {
		var format = ""
		let validSeparators = "[+() -]{0,}"
		for letters in phoneLetters {
			let array = letters.map { String($0) }
			let orSequense = array.joined(separator: "|")
			format += "(" + orSequense + ")" + validSeparators
		}
		return format
	}

	fileprivate func wordsRegularExpresionCoreData(for phoneLetters:[String]) -> String {
		let format = wordsRegularExpresionString(for:phoneLetters)
		let result = ".*" + format + ".*"
		return result
	}
}

extension KeyboardViewModel {
    /// Формирует оригинальную строчку - нмоер из списка цифр
    ///
    /// - Parameters:
    ///   - digits: Список цифр
    ///   - addPlus: Отображать ли плюс в начале
    /// - Returns: Строку номера
    fileprivate func originalInput(from digits: [Int], addPlus: Bool = false) -> String {
        let stringDigits = digits.map(String.init).joined()
        if addPlus {
            return "+" + stringDigits
        } else {
            return stringDigits
        }
    }
    
    /// Форматирует оригинальную стрчоку номера в приятную для глаза
    ///
    /// - Parameter original: Строчка номера
    /// - Returns: Красивая строчка
    fileprivate func formattedInput(from original: String) -> String {
        return partialFormater.formatPartial(original)
    }
    
    /// Создает упорядоченный список выбранных букв для поиска по базе
    ///
    /// - Parameter digits: Список цифр
    /// - Returns: Списоц букв
    fileprivate func selectedLetters(from digits: [Int]) -> [String] {
        return digits
            .map({ buttonsToLetterMap[$0] })
            .compactMap({ $0 })
    }
}

/// TableView Data Source
extension KeyboardViewModel {

	func numberOfItems() -> Int {
		return dbController?.sections?[0].numberOfObjects ?? 0
	}

	func item(at index:Int) -> KeyboardContacCellItem {
		let indexPath = IndexPath(row: index, section: 0)
		let object = self.dbController?.object(at: indexPath) as! Contacts
		let cellItem = generateCellItem(contact: object)
		return cellItem
	}

	func generateCellItem(contact:Contacts) -> KeyboardContacCellItem {
		let first = contact.firstName ?? ""
		let last = contact.lastName ?? ""
		let fullTitle = first + " " + last
		var details:[KeyboardContacCellItem.StringItems] = []

		if let phones = contact.phones as? Set<Phones> {
			let pattern = self.regularExpresionString(for: originalInput)
//			let r = NSRegularExpression(pattern: regexp, options: .caseInsensitive)
			for phone in phones {
				let phoneNumber = phone.digits ?? ""
				let range = (phoneNumber as NSString).range(of: pattern,
				                                            options: .regularExpression,
				                                            range: NSMakeRange(0, phoneNumber.count),
				                                            locale: nil)
				// выводим только 1 номер
				if range.location != NSNotFound {
					let detailItem = KeyboardContacCellItem.StringItems(title: phoneNumber, hightlitedRange: range)
					details.append(detailItem)
					break
				}
			}
		}

		if details.count == 0 && contact.phones != nil && contact.phones!.count > 0 {
			let phone = contact.phones?.anyObject() as? Phones
			let phoneNumber = phone?.digits ?? ""
			let detailItem = KeyboardContacCellItem.StringItems(title: phoneNumber, hightlitedRange: nil)
			details.append(detailItem)
		}

		let pattern = self.wordsRegularExpresionString(for: selectedLetters)
		let range = (fullTitle as NSString).range(of: pattern,
		                                          options: [.regularExpression, .caseInsensitive],
		                                          range: NSMakeRange(0, fullTitle.count),
		                                          locale: nil)
		let resultRange:NSRange? = (range.location != NSNotFound) ? range : nil
		let titleItem = KeyboardContacCellItem.StringItems(title: fullTitle, hightlitedRange: resultRange)
		let cellItem = KeyboardContacCellItem(contact: contact, title: titleItem, detail: details)
		return cellItem
	}
}

extension String {

	func nsRange(from range: Range<String.Index>) -> NSRange {
		guard let from = range.lowerBound.samePosition(in: utf16),
              let to = range.upperBound.samePosition(in: utf16) else {
            return NSRange()
        }
		return NSRange(location: utf16.distance(from: utf16.startIndex, to: from),
		               length: utf16.distance(from: from, to: to))
	}

}

extension KeyboardViewModel: ContactConverterDelegate {
	func contactConverterDidCompleteWithContact(converter: ContactControllerDelegateConverter, contact: CNContact?) {

		guard let contact = contact else {
			return
		}
		switch converter.type {
		case .creation:
			ContactsManager.instance.addNewContactToDB(contact)
            Metrica.instance.send(.commonContactIsCreated)
		case .editing:
			ContactsManager.instance.updateContactInDB(contact)
		}
	}
}
