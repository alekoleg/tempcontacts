//
//  KeyboardViewController.swift
//  TemporaryContacts
//
//  Created by Konshin on 01.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import PhoneNumberKit
import ReverseExtension

/// Экран набора с клавиш
class KeyboardViewController: UIViewController {

	fileprivate let viewModel:KeyboardViewModel
	@IBOutlet var clearButton:UIButton!
	@IBOutlet var phoneLabel:UILabel!
	@IBOutlet var removeLastButton:UIButton!
	@IBOutlet var tableView:UITableView!
    @IBOutlet private var emblemLabel: UILabel!

	init(viewModel:KeyboardViewModel) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
		initTabBar()
		viewModel.delegate = self

	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override var canBecomeFirstResponder: Bool {
		return true
	}
    
    override func viewDidLoad() {
        super.viewDidLoad()
		self.updateClearButtonVisability()
		tableView.re.dataSource = self
		tableView.re.delegate = self
		tableView.tableFooterView = UIView()
		tableView.register(KeyboardContacCell.self, forCellReuseIdentifier: "KeyboardContacCell")
        
        setupEmplemLabel()
    }

	override var preferredStatusBarStyle: UIStatusBarStyle {
		get {
			return .lightContent
		}
	}

	// MARK: - Actions -
    
    private func setupEmplemLabel() {
        guard let semibold = UIFont(name: "MyriadPro-Semibold", size: 19),
            let light = UIFont(name: "MyriadPro-Light", size: 19) else {
                fatalError("Не найдены шрифты")
        }
        let textColor = UIColor.white
        let attTitle = NSMutableAttributedString()
        attTitle.append(
            NSAttributedString(
                string: "Temp",
                attributes: [
                    NSAttributedString.Key.font: semibold,
                    NSAttributedString.Key.foregroundColor: textColor
                ]
            )
        )
        attTitle.append(
            NSAttributedString(
                string: "Phones",
                attributes: [
                    NSAttributedString.Key.font: light,
                    NSAttributedString.Key.foregroundColor: textColor
                ]
            )
        )
        emblemLabel.attributedText = attTitle
    }

	@IBAction func clearInput(sender:Any) {
		viewModel.clearInput()
	}

	@IBAction func removeLastInput() {
		viewModel.deleteLastCharacter()
	}

	@IBAction func numberInputed(sender:UIButton) {
		self.viewModel.didTaped(button: sender)
	}

	@IBAction func plusInputer(tap:UILongPressGestureRecognizer) {
		switch tap.state {
		case .ended:
			self.viewModel.insetPlus()
		default:
			break
		}
	}

	@IBAction func call() {
		self.viewModel.call()
	}

	@IBAction func sendMessage() {
		self.viewModel.sendSMS()
	}

	@IBAction func addNewContact() {
		self.viewModel.addNewContact()
	}

	@IBAction func showPasteMenu() {
		if (UIPasteboard.general.string != nil) {
			self.becomeFirstResponder()
			let item = UIMenuItem(title: "paste".localized,
			                      action: #selector(KeyboardViewController.pastePhone))

			let controller = UIMenuController.shared
			controller.setTargetRect(phoneLabel.frame, in: phoneLabel.superview!)
			controller.arrowDirection = .down
			controller.menuItems = [item]
			controller.setMenuVisible(true, animated: true)
		}
	}

	@objc private func pastePhone() {
		if let string = UIPasteboard.general.string {
			self.viewModel.pasteString(str: string)
		}
	}

    //MARK: - tabBar

    private func initTabBar() {
        tabBarItem.title = "keys.title".localized
        tabBarItem.image = #imageLiteral(resourceName: "tab_bar_keys")
    }

	fileprivate func updateClearButtonVisability() {
		self.clearButton.alpha = (self.phoneLabel.text ?? "").count > 0 ? 1.0 : 0.0
		self.removeLastButton.alpha = self.clearButton.alpha
	}
}

extension KeyboardViewController: KeyboardViewModelDelegate {

	func vmUpdatePhone(phone: String) {
		self.phoneLabel.text = phone
		updateClearButtonVisability()
	}

	func vmReloadData() {
		self.tableView.reloadData()
	}

	func vmShowAlertActions(actions: [UIAlertAction]) {
		let vc = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		for action in actions {
			vc.addAction(action)
		}
		vc.addAction(UIAlertAction(title: "Cancel".localized,
		                           style: .default, 
		                           handler: nil))
		self.present(vc, animated: true, completion: nil)
	}
}

extension KeyboardViewController: UITableViewDelegate, UITableViewDataSource {

	// MARK: - Table view data source
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.numberOfItems()
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
	}

	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let object = self.viewModel.item(at: indexPath.row)

		let cell = tableView.dequeueReusableCell(withIdentifier: "KeyboardContacCell", for: indexPath) as! KeyboardContacCell

		cell.configure(item: object)

		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		self.viewModel.didSelectCell(at:indexPath.row)
	}
}

