//
//  KeyboardRouter.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 12/04/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import Contacts
import ContactsUI
import MessageUI

class KeyboardRouter: Router {
    
    typealias RootController = UINavigationController
    
    /// Зависимости
    let dependencies: DependenciesStorage

    fileprivate let navigationController = UINavigationController()
    
    private lazy var communicationHelper: CommunicationHelper = CommunicationHelper(presenter: self)
    
    private var contactCreateConverter: ContactControllerDelegateConverter?
    private var contactInfoConverter: ContactControllerDelegateConverter?

	init(dependencies: DependenciesStorage) {
        self.dependencies = dependencies
        
        navigationController.isNavigationBarHidden = true
        
		let vm = KeyboardViewModel(router: self, communicationHelper: communicationHelper)
		let keyboardVC = KeyboardViewController(viewModel: vm)
		navigationController.viewControllers = [keyboardVC]
	}
    
    var rootController: UINavigationController {
        return navigationController
    }
    
    // MARK: - routing

	func showContactCreator(delegate: ContactConverterDelegate?) {
		let converter = ContactControllerDelegateConverter(type: .creation)
		converter.converterDelegate = delegate
		let vc = CNContactViewController(forNewContact: nil)
		vc.view.tintColor = ColorHelper.AppTintColor()
		vc.delegate = converter

		let nc = UINavigationController(rootViewController: vc)
		configureContactsNC(nc)
		navigationController.present(nc, animated: true, completion: nil)

		contactCreateConverter = converter
	}

	// MARK: - private

	private func configureContactsNC(_ nc: UINavigationController) {
		nc.view.backgroundColor = ColorHelper.AppTintColor()
		nc.navigationBar.isTranslucent = false
	}
}

extension KeyboardRouter: ControllerPresenter {
    func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?) {
        navigationController.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}
