//
//  TextHelper.swift
//  TemporaryContacts
//
//  Created by User on 20/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit
import SwiftIconFont

class TextHelper{
    public static func getTextWithAwesomeIcon(text: String, iconName: String, size: Int ,color: UIColor) -> NSMutableAttributedString{
        
        let result = NSMutableAttributedString(string: text)
        result.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: CGFloat(size)), range: NSRange(location: 0, length: result.length))
        
        if let iconCode = String.fontAwesome5Icon(iconName){
            let icon = NSMutableAttributedString(
                string: iconCode,
                attributes: [NSAttributedString.Key.font:
                    UIFont.icon(from: .fontAwesome5, ofSize: CGFloat(size))
                ]
            )
            result.append(NSAttributedString(string: "  "))
            result.append(icon)
        }
        else{
            print("иконка не найдена (\(iconName))")
        }
        
        result.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSRange(location: 0, length: result.length))
        return result
    }
    
    public static func getAwesomeIcon(iconName: String, size: Int ,color: UIColor) -> NSMutableAttributedString{
        let result = NSMutableAttributedString(string: "")
        result.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: CGFloat(size)), range: NSRange(location: 0, length: result.length))
        
        if let iconCode = String.fontAwesome5Icon(iconName){
            let icon = NSMutableAttributedString(
                string: iconCode,
                attributes: [NSAttributedString.Key.font:
                    UIFont.icon(from: .fontAwesome5, ofSize: CGFloat(size))
                ]
            )
            result.append(icon)
        }
        else{
            print("иконка не найдена (\(iconName))")
        }
        
        result.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSRange(location: 0, length: result.length))
        return result
    }
    
    public static func randomHash(length: Int = 16) -> String {
        let base = "abcdefghijklmnopqrstuvwxyz0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        
        return randomString
    }
}
