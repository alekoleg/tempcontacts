//
//  DataSelectorController.swift
//  TemporaryContacts
//
//  Created by Konshin on 20.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

private let reuseIdentifier = #file

class DataSelectorController: UITableViewController {
    enum CompleteDismissAction {
        case none
        case dismiss
        case pop
    }
    
    fileprivate let items: [DataSelectorItem]

    // MARK: - settings
    
    var completeDismissAction: CompleteDismissAction = .dismiss
    
    var rowHeight: CGFloat {
        get { return tableView.rowHeight }
        set { tableView.rowHeight = newValue }
    }
    
    var font: UIFont = UIFont.systemFont(ofSize: 16)
    
    var textColor: UIColor = ColorHelper.AppDarkTextColor()
    
    // MARK: - lifeCycle
    
    init(items: [DataSelectorItem]) {
        self.items = items
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
    }
    
    // MARK: - getters
    
    var contentHeight: CGFloat {
        return CGFloat(items.count) * rowHeight
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        let item = items[indexPath.row]
        cell.imageView?.image = item.image
        cell.textLabel?.text = item.text
        cell.textLabel?.textColor = textColor
        cell.textLabel?.font = font
        if let label = cell.textLabel {
            label.subviews.forEach { $0.removeFromSuperview() }
            if item.showBadge {
                let imageView = UIImageView(image: #imageLiteral(resourceName: "contacts_context_menu_notifications_badge"))
                label.addSubview(imageView)
                imageView.snp.remakeConstraints { maker in
                    maker.top.equalToSuperview().offset(4)
                    maker.right.equalToSuperview()
                }
            }
        }

        return cell
    }

    // MARK: - tableview delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        switch completeDismissAction {
        case .dismiss:
            dismiss(animated: true, completion: nil)
        case .pop:
            navigationController?.popViewController(animated: true)
        default:
            break
        }
        item.action()
    }
}
