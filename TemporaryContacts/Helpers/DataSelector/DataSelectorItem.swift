//
//  DataSelectorItem.swift
//  TemporaryContacts
//
//  Created by Konshin on 20.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

struct DataSelectorItem {
    typealias Action = () -> Void
    
    let image: UIImage?
    let text: String?
    let action: Action
    let showBadge: Bool
    
    init(image: UIImage?, text: String?, action: @escaping Action, showBadge: Bool = false) {
        self.image = image
        self.text = text
        self.action = action
        self.showBadge = showBadge
    }
}
