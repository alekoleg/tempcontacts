//
//  UITextElements+Localization.swift
//  TemporaryContacts
//
//  Created by Aleksey on 20.08.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

extension UILabel {

    /// Ключ для локализованной строки
    @IBInspectable var localizableTextKey: String? {
        get {
            return text
        }
        set {
            text = newValue?.localized
            setNeedsDisplay()
        }
    }
}

extension UIButton {
    
    /// Ключ для локализованной строки
    @IBInspectable var localizableTitleKey: String? {
        get {
            return title(for: .normal)
        }
        set {
            setTitle(newValue?.localized, for: .normal)
            setNeedsDisplay()
        }
    }
}

extension UITextField {
    
    /// Ключ для локализованной строки
    @IBInspectable var localizableTextKey: String? {
        get {
            return text
        }
        set {
            text = newValue?.localized
            setNeedsDisplay()
        }
    }
    
    /// Ключ для локализованного плейсхолдера
    @IBInspectable var localizablePlaceholderKey: String? {
        get {
            return placeholder
        }
        set {
            placeholder = newValue?.localized
            setNeedsDisplay()
        }
    }
}
