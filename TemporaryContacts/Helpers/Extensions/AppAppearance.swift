//
//  UIAppearance+Extensions.swift
//  TemporaryContacts
//
//  Created by Konshin on 07.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

class AppAppearance {
    
    /// Задает аппиренс приложения
    static func setup() {
        UITabBarItem.appearance().setTitleTextAttributes(
            [
                NSAttributedString.Key.foregroundColor : UIColor.white,
                NSAttributedString.Key.font: UIFont.appFont(size: 11, typeface: .medium)
            ],
            for: .normal
        )
        
        UISearchBar.appearance().barTintColor = ColorHelper.AppTintColor()
        UISearchBar.appearance().tintColor = ColorHelper.AppTintColor()
        UISearchBar.appearance().searchBarStyle = .minimal
        UITextField.appearance().keyboardAppearance = .dark
        UIButton.appearance(whenContainedInInstancesOf: [UITextField.self])
            .setBackgroundImage(#imageLiteral(resourceName: "removeSearchButton").withRenderingMode(.alwaysOriginal), for: .normal)
        let appearance = CustomSearchBar.appearance()
        appearance.tintColor = ColorHelper.AppTintColor()
        appearance.cancelButtonTitle = "Cancel".localized
        appearance.placeholder = "Search".localized
        
        setupNavigationBar()
    }
    
    /// Задает аппиренс приложения для UINavigationBar
    static func setupNavigationBar() {
        let appearance = UINavigationBar.appearance()
        appearance.barTintColor = ColorHelper.AppTintColor()
        appearance.tintColor = UIColor.white
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        appearance.isTranslucent = false
        appearance.setBackgroundImage(UIImage(), for: .default)
        appearance.shadowImage = UIImage()
    }
}
