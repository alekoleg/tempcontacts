//
//  String+LineHeight.swift
//  TemporaryContacts
//
//  Created by Aleksey on 20.08.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

extension String {
    
    /// Аттрибученная строка с заданной высотой линии
    ///
    /// - Parameter lineHeight: Высота линии
    func attributedString(lineHeight: CGFloat,
                          font: UIFont = UIFont.appFont(size: 17),
                          textAlignment: NSTextAlignment = .left) -> NSAttributedString {
        return NSAttributedString(
            string: self,
            attributes: [
                NSAttributedString.Key.paragraphStyle: NSParagraphStyle.defaultWith(lineHeight: lineHeight,
                                                                            textAlignment: textAlignment),
                NSAttributedString.Key.font: font,
            ]
        )
    }
}

extension NSParagraphStyle {
    
    static func defaultWith(lineHeight: CGFloat? = nil, textAlignment: NSTextAlignment? = nil) -> NSParagraphStyle {
        guard let style = NSMutableParagraphStyle.default.mutableCopy() as? NSMutableParagraphStyle else {
            return NSParagraphStyle.default
        }

        if let height = lineHeight {
            style.minimumLineHeight = height
            style.maximumLineHeight = height
        }
        if let alignment = textAlignment {
            style.alignment = alignment
        }
        
        return style
    }
}
