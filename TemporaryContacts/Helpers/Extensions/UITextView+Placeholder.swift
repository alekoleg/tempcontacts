//
//  UITextView+Placeholder.swift
//  applicant
//
//  Created by Алексей Коньшин on 25.05.17.
//  Copyright © 2017 SuperJob. All rights reserved.
//

import UIKit
import RxSwift

private var placeholderLabelAssociatedKey: UInt8 = 0
private var disposeBagAssociatedKey: UInt8 = 0

// MARK: - Расширение, позволяющее отображать плейсхолдер на UITextView
extension UITextView {

    /// Объект сепоратора
    private var sjDisposeBag: DisposeBag {
        if let label = objc_getAssociatedObject(self, &disposeBagAssociatedKey) as? DisposeBag {
            return label
        } else {
            let dispose = DisposeBag()
            objc_setAssociatedObject(self, &disposeBagAssociatedKey, dispose, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            return dispose
        }
    }

    /// Объект сепоратора
    private var sjPlaceholderLabel: UILabel {
        if let label = objc_getAssociatedObject(self, &placeholderLabelAssociatedKey) as? UILabel {
            return label
        } else {
            let label = sj_createPlaceholderLabel()
            sj_createObservation(placeholderLabel: label)
            objc_setAssociatedObject(self, &placeholderLabelAssociatedKey, label, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            return label
        }
    }

    private func sj_createPlaceholderLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = font
        label.textColor = UIColor.black.withAlphaComponent(0.37)

        addSubview(label)
        sj_remakeConstraints(placeholderLabel: label, insets: textContainerInset)

        return label
    }

    private func sj_createObservation(placeholderLabel: UILabel) {
        let dispose = sjDisposeBag

        rx.text
            .map { $0?.isEmpty == false }
            .bind(to: placeholderLabel.rx.isHidden)
            .addDisposableTo(dispose)

        rx.observe(UIEdgeInsets.self, "textContainerInset")
            .subscribe(onNext: { [weak self] insets in
                guard let insets = insets, let sSelf = self else {
                    return
                }

                sSelf.sj_remakeConstraints(placeholderLabel: placeholderLabel, insets: insets)
            })
            .addDisposableTo(dispose)
    }

    private func sj_remakeConstraints(placeholderLabel: UILabel, insets: UIEdgeInsets) {
        placeholderLabel.snp.remakeConstraints { maker in
            var insets = textContainerInset
            // Добавляем магическое 6, т.к. UITextView имеет свои отступы для текста, помимо textContainerInset
            insets.left += 6
            insets.right += 6
            maker.left.top.equalToSuperview().inset(insets)
            maker.width.equalToSuperview().offset(-insets.left-insets.right)
            maker.height.lessThanOrEqualToSuperview().offset(-insets.top-insets.bottom)
        }
    }

    /// Плейсхолдер
    var placeholder: String? {
        get { return sjPlaceholderLabel.text }
        set { sjPlaceholderLabel.text = newValue }
    }
}
