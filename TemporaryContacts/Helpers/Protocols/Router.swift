//
//  Router.swift
//  TemporaryContacts
//
//  Created by Konshin on 12.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol Router {
    
    associatedtype RootController: UIViewController
    /// Основной контроллер роутера, отобразив который, начинается работа роутера
    var rootController: RootController { get }
    
    var dependencies: DependenciesStorage { get }
}
