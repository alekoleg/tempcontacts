//
//  ControllerProtocol.swift
//  TemporaryContacts
//
//  Created by Konshin on 23.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol ControllerProtocol {
    /// Возвращает ссылка на контроллер для манипуляций с пушами, навигацией через роутер и др
    func asViewController() -> UIViewController
}


extension UIViewController: ControllerProtocol {
    func asViewController() -> UIViewController {
        return self
    }
}
