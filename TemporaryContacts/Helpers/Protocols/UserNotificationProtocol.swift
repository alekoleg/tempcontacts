//
//  UserNotificationProtocol.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

protocol UserNotificationProtocol: class {
    /// Отображает алерт
    func showAlert(title: String?, message: String?, actions: [UIAlertAction])
    
    /// Отображает экран благодарности
    func showThanksScreen()
}

extension UserNotificationProtocol where Self: UIViewController {
    
    func showAlert(title: String?, message: String?, actions: [UIAlertAction]) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        for action in actions {
            alertController.addAction(action)
        }
        alertController.view.tintColor = ColorHelper.AppDarkTextColor()
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showThanksScreen() {
        let vc = ThanksViewController(closeAction: nil)
        if let navigation = navigationController {
            navigation.pushViewController(vc, animated: true)
        } else {
            present(vc, animated: true, completion: nil)
        }
    }
}
