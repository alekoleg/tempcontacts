//
//  CommunicationHelper.swift
//  TemporaryContacts
//
//  Created by Konshin on 03.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import MessageUI

private struct Constants {
    static let appStoreID = "1190275198"
}

protocol ControllerPresenter: class {
    func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?)
}

final class CommunicationHelper: NSObject {
    enum ErrorType: Error {
        case contactDetailsIsEmpty
        case cannotOpenURL
        case mailServiceNotAvailable
    }
    
    enum PhoneAction {
        case call
        case message
        case faceTime
    }
    /// Контроллер для отображения алертов
    fileprivate weak var presenter: ControllerPresenter?
    
    init(presenter: ControllerPresenter?) {
        self.presenter = presenter
        super.init()
    }
    
    // MARK: - phone actions
    
    /// Отображает выбиратор телефона и выполняет действие
    ///
    /// - Parameters:
    ///   - phones: список телефонов для выбора
    ///   - action: действие
    /// - Throws: могут быть ошибки
    func selectPhone(from phones:[Phones], andMakeAction action: PhoneAction) throws {
        let phoneNumbers = phones
            .compactMap({ $0.digits })
            .map({ self.normalizePhone($0) })
        
        func doAction(withNumber: String) throws {
            switch action {
            case .call:
                try call(withNumber)
            case .faceTime:
                try callFaceTime(withNumber)
            case .message:
                sendMessage(withNumber)
            }
        }
        
        switch phoneNumbers.count {
        case 0:
            throw ErrorType.contactDetailsIsEmpty
        case 1:
            try doAction(withNumber: phoneNumbers[0])
        default:
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            for phone in phoneNumbers {
                let action = UIAlertAction(title: "\(phone)", style: .default) { (action) in
                    try? doAction(withNumber: phone)
                }
                alertController.addAction(action)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel) { _ in }
            
            alertController.addAction(cancelAction)
            alertController.view.tintColor = ColorHelper.AppDarkTextColor()
            presenter?.present(alertController, animated: true, completion: nil)
        }
    }
    
    /// Выполняет действие над моделью телефона
    ///
    /// - Parameters:
    ///   - withPhone: Телефон
    ///   - action: Действие
    /// - Throws: Возможны ошибки
    func doAction(withPhone: Phones, action: PhoneAction) throws {
        guard let phoneNumber = withPhone.digits else {
            throw ErrorType.contactDetailsIsEmpty
        }
        let number = normalizePhone(phoneNumber)
        switch action {
        case .call:
            try call(number)
        case .faceTime:
            try callFaceTime(number)
        case .message:
            sendMessage(number)
        }
    }
    
    /// Звонит на номер телефона
    ///
    /// - Parameter phone: Подготовленный плоский номер телефона
    func call(_ phone: String) throws {
        if let url = URL(string: "tel://\(phone)"){
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
                return
            }
        }
        
        let alert = UIAlertController(title: "Message".localized, message: "UnavailableCall".localized, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Accept".localized, style: UIAlertAction.Style.default, handler: nil))
        presenter?.present(alert, animated: true, completion: nil)
        throw ErrorType.cannotOpenURL
    }
    
    /// Нормализует номер и выполняет на него звонок
    ///
    /// - Parameter unpreparedNumber: Номер телефона
    /// - Throws: ВОзможны ошибки
    func normalizeAndCall(to number: String) throws {
        let normalized = normalizePhone(number)
        try call(normalized)
    }
    
    /// Отображает экран отправки смс
    ///
    /// - Parameter phone: Номер телефона
    func sendMessage(_ phone: String) {
        sendMessage(to: [phone])
    }
    
    /// Отображает экран отправки смс нескольким адресатам
    ///
    /// - Parameter phone: Номер телефона
    func sendMessage(to phones: [String]) {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        UIBarButtonItem.appearance().setTitleTextAttributes(nil, for: .normal)
        let messageVC = MFMessageComposeViewController()
        messageVC.body = "";
        messageVC.recipients = phones
        messageVC.messageComposeDelegate = self;
        presenter?.present(messageVC, animated: false, completion: nil)
        Metrica.instance.send(.smsSended)
        AppAppearance.setupNavigationBar()
    }
    
    /// Осуществляет звонок через фейстайм
    ///
    /// - Parameter phone: Номер телефона
    func callFaceTime(_ phone: String) throws {
        if let facetimeURL = URL(string: "facetime://\(phone)") {
            if UIApplication.shared.canOpenURL(facetimeURL) {
                UIApplication.shared.openURL(facetimeURL)
                return
            }
        }
        let alert = UIAlertController(title: "Message".localized, message: "UnavailableFacetime".localized, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Accept".localized, style: UIAlertAction.Style.default, handler: nil))
        presenter?.present(alert, animated: true, completion: nil)
        throw ErrorType.cannotOpenURL
    }
    
    // MARK: - emails
    
    /// Отображает выбиратор почтового адреса из нескольких и открывает экран отправления почты
    ///
    /// - Parameter emails: Список моделей почтовых адресов
    /// - Throws: Возможны ошибки
    func selectAddressAndSendEmail(emails: [Emails]) throws {
        let emailAddresses = emails.compactMap({ $0.email })
        switch emailAddresses.count {
        case 0:
            throw ErrorType.contactDetailsIsEmpty
        case 1:
            try sendEmail(emailAddress: emailAddresses.first!)
            return
        default:
            break
        }
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for address in emailAddresses {
            let action = UIAlertAction(title: "\(address)", style: .default) { (action) in
                try? self.sendEmail(emailAddress: address)
            }
            alertController.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel) { _ in }
        alertController.addAction(cancelAction)
        alertController.view.tintColor = ColorHelper.AppDarkTextColor()
        presenter?.present(alertController, animated: true, completion: nil)
    }

    /// Открывает диалог отправки емейла
    ///
    /// - Parameters:
    ///   - emailAddress: Адрес, куда отправляем
    func sendEmail(emailAddress: String, message: String? = nil) throws {
        try sendEmail(to: [emailAddress], message: message)
    }
    
    /// Открывает диалог отправки почты нескольким адресатам
    ///
    /// - Parameter addresses: Список адресатов
    func sendEmail(to addresses: [String], message: String? = nil) throws {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            let alert = UIAlertController(title: "Message".localized, message: "UnavailableEmail".localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Accept".localized, style: UIAlertAction.Style.default, handler: nil))
            presenter?.present(alert, animated: true, completion: nil)
            throw ErrorType.mailServiceNotAvailable
        }
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setToRecipients(addresses)
        mailVC.setSubject("")
        mailVC.setMessageBody(message ?? "", isHTML: false)
        
        presenter?.present(mailVC, animated: true, completion: nil)
    }
    
    // MARK: - open web 
    
    func webURL(from string: String) -> URL? {
        if string.range(of: "http://") == nil {
            let modifiedString = "http://" + string
            return URL(string: modifiedString)
        } else {
            return URL(string: string)
        }
    }
    
    /// Открывает УРЛ в браузере
    ///
    /// - Parameter url: URL
    func goToWebURL(_ url: URL) {
        try? openURL(url: url)
    }
    
    // MARK: - other
    
    /// Нормализует формат номера для звонка
    func normalizePhone(_ phone: String) -> String{
        let digits = phone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        if digits.first == "7"{
            return "+\(digits)"
        }
        else {
            return digits
        }
    }
    
    /// Открывает URL
    ///
    /// - Parameter url: URL
    func openURL(url: URL) throws {
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        } else {
            throw ErrorType.cannotOpenURL
        }
    }
    
    /// Открывает страницу приложения в апп сторе
    ///
    /// - Parameter forReview: Если да, показывает сразу окно оставки отзыва
    func openInAppStore(forReview: Bool) {
        let appId = Constants.appStoreID
        let stringURL: String
        if forReview {
            stringURL = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=\(appId)"
        } else {
            stringURL = "itms-apps://itunes.apple.com/app/id\(appId)"
        }
        guard let url = URL(string: stringURL) else {
            return
        }
        try? openURL(url: url)
    }
}


extension CommunicationHelper: MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
        var message: String
        switch (result) {
        case .sent:
            message = "EmailSent".localized
        case .failed:
            message = "EmailFailed"
        case .cancelled:
            message = "SendingCanceled".localized
        case .saved:
            message = "EmailSaved".localized
        @unknown default:
            fatalError()
        }
        print("MFMailComposeViewController: \(message)")
    }
}


extension CommunicationHelper: MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
        
        var message: String
        switch (result) {
        case .sent:
            message = "SMSSent".localized
        case .failed:
            message = "SMSFailed".localized
        case .cancelled:
            message = "SMSCanceled".localized
        @unknown default:
            fatalError()
        }
        
        print("MFMessageComposeViewController: \(message)")
    }
}


extension UIViewController: ControllerPresenter {}

// MARK: - Лайфхак для перекрашивания навигейшен бара у MFMailComposeViewController
extension MFMailComposeViewController {
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.isTranslucent = false
        navigationBar.isOpaque = false
        navigationBar.barTintColor = ColorHelper.AppTintColor()
        navigationBar.tintColor = ColorHelper.AppGreenColor()
    }
}
