//
//  FetchedResultUpdate.swift
//  TemporaryContacts
//
//  Created by Konshin on 08.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

enum FetchedResultUpdate {
    case insert(IndexPath)
    case delete(IndexPath)
    case update(IndexPath)
    case move(from: IndexPath, to: IndexPath)
    case insertSection(Int)
    case deleteSection(Int)
}


extension UITableView {
    func applyFetchedResultsUpdate(
        _ update: FetchedResultUpdate,
        with animation: UITableView.RowAnimation = .automatic
        )
    {
        switch update {
        case .insert(let ip):
            insertRows(at: [ip], with: animation)
        case .delete(let ip):
            deleteRows(at: [ip], with: animation)
        case .move(let at, let to):
            moveRow(at: at, to: to)
        case .update(let ip):
            reloadRows(at: [ip], with: animation)
        case .insertSection(let section):
            insertSections(IndexSet(integer: section), with: animation)
        case .deleteSection(let section):
            deleteSections(IndexSet(integer: section), with: animation)
        }
    }
}

extension UICollectionView {
    func applyFetchedResultsUpdate(
        _ update: FetchedResultUpdate
        )
    {
        switch update {
        case .insert(let ip):
            insertItems(at: [ip])
        case .delete(let ip):
            deleteItems(at: [ip])
        case .move(let at, let to):
            moveItem(at: at, to: to)
        case .update(let ip):
            reloadItems(at: [ip])
        case .insertSection(let section):
            insertSections(IndexSet(integer: section))
        case .deleteSection(let section):
            deleteSections(IndexSet(integer: section))
        }
    }
}
