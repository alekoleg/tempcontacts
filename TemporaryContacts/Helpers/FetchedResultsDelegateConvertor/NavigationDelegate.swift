//
//  NavigationDelegate.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import ContactsUI

protocol NavigationDelegate: class {
    func navigationDelegateSomeControllerWantToDismiss(controller: UIViewController)
}

extension Router where Self: NavigationDelegate, RootController == UINavigationController {
    func navigationDelegateSomeControllerWantToDismiss(controller: UIViewController) {
        if let nc = controller.navigationController, nc != rootController {
            // Экран создания контроллера. Отображается модально
            nc.dismiss(animated: true, completion: nil)
        } else if !(controller is CNContactViewController) {
            rootController.popViewController(animated: true)
        }
    }
}

