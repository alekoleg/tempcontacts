//
//  CNContactControllerDelegateConvertor.swift
//  TemporaryContacts
//
//  Created by Konshin on 09.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import ContactsUI

protocol ContactConverterDelegate: class {
    func contactConverterDidCompleteWithContact(converter: ContactControllerDelegateConverter, contact: CNContact?)
}

class ContactControllerDelegateConverter: NSObject, CNContactViewControllerDelegate {
    enum `Type` {
        case creation
        case editing
    }
    
    weak var converterDelegate: ContactConverterDelegate?
    weak var navigationDelegate: NavigationDelegate?
    let type: Type
    
    init(type: Type) {
        self.type = type
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        viewController.dismiss(animated: true, completion: nil)
        converterDelegate?.contactConverterDidCompleteWithContact(
            converter: self, 
            contact: contact
        )
        navigationDelegate?.navigationDelegateSomeControllerWantToDismiss(controller: viewController)
    }

	func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool {
		return true
	}
}
