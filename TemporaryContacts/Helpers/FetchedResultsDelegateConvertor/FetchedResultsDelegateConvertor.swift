//
//  FetchedResultsDelegateConvertor.swift
//  TemporaryContacts
//
//  Created by Konshin on 08.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import CoreData

protocol FetchedResultsUpdatesConverterDelegate: class {
    func updatesConvertorHasReceivedfUpdates(_ convertor: FetchedResultsUpdatesConverter, updates: [FetchedResultUpdate])
    func updatesConvertorHasReceivedError(_ convertor: FetchedResultsUpdatesConverter, error: Error)
    func updatesConvertorSectionTitle(_ convertor: FetchedResultsUpdatesConverter, for sectionName: String) -> String?
}


extension FetchedResultsUpdatesConverterDelegate {
    func updatesConvertorSectionTitle(_ convertor: FetchedResultsUpdatesConverter, for sectionName: String) -> String? {
        return sectionName
    }
}


class FetchedResultsUpdatesConverter: NSObject, NSFetchedResultsControllerDelegate {
    enum UpdateError: Error {
        case wrongUpdateInstant
        case wrongIndexPath
    }
    
    enum UpdateState {
        case inUpdate([FetchedResultUpdate])
        case stoped
    }
    
    weak var delegate: FetchedResultsUpdatesConverterDelegate? {
        didSet {
            updateState = UpdateState.stoped
        }
    }
    
    private var updateState = UpdateState.stoped
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        switch updateState {
        case .inUpdate(_):
            delegate?.updatesConvertorHasReceivedError(self, error: UpdateError.wrongUpdateInstant)
            updateState = .stoped
        default:
            updateState = .inUpdate([])
            break
        }
    }
    
    func controller(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange anObject: Any,
        at indexPath: IndexPath?,
        for type: NSFetchedResultsChangeType,
        newIndexPath: IndexPath?)
    {
        var updates: [FetchedResultUpdate]
        switch updateState {
        case .inUpdate(let current):
            updates = current
        case .stoped:
            return
        }
        
        switch type {
        case .insert:
            guard let ip = newIndexPath else {
                delegate?.updatesConvertorHasReceivedError(self, error: UpdateError.wrongIndexPath)
                return
            }
            updates.append(FetchedResultUpdate.insert(ip))
        case .delete:
            guard let ip = indexPath else {
                delegate?.updatesConvertorHasReceivedError(self, error: UpdateError.wrongIndexPath)
                return
            }
            updates.append(FetchedResultUpdate.delete(ip))
        case .move:
            guard let ip = indexPath, let newIp = newIndexPath else {
                delegate?.updatesConvertorHasReceivedError(self, error: UpdateError.wrongIndexPath)
                return
            }
            updates.append(FetchedResultUpdate.move(from: ip, to: newIp))
        case .update:
            guard let ip = indexPath else {
                delegate?.updatesConvertorHasReceivedError(self, error: UpdateError.wrongIndexPath)
                return
            }
            updates.append(FetchedResultUpdate.update(ip))
        }
        updateState = .inUpdate(updates)
    }
    
    func controller(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange sectionInfo: NSFetchedResultsSectionInfo,
        atSectionIndex sectionIndex: Int,
        for type: NSFetchedResultsChangeType
        )
    {
        var updates: [FetchedResultUpdate]
        switch updateState {
        case .inUpdate(let current):
            updates = current
        case .stoped:
            return
        }
        
        switch type {
        case .insert:
            updates.append(FetchedResultUpdate.insertSection(sectionIndex))
        case .delete:
            updates.append(FetchedResultUpdate.deleteSection(sectionIndex))
        default:
            break
        }
        updateState = .inUpdate(updates)
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        switch updateState {
        case .inUpdate(let updates):
            updateState = .stoped
            let optimizedUpdates = optimizeUpdates(updates: updates)
            delegate?.updatesConvertorHasReceivedfUpdates(self, updates: optimizedUpdates)
        case .stoped:
            delegate?.updatesConvertorHasReceivedError(self, error: UpdateError.wrongUpdateInstant)
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
        return delegate?.updatesConvertorSectionTitle(self, for: sectionName)
    }
    
    private func optimizeUpdates(updates: [FetchedResultUpdate]) -> [FetchedResultUpdate] {
        var result = updates
        for update in updates {
            switch update {
            case .insertSection(let section):
                result = result.filter { update in
                    switch update {
                    case .insert(let ip) where ip.section == section:
                        return false
                    default:
                        return true
                    }
                }
            case .deleteSection(let section):
                result = result.filter { update in
                    switch update {
                    case .delete(let ip) where ip.section == section:
                        return false
                    default:
                        return true
                    }
                }
            default:
                break
            }
        }
        return result
    }
}
