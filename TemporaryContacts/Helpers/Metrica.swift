//
//  Metrica.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.06.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import YandexMobileMetrica
import Crashlytics

/// Энум для отправки метрик
class Metrica {
    
    static let instance = Metrica()
    
    private init() {}
    
    // MARK: - actions
    
    func initialize() {
        // Инициализируем фабрик
        if let configuration = YMMYandexMetricaConfiguration(apiKey: "729946c7-191c-45eb-85d3-3755f65664bf") {
            //  Инициализируем аналитику от Yandex (AppMetrica)
            YMMYandexMetrica.activate(with: configuration)
        }
    }
}

extension Metrica {
    
    enum Event {
        case markContatAsTemporary
        case groupIsCreated
        case groupIsDeleted
        case groupIsDisbanded
        case commonContactIsCreated
        case photoContactIsCreated
        case businessCardIsCreatedFromCamera
        case businessCardIsCreatedFromGallery
        case businessCardIsDeleted
        case callFromBusinessCardsScreen
        case emailSendedFromBusinessCardsScreen
        case browserIsOpenFromBusinessCardsScreen
        case notificationIsCreated
        case smsSended
        case contactIsAddedToFavorites
        case calledFromKeyboardScreen
        case calledFromFavoritesScreen
        case calledFromGroup
        
        fileprivate var text: String {
            switch self {
            case .markContatAsTemporary:
                return "Контакт: Помечен как Временный"
            case .groupIsCreated:
                return "Группа: Группа создана"
            case .groupIsDeleted:
                return "Группа: Группа удалена"
            case .groupIsDisbanded:
                return "Группа: Группа расформированна"
            case .commonContactIsCreated:
                return "Контакт: Создан стандартный контакт"
            case .photoContactIsCreated:
                return "Контакт: создан фото контакт"
            case .businessCardIsCreatedFromCamera:
                return "Визитки: Добавлена визитка с камеры"
            case .businessCardIsCreatedFromGallery:
                return "Визитки: Добавлена визитка с галереи"
            case .businessCardIsDeleted:
                return "Визитки: Удалена визитка"
            case .callFromBusinessCardsScreen:
                return "Визитки: Осущетвлен звонок с экрана визитки"
            case .emailSendedFromBusinessCardsScreen:
                return "Визитки: Отправлен email с экрана визитки"
            case .browserIsOpenFromBusinessCardsScreen:
                return "Визитки: Открыт браузер с экрана визитки"
            case .notificationIsCreated:
                return "Напоминания: Создано напоминание"
            case .smsSended:
                return "СМС: Отправлено СМС"
            case .contactIsAddedToFavorites:
                return "Контакт: Контакт добавлен в избранное"
            case .calledFromKeyboardScreen:
                return "Звонок: Осуществлен звонок с экрана клавиш"
            case .calledFromFavoritesScreen:
                return "Звонок: Осуществлен звонок с экрана избранного"
            case .calledFromGroup:
                return "Звонок: Осуществлен звонок с экрана группы"
            }
        }
    }
    
    func send(_ event: Event) {
        YMMYandexMetrica.reportEvent(event.text, onFailure: nil)
    }
    
    /// Отчет обошибке
    ///
    /// - Parameter error: Ошибка
    func report(_ error: Error,
                description: String?) {
        let systemVersion = UIDevice.current.systemVersion
        var userInfo = ["iOS": systemVersion]
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            userInfo["version"] = version
        }
        if let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            userInfo["build"] = buildNumber
        }
        if let description = description {
            userInfo["description"] = description
        }
        
        Crashlytics.sharedInstance()
            .recordError(error,
                         withAdditionalUserInfo: userInfo)
    }
    
}
