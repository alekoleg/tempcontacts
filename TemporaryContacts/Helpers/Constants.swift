//
//  Constants.swift
//  TemporaryContacts
//
//  Created by Konshin on 26.07.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

private func isScreenGreaterThanIphone5() -> Bool {
    let screenWidth = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
    return screenWidth > 320
}

struct AppConstants {
    static let isScreenSizeIsGreaterThanIPhone5 = isScreenGreaterThanIphone5()
}
