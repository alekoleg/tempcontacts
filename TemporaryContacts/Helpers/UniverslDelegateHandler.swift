//
//  UniverslDelegateHandler.swift
//  TemporaryContacts
//
//  Created by Konshin on 24.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation

/// Универсальный обработчик делегатов
class UniverslDelegateHandler {
    
}

extension UniverslDelegateHandler: PhotoContactCreateDelegate {
    func photoContactDidCreate(contact: Contacts, businessCards: [BusinessCard]) {
        let context = contact.managedObjectContext
        // Создаем новый системный контакт
        let newSystemContact = ContactsManager.instance.addContact(dbContact: contact)
        // Удаляем созданный объект
        context?.delete(contact)
        
        guard let systemContact = newSystemContact else {
            return
        }
        // Сохраняем системный контакт в базу
        ContactsManager.instance.addNewContactToDB(systemContact, businessCards: businessCards, context: context)
        Metrica.instance.send(.photoContactIsCreated)
    }
}
