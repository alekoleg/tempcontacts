//
//  ApplicationNavDelegate.swift
//  TemporaryContacts
//
//  Created by Konshin on 21.05.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

/// Типы ВьюКонтроллеров, которые необходимо удалять из стека навигации при переходе их них в другие экраны
private let stackHiddingClassess: [AnyClass] = [
    ContactsSelectionListController.self
]

/// Делегат для контроллеров навигации
class ApplicationNavDelegate: NSObject, UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        navigationController.viewControllers = navigationController.viewControllers
            .filter() { vc in
                let isLast = navigationController.viewControllers.last == vc
                if isLast {
                    return true
                }
                
                for type in stackHiddingClassess {
                    if Swift.type(of: vc) == type {
                        return false
                    }
                }
                return true
        }
    }
}
