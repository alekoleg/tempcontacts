//
//  UIColor+Extensions.swift
//  TemporaryContacts
//
//  Created by Konshin on 11.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit

extension UIColor{
    /// Создает экземпляр UIColor из UInt представления hex
    ///  Например, для получения белого цвета необходимо передать 0xFFFFFF
    ///
    /// - Parameters:
    ///   - hex: Хекс представление цвета
    ///   - alpha: Значение альфы
    convenience init(hex: UInt, alpha: CGFloat = 1) {
        self.init(
            red: CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(hex & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}
