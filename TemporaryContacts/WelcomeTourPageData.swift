//
//  WelcomeTourPageData.swift
//  ScienceJet
//
//  Created by Michail Ovchinnikov on 09/12/2016.
//  Copyright © 2016 ScienceJet. All rights reserved.
//

import UIKit

public struct WelcomeTourPageData {
    let image: UIImage?
    let caption: String?
    let startButtonVisible: Bool
    
    init(image imageName: String, caption: String?, startButtonVisible: Bool = false) {
        self.image = UIImage(named: imageName)
        self.caption = caption
        self.startButtonVisible = startButtonVisible
    }
}
