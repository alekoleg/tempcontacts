//
//  AppNotificationsManager.swift
//  TemporaryContacts
//
//  Created by Konshin on 30.04.17.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import UIKit
import CoreData

let appNotificationIdentifierKey = "notificationIdentifier"
/// Интервал, по достижению которого сработавшие уведомления удаляются
private let removeNotificationsInterval: TimeInterval = 2 * 24 * 60 * 60

/// Менеджер по работе с уведомлениями в приложении
class AppNotificationsManager {
    fileprivate let context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    // MARK: - getters
    
    private func request() -> NSFetchRequest<AppNotification> {
        return NSFetchRequest(entityName: AppNotification.entityName)
    }
    
    
    func appNotification(from notification: UILocalNotification) -> AppNotification? {
        guard let identifier = notification.userInfo?[appNotificationIdentifierKey] as? String,
            let url = URL(string: identifier),
            let objectID = context.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: url) else {
                return nil
        }
        
        if let object = context.object(with: objectID) as? AppNotification {
            return object
        } else {
            return nil
        }
    }
    
    // MARK: - actions
    
    /// Удаляет уведомления, у которых дата срабатывания в прошлом
    func removeOldNotifications() {
        let req = request()
        req.predicate = NSPredicate(format: "date < %@", NSDate())
        do {
            let notifications = try context.fetch(req)
            for n in notifications {
                removeNotificationModelIfNeeded(n)
            }
            CoreDataManager.instance.saveContext(context)
        } catch {
            print("Ошибка затирания старых уведомлений: \(error)")
        }
    }
    
    func startObserving() {
        setupNotifications()
    }
    
    // MARK: private
    
    private func setupNotifications() {
        NotificationCenter.default.addObserver(
            forName: Notification.Name.NSManagedObjectContextObjectsDidChange,
            object: context,
            queue: nil,
            using: handleNotification
        )
        NotificationCenter.default.addObserver(
            forName: Notification.Name.didChangeNotificationsRemoveSettings,
            object: nil,
            queue: nil,
            using: didUpdateRemoveNotificationsSettings
        )
    }
    
    private func didUpdateRemoveNotificationsSettings(_ n: Notification) {
        if Settings.instance.shouldRemoveNotificationsAfterFewDaysAfterView {
            removeOldNotifications()
        }
    }
    
    private func handleNotification(_ n: Notification) {
        removeAllNotifications()
        
        let req = request()
        do {
            let notifications = try context.fetch(req)
            for n in notifications {
                if n.isExpired {
                    removeNotificationModelIfNeeded(n)
                } else {
                    createNotification(for: n)
                }
            }
        } catch {
            print("Ошибка затирания старых уведомлений: \(error)")
        }
    }
    
    private func removeNotificationModelIfNeeded(_ model: AppNotification) {
        guard Settings.instance.shouldRemoveNotificationsAfterFewDaysAfterView, let viewDate = model.viewDate else {
            return
        }

        if viewDate.timeIntervalSinceNow <= -removeNotificationsInterval {
            model.managedObjectContext?.delete(model)
        }
    }
    
    // MARK: - create
    
    private func createNotification(for appNotification: AppNotification) {
        if appNotification.objectID.isTemporaryID {
            _ = try? context.obtainPermanentIDs(for: [appNotification])
        }
        
        let notification = UILocalNotification()
        let identifier = appNotification.localNotificationIdentifier
        let userInfo = [appNotificationIdentifierKey: identifier]
        let objectName: String
        if let group = appNotification.group {
            objectName = "Group".localized + " \(group.name ?? "")"
        } else if let contact = appNotification.contact {
            objectName = [contact.firstName, contact.lastName]
                .flatMap({ $0 })
                .joined(separator: " ")
        } else {
            return
        }
        let text: String
        if let nText = appNotification.text, !nText.isEmpty {
            text = "\(objectName) - \(nText)"
        } else {
            text = objectName
        }
        
        notification.userInfo = userInfo
        notification.alertBody = text
        notification.alertAction = "Close".localized
        notification.fireDate = appNotification.date as Date?
        notification.category = "TemporaryContactsNotification"
        notification.timeZone = TimeZone.current
        notification.soundName = "notifications.wav"
        notification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        
        let application = UIApplication.shared
        application.scheduleLocalNotification(notification)
    }
    
    // MARK: - remove
    
    private func removeAllNotifications() {
        let app:UIApplication = UIApplication.shared
        
        for n in app.scheduledLocalNotifications ?? [] {
            app.cancelLocalNotification(n)
        }
    }
    
    func removeNotification(for appNotification: AppNotification) {
        let app:UIApplication = UIApplication.shared
        let filtedredNotifications = app.scheduledLocalNotifications?.filter() { n in
            return n.userInfo?[appNotificationIdentifierKey] as? String == appNotification.localNotificationIdentifier
        } ?? []
        
        for n in filtedredNotifications {
            app.cancelLocalNotification(n)
        }
    }
}


extension AppNotification {
    fileprivate var localNotificationIdentifier: String {
        return objectID.uriRepresentation().absoluteString
    }
}
