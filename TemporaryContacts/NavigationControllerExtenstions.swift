//
//  UINavigationController.swift
//  TemporaryContacts
//
//  Created by Oleg Alekseenko on 28/02/2017.
//  Copyright © 2017 AppCraft. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {

    open override var childForStatusBarStyle: UIViewController? {
		get {
			return self.viewControllers.last
		}
	}
}
