//
//  ViewController.swift
//  TemporaryContacts
//
//  Created by User on 20/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let defaults = UserDefaults.standard
        let skipTutorial = defaults.bool(forKey: "SkipTutorial")
        if !skipTutorial {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let welcomeTourViewController = storyboard.instantiateViewController(withIdentifier: "WelcomeTourViewController")
            self.present(welcomeTourViewController, animated: false, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

