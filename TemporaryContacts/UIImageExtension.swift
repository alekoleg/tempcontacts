//
//  UIImageExtension.swift
//  TemporaryContacts
//
//  Created by User on 28/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

public extension UIImage{
    
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func correctlyOrientedImage() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x:0, y:0, width:self.size.width, height:self.size.height))
        if let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext();
            return normalizedImage
        }
        else{
            UIGraphicsEndImageContext();
            return self
        }
    }
    
    class func combineTopToBottom(imagesArray:[UIImage]) -> UIImage? {
        if imagesArray.count == 0 { return nil}
        if imagesArray.count == 1{
            return imagesArray[0]
        }
        
        if imagesArray.count == 2{
            
            let sorted = imagesArray.sorted(by: { $0.size.width < $1.size.width})
            
            let s1 = sorted[1].size
            let size0 = sorted[0].size
            let size1 = CGSize(width: s1.width * (size0.height) / s1.height, height: size0.height)
            
            let height = size0.height
            let dimensions = CGSize(width: size0.width + size1.width, height: height)
            let sizedImages: [(image: UIImage, size: CGSize)] = [(sorted[0], size0),(sorted[1], size1)]
            
            UIGraphicsBeginImageContext(dimensions)
            
            let background = UIImage(color: .clear, size: dimensions)
            background?.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: dimensions))
            
            var drawPoint = CGPoint(x: 0, y: 0)
            for i in 0...1 {
                let image = sizedImages[i]
                image.image.draw(in:CGRect(x: drawPoint.x, y: drawPoint.y, width: image.size.width, height: image.size.height))
                if i == 0{
                    drawPoint = CGPoint(x: size0.width, y: 0)
                }
            }
            
            if let context = UIGraphicsGetCurrentContext(){
                context.setLineWidth(8.0)
                context.setStrokeColor(UIColor.white.cgColor)
                context.move(to: CGPoint(x: sizedImages[0].size.width, y: 0))
                context.addLine(to: CGPoint(x: sizedImages[0].size.width, y: dimensions.height))
                context.strokePath()
            }
            
            let finalImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return finalImage
        }
        
        let sorted = imagesArray.sorted(by: { $0.size.width < $1.size.width})
      
        
        let s1 = sorted[1].size
        let s2 = sorted[2].size
        
        let size0 = sorted[0].size
        let size1 = CGSize(width: s1.width * (size0.height/2) / s1.height, height: size0.height/2)
        let size2 = CGSize(width: s2.width * (size0.height/2) / s2.height, height: size0.height/2)
        
        var width: CGFloat = CGFloat.greatestFiniteMagnitude
        for size in [size1, size2] {
            width = min(width, size.width)
        }
        let height = size0.height
        let dimensions = CGSize(width: size0.width + width, height: height)
        let sizedImages: [(image: UIImage, size: CGSize)] = [(sorted[0], size0),(sorted[1], size1),(sorted[2],size2)]
        
        UIGraphicsBeginImageContext(dimensions)
        
        let background = UIImage(color: .clear, size: dimensions)
        background?.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: dimensions))
        
        var drawPoint = CGPoint(x: 0, y: 0)
        for i in 0...2 {
            let image = sizedImages[i]
            image.image.draw(in:CGRect(x: drawPoint.x, y: drawPoint.y, width: image.size.width, height: image.size.height))
            if i == 0{
                drawPoint = CGPoint(x: size0.width, y: 0)
            }
            else{
                drawPoint = CGPoint(x: size0.width, y: size0.height/2)
            }
        }
        
        if let context = UIGraphicsGetCurrentContext(){
            context.setLineWidth(8.0)
            context.setStrokeColor(UIColor.white.cgColor)
            context.move(to: CGPoint(x: size0.width, y: 0))
            context.addLine(to: CGPoint(x: size0.width, y: size0.height))
            context.move(to: CGPoint(x: size0.width, y: size0.height/2))
            context.addLine(to: CGPoint(x: dimensions.width, y: size0.height/2))
            context.strokePath()
        }
        
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return finalImage
    }
    
    func resizeImage(newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage ?? self
    }

}
