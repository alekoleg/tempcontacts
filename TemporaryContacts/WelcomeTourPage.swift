//
//  WelcomeTourPage.swift
//  ScienceJet
//
//  Created by Michail Ovchinnikov on 09/12/2016.
//  Copyright © 2016 ScienceJet. All rights reserved.
//

import UIKit
import BWWalkthrough

protocol WelcomeTourPageDelegate: class {
    func start()
}


final class WelcomeTourPage: BWWalkthroughPageViewController {

    @IBOutlet private var startButton: UIButton!
    
    private var pageData: WelcomeTourPageData
    
    weak var delegate: WelcomeTourPageDelegate?

    // MARK: Interface builder bindings

    @IBOutlet weak var pageImageView: UIImageView!
    @IBOutlet weak var pageLabel: UILabel!

    init(pageData: WelcomeTourPageData) {
        self.pageData = pageData
        super.init(nibName: "WelcomeTourPage", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        pageImageView.image = pageData.image
        startButton.setTitle("Start".localized, for: .normal)
        startButton.isHidden = !pageData.startButtonVisible
    }

    @IBAction func start() {
        delegate?.start()
    }
}
